﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// UnityEngine.UnityAPICompatibilityVersionAttribute
struct UnityAPICompatibilityVersionAttribute_tA38D4489F5F2D7CD11B616FBF60FA18402572590;

IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;

struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct  InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.ParamArrayAttribute
struct  ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.UnityAPICompatibilityVersionAttribute
struct  UnityAPICompatibilityVersionAttribute_tA38D4489F5F2D7CD11B616FBF60FA18402572590  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.UnityAPICompatibilityVersionAttribute::_version
	String_t* ____version_0;
	// System.String[] UnityEngine.UnityAPICompatibilityVersionAttribute::_configurationAssembliesHashes
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____configurationAssembliesHashes_1;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(UnityAPICompatibilityVersionAttribute_tA38D4489F5F2D7CD11B616FBF60FA18402572590, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}

	inline static int32_t get_offset_of__configurationAssembliesHashes_1() { return static_cast<int32_t>(offsetof(UnityAPICompatibilityVersionAttribute_tA38D4489F5F2D7CD11B616FBF60FA18402572590, ____configurationAssembliesHashes_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__configurationAssembliesHashes_1() const { return ____configurationAssembliesHashes_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__configurationAssembliesHashes_1() { return &____configurationAssembliesHashes_1; }
	inline void set__configurationAssembliesHashes_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____configurationAssembliesHashes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____configurationAssembliesHashes_1), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.UnityAPICompatibilityVersionAttribute::.ctor(System.String,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAPICompatibilityVersionAttribute__ctor_mF698CACA8BFE3F6B56939F59A0D23E7980D125FD (UnityAPICompatibilityVersionAttribute_tA38D4489F5F2D7CD11B616FBF60FA18402572590 * __this, String_t* ___version0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___configurationAssembliesHashes1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
static void Facebook_Unity_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x63\x65\x62\x6F\x6F\x6B\x2E\x55\x6E\x69\x74\x79\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x63\x65\x62\x6F\x6F\x6B\x2E\x55\x6E\x69\x74\x79\x2E\x41\x6E\x64\x72\x6F\x69\x64"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x63\x65\x62\x6F\x6F\x6B\x2E\x55\x6E\x69\x74\x79\x2E\x43\x61\x6E\x76\x61\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x63\x65\x62\x6F\x6F\x6B\x2E\x55\x6E\x69\x74\x79\x2E\x47\x61\x6D\x65\x72\x6F\x6F\x6D"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x63\x65\x62\x6F\x6F\x6B\x2E\x55\x6E\x69\x74\x79\x2E\x49\x4F\x53"), NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_1 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x3A\x38\x34\x35\x36\x45\x39\x35\x33\x41\x30\x36\x30\x31\x37\x32\x39\x35\x37\x39\x46\x34\x45\x30\x39\x39\x35\x39\x36\x35\x38\x30\x46\x35\x35\x32\x38\x30\x46\x31\x44"));
		UnityAPICompatibilityVersionAttribute_tA38D4489F5F2D7CD11B616FBF60FA18402572590 * tmp = (UnityAPICompatibilityVersionAttribute_tA38D4489F5F2D7CD11B616FBF60FA18402572590 *)cache->attributes[9];
		UnityAPICompatibilityVersionAttribute__ctor_mF698CACA8BFE3F6B56939F59A0D23E7980D125FD(tmp, il2cpp_codegen_string_new_wrapper("\x32\x30\x32\x30\x2E\x33\x2E\x31\x66\x31"), _tmp_1, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[10];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CCurrentAccessTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CExpirationTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CPermissionsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CUserIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CLastRefreshU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CGraphDomainU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_CurrentAccessToken_m68F8FBAE40A2C05C8085CA3DF3B4A30C2D59BB39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_CurrentAccessToken_m63FA7CCAF4D748635020E08AB401E7350235F9B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_TokenString_m42882F2AE3BB424792602A331ED69BC42F7F9DCE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_TokenString_m8574AD2AC5B429CFD539E1E880981E7113FB47EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_ExpirationTime_m57DC46687C401A994FD08179DE31115BF421965C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_ExpirationTime_m7228C202A7E60D4C74FF7662139FF9B93561BCBF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_Permissions_m22CD07DF3993F1890B400D017D8A251152DEC3AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_Permissions_m1C6A4706BE77DC950E06EA93890F407B71850936(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_UserId_m00A570C3A2C9E244DECCFD4EDB3319AF27B7F7CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_UserId_m3C7C0021EBB9F4E6890B24EFF97D9B614042F7D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_LastRefresh_mC60959BF2FAA8D09EDDE6D41C720DA3A0E4FAC75(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_LastRefresh_m4291CCBCAD1D47C532BE824BC98DD760F1E2E8B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_GraphDomain_mD2C34BB0FE21303125493F9DB5DB8B8409606186(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_GraphDomain_m8412CF6CE3A0DC3AA532985C523047169B5E3C78(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_U3CAppIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_U3CClientTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_U3COnDLLLoadedDelegateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_get_AppId_mDC50F9EEABA4ECAE2810B8DF3CF41A82D965F995(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_set_AppId_m41A5BBE82AC85E920D1E25C8BC42CBFA8DF2980D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_get_ClientToken_m4548A64C0BD9CA828B448C84D86032BDC60C79C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_set_ClientToken_mBFB85D294FF628674A38EB8094CE9E9665835063(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_get_OnDLLLoadedDelegate_m68AFB315121C26792E9F3289F97F5BFE10BC8378(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_set_OnDLLLoadedDelegate_m40A415D10186935193B5E0EC343A4FE470CC78C6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass36_0_t3FF33A401B7322687B37ECE6DB14F2D27B784521_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_U3CInitializedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_U3CCallbackManagerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_get_Initialized_m5DC69D32E618C8B600A6FB619F1B6BEBC39C94C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_set_Initialized_mBC6032B47F78047FFA549EA661CB0B4AB53D582E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_get_CallbackManager_m3A96D4B6632C96E2AD4CE94F6681DE16E2006599(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_set_CallbackManager_m057CF6DD5BA2C75C269FB80148F40DCD76BBD967(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_U3COnInitCompleteU3Eb__35_0_mBCF8E6AFAB3EA568481D9333EAD1D5DE0E827713(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tDF33ADB9AFC20D8BE3D569C61424416DFC352C53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookGameObject_t721AB84789487E07D28110F29CD10434A99529F0_CustomAttributesCacheGenerator_U3CFacebookU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookGameObject_t721AB84789487E07D28110F29CD10434A99529F0_CustomAttributesCacheGenerator_FacebookGameObject_get_Facebook_m740762DE179046787FC0A7EA1394E6BB5D0AA509(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookGameObject_t721AB84789487E07D28110F29CD10434A99529F0_CustomAttributesCacheGenerator_FacebookGameObject_set_Facebook_m37C5BF1F3C26C5D1DD9735CC4E078ADAEFC79B1D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_U3CMethodNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_U3CCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_U3CFacebookImplU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_get_MethodName_m27A9DC445F6AE0092221D2ED51B5982F7F24804A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_set_MethodName_m6A50CCF3650E3F7ADE95BA6CF125862B7777B2B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_get_Callback_m2D5CF25678AEF53B999395CBED4BDE975E2CF0D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_set_Callback_m701826DAC6D6567A04613790EA85F695D81915FF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_set_FacebookImpl_m12FD236E646B8F0E3A9790C0DFC647D0FF7C82EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_set_Parameters_m724E5838F408F4976DB03135F7D261D22012679C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CTitleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CProductIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CDescriptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CImageURIU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CPriceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CPriceCurrencyCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_Title_m3CCEEC601D3E3FD58085D42D119E2CD72214284B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_Title_mA00BBE60194B8031E8DC9795C5AAD7C478405407(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_ProductID_m6D18E88F73E4DAC3CDE6CC39B8F1B95AA1B812DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_ProductID_m17F36B75492158330E4B18ABD6A5FED2BE4DD056(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_Description_m6203FA94ECD917A85941F5E2AEFBEB04995B101E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_Description_m52B3E12C5CFDB52A82F25733B2CF3A2B297E6AB5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_ImageURI_mFA357CFE65D3E3B575512089D2E5C96566BC981D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_ImageURI_m04B6F48A7F4ADB7287540E1256C7A8DDB504B906(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_Price_mEB2AABDE5B502465B3B2D98DEAC634264DB79860(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_Price_m6D902C4B4E14EB609882AB27184349A19EA19013(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_PriceCurrencyCode_m4C82A2B515E6F73F138952CF270FF8B34B4F1AB2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_PriceCurrencyCode_m2E26A166A9B6FE42E0CAF3A9051AFF42789412EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CIsConsumedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CDeveloperPayloadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CPaymentIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CProductIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CPurchaseTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CPurchaseTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CSignedRequestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_IsConsumed_mBDF2802687500F1A175DAD081CC0AF850CD6F56F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_DeveloperPayload_m45989DD105423409B2B38375197D17EDD9425576(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_DeveloperPayload_mA0D6444F2D600A519E3031A773F83E05EDE6385D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_PaymentID_mFA4AC208C695B7254510E9B7ECEF518D7E55F203(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_PaymentID_mF5980DC233FEFA0428D8CED4F35DDFBC46BF64B4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_ProductID_mC6DA4ED2B9A12DE3653F1F86F04AA65128DC4163(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_ProductID_mB8FADC80738017AB3FE268933F4912F7E4FBAE98(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_PurchaseTime_m0C3548BF414916488397B6AD6A5B8F24EF74DB1D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_PurchaseTime_m0FF2CEA8C911631DD04E1F6787D60005E7B178A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_PurchaseToken_mB931680FED62380B1EBFBABB2C003C26059B2374(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_PurchaseToken_m0869B9434D1A7E7D98A55863BC7F8DC824E438F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_SignedRequest_mA385E94DC9A29A558111601A4CAA9C3B34D1C1F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_SignedRequest_m743796F39F3443AC9F43B6E84DCAD00E3FE42AD0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessTokenRefreshResult_t96D3BE2DD9B98DD46AA9667EBEDDFF9D8998E3B0_CustomAttributesCacheGenerator_U3CAccessTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessTokenRefreshResult_t96D3BE2DD9B98DD46AA9667EBEDDFF9D8998E3B0_CustomAttributesCacheGenerator_AccessTokenRefreshResult_get_AccessToken_mE6DB4980B770602E587E17E4F9C8995CA5762828(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AccessTokenRefreshResult_t96D3BE2DD9B98DD46AA9667EBEDDFF9D8998E3B0_CustomAttributesCacheGenerator_AccessTokenRefreshResult_set_AccessToken_mF8AB8ACBE83AAA4A609C913E535E59DBA6ABD24E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_U3CUrlU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_U3CTargetUrlU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_U3CRefU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_U3CExtrasU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_get_Url_mBA3FBE84A4830A4B2B01E75074436694572678A5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_set_Url_mF2F7158EC461D9736B9CAE3E9998621AC7D8B4B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_get_TargetUrl_mCA56B7E750E5CBC7F2872750A6010F7246AC9739(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_set_TargetUrl_m4C005ED078EF6E4596E17F8A360FA6AF2AD58D23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_get_Ref_m30B8D8B6DEBC0742580F29FE2CD7538A39A3B58E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_set_Ref_m34E37AD58AF1660CC70C39522218B04BC791B1A1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_get_Extras_m2987F464221039109F018DF9BFD65452DFCEFEC8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_set_Extras_mA2C8B74C9A4C6247E36C2B4AEAE2E69BFA7AA76A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_U3CRequestIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_U3CToU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_AppRequestResult_get_RequestID_m0FB4B77116C02ABF8EC18E3618728BC610F079E8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_AppRequestResult_set_RequestID_mE05C9016FD9B06C30F56F814FE0152AF8B4F8607(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_AppRequestResult_get_To_m37F76214768FC29EEC788C2304073F59F784458B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_AppRequestResult_set_To_m574F932ABB3C52A0E79B9A4B6F4B288C1031A22A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CatalogResult_t0FBD9C74E10478FA59386E1AAB78EC4121653209_CustomAttributesCacheGenerator_U3CProductsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CatalogResult_t0FBD9C74E10478FA59386E1AAB78EC4121653209_CustomAttributesCacheGenerator_CatalogResult_get_Products_m7182B5FDDFB039C3A65E610E603BEB108AAD4492(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CatalogResult_t0FBD9C74E10478FA59386E1AAB78EC4121653209_CustomAttributesCacheGenerator_CatalogResult_set_Products_m56CFF78280420A4520B2B5493D472E3242FBCA29(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_U3CResultListU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_U3CTextureU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_GraphResult_set_ResultList_m895C92743657296186ADB66CEA4BBC5D487E1408(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_GraphResult_get_Texture_m52F3139402C071CB46BC9DC1590FDE6B996AC2D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_GraphResult_set_Texture_m45BF75B17E4FB6EF772BE00CEDD52CC3C45F464E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_U3CAccessTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_U3CAuthenticationTokenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_LoginResult_get_AccessToken_mAE248509070F413BD07740D74EFA1AB09C69965E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_LoginResult_set_AccessToken_m019741BF9BB4EC4104805DC408D0F0E3437B0D47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_LoginResult_get_AuthenticationToken_mD59EDC4AC3A0C458D1AC86535E07A971FE1BC02E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_LoginResult_set_AuthenticationToken_m8EE993212DBD9D77DA84F75ADC5ED1368E1660BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginStatusResult_t6878E3EFC9044FC04C7B805B01A801D31FD712E3_CustomAttributesCacheGenerator_U3CFailedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginStatusResult_t6878E3EFC9044FC04C7B805B01A801D31FD712E3_CustomAttributesCacheGenerator_LoginStatusResult_get_Failed_m7E910019B793A2AE97C09E353E07CCAFF5198F06(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoginStatusResult_t6878E3EFC9044FC04C7B805B01A801D31FD712E3_CustomAttributesCacheGenerator_LoginStatusResult_set_Failed_mAA0F6B0BA9EE37924C64E21610D3ADA9DEC93258(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PayloadResult_t41AA7D821534B3A16FD21ED7C4C6C3E2038FF088_CustomAttributesCacheGenerator_U3CPayloadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PayloadResult_t41AA7D821534B3A16FD21ED7C4C6C3E2038FF088_CustomAttributesCacheGenerator_PayloadResult_get_Payload_m45F5E62B679A74AA935056EB36FCE93D2EF679D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PayloadResult_t41AA7D821534B3A16FD21ED7C4C6C3E2038FF088_CustomAttributesCacheGenerator_PayloadResult_set_Payload_mE6BEDB93CCB0B255C47BBC38E1FF1C67BE2DD385(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PurchaseResult_t912808E8112D59AC66016DA4F0559487A4C63912_CustomAttributesCacheGenerator_U3CPurchaseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PurchaseResult_t912808E8112D59AC66016DA4F0559487A4C63912_CustomAttributesCacheGenerator_PurchaseResult_get_Purchase_m227AF577E2F5EAD8C2475FF8C157F503A69F0173(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PurchaseResult_t912808E8112D59AC66016DA4F0559487A4C63912_CustomAttributesCacheGenerator_PurchaseResult_set_Purchase_mF6DC16F48C0BAFC85181E26CB3DAA17DEB3A3EDE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PurchasesResult_tD6A45137651F07AAE3035545079866D75936554F_CustomAttributesCacheGenerator_U3CPurchasesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PurchasesResult_tD6A45137651F07AAE3035545079866D75936554F_CustomAttributesCacheGenerator_PurchasesResult_get_Purchases_m52BA69945FED109ED71BFCC966E7BE9CD533D7AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PurchasesResult_tD6A45137651F07AAE3035545079866D75936554F_CustomAttributesCacheGenerator_PurchasesResult_set_Purchases_m41CFF14B70FE8C8E855DF245D0859329660E2C66(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CErrorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CErrorDictionaryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CResultDictionaryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CRawResultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CCancelledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CCallbackIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CCanvasErrorCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_Error_mCC0909B7B88BA1EAC4175B6E13C86019F0B65D25(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_Error_mDE71B35D7C648815A395ACEACEFB2E1731692482(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_ErrorDictionary_m49CA2E69CD572D72EF9A38656ACF234C2B6BAAE9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_ErrorDictionary_m1CA8FB49C25033B3B263CA80A75ADDEB5BFF1D63(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_ResultDictionary_mD20F414B2C7D529F85356ABE2283D2341B7AB5B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_ResultDictionary_m4241527675668D7BA49F868F0F9171CA2169A776(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_RawResult_mFFF2523AD1CDC61D6A1B22F27B72A42C6309AD2D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_RawResult_m05E3C92A53BAC1479BF04DE3EF7F8E12C103A65A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_Cancelled_mDD7F37569D6DBF9857CEDF7121022A84AB04DA62(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_Cancelled_mBF2A85838F542800DE8CE18B23CD96EEE09926AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_CallbackId_mD5740195C1CB90B66BF4AD373DE6541FB342D320(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_CallbackId_m6BD9FE80163FA42DA2F480244F9FEE7963E715F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_CanvasErrorCode_mA4234ACCBEA60FB65EB4840FA986C5ACEEAE8296(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_CanvasErrorCode_m8F1FA8B0AE7ECB1350BE35CA064DABE9C168E660(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_U3CRawResultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_U3CResultDictionaryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_ResultContainer_get_RawResult_mE6B6064A98F862CF47C25572CDA1F841CA543016(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_ResultContainer_set_RawResult_mEDA77372AEB21EDAE82C78089DB9386094EF67F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_ResultContainer_get_ResultDictionary_m2D22D637E456BBFE03A180CE2F25EEF556698932(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_ResultContainer_set_ResultDictionary_m960C623B0C08F561E21E9D2AB45860FB7E841098(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShareResult_tCB2B5A8BF41384D3F7F1574B73A19861C5C4DC0F_CustomAttributesCacheGenerator_U3CPostIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShareResult_tCB2B5A8BF41384D3F7F1574B73A19861C5C4DC0F_CustomAttributesCacheGenerator_ShareResult_get_PostId_mE57DC5964B14931326498A079D9750D3A3BA24AF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShareResult_tCB2B5A8BF41384D3F7F1574B73A19861C5C4DC0F_CustomAttributesCacheGenerator_ShareResult_set_PostId_mE962B736189DBCF1D1DECCF30B661E5A7581BCF1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9__ctor_m91E8453F9A21FA4964B22F7E00DFBDBF320FE4B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_IDisposable_Dispose_mAE3163411A60AF021C851814054C5C2CED63C843(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE50330F88F71D02DB3524466A22E02125CCC3A27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m4AD7E69240BB16630A342A950E247C17CA66CA26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m9ACE793E1E1660527DB7FCF8B756ABA8CA6795B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FacebookLogger_tAB00CE7381868963272FBBF8424E15427AB5B579_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookLogger_tAB00CE7381868963272FBBF8424E15427AB5B579_CustomAttributesCacheGenerator_FacebookLogger_get_Instance_m0A479DAE88A8B5FE53DAC0FCEC98D50EA647CBCF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookLogger_tAB00CE7381868963272FBBF8424E15427AB5B579_CustomAttributesCacheGenerator_FacebookLogger_set_Instance_m828EEDA7C96A62FE2D525A006751147505C24264(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FacebookLogger_tAB00CE7381868963272FBBF8424E15427AB5B579_CustomAttributesCacheGenerator_FacebookLogger_Warn_m9A0E2D966A5C4A77930B8DFDB927F46E52D24485____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_TryGetValue_m259192CD84CF546CBFA0B336B7C65D15789ACA2F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_TotalSeconds_mA49A914FAEFFBA5834F1FB7B6EF3E0D67BAE38A5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_GetValueOrDefault_mE72B7245DA2218DEBD6E9BD06B116924354577B5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_ToCommaSeparateList_m3925082326445282086770A30C42E9CED61FE092(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_AbsoluteUrlOrEmptyString_m716B9F8FDA319A4FA6E1E5774D0F9DC5F6FC68E8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_ToJson_m0F09663A04FA1B2674B2E0D177A132C429D7171E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_AddAllKVPFrom_m360221C06D664FF3B618D681FBB2CE11396C797C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_ToStringNullOk_m3894054C9E1D3C06482160DD8AD34E18D478730B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec_t40E5AEBEB9F3E63F84D5BD3FD2C58C3F6C07552B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1__ctor_m38E0B6E65329104B15319617C622D64F4404F92C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1_System_IDisposable_Dispose_mCA96EF4F77E659BC5E04A98B17FA8F5DF9A628BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66F8B4754D0F53AFB18E324FE98F53B2B7AFB418(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1_System_Collections_IEnumerator_Reset_mCC535E07AE97CD37C62153C1E756FE5FD55FDBE8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1_System_Collections_IEnumerator_get_Current_mF83DB1DF0B887609F322379D06749009AB0BFE56(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4__ctor_mC9ECD537808627E62E02D894E4FB96F7E78545F2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4_System_IDisposable_Dispose_m80935841B7055D1AF032835FC1BF8DEF0D6867F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0C2F1C8A514CA6D3F1E580DD4521E47FF72BB25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_Reset_m31E52EDA4D8A81C47C4ECED631B7EC48384A7FB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_get_Current_m9BB9ADC38539B8FE9900FBCBA85818870B5E2428(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CodelessUIInteractEvent_t29A978FBD7F644E4AFA6F7D4400EC0119EE616C9_CustomAttributesCacheGenerator_U3CeventBindingManagerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CodelessUIInteractEvent_t29A978FBD7F644E4AFA6F7D4400EC0119EE616C9_CustomAttributesCacheGenerator_CodelessUIInteractEvent_get_eventBindingManager_m09E20CAC00FEFF3D70AA3A0982A408B0305AD9BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CodelessUIInteractEvent_t29A978FBD7F644E4AFA6F7D4400EC0119EE616C9_CustomAttributesCacheGenerator_CodelessUIInteractEvent_set_eventBindingManager_mFA2C3761A6E7FF12C7DBD697FFF200459CCDA9A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CclassNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CtextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3ChintU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CdescU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CtagU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CindexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CsectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CrowU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CmatchBitmaskU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_get_className_m54C63AD717ACDC4692ACC6DE94804F877B55DC6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_className_m3CD005890C7841B28A175D32951B2CDEB5BE3AEC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_text_mBA33C1DB8F9456C8071B2199CF5937AD68B48742(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_hint_m504833A7DC08EA59526D8E6F37A4FBA8EB23E866(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_desc_mECBECABCFA864C2C07BCB75612AF71B9DC374981(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_tag_m4712160C1725335A11F346A575FCCCEE2B095F82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_index_m1F0A3266005D13D0138D95BD9D1277D47BDB0584(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_section_m5C12C24C6D15A68DC696F89881FA07393D4DB5AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_row_m55AAA375912C9B25A334BE09A078B84F0D75F857(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_matchBitmask_mBF4D4DAD0701174DD4657A83807584EC5F16C77C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CeventNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CeventTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CappVersionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CpathTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CpathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CparametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_get_eventName_m663422CB21B0DAF91675DE2D6877078B51321884(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_set_eventName_m84494EBB6BFFF6C9DC0741E6D10C4040926B476C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_get_eventType_mBB79F9BFE1BE931019D05F072537ED5EFBF522E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_set_eventType_mE488464891341EC1396E33ECB22F7C33DB1BA9B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_get_appVersion_mBDFA265D6A3311D6D1E18CAC8CFC3130905F45B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_set_appVersion_mE6157B6AA510EB7499DE7F53C497A6CB5504CB2D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_get_path_mC979F6275E5CFA7F97AB4A63C27FDF4D6F6C89A5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_set_path_m1F283930D25BE4B316173A0595F503AC33B98C61(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBindingManager_t801F52EFD5E87E7FAF56FDF88CC5E3A961F02D7E_CustomAttributesCacheGenerator_U3CeventBindingsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBindingManager_t801F52EFD5E87E7FAF56FDF88CC5E3A961F02D7E_CustomAttributesCacheGenerator_FBSDKEventBindingManager_get_eventBindings_mD03FDCE6DFA0E75C406989DC46F5C75CB653A000(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBSDKEventBindingManager_t801F52EFD5E87E7FAF56FDF88CC5E3A961F02D7E_CustomAttributesCacheGenerator_FBSDKEventBindingManager_set_eventBindings_m27574887FFAC62D99CF0A359B6B691663C7F1249(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MediaUploadResult_t80F5A3F9E2FC6BAE530F2C0B95F8AE7619C6D224_CustomAttributesCacheGenerator_U3CMediaIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MediaUploadResult_t80F5A3F9E2FC6BAE530F2C0B95F8AE7619C6D224_CustomAttributesCacheGenerator_MediaUploadResult_get_MediaId_mB577FF8F14B267504DDE51320F9ECD51EC30CEB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MediaUploadResult_t80F5A3F9E2FC6BAE530F2C0B95F8AE7619C6D224_CustomAttributesCacheGenerator_MediaUploadResult_set_MediaId_m74AE625BED87E80F418CBB1F34CE134591121CB0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_U3CTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_U3CNonceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_AuthenticationToken_get_TokenString_m0FF339C87DB816343FB1B683AB499061E98E9749(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_AuthenticationToken_set_TokenString_mD671893C5AAAFF41B601435982FA9BC9CAD921C6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_AuthenticationToken_get_Nonce_m3D2F427FC8C38DFB78B4D8426A47068DC8B66538(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_AuthenticationToken_set_Nonce_m9010ECD323D9E5FA397A0092FC63FE3F6DDD2CDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameroomFacebook_t774F9051490CFCF5E398400C9E59BEF0AFCD65DD_CustomAttributesCacheGenerator_U3CLimitEventUsageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameroomFacebook_t774F9051490CFCF5E398400C9E59BEF0AFCD65DD_CustomAttributesCacheGenerator_GameroomFacebook_get_LimitEventUsage_m983B24C2E6A386AC28B5694C4B7B2AB54EBCC556(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameroomFacebook_t774F9051490CFCF5E398400C9E59BEF0AFCD65DD_CustomAttributesCacheGenerator_GameroomFacebook_set_LimitEventUsage_m290ADD7D2C3467390F8F05BC9EBB7E1B07C7B3F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4__ctor_mB6833EEC4A53FC762AE059C6BF00332A93A417D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4_System_IDisposable_Dispose_mE78F1C9BAA388D0BC289EEBDF468E596E2D2F82F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73EC4ACF1CB79293E941E42E754B5134478B3880(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_Reset_m3E3A3AB0086839F189D4F6BACB48B6C87E4E62B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_get_Current_m3B4DCFA7715371F1A514BCB9A973661D05E977B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_U3CLimitEventUsageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_U3CShareDialogModeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_EditorFacebook_get_LimitEventUsage_mE539027EAEE9B6EAEF31BA24D1B6A2A69D8F937F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_EditorFacebook_set_LimitEventUsage_mA0FB2C32D5CBEACBF2237C08D140FA71EE18AE98(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_EditorFacebook_set_ShareDialogMode_mFE8F73284D51D0505B5D18595F77A5FB5F7A803E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_U3CCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_U3CCallbackIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_EditorFacebookMockDialog_get_Callback_mB980E4C2DE7886FE9AE6A473BD25FB8F09EC593A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_EditorFacebookMockDialog_set_Callback_m79EDB0181B2B739751E3F32E8B39063260115BA5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_EditorFacebookMockDialog_get_CallbackID_m2F061BD27D5E62C9042B4D8FF694157E3BCC7698(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_EditorFacebookMockDialog_set_CallbackID_m56FE30D393DFCF716A4E8F8FCC649B23682DB85B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EmptyMockDialog_t4EF1A420C082896B499485BF6FFB0FD18FDE8BFB_CustomAttributesCacheGenerator_U3CEmptyDialogTitleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EmptyMockDialog_t4EF1A420C082896B499485BF6FFB0FD18FDE8BFB_CustomAttributesCacheGenerator_EmptyMockDialog_get_EmptyDialogTitle_mBED1FE8DF023CBE518BD3B3FF8175EFB8E73B917(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EmptyMockDialog_t4EF1A420C082896B499485BF6FFB0FD18FDE8BFB_CustomAttributesCacheGenerator_EmptyMockDialog_set_EmptyDialogTitle_mFC70B0CB05F99728777D5B1160C9EA23692CC7CB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t0C68F529910B6474CCDC4E7CFC56C77EE843E4C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_1_t2436132D20D586A6F1DF8223EB4853B0B02DD677_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MockShareDialog_tA5118DE775F149D51666E30BA8F75252C2D8D66E_CustomAttributesCacheGenerator_U3CSubTitleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MockShareDialog_tA5118DE775F149D51666E30BA8F75252C2D8D66E_CustomAttributesCacheGenerator_MockShareDialog_get_SubTitle_mB9F0FB095CD3B3C5933B84E627CDF5D9EF7F4CAD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MockShareDialog_tA5118DE775F149D51666E30BA8F75252C2D8D66E_CustomAttributesCacheGenerator_MockShareDialog_set_SubTitle_mB4D39F272D2A770A4D657B1B39B0FE5BA6A8431B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_U3CNumEntriesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_U3CKeysU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_U3CValuesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_get_NumEntries_mD08372040D670F3B2E8DAFB666908C69494614E2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_set_NumEntries_m964DA95E7E5391D1EAA539A665BD8CC3EFF7A187(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_get_Keys_m6791D63A565CFEB36B174D049CE45127815BD3C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_set_Keys_m8815A402AA77D607854B7056654D93CB32A085F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_get_Values_m52424E4E1B8CAF63C8DA1AA7DF11E1F2D7C522FF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_set_Values_m165D1C1EDF6B83713A73DF8BC8E52E958BDF86DA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AndroidFacebook_tFCC9786B4D427533398880125C33DB23869C8B14_CustomAttributesCacheGenerator_U3CKeyHashU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AndroidFacebook_tFCC9786B4D427533398880125C33DB23869C8B14_CustomAttributesCacheGenerator_AndroidFacebook_set_KeyHash_m15258DF6E2CE5A72887805F580CECF7CD737BA01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IAndroidWrapper_tBEEEF39498EF0F6A255870C55687140B4A57561B_CustomAttributesCacheGenerator_IAndroidWrapper_CallStatic_m34DFE8A082A6A60CE6BBA7B9277AEA51913C7D29____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void CanvasFacebook_t621FF17CB19E68AF86753CD11C295A63F115C146_CustomAttributesCacheGenerator_U3CLimitEventUsageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasFacebook_t621FF17CB19E68AF86753CD11C295A63F115C146_CustomAttributesCacheGenerator_CanvasFacebook_get_LimitEventUsage_mB265FDF5F0E5215548A9593C43ADAB62142BD53D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasFacebook_t621FF17CB19E68AF86753CD11C295A63F115C146_CustomAttributesCacheGenerator_CanvasFacebook_set_LimitEventUsage_m28EFAAF8B4917567E5824048435B3E52E469C8FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasFacebook_t621FF17CB19E68AF86753CD11C295A63F115C146_CustomAttributesCacheGenerator_CanvasFacebook_U3COnLoginCompleteU3Eb__37_0_m4884AECAD1936BCCD5FA80EE08412CD1ABD0843D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t2667CE90327C46A8122910DEA9FB55005AF36BE0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass47_0_t2617F6473CF0554C5D881E0CA0444759F9DA39E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Facebook_Unity_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Facebook_Unity_AttributeGenerators[294] = 
{
	U3CU3Ec__DisplayClass36_0_t3FF33A401B7322687B37ECE6DB14F2D27B784521_CustomAttributesCacheGenerator,
	U3CU3Ec_tDF33ADB9AFC20D8BE3D569C61424416DFC352C53_CustomAttributesCacheGenerator,
	U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator,
	U3CU3Ec_t40E5AEBEB9F3E63F84D5BD3FD2C58C3F6C07552B_CustomAttributesCacheGenerator,
	U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator,
	U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator,
	U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t0C68F529910B6474CCDC4E7CFC56C77EE843E4C4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_1_t2436132D20D586A6F1DF8223EB4853B0B02DD677_CustomAttributesCacheGenerator,
	U3CU3Ec_t2667CE90327C46A8122910DEA9FB55005AF36BE0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass47_0_t2617F6473CF0554C5D881E0CA0444759F9DA39E6_CustomAttributesCacheGenerator,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CCurrentAccessTokenU3Ek__BackingField,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CTokenStringU3Ek__BackingField,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CExpirationTimeU3Ek__BackingField,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CPermissionsU3Ek__BackingField,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CUserIdU3Ek__BackingField,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CLastRefreshU3Ek__BackingField,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_U3CGraphDomainU3Ek__BackingField,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_U3CAppIdU3Ek__BackingField,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_U3CClientTokenU3Ek__BackingField,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_U3COnDLLLoadedDelegateU3Ek__BackingField,
	FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_U3CInitializedU3Ek__BackingField,
	FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_U3CCallbackManagerU3Ek__BackingField,
	FacebookGameObject_t721AB84789487E07D28110F29CD10434A99529F0_CustomAttributesCacheGenerator_U3CFacebookU3Ek__BackingField,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_U3CMethodNameU3Ek__BackingField,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_U3CCallbackU3Ek__BackingField,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_U3CFacebookImplU3Ek__BackingField,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_U3CParametersU3Ek__BackingField,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CTitleU3Ek__BackingField,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CProductIDU3Ek__BackingField,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CDescriptionU3Ek__BackingField,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CImageURIU3Ek__BackingField,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CPriceU3Ek__BackingField,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_U3CPriceCurrencyCodeU3Ek__BackingField,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CIsConsumedU3Ek__BackingField,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CDeveloperPayloadU3Ek__BackingField,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CPaymentIDU3Ek__BackingField,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CProductIDU3Ek__BackingField,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CPurchaseTimeU3Ek__BackingField,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CPurchaseTokenU3Ek__BackingField,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_U3CSignedRequestU3Ek__BackingField,
	AccessTokenRefreshResult_t96D3BE2DD9B98DD46AA9667EBEDDFF9D8998E3B0_CustomAttributesCacheGenerator_U3CAccessTokenU3Ek__BackingField,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_U3CUrlU3Ek__BackingField,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_U3CTargetUrlU3Ek__BackingField,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_U3CRefU3Ek__BackingField,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_U3CExtrasU3Ek__BackingField,
	AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_U3CRequestIDU3Ek__BackingField,
	AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_U3CToU3Ek__BackingField,
	CatalogResult_t0FBD9C74E10478FA59386E1AAB78EC4121653209_CustomAttributesCacheGenerator_U3CProductsU3Ek__BackingField,
	GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_U3CResultListU3Ek__BackingField,
	GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_U3CTextureU3Ek__BackingField,
	LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_U3CAccessTokenU3Ek__BackingField,
	LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_U3CAuthenticationTokenU3Ek__BackingField,
	LoginStatusResult_t6878E3EFC9044FC04C7B805B01A801D31FD712E3_CustomAttributesCacheGenerator_U3CFailedU3Ek__BackingField,
	PayloadResult_t41AA7D821534B3A16FD21ED7C4C6C3E2038FF088_CustomAttributesCacheGenerator_U3CPayloadU3Ek__BackingField,
	PurchaseResult_t912808E8112D59AC66016DA4F0559487A4C63912_CustomAttributesCacheGenerator_U3CPurchaseU3Ek__BackingField,
	PurchasesResult_tD6A45137651F07AAE3035545079866D75936554F_CustomAttributesCacheGenerator_U3CPurchasesU3Ek__BackingField,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CErrorU3Ek__BackingField,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CErrorDictionaryU3Ek__BackingField,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CResultDictionaryU3Ek__BackingField,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CRawResultU3Ek__BackingField,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CCancelledU3Ek__BackingField,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CCallbackIdU3Ek__BackingField,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_U3CCanvasErrorCodeU3Ek__BackingField,
	ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_U3CRawResultU3Ek__BackingField,
	ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_U3CResultDictionaryU3Ek__BackingField,
	ShareResult_tCB2B5A8BF41384D3F7F1574B73A19861C5C4DC0F_CustomAttributesCacheGenerator_U3CPostIdU3Ek__BackingField,
	FacebookLogger_tAB00CE7381868963272FBBF8424E15427AB5B579_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	CodelessUIInteractEvent_t29A978FBD7F644E4AFA6F7D4400EC0119EE616C9_CustomAttributesCacheGenerator_U3CeventBindingManagerU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CclassNameU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CtextU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3ChintU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CdescU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CtagU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CindexU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CsectionU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CrowU3Ek__BackingField,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_U3CmatchBitmaskU3Ek__BackingField,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CeventNameU3Ek__BackingField,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CeventTypeU3Ek__BackingField,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CappVersionU3Ek__BackingField,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CpathTypeU3Ek__BackingField,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CpathU3Ek__BackingField,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_U3CparametersU3Ek__BackingField,
	FBSDKEventBindingManager_t801F52EFD5E87E7FAF56FDF88CC5E3A961F02D7E_CustomAttributesCacheGenerator_U3CeventBindingsU3Ek__BackingField,
	MediaUploadResult_t80F5A3F9E2FC6BAE530F2C0B95F8AE7619C6D224_CustomAttributesCacheGenerator_U3CMediaIdU3Ek__BackingField,
	AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_U3CTokenStringU3Ek__BackingField,
	AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_U3CNonceU3Ek__BackingField,
	GameroomFacebook_t774F9051490CFCF5E398400C9E59BEF0AFCD65DD_CustomAttributesCacheGenerator_U3CLimitEventUsageU3Ek__BackingField,
	EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_U3CLimitEventUsageU3Ek__BackingField,
	EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_U3CShareDialogModeU3Ek__BackingField,
	EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_U3CCallbackU3Ek__BackingField,
	EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_U3CCallbackIDU3Ek__BackingField,
	EmptyMockDialog_t4EF1A420C082896B499485BF6FFB0FD18FDE8BFB_CustomAttributesCacheGenerator_U3CEmptyDialogTitleU3Ek__BackingField,
	MockShareDialog_tA5118DE775F149D51666E30BA8F75252C2D8D66E_CustomAttributesCacheGenerator_U3CSubTitleU3Ek__BackingField,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_U3CNumEntriesU3Ek__BackingField,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_U3CKeysU3Ek__BackingField,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_U3CValuesU3Ek__BackingField,
	AndroidFacebook_tFCC9786B4D427533398880125C33DB23869C8B14_CustomAttributesCacheGenerator_U3CKeyHashU3Ek__BackingField,
	CanvasFacebook_t621FF17CB19E68AF86753CD11C295A63F115C146_CustomAttributesCacheGenerator_U3CLimitEventUsageU3Ek__BackingField,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_CurrentAccessToken_m68F8FBAE40A2C05C8085CA3DF3B4A30C2D59BB39,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_CurrentAccessToken_m63FA7CCAF4D748635020E08AB401E7350235F9B7,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_TokenString_m42882F2AE3BB424792602A331ED69BC42F7F9DCE,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_TokenString_m8574AD2AC5B429CFD539E1E880981E7113FB47EF,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_ExpirationTime_m57DC46687C401A994FD08179DE31115BF421965C,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_ExpirationTime_m7228C202A7E60D4C74FF7662139FF9B93561BCBF,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_Permissions_m22CD07DF3993F1890B400D017D8A251152DEC3AD,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_Permissions_m1C6A4706BE77DC950E06EA93890F407B71850936,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_UserId_m00A570C3A2C9E244DECCFD4EDB3319AF27B7F7CD,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_UserId_m3C7C0021EBB9F4E6890B24EFF97D9B614042F7D5,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_LastRefresh_mC60959BF2FAA8D09EDDE6D41C720DA3A0E4FAC75,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_LastRefresh_m4291CCBCAD1D47C532BE824BC98DD760F1E2E8B6,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_get_GraphDomain_mD2C34BB0FE21303125493F9DB5DB8B8409606186,
	AccessToken_t9E138FFF46145235AABAAB381CED7F988A4E0FED_CustomAttributesCacheGenerator_AccessToken_set_GraphDomain_m8412CF6CE3A0DC3AA532985C523047169B5E3C78,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_get_AppId_mDC50F9EEABA4ECAE2810B8DF3CF41A82D965F995,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_set_AppId_m41A5BBE82AC85E920D1E25C8BC42CBFA8DF2980D,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_get_ClientToken_m4548A64C0BD9CA828B448C84D86032BDC60C79C0,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_set_ClientToken_mBFB85D294FF628674A38EB8094CE9E9665835063,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_get_OnDLLLoadedDelegate_m68AFB315121C26792E9F3289F97F5BFE10BC8378,
	FB_tF8F52908361C17160C556029DF8478E73F98595C_CustomAttributesCacheGenerator_FB_set_OnDLLLoadedDelegate_m40A415D10186935193B5E0EC343A4FE470CC78C6,
	FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_get_Initialized_m5DC69D32E618C8B600A6FB619F1B6BEBC39C94C2,
	FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_set_Initialized_mBC6032B47F78047FFA549EA661CB0B4AB53D582E,
	FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_get_CallbackManager_m3A96D4B6632C96E2AD4CE94F6681DE16E2006599,
	FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_set_CallbackManager_m057CF6DD5BA2C75C269FB80148F40DCD76BBD967,
	FacebookBase_t0EAE26019ADBC7B2E5C53B75E82C61D4D195454B_CustomAttributesCacheGenerator_FacebookBase_U3COnInitCompleteU3Eb__35_0_mBCF8E6AFAB3EA568481D9333EAD1D5DE0E827713,
	FacebookGameObject_t721AB84789487E07D28110F29CD10434A99529F0_CustomAttributesCacheGenerator_FacebookGameObject_get_Facebook_m740762DE179046787FC0A7EA1394E6BB5D0AA509,
	FacebookGameObject_t721AB84789487E07D28110F29CD10434A99529F0_CustomAttributesCacheGenerator_FacebookGameObject_set_Facebook_m37C5BF1F3C26C5D1DD9735CC4E078ADAEFC79B1D,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_get_MethodName_m27A9DC445F6AE0092221D2ED51B5982F7F24804A,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_set_MethodName_m6A50CCF3650E3F7ADE95BA6CF125862B7777B2B9,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_get_Callback_m2D5CF25678AEF53B999395CBED4BDE975E2CF0D6,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_set_Callback_m701826DAC6D6567A04613790EA85F695D81915FF,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_set_FacebookImpl_m12FD236E646B8F0E3A9790C0DFC647D0FF7C82EE,
	MethodCall_1_tE1B9C798712B4DC47E4FC273B79F5A696D5AAE7C_CustomAttributesCacheGenerator_MethodCall_1_set_Parameters_m724E5838F408F4976DB03135F7D261D22012679C,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_Title_m3CCEEC601D3E3FD58085D42D119E2CD72214284B,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_Title_mA00BBE60194B8031E8DC9795C5AAD7C478405407,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_ProductID_m6D18E88F73E4DAC3CDE6CC39B8F1B95AA1B812DB,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_ProductID_m17F36B75492158330E4B18ABD6A5FED2BE4DD056,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_Description_m6203FA94ECD917A85941F5E2AEFBEB04995B101E,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_Description_m52B3E12C5CFDB52A82F25733B2CF3A2B297E6AB5,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_ImageURI_mFA357CFE65D3E3B575512089D2E5C96566BC981D,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_ImageURI_m04B6F48A7F4ADB7287540E1256C7A8DDB504B906,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_Price_mEB2AABDE5B502465B3B2D98DEAC634264DB79860,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_Price_m6D902C4B4E14EB609882AB27184349A19EA19013,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_get_PriceCurrencyCode_m4C82A2B515E6F73F138952CF270FF8B34B4F1AB2,
	Product_tCE1C1FBA0D3D0AA2355AAB9A29595D2CD1CC2CD1_CustomAttributesCacheGenerator_Product_set_PriceCurrencyCode_m2E26A166A9B6FE42E0CAF3A9051AFF42789412EF,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_IsConsumed_mBDF2802687500F1A175DAD081CC0AF850CD6F56F,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_DeveloperPayload_m45989DD105423409B2B38375197D17EDD9425576,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_DeveloperPayload_mA0D6444F2D600A519E3031A773F83E05EDE6385D,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_PaymentID_mFA4AC208C695B7254510E9B7ECEF518D7E55F203,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_PaymentID_mF5980DC233FEFA0428D8CED4F35DDFBC46BF64B4,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_ProductID_mC6DA4ED2B9A12DE3653F1F86F04AA65128DC4163,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_ProductID_mB8FADC80738017AB3FE268933F4912F7E4FBAE98,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_PurchaseTime_m0C3548BF414916488397B6AD6A5B8F24EF74DB1D,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_PurchaseTime_m0FF2CEA8C911631DD04E1F6787D60005E7B178A0,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_PurchaseToken_mB931680FED62380B1EBFBABB2C003C26059B2374,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_PurchaseToken_m0869B9434D1A7E7D98A55863BC7F8DC824E438F2,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_get_SignedRequest_mA385E94DC9A29A558111601A4CAA9C3B34D1C1F9,
	Purchase_t281333590629A01B656AC5B9BC37CD6ADFC55D49_CustomAttributesCacheGenerator_Purchase_set_SignedRequest_m743796F39F3443AC9F43B6E84DCAD00E3FE42AD0,
	AccessTokenRefreshResult_t96D3BE2DD9B98DD46AA9667EBEDDFF9D8998E3B0_CustomAttributesCacheGenerator_AccessTokenRefreshResult_get_AccessToken_mE6DB4980B770602E587E17E4F9C8995CA5762828,
	AccessTokenRefreshResult_t96D3BE2DD9B98DD46AA9667EBEDDFF9D8998E3B0_CustomAttributesCacheGenerator_AccessTokenRefreshResult_set_AccessToken_mF8AB8ACBE83AAA4A609C913E535E59DBA6ABD24E,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_get_Url_mBA3FBE84A4830A4B2B01E75074436694572678A5,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_set_Url_mF2F7158EC461D9736B9CAE3E9998621AC7D8B4B7,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_get_TargetUrl_mCA56B7E750E5CBC7F2872750A6010F7246AC9739,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_set_TargetUrl_m4C005ED078EF6E4596E17F8A360FA6AF2AD58D23,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_get_Ref_m30B8D8B6DEBC0742580F29FE2CD7538A39A3B58E,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_set_Ref_m34E37AD58AF1660CC70C39522218B04BC791B1A1,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_get_Extras_m2987F464221039109F018DF9BFD65452DFCEFEC8,
	AppLinkResult_tEBB96DCD0A111C7F513395173F3ADD21A52857CD_CustomAttributesCacheGenerator_AppLinkResult_set_Extras_mA2C8B74C9A4C6247E36C2B4AEAE2E69BFA7AA76A,
	AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_AppRequestResult_get_RequestID_m0FB4B77116C02ABF8EC18E3618728BC610F079E8,
	AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_AppRequestResult_set_RequestID_mE05C9016FD9B06C30F56F814FE0152AF8B4F8607,
	AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_AppRequestResult_get_To_m37F76214768FC29EEC788C2304073F59F784458B,
	AppRequestResult_t2039A33C4C80FF50A1A3EAF011905EBA555F2958_CustomAttributesCacheGenerator_AppRequestResult_set_To_m574F932ABB3C52A0E79B9A4B6F4B288C1031A22A,
	CatalogResult_t0FBD9C74E10478FA59386E1AAB78EC4121653209_CustomAttributesCacheGenerator_CatalogResult_get_Products_m7182B5FDDFB039C3A65E610E603BEB108AAD4492,
	CatalogResult_t0FBD9C74E10478FA59386E1AAB78EC4121653209_CustomAttributesCacheGenerator_CatalogResult_set_Products_m56CFF78280420A4520B2B5493D472E3242FBCA29,
	GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_GraphResult_set_ResultList_m895C92743657296186ADB66CEA4BBC5D487E1408,
	GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_GraphResult_get_Texture_m52F3139402C071CB46BC9DC1590FDE6B996AC2D9,
	GraphResult_t28A2C6FBE6E462EA9C99BFBE3C6257DCCB2624E1_CustomAttributesCacheGenerator_GraphResult_set_Texture_m45BF75B17E4FB6EF772BE00CEDD52CC3C45F464E,
	LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_LoginResult_get_AccessToken_mAE248509070F413BD07740D74EFA1AB09C69965E,
	LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_LoginResult_set_AccessToken_m019741BF9BB4EC4104805DC408D0F0E3437B0D47,
	LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_LoginResult_get_AuthenticationToken_mD59EDC4AC3A0C458D1AC86535E07A971FE1BC02E,
	LoginResult_t18593CC0EB0E1EF1FFE2B4F734143A1D2FFB8FDD_CustomAttributesCacheGenerator_LoginResult_set_AuthenticationToken_m8EE993212DBD9D77DA84F75ADC5ED1368E1660BF,
	LoginStatusResult_t6878E3EFC9044FC04C7B805B01A801D31FD712E3_CustomAttributesCacheGenerator_LoginStatusResult_get_Failed_m7E910019B793A2AE97C09E353E07CCAFF5198F06,
	LoginStatusResult_t6878E3EFC9044FC04C7B805B01A801D31FD712E3_CustomAttributesCacheGenerator_LoginStatusResult_set_Failed_mAA0F6B0BA9EE37924C64E21610D3ADA9DEC93258,
	PayloadResult_t41AA7D821534B3A16FD21ED7C4C6C3E2038FF088_CustomAttributesCacheGenerator_PayloadResult_get_Payload_m45F5E62B679A74AA935056EB36FCE93D2EF679D5,
	PayloadResult_t41AA7D821534B3A16FD21ED7C4C6C3E2038FF088_CustomAttributesCacheGenerator_PayloadResult_set_Payload_mE6BEDB93CCB0B255C47BBC38E1FF1C67BE2DD385,
	PurchaseResult_t912808E8112D59AC66016DA4F0559487A4C63912_CustomAttributesCacheGenerator_PurchaseResult_get_Purchase_m227AF577E2F5EAD8C2475FF8C157F503A69F0173,
	PurchaseResult_t912808E8112D59AC66016DA4F0559487A4C63912_CustomAttributesCacheGenerator_PurchaseResult_set_Purchase_mF6DC16F48C0BAFC85181E26CB3DAA17DEB3A3EDE,
	PurchasesResult_tD6A45137651F07AAE3035545079866D75936554F_CustomAttributesCacheGenerator_PurchasesResult_get_Purchases_m52BA69945FED109ED71BFCC966E7BE9CD533D7AA,
	PurchasesResult_tD6A45137651F07AAE3035545079866D75936554F_CustomAttributesCacheGenerator_PurchasesResult_set_Purchases_m41CFF14B70FE8C8E855DF245D0859329660E2C66,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_Error_mCC0909B7B88BA1EAC4175B6E13C86019F0B65D25,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_Error_mDE71B35D7C648815A395ACEACEFB2E1731692482,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_ErrorDictionary_m49CA2E69CD572D72EF9A38656ACF234C2B6BAAE9,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_ErrorDictionary_m1CA8FB49C25033B3B263CA80A75ADDEB5BFF1D63,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_ResultDictionary_mD20F414B2C7D529F85356ABE2283D2341B7AB5B1,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_ResultDictionary_m4241527675668D7BA49F868F0F9171CA2169A776,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_RawResult_mFFF2523AD1CDC61D6A1B22F27B72A42C6309AD2D,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_RawResult_m05E3C92A53BAC1479BF04DE3EF7F8E12C103A65A,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_Cancelled_mDD7F37569D6DBF9857CEDF7121022A84AB04DA62,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_Cancelled_mBF2A85838F542800DE8CE18B23CD96EEE09926AC,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_CallbackId_mD5740195C1CB90B66BF4AD373DE6541FB342D320,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_CallbackId_m6BD9FE80163FA42DA2F480244F9FEE7963E715F9,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_get_CanvasErrorCode_mA4234ACCBEA60FB65EB4840FA986C5ACEEAE8296,
	ResultBase_tD143E7247B874F4E3BF8FE48FEE312DC27345285_CustomAttributesCacheGenerator_ResultBase_set_CanvasErrorCode_m8F1FA8B0AE7ECB1350BE35CA064DABE9C168E660,
	ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_ResultContainer_get_RawResult_mE6B6064A98F862CF47C25572CDA1F841CA543016,
	ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_ResultContainer_set_RawResult_mEDA77372AEB21EDAE82C78089DB9386094EF67F7,
	ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_ResultContainer_get_ResultDictionary_m2D22D637E456BBFE03A180CE2F25EEF556698932,
	ResultContainer_tEF16B9BF5CB8541B11CED7921FE832BBA40B8327_CustomAttributesCacheGenerator_ResultContainer_set_ResultDictionary_m960C623B0C08F561E21E9D2AB45860FB7E841098,
	ShareResult_tCB2B5A8BF41384D3F7F1574B73A19861C5C4DC0F_CustomAttributesCacheGenerator_ShareResult_get_PostId_mE57DC5964B14931326498A079D9750D3A3BA24AF,
	ShareResult_tCB2B5A8BF41384D3F7F1574B73A19861C5C4DC0F_CustomAttributesCacheGenerator_ShareResult_set_PostId_mE962B736189DBCF1D1DECCF30B661E5A7581BCF1,
	U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9__ctor_m91E8453F9A21FA4964B22F7E00DFBDBF320FE4B3,
	U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_IDisposable_Dispose_mAE3163411A60AF021C851814054C5C2CED63C843,
	U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE50330F88F71D02DB3524466A22E02125CCC3A27,
	U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m4AD7E69240BB16630A342A950E247C17CA66CA26,
	U3CStartU3Ed__9_tE7EACBFC3BC6A2784D0E62B2505455E3D8904EF3_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m9ACE793E1E1660527DB7FCF8B756ABA8CA6795B2,
	FacebookLogger_tAB00CE7381868963272FBBF8424E15427AB5B579_CustomAttributesCacheGenerator_FacebookLogger_get_Instance_m0A479DAE88A8B5FE53DAC0FCEC98D50EA647CBCF,
	FacebookLogger_tAB00CE7381868963272FBBF8424E15427AB5B579_CustomAttributesCacheGenerator_FacebookLogger_set_Instance_m828EEDA7C96A62FE2D525A006751147505C24264,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_TryGetValue_m259192CD84CF546CBFA0B336B7C65D15789ACA2F,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_TotalSeconds_mA49A914FAEFFBA5834F1FB7B6EF3E0D67BAE38A5,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_GetValueOrDefault_mE72B7245DA2218DEBD6E9BD06B116924354577B5,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_ToCommaSeparateList_m3925082326445282086770A30C42E9CED61FE092,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_AbsoluteUrlOrEmptyString_m716B9F8FDA319A4FA6E1E5774D0F9DC5F6FC68E8,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_ToJson_m0F09663A04FA1B2674B2E0D177A132C429D7171E,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_AddAllKVPFrom_m360221C06D664FF3B618D681FBB2CE11396C797C,
	Utilities_t35E1C9AABF05B07A271D9B0D06042F8FEB7E146C_CustomAttributesCacheGenerator_Utilities_ToStringNullOk_m3894054C9E1D3C06482160DD8AD34E18D478730B,
	U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1__ctor_m38E0B6E65329104B15319617C622D64F4404F92C,
	U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1_System_IDisposable_Dispose_mCA96EF4F77E659BC5E04A98B17FA8F5DF9A628BC,
	U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66F8B4754D0F53AFB18E324FE98F53B2B7AFB418,
	U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1_System_Collections_IEnumerator_Reset_mCC535E07AE97CD37C62153C1E756FE5FD55FDBE8,
	U3CDelayEventU3Ed__1_t93F792287B5B6F7F0BB658B49AEA056B9396BF8E_CustomAttributesCacheGenerator_U3CDelayEventU3Ed__1_System_Collections_IEnumerator_get_Current_mF83DB1DF0B887609F322379D06749009AB0BFE56,
	U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4__ctor_mC9ECD537808627E62E02D894E4FB96F7E78545F2,
	U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4_System_IDisposable_Dispose_m80935841B7055D1AF032835FC1BF8DEF0D6867F0,
	U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0C2F1C8A514CA6D3F1E580DD4521E47FF72BB25,
	U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_Reset_m31E52EDA4D8A81C47C4ECED631B7EC48384A7FB2,
	U3CGenSnapshotU3Ed__4_t6F594827146A485EE422FE887034AE005D122A67_CustomAttributesCacheGenerator_U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_get_Current_m9BB9ADC38539B8FE9900FBCBA85818870B5E2428,
	CodelessUIInteractEvent_t29A978FBD7F644E4AFA6F7D4400EC0119EE616C9_CustomAttributesCacheGenerator_CodelessUIInteractEvent_get_eventBindingManager_m09E20CAC00FEFF3D70AA3A0982A408B0305AD9BB,
	CodelessUIInteractEvent_t29A978FBD7F644E4AFA6F7D4400EC0119EE616C9_CustomAttributesCacheGenerator_CodelessUIInteractEvent_set_eventBindingManager_mFA2C3761A6E7FF12C7DBD697FFF200459CCDA9A4,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_get_className_m54C63AD717ACDC4692ACC6DE94804F877B55DC6E,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_className_m3CD005890C7841B28A175D32951B2CDEB5BE3AEC,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_text_mBA33C1DB8F9456C8071B2199CF5937AD68B48742,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_hint_m504833A7DC08EA59526D8E6F37A4FBA8EB23E866,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_desc_mECBECABCFA864C2C07BCB75612AF71B9DC374981,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_tag_m4712160C1725335A11F346A575FCCCEE2B095F82,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_index_m1F0A3266005D13D0138D95BD9D1277D47BDB0584,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_section_m5C12C24C6D15A68DC696F89881FA07393D4DB5AD,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_row_m55AAA375912C9B25A334BE09A078B84F0D75F857,
	FBSDKCodelessPathComponent_tE5714F774CD65D7852C938741368D2DF39A39081_CustomAttributesCacheGenerator_FBSDKCodelessPathComponent_set_matchBitmask_mBF4D4DAD0701174DD4657A83807584EC5F16C77C,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_get_eventName_m663422CB21B0DAF91675DE2D6877078B51321884,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_set_eventName_m84494EBB6BFFF6C9DC0741E6D10C4040926B476C,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_get_eventType_mBB79F9BFE1BE931019D05F072537ED5EFBF522E9,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_set_eventType_mE488464891341EC1396E33ECB22F7C33DB1BA9B2,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_get_appVersion_mBDFA265D6A3311D6D1E18CAC8CFC3130905F45B8,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_set_appVersion_mE6157B6AA510EB7499DE7F53C497A6CB5504CB2D,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_get_path_mC979F6275E5CFA7F97AB4A63C27FDF4D6F6C89A5,
	FBSDKEventBinding_t1F98B5B8A5EBECB51AEFA2F8B19B3147A6D8EDA8_CustomAttributesCacheGenerator_FBSDKEventBinding_set_path_m1F283930D25BE4B316173A0595F503AC33B98C61,
	FBSDKEventBindingManager_t801F52EFD5E87E7FAF56FDF88CC5E3A961F02D7E_CustomAttributesCacheGenerator_FBSDKEventBindingManager_get_eventBindings_mD03FDCE6DFA0E75C406989DC46F5C75CB653A000,
	FBSDKEventBindingManager_t801F52EFD5E87E7FAF56FDF88CC5E3A961F02D7E_CustomAttributesCacheGenerator_FBSDKEventBindingManager_set_eventBindings_m27574887FFAC62D99CF0A359B6B691663C7F1249,
	MediaUploadResult_t80F5A3F9E2FC6BAE530F2C0B95F8AE7619C6D224_CustomAttributesCacheGenerator_MediaUploadResult_get_MediaId_mB577FF8F14B267504DDE51320F9ECD51EC30CEB6,
	MediaUploadResult_t80F5A3F9E2FC6BAE530F2C0B95F8AE7619C6D224_CustomAttributesCacheGenerator_MediaUploadResult_set_MediaId_m74AE625BED87E80F418CBB1F34CE134591121CB0,
	AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_AuthenticationToken_get_TokenString_m0FF339C87DB816343FB1B683AB499061E98E9749,
	AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_AuthenticationToken_set_TokenString_mD671893C5AAAFF41B601435982FA9BC9CAD921C6,
	AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_AuthenticationToken_get_Nonce_m3D2F427FC8C38DFB78B4D8426A47068DC8B66538,
	AuthenticationToken_t77405A31AE5D114FA45948BDC237A2FA41C9E215_CustomAttributesCacheGenerator_AuthenticationToken_set_Nonce_m9010ECD323D9E5FA397A0092FC63FE3F6DDD2CDB,
	GameroomFacebook_t774F9051490CFCF5E398400C9E59BEF0AFCD65DD_CustomAttributesCacheGenerator_GameroomFacebook_get_LimitEventUsage_m983B24C2E6A386AC28B5694C4B7B2AB54EBCC556,
	GameroomFacebook_t774F9051490CFCF5E398400C9E59BEF0AFCD65DD_CustomAttributesCacheGenerator_GameroomFacebook_set_LimitEventUsage_m290ADD7D2C3467390F8F05BC9EBB7E1B07C7B3F8,
	U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4__ctor_mB6833EEC4A53FC762AE059C6BF00332A93A417D1,
	U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4_System_IDisposable_Dispose_mE78F1C9BAA388D0BC289EEBDF468E596E2D2F82F,
	U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73EC4ACF1CB79293E941E42E754B5134478B3880,
	U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_Reset_m3E3A3AB0086839F189D4F6BACB48B6C87E4E62B2,
	U3CWaitForPipeResponseU3Ed__4_t072D5263750DDE3B47E298C18A7331928F6F7A87_CustomAttributesCacheGenerator_U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_get_Current_m3B4DCFA7715371F1A514BCB9A973661D05E977B1,
	EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_EditorFacebook_get_LimitEventUsage_mE539027EAEE9B6EAEF31BA24D1B6A2A69D8F937F,
	EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_EditorFacebook_set_LimitEventUsage_mA0FB2C32D5CBEACBF2237C08D140FA71EE18AE98,
	EditorFacebook_tE5DD746454174D7AD779765A9DCE6D702D28B141_CustomAttributesCacheGenerator_EditorFacebook_set_ShareDialogMode_mFE8F73284D51D0505B5D18595F77A5FB5F7A803E,
	EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_EditorFacebookMockDialog_get_Callback_mB980E4C2DE7886FE9AE6A473BD25FB8F09EC593A,
	EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_EditorFacebookMockDialog_set_Callback_m79EDB0181B2B739751E3F32E8B39063260115BA5,
	EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_EditorFacebookMockDialog_get_CallbackID_m2F061BD27D5E62C9042B4D8FF694157E3BCC7698,
	EditorFacebookMockDialog_tCA688193A2D14689704B9DD21CA319FD57CCDEBE_CustomAttributesCacheGenerator_EditorFacebookMockDialog_set_CallbackID_m56FE30D393DFCF716A4E8F8FCC649B23682DB85B,
	EmptyMockDialog_t4EF1A420C082896B499485BF6FFB0FD18FDE8BFB_CustomAttributesCacheGenerator_EmptyMockDialog_get_EmptyDialogTitle_mBED1FE8DF023CBE518BD3B3FF8175EFB8E73B917,
	EmptyMockDialog_t4EF1A420C082896B499485BF6FFB0FD18FDE8BFB_CustomAttributesCacheGenerator_EmptyMockDialog_set_EmptyDialogTitle_mFC70B0CB05F99728777D5B1160C9EA23692CC7CB,
	MockShareDialog_tA5118DE775F149D51666E30BA8F75252C2D8D66E_CustomAttributesCacheGenerator_MockShareDialog_get_SubTitle_mB9F0FB095CD3B3C5933B84E627CDF5D9EF7F4CAD,
	MockShareDialog_tA5118DE775F149D51666E30BA8F75252C2D8D66E_CustomAttributesCacheGenerator_MockShareDialog_set_SubTitle_mB4D39F272D2A770A4D657B1B39B0FE5BA6A8431B,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_get_NumEntries_mD08372040D670F3B2E8DAFB666908C69494614E2,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_set_NumEntries_m964DA95E7E5391D1EAA539A665BD8CC3EFF7A187,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_get_Keys_m6791D63A565CFEB36B174D049CE45127815BD3C2,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_set_Keys_m8815A402AA77D607854B7056654D93CB32A085F9,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_get_Values_m52424E4E1B8CAF63C8DA1AA7DF11E1F2D7C522FF,
	NativeDict_t4602A467E7260777C08B8BB38393EB0EF8D0E0D5_CustomAttributesCacheGenerator_NativeDict_set_Values_m165D1C1EDF6B83713A73DF8BC8E52E958BDF86DA,
	AndroidFacebook_tFCC9786B4D427533398880125C33DB23869C8B14_CustomAttributesCacheGenerator_AndroidFacebook_set_KeyHash_m15258DF6E2CE5A72887805F580CECF7CD737BA01,
	CanvasFacebook_t621FF17CB19E68AF86753CD11C295A63F115C146_CustomAttributesCacheGenerator_CanvasFacebook_get_LimitEventUsage_mB265FDF5F0E5215548A9593C43ADAB62142BD53D,
	CanvasFacebook_t621FF17CB19E68AF86753CD11C295A63F115C146_CustomAttributesCacheGenerator_CanvasFacebook_set_LimitEventUsage_m28EFAAF8B4917567E5824048435B3E52E469C8FD,
	CanvasFacebook_t621FF17CB19E68AF86753CD11C295A63F115C146_CustomAttributesCacheGenerator_CanvasFacebook_U3COnLoginCompleteU3Eb__37_0_m4884AECAD1936BCCD5FA80EE08412CD1ABD0843D,
	FacebookLogger_tAB00CE7381868963272FBBF8424E15427AB5B579_CustomAttributesCacheGenerator_FacebookLogger_Warn_m9A0E2D966A5C4A77930B8DFDB927F46E52D24485____args1,
	IAndroidWrapper_tBEEEF39498EF0F6A255870C55687140B4A57561B_CustomAttributesCacheGenerator_IAndroidWrapper_CallStatic_m34DFE8A082A6A60CE6BBA7B9277AEA51913C7D29____args1,
	Facebook_Unity_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
