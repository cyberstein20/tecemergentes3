﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Object Facebook.MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_m8A4873939A88E3C92D498920B3DA32DAB473C380 (void);
// 0x00000002 System.String Facebook.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_m7A5CC1F648D014786ECEB63ADF37FA2F87C1D1C9 (void);
// 0x00000003 System.Void Facebook.MiniJSON.Json::.cctor()
extern void Json__cctor_m86A124542557ED1615CA91D6B5F7F2A1E7676301 (void);
// 0x00000004 System.Void Facebook.MiniJSON.Json/Parser::.ctor(System.String)
extern void Parser__ctor_m3EAE969C4F18DF8C6C138B6EAE74E8847E0B0CFA (void);
// 0x00000005 System.Char Facebook.MiniJSON.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_mB8C2C7F4878CEB598583D806CE0F110317E845C8 (void);
// 0x00000006 System.Char Facebook.MiniJSON.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_m42CA3391DD63DE468017D36C83D836374DA2A11D (void);
// 0x00000007 System.String Facebook.MiniJSON.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_mB3AD0907F66FD0330F9D3340DD9FB70F580AF4E0 (void);
// 0x00000008 Facebook.MiniJSON.Json/Parser/TOKEN Facebook.MiniJSON.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_mC4877723D8402C82A77874CD29FB1A9B25677926 (void);
// 0x00000009 System.Object Facebook.MiniJSON.Json/Parser::Parse(System.String)
extern void Parser_Parse_m38B0EA54936E41916AC7A4806EC7D92429468E81 (void);
// 0x0000000A System.Void Facebook.MiniJSON.Json/Parser::Dispose()
extern void Parser_Dispose_mF9C0AC9941324651E87EB630527F3D1915C6FB83 (void);
// 0x0000000B System.Collections.Generic.Dictionary`2<System.String,System.Object> Facebook.MiniJSON.Json/Parser::ParseObject()
extern void Parser_ParseObject_m49FE9211987CD1CBD7D473A2F25E5A7E52A5177F (void);
// 0x0000000C System.Collections.Generic.List`1<System.Object> Facebook.MiniJSON.Json/Parser::ParseArray()
extern void Parser_ParseArray_m0ECC260EA4F69E154D9C4F428E0DE604D5C7F562 (void);
// 0x0000000D System.Object Facebook.MiniJSON.Json/Parser::ParseValue()
extern void Parser_ParseValue_m97042849CCE35B06AF36216F9F269EEB663CC507 (void);
// 0x0000000E System.Object Facebook.MiniJSON.Json/Parser::ParseByToken(Facebook.MiniJSON.Json/Parser/TOKEN)
extern void Parser_ParseByToken_m12B4B3A89B5DA35D60357C70FD9F1CD53B720F9F (void);
// 0x0000000F System.String Facebook.MiniJSON.Json/Parser::ParseString()
extern void Parser_ParseString_m4BA16C7B543BDB7E19825C33F5332A411D34E2B7 (void);
// 0x00000010 System.Object Facebook.MiniJSON.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_m88A7B428BA2107FA5598FAF365CF8BEEF1F5CBC7 (void);
// 0x00000011 System.Void Facebook.MiniJSON.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_m06A3D69A6B90246196E2133ABF213E3A3F2C2387 (void);
// 0x00000012 System.Void Facebook.MiniJSON.Json/Serializer::.ctor()
extern void Serializer__ctor_m4640B8CDB217BFF63F603993504CBB614490D094 (void);
// 0x00000013 System.String Facebook.MiniJSON.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m7EEFD2C8400A6682457C734866767E501A34E064 (void);
// 0x00000014 System.Void Facebook.MiniJSON.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_mB3DE0F17E69D42DE7E12C920098A03544067ADF3 (void);
// 0x00000015 System.Void Facebook.MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m8BEA0ED5BA09CF5A40CE90BEDD925CE76B24EEF5 (void);
// 0x00000016 System.Void Facebook.MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_mD5CAD8CB4055BD0433F23F0AF75A95E50DDB3B17 (void);
// 0x00000017 System.Void Facebook.MiniJSON.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mD28BF565B12B827B5A303E60C59A05A178F02976 (void);
// 0x00000018 System.Void Facebook.MiniJSON.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m6F0F9EB4C95A4F2A0F2856148CF0610B4955896C (void);
// 0x00000019 System.Void Facebook.Unity.AccessToken::.ctor(System.String,System.String,System.DateTime,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.DateTime>,System.String)
extern void AccessToken__ctor_m868A26BB81086128A8008C972BC9E9D68CDAF147 (void);
// 0x0000001A Facebook.Unity.AccessToken Facebook.Unity.AccessToken::get_CurrentAccessToken()
extern void AccessToken_get_CurrentAccessToken_m68F8FBAE40A2C05C8085CA3DF3B4A30C2D59BB39 (void);
// 0x0000001B System.Void Facebook.Unity.AccessToken::set_CurrentAccessToken(Facebook.Unity.AccessToken)
extern void AccessToken_set_CurrentAccessToken_m63FA7CCAF4D748635020E08AB401E7350235F9B7 (void);
// 0x0000001C System.String Facebook.Unity.AccessToken::get_TokenString()
extern void AccessToken_get_TokenString_m42882F2AE3BB424792602A331ED69BC42F7F9DCE (void);
// 0x0000001D System.Void Facebook.Unity.AccessToken::set_TokenString(System.String)
extern void AccessToken_set_TokenString_m8574AD2AC5B429CFD539E1E880981E7113FB47EF (void);
// 0x0000001E System.DateTime Facebook.Unity.AccessToken::get_ExpirationTime()
extern void AccessToken_get_ExpirationTime_m57DC46687C401A994FD08179DE31115BF421965C (void);
// 0x0000001F System.Void Facebook.Unity.AccessToken::set_ExpirationTime(System.DateTime)
extern void AccessToken_set_ExpirationTime_m7228C202A7E60D4C74FF7662139FF9B93561BCBF (void);
// 0x00000020 System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AccessToken::get_Permissions()
extern void AccessToken_get_Permissions_m22CD07DF3993F1890B400D017D8A251152DEC3AD (void);
// 0x00000021 System.Void Facebook.Unity.AccessToken::set_Permissions(System.Collections.Generic.IEnumerable`1<System.String>)
extern void AccessToken_set_Permissions_m1C6A4706BE77DC950E06EA93890F407B71850936 (void);
// 0x00000022 System.String Facebook.Unity.AccessToken::get_UserId()
extern void AccessToken_get_UserId_m00A570C3A2C9E244DECCFD4EDB3319AF27B7F7CD (void);
// 0x00000023 System.Void Facebook.Unity.AccessToken::set_UserId(System.String)
extern void AccessToken_set_UserId_m3C7C0021EBB9F4E6890B24EFF97D9B614042F7D5 (void);
// 0x00000024 System.Nullable`1<System.DateTime> Facebook.Unity.AccessToken::get_LastRefresh()
extern void AccessToken_get_LastRefresh_mC60959BF2FAA8D09EDDE6D41C720DA3A0E4FAC75 (void);
// 0x00000025 System.Void Facebook.Unity.AccessToken::set_LastRefresh(System.Nullable`1<System.DateTime>)
extern void AccessToken_set_LastRefresh_m4291CCBCAD1D47C532BE824BC98DD760F1E2E8B6 (void);
// 0x00000026 System.String Facebook.Unity.AccessToken::get_GraphDomain()
extern void AccessToken_get_GraphDomain_mD2C34BB0FE21303125493F9DB5DB8B8409606186 (void);
// 0x00000027 System.Void Facebook.Unity.AccessToken::set_GraphDomain(System.String)
extern void AccessToken_set_GraphDomain_m8412CF6CE3A0DC3AA532985C523047169B5E3C78 (void);
// 0x00000028 System.String Facebook.Unity.AccessToken::ToString()
extern void AccessToken_ToString_m5C8986D79621CBFF34C2452F504442F117530B14 (void);
// 0x00000029 System.String Facebook.Unity.AccessToken::ToJson()
extern void AccessToken_ToJson_m04EFC8D86152E0A4DA6FB9492049382F927643CE (void);
// 0x0000002A System.String Facebook.Unity.CallbackManager::AddFacebookDelegate(Facebook.Unity.FacebookDelegate`1<T>)
// 0x0000002B System.Void Facebook.Unity.CallbackManager::OnFacebookResponse(Facebook.Unity.IInternalResult)
extern void CallbackManager_OnFacebookResponse_mD758F6BA1AFA4B9EBFED12C56E2A88B7F7484A97 (void);
// 0x0000002C System.Void Facebook.Unity.CallbackManager::CallCallback(System.Object,Facebook.Unity.IResult)
extern void CallbackManager_CallCallback_mA8C8B2E25F093F96C5E1E0B48A72B59D32FDB013 (void);
// 0x0000002D System.Boolean Facebook.Unity.CallbackManager::TryCallCallback(System.Object,Facebook.Unity.IResult)
// 0x0000002E System.Void Facebook.Unity.CallbackManager::.ctor()
extern void CallbackManager__ctor_m763C66B56D10819458B7FAAAA56F0609835CA615 (void);
// 0x0000002F UnityEngine.GameObject Facebook.Unity.ComponentFactory::get_FacebookGameObject()
extern void ComponentFactory_get_FacebookGameObject_m5CBDF2CF11D38884E02214311AE4064A80A992CC (void);
// 0x00000030 T Facebook.Unity.ComponentFactory::GetComponent(Facebook.Unity.ComponentFactory/IfNotExist)
// 0x00000031 T Facebook.Unity.ComponentFactory::AddComponent()
// 0x00000032 System.Uri Facebook.Unity.Constants::get_GraphUrl()
extern void Constants_get_GraphUrl_mBDCB7D0383D19AD0992E4A3BC0B67CC39F51EEB4 (void);
// 0x00000033 System.String Facebook.Unity.Constants::get_GraphApiUserAgent()
extern void Constants_get_GraphApiUserAgent_m580B293788FB75973B68E7D8833D70692DF82FB3 (void);
// 0x00000034 System.Boolean Facebook.Unity.Constants::get_IsMobile()
extern void Constants_get_IsMobile_m186265C246D711A4AEBA58833C656492567572BF (void);
// 0x00000035 System.Boolean Facebook.Unity.Constants::get_IsEditor()
extern void Constants_get_IsEditor_mB569B724E025232BCB4C8994E10EB7DF4DA98111 (void);
// 0x00000036 System.Boolean Facebook.Unity.Constants::get_IsWeb()
extern void Constants_get_IsWeb_m4029C384BFAAA99F8BF8F5C669C720EC9D6E1205 (void);
// 0x00000037 System.Boolean Facebook.Unity.Constants::get_IsGameroom()
extern void Constants_get_IsGameroom_mE062F17073791A2BBD8F3E93A4DD51DBF3F3192B (void);
// 0x00000038 System.String Facebook.Unity.Constants::get_UnitySDKUserAgentSuffixLegacy()
extern void Constants_get_UnitySDKUserAgentSuffixLegacy_m4D71B001D94DBD109D9DDF4BD537D89206508D9E (void);
// 0x00000039 System.String Facebook.Unity.Constants::get_UnitySDKUserAgent()
extern void Constants_get_UnitySDKUserAgent_m00D799BB1B54747B8D89677B8F367EAD5B14ABD2 (void);
// 0x0000003A System.Boolean Facebook.Unity.Constants::get_DebugMode()
extern void Constants_get_DebugMode_m523E57985E18A348C01D589A2CD387AD945E7A99 (void);
// 0x0000003B Facebook.Unity.FacebookUnityPlatform Facebook.Unity.Constants::get_CurrentPlatform()
extern void Constants_get_CurrentPlatform_m79DC42780CD59233CFBF88661B0F3E93980CFA42 (void);
// 0x0000003C Facebook.Unity.FacebookUnityPlatform Facebook.Unity.Constants::GetCurrentPlatform()
extern void Constants_GetCurrentPlatform_mA9C24D84007393900A66F5F5A60B9F2AD4B711EA (void);
// 0x0000003D System.String Facebook.Unity.FB::get_AppId()
extern void FB_get_AppId_mDC50F9EEABA4ECAE2810B8DF3CF41A82D965F995 (void);
// 0x0000003E System.Void Facebook.Unity.FB::set_AppId(System.String)
extern void FB_set_AppId_m41A5BBE82AC85E920D1E25C8BC42CBFA8DF2980D (void);
// 0x0000003F System.String Facebook.Unity.FB::get_ClientToken()
extern void FB_get_ClientToken_m4548A64C0BD9CA828B448C84D86032BDC60C79C0 (void);
// 0x00000040 System.Void Facebook.Unity.FB::set_ClientToken(System.String)
extern void FB_set_ClientToken_mBFB85D294FF628674A38EB8094CE9E9665835063 (void);
// 0x00000041 System.String Facebook.Unity.FB::get_GraphApiVersion()
extern void FB_get_GraphApiVersion_mB9418431A4CD2C22072A25C63ED38910D5366E3E (void);
// 0x00000042 System.Void Facebook.Unity.FB::set_GraphApiVersion(System.String)
extern void FB_set_GraphApiVersion_m708B5205194016FDE8655351D923639F96689700 (void);
// 0x00000043 System.Boolean Facebook.Unity.FB::get_IsLoggedIn()
extern void FB_get_IsLoggedIn_m1C3F0E0A4D2D8715E88D703D8B14856CE38C4B32 (void);
// 0x00000044 System.Boolean Facebook.Unity.FB::get_IsInitialized()
extern void FB_get_IsInitialized_m226A5FACF50DCF9DD52D226118E653728C2B5DC7 (void);
// 0x00000045 System.Boolean Facebook.Unity.FB::get_LimitAppEventUsage()
extern void FB_get_LimitAppEventUsage_mEA61CF1E4FC84AD3E5429876F9F6449CA4079F7C (void);
// 0x00000046 System.Void Facebook.Unity.FB::set_LimitAppEventUsage(System.Boolean)
extern void FB_set_LimitAppEventUsage_m08C030622EEDB6B941BC39F5D494BD3942311579 (void);
// 0x00000047 Facebook.Unity.IFacebook Facebook.Unity.FB::get_FacebookImpl()
extern void FB_get_FacebookImpl_m9DDD32735B11AF92957E015A4FB4A70C79DA8B23 (void);
// 0x00000048 System.Void Facebook.Unity.FB::set_FacebookImpl(Facebook.Unity.IFacebook)
extern void FB_set_FacebookImpl_m9ECE851AD12B9C3B950293000AAFF57B66239E85 (void);
// 0x00000049 System.String Facebook.Unity.FB::get_FacebookDomain()
extern void FB_get_FacebookDomain_m48ED648280D35BF95A9163267EA26C9532928797 (void);
// 0x0000004A System.Void Facebook.Unity.FB::set_FacebookDomain(System.String)
extern void FB_set_FacebookDomain_m2805CB9A186C2F039ED32226079918081EBC9245 (void);
// 0x0000004B Facebook.Unity.FB/OnDLLLoaded Facebook.Unity.FB::get_OnDLLLoadedDelegate()
extern void FB_get_OnDLLLoadedDelegate_m68AFB315121C26792E9F3289F97F5BFE10BC8378 (void);
// 0x0000004C System.Void Facebook.Unity.FB::set_OnDLLLoadedDelegate(Facebook.Unity.FB/OnDLLLoaded)
extern void FB_set_OnDLLLoadedDelegate_m40A415D10186935193B5E0EC343A4FE470CC78C6 (void);
// 0x0000004D System.Void Facebook.Unity.FB::Init(Facebook.Unity.InitDelegate,Facebook.Unity.HideUnityDelegate,System.String)
extern void FB_Init_m5A02A3E5BC5D5F6A4C62B8D263A38124EBF9A098 (void);
// 0x0000004E System.Void Facebook.Unity.FB::Init(System.String,System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void FB_Init_mEE0F3EC6A526B7E25CC022E753EB4F47DD73CA79 (void);
// 0x0000004F System.Void Facebook.Unity.FB::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void FB_LogInWithPublishPermissions_mD1592B197BA920DA4F2E3349FDB6724C62844D5D (void);
// 0x00000050 System.Void Facebook.Unity.FB::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void FB_LogInWithReadPermissions_m96613A9D72165192B22C0F82BC1143EE927210C9 (void);
// 0x00000051 System.Void Facebook.Unity.FB::LogOut()
extern void FB_LogOut_m0D25B168F57A13462B6AAAC91D7356FF4D87C788 (void);
// 0x00000052 System.Void Facebook.Unity.FB::AppRequest(System.String,Facebook.Unity.OGActionType,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_mC44637F293AC033A9BFD9F3ED87C7EA00FE4F137 (void);
// 0x00000053 System.Void Facebook.Unity.FB::AppRequest(System.String,Facebook.Unity.OGActionType,System.String,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_m3FE0AA3F6490736DB6B3CA06908FB7AE01D562E4 (void);
// 0x00000054 System.Void Facebook.Unity.FB::AppRequest(System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_m43363842AC368438E1D9C8EE6F91F04B989D205C (void);
// 0x00000055 System.Void Facebook.Unity.FB::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void FB_ShareLink_m99C29A3AE0A5E08BEA9732E53EBC93928881BB81 (void);
// 0x00000056 System.Void Facebook.Unity.FB::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void FB_FeedShare_m4FC571ABEA1D35B5FCB3714DEE6902130D71B0A5 (void);
// 0x00000057 System.Void Facebook.Unity.FB::API(System.String,Facebook.Unity.HttpMethod,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void FB_API_m65BFFC9E9D451EFB81538A26FA221CCA2B6E2649 (void);
// 0x00000058 System.Void Facebook.Unity.FB::API(System.String,Facebook.Unity.HttpMethod,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>,UnityEngine.WWWForm)
extern void FB_API_m1B6845C837D836D59337ED16D723E00746AEA547 (void);
// 0x00000059 System.Void Facebook.Unity.FB::ActivateApp()
extern void FB_ActivateApp_m1ADB1549DED591571736095BE961B44CECBC01CB (void);
// 0x0000005A System.Void Facebook.Unity.FB::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void FB_GetAppLink_mA04CDA4C0EBD32B49888F2CD02DFE4118E55B183 (void);
// 0x0000005B System.Void Facebook.Unity.FB::ClearAppLink()
extern void FB_ClearAppLink_mF46B63A9C8A001A0B20400B94328A22F184A4E46 (void);
// 0x0000005C System.Void Facebook.Unity.FB::LogAppEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogAppEvent_m2D94462AAA8AEF7277BED399335572FC5BD439FA (void);
// 0x0000005D System.Void Facebook.Unity.FB::LogPurchase(System.Decimal,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogPurchase_mD3114E2A71063653FA968C36D30F5610D91B5592 (void);
// 0x0000005E System.Void Facebook.Unity.FB::LogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogPurchase_m623F36D67FC9B24FFAA8270DF86010138A37B7AD (void);
// 0x0000005F System.Void Facebook.Unity.FB::LogVersion()
extern void FB_LogVersion_m782E87C23A5C914B171A76951C2AF89BFAD625D9 (void);
// 0x00000060 System.Void Facebook.Unity.FB::.ctor()
extern void FB__ctor_m1FEE8CECF7AB4EE3E3BE7DDDA5C5BEAB73355BC6 (void);
// 0x00000061 System.Void Facebook.Unity.FB::.cctor()
extern void FB__cctor_m1A31BC451AE86EEDF2615C5631983EA5DACD834A (void);
// 0x00000062 System.Void Facebook.Unity.FB/OnDLLLoaded::.ctor(System.Object,System.IntPtr)
extern void OnDLLLoaded__ctor_m72B76C3B929B38EB8184944717FD701C15DE3B11 (void);
// 0x00000063 System.Void Facebook.Unity.FB/OnDLLLoaded::Invoke()
extern void OnDLLLoaded_Invoke_m3E5BDEB9570ED39708B6A06A680A4257963E4BCB (void);
// 0x00000064 System.IAsyncResult Facebook.Unity.FB/OnDLLLoaded::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDLLLoaded_BeginInvoke_m11340471648103A288D7FC4587363355872F5240 (void);
// 0x00000065 System.Void Facebook.Unity.FB/OnDLLLoaded::EndInvoke(System.IAsyncResult)
extern void OnDLLLoaded_EndInvoke_mAF6D67493AA53FF2BF91B900CA94E3E6539E0660 (void);
// 0x00000066 Facebook.Unity.IPayFacebook Facebook.Unity.FB/Canvas::get_FacebookPayImpl()
extern void Canvas_get_FacebookPayImpl_mBA4900DC0F60567D8A7B9914628908A4FF678F61 (void);
// 0x00000067 System.Void Facebook.Unity.FB/Canvas::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void Canvas_Pay_mC50F771C4A54D295AFD8F0FA62076E1767825841 (void);
// 0x00000068 System.Void Facebook.Unity.FB/Mobile::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void Mobile_set_ShareDialogMode_m104137D266702C132CE01F700A6A67A780060CF0 (void);
// 0x00000069 Facebook.Unity.Mobile.IMobileFacebook Facebook.Unity.FB/Mobile::get_MobileFacebookImpl()
extern void Mobile_get_MobileFacebookImpl_m5E6A8CC1A63B65EC4C2244390E29F8E331193653 (void);
// 0x0000006A System.Void Facebook.Unity.FB/Mobile::FetchDeferredAppLinkData(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void Mobile_FetchDeferredAppLinkData_mD21280CA395985D45F257025E76CB39AEDB2150F (void);
// 0x0000006B System.Void Facebook.Unity.FB/Mobile::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern void Mobile_RefreshCurrentAccessToken_m8CFC22E8C68E570666309A0C18A55C786F852D60 (void);
// 0x0000006C System.Boolean Facebook.Unity.FB/Mobile::IsImplicitPurchaseLoggingEnabled()
extern void Mobile_IsImplicitPurchaseLoggingEnabled_mA927ACFFF7E5BEF2DC35B4671E9ABB76FBF0300A (void);
// 0x0000006D Facebook.Unity.FacebookGameObject Facebook.Unity.FB/CompiledFacebookLoader::get_FBGameObject()
// 0x0000006E System.Void Facebook.Unity.FB/CompiledFacebookLoader::Start()
extern void CompiledFacebookLoader_Start_m423FD3259B28B14A2D6D212098ACE84171D3E802 (void);
// 0x0000006F System.Void Facebook.Unity.FB/CompiledFacebookLoader::.ctor()
extern void CompiledFacebookLoader__ctor_m68C8597BA48F90FB3D4AFFD66F9E8A4245D6762D (void);
// 0x00000070 System.Void Facebook.Unity.FB/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m4F699BB647DFEBEA39ECA67ECB2E4518D0AEAB85 (void);
// 0x00000071 System.Void Facebook.Unity.FB/<>c__DisplayClass36_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__0_mBC4A9768979E3164490158FCA938EF8F31F7027C (void);
// 0x00000072 System.Void Facebook.Unity.FB/<>c__DisplayClass36_0::<Init>b__1()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__1_m46947DCD1D39783A33A2F2AAE042941E508C02EA (void);
// 0x00000073 System.Void Facebook.Unity.FB/<>c__DisplayClass36_0::<Init>b__2()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__2_m7849F2FE7E358D938290AF87AD77849111DCB928 (void);
// 0x00000074 System.Void Facebook.Unity.FB/<>c__DisplayClass36_0::<Init>b__3()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__3_m98E511AB15B5BF687F83644796C871A83407425B (void);
// 0x00000075 System.Void Facebook.Unity.FB/<>c__DisplayClass36_0::<Init>b__4()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__4_m232FD2A6A1F1D12CEE743B12063B1C3C3AFEE4D6 (void);
// 0x00000076 System.Void Facebook.Unity.FBGamingServices::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void FBGamingServices_OpenFriendFinderDialog_m5DD6C0955411778CF3E0D74878C25CB2BBFAF510 (void);
// 0x00000077 System.Void Facebook.Unity.FBGamingServices::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void FBGamingServices_UploadImageToMediaLibrary_m3EE26DB2D18DFBD5849F5D70902D937A4468B0F9 (void);
// 0x00000078 System.Void Facebook.Unity.FBGamingServices::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void FBGamingServices_UploadVideoToMediaLibrary_m269C3798B983ECFA1BEC25103D31E28767D70E01 (void);
// 0x00000079 System.Void Facebook.Unity.FBGamingServices::OnIAPReady(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IIAPReadyResult>)
extern void FBGamingServices_OnIAPReady_m8813CE330E24341C9B743E4C5B1CD31A21574B50 (void);
// 0x0000007A System.Void Facebook.Unity.FBGamingServices::GetCatalog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ICatalogResult>)
extern void FBGamingServices_GetCatalog_m2B3E4C7F66F56EEE476953FBEE411FBC9E89AE92 (void);
// 0x0000007B System.Void Facebook.Unity.FBGamingServices::GetPurchases(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchasesResult>)
extern void FBGamingServices_GetPurchases_mCB9F083309D7FF911BB4CC32537FDAB1BEE63210 (void);
// 0x0000007C System.Void Facebook.Unity.FBGamingServices::Purchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchaseResult>,System.String)
extern void FBGamingServices_Purchase_mD639AE3DC7AD7111805A9FE941F75C3010151EB1 (void);
// 0x0000007D System.Void Facebook.Unity.FBGamingServices::ConsumePurchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IConsumePurchaseResult>)
extern void FBGamingServices_ConsumePurchase_m282F1B8E82F9B65C3BDDC71D9E1CA5C9054CD6DF (void);
// 0x0000007E System.Void Facebook.Unity.FBGamingServices::InitCloudGame(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInitCloudGameResult>)
extern void FBGamingServices_InitCloudGame_m7AEDF5833930F5CEE740E0A7D943D1270FB5C319 (void);
// 0x0000007F System.Void Facebook.Unity.FBGamingServices::ScheduleAppToUserNotification(System.String,System.String,System.Uri,System.Int32,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IScheduleAppToUserNotificationResult>)
extern void FBGamingServices_ScheduleAppToUserNotification_m28595A8F944CA66A0F752EF954EDAE4CDC73A021 (void);
// 0x00000080 System.Void Facebook.Unity.FBGamingServices::LoadInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
extern void FBGamingServices_LoadInterstitialAd_m640EB91774097945028192F7F9BA6AB3022B8F90 (void);
// 0x00000081 System.Void Facebook.Unity.FBGamingServices::ShowInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
extern void FBGamingServices_ShowInterstitialAd_mADFF84923F9E66DC6EDFE4C0792B39E11B424327 (void);
// 0x00000082 System.Void Facebook.Unity.FBGamingServices::LoadRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
extern void FBGamingServices_LoadRewardedVideo_m18E8D0B4A9E423B1B125DCE18CAFCD2C9B6587D7 (void);
// 0x00000083 System.Void Facebook.Unity.FBGamingServices::ShowRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
extern void FBGamingServices_ShowRewardedVideo_m5F887E64B1564CA465EEE123F62EDC8D2C5555CA (void);
// 0x00000084 System.Void Facebook.Unity.FBGamingServices::GetPayload(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayloadResult>)
extern void FBGamingServices_GetPayload_m66F3087E3831BB2C39C209FDCFDECE58AAD3619F (void);
// 0x00000085 System.Void Facebook.Unity.FBGamingServices::PostSessionScore(System.Int32,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ISessionScoreResult>)
extern void FBGamingServices_PostSessionScore_m4BDABA0390EA639A0BA07016833A95B8F89973A4 (void);
// 0x00000086 System.Void Facebook.Unity.FBGamingServices::OpenAppStore(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IOpenAppStoreResult>)
extern void FBGamingServices_OpenAppStore_mEDFD19A576679F0DBD00B3048FDD007875119DCD (void);
// 0x00000087 Facebook.Unity.Mobile.IMobileFacebook Facebook.Unity.FBGamingServices::get_MobileFacebookImpl()
extern void FBGamingServices_get_MobileFacebookImpl_m5A8D530741D37D39BA4633292FF0922BAEE482EC (void);
// 0x00000088 System.Void Facebook.Unity.FBGamingServices::.ctor()
extern void FBGamingServices__ctor_m3A77F49C599439C907F372B2A8FEFE727DD0DDFC (void);
// 0x00000089 System.Void Facebook.Unity.FacebookBase::.ctor(Facebook.Unity.CallbackManager)
extern void FacebookBase__ctor_m5F5458781CCBA9AFC2921FE6A22B33C9B0AF0E49 (void);
// 0x0000008A System.Boolean Facebook.Unity.FacebookBase::get_LimitEventUsage()
// 0x0000008B System.Void Facebook.Unity.FacebookBase::set_LimitEventUsage(System.Boolean)
// 0x0000008C System.String Facebook.Unity.FacebookBase::get_SDKName()
// 0x0000008D System.String Facebook.Unity.FacebookBase::get_SDKVersion()
// 0x0000008E System.String Facebook.Unity.FacebookBase::get_SDKUserAgent()
extern void FacebookBase_get_SDKUserAgent_mDDCD16907E906D2DFF381CBF2FB06FE6F76DDE37 (void);
// 0x0000008F System.Boolean Facebook.Unity.FacebookBase::get_LoggedIn()
extern void FacebookBase_get_LoggedIn_m33A7F0BA33E2577AB9E0E28790C9C53A9DD1C909 (void);
// 0x00000090 System.Boolean Facebook.Unity.FacebookBase::get_Initialized()
extern void FacebookBase_get_Initialized_m5DC69D32E618C8B600A6FB619F1B6BEBC39C94C2 (void);
// 0x00000091 System.Void Facebook.Unity.FacebookBase::set_Initialized(System.Boolean)
extern void FacebookBase_set_Initialized_mBC6032B47F78047FFA549EA661CB0B4AB53D582E (void);
// 0x00000092 Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::get_CallbackManager()
extern void FacebookBase_get_CallbackManager_m3A96D4B6632C96E2AD4CE94F6681DE16E2006599 (void);
// 0x00000093 System.Void Facebook.Unity.FacebookBase::set_CallbackManager(Facebook.Unity.CallbackManager)
extern void FacebookBase_set_CallbackManager_m057CF6DD5BA2C75C269FB80148F40DCD76BBD967 (void);
// 0x00000094 System.Void Facebook.Unity.FacebookBase::Init(Facebook.Unity.InitDelegate)
extern void FacebookBase_Init_m4B5F140C94F0F0FD114695EEA2D3FC4EBA09CC65 (void);
// 0x00000095 System.Void Facebook.Unity.FacebookBase::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x00000096 System.Void Facebook.Unity.FacebookBase::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x00000097 System.Void Facebook.Unity.FacebookBase::LogOut()
extern void FacebookBase_LogOut_m9F8B5EF7A5B3D8C9980D74B03FE46F0E18EE5866 (void);
// 0x00000098 System.Void Facebook.Unity.FacebookBase::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
// 0x00000099 System.Void Facebook.Unity.FacebookBase::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x0000009A System.Void Facebook.Unity.FacebookBase::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x0000009B System.Void Facebook.Unity.FacebookBase::API(System.String,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void FacebookBase_API_m732AA2837BDA43339DC49987F08F6A7880B87CD9 (void);
// 0x0000009C System.Void Facebook.Unity.FacebookBase::API(System.String,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void FacebookBase_API_m5B6F889AB5586F55164655D5007A15676A0AD449 (void);
// 0x0000009D System.Void Facebook.Unity.FacebookBase::ActivateApp(System.String)
// 0x0000009E System.Void Facebook.Unity.FacebookBase::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x0000009F System.Void Facebook.Unity.FacebookBase::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000A0 System.Void Facebook.Unity.FacebookBase::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000A1 System.Void Facebook.Unity.FacebookBase::OnInitComplete(Facebook.Unity.ResultContainer)
extern void FacebookBase_OnInitComplete_m5EB5BFE3254C6419AC1D6D2B5B7186DB3534B126 (void);
// 0x000000A2 System.Void Facebook.Unity.FacebookBase::OnLoginComplete(Facebook.Unity.ResultContainer)
// 0x000000A3 System.Void Facebook.Unity.FacebookBase::OnLogoutComplete(Facebook.Unity.ResultContainer)
extern void FacebookBase_OnLogoutComplete_m6BC78C7BEEFC2B1CD5969B0AA413E0534D81B946 (void);
// 0x000000A4 System.Void Facebook.Unity.FacebookBase::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x000000A5 System.Void Facebook.Unity.FacebookBase::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
// 0x000000A6 System.Void Facebook.Unity.FacebookBase::OnShareLinkComplete(Facebook.Unity.ResultContainer)
// 0x000000A7 System.Void Facebook.Unity.FacebookBase::ValidateAppRequestArgs(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FacebookBase_ValidateAppRequestArgs_m2731AD5CE8FE6AC86F65BA6321F934A5E3AD0327 (void);
// 0x000000A8 System.Void Facebook.Unity.FacebookBase::OnAuthResponse(Facebook.Unity.LoginResult)
extern void FacebookBase_OnAuthResponse_mD467E7FB31E8C0F26D9FC9357ADB8ED7A632E6FA (void);
// 0x000000A9 System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.FacebookBase::CopyByValue(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void FacebookBase_CopyByValue_mD91FE265F3A6AA6644EB7C55D0AC6D4D6904BD08 (void);
// 0x000000AA System.Uri Facebook.Unity.FacebookBase::GetGraphUrl(System.String)
extern void FacebookBase_GetGraphUrl_m148C245B755FD97DBA264D3BF718BFB3DBCBA1BB (void);
// 0x000000AB System.Void Facebook.Unity.FacebookBase::<OnInitComplete>b__35_0(Facebook.Unity.ILoginResult)
extern void FacebookBase_U3COnInitCompleteU3Eb__35_0_mBCF8E6AFAB3EA568481D9333EAD1D5DE0E827713 (void);
// 0x000000AC System.Void Facebook.Unity.FacebookBase/<>c::.cctor()
extern void U3CU3Ec__cctor_m3FD929F62DFC2F903AD7DEEB1729B094194E373F (void);
// 0x000000AD System.Void Facebook.Unity.FacebookBase/<>c::.ctor()
extern void U3CU3Ec__ctor_m8E17DA27E2486957C417694CF1C85C57C892AE20 (void);
// 0x000000AE System.Boolean Facebook.Unity.FacebookBase/<>c::<ValidateAppRequestArgs>b__41_0(System.String)
extern void U3CU3Ec_U3CValidateAppRequestArgsU3Eb__41_0_m358DE4B56E8939DF48480C0F177D8CB7E35FB9A9 (void);
// 0x000000AF System.Void Facebook.Unity.InitDelegate::.ctor(System.Object,System.IntPtr)
extern void InitDelegate__ctor_mB3E00CB4B2CEEC79F308B18FAB5E33E8AC545D1B (void);
// 0x000000B0 System.Void Facebook.Unity.InitDelegate::Invoke()
extern void InitDelegate_Invoke_m5E0ED52D4D85D78000E3058FE7272076F04BD437 (void);
// 0x000000B1 System.IAsyncResult Facebook.Unity.InitDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void InitDelegate_BeginInvoke_m764FAE5D802F83DB32EA08055AADA71E48076986 (void);
// 0x000000B2 System.Void Facebook.Unity.InitDelegate::EndInvoke(System.IAsyncResult)
extern void InitDelegate_EndInvoke_m6FF9B83F97E46505746CDBE3EEEB30A1D8E0892B (void);
// 0x000000B3 System.Void Facebook.Unity.FacebookDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x000000B4 System.Void Facebook.Unity.FacebookDelegate`1::Invoke(T)
// 0x000000B5 System.IAsyncResult Facebook.Unity.FacebookDelegate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000000B6 System.Void Facebook.Unity.FacebookDelegate`1::EndInvoke(System.IAsyncResult)
// 0x000000B7 System.Void Facebook.Unity.HideUnityDelegate::.ctor(System.Object,System.IntPtr)
extern void HideUnityDelegate__ctor_m184BF575CF91A178A3FB5347E8210C294930DC49 (void);
// 0x000000B8 System.Void Facebook.Unity.HideUnityDelegate::Invoke(System.Boolean)
extern void HideUnityDelegate_Invoke_mB19B711AA04813ECBAF5F1B3BC233610774035E8 (void);
// 0x000000B9 System.IAsyncResult Facebook.Unity.HideUnityDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void HideUnityDelegate_BeginInvoke_mF0552DBDC4CE2268B6ECA0531E6931BEF1925793 (void);
// 0x000000BA System.Void Facebook.Unity.HideUnityDelegate::EndInvoke(System.IAsyncResult)
extern void HideUnityDelegate_EndInvoke_m7120D8E66259DD68549E47534AE7B05CA4DB8DA3 (void);
// 0x000000BB Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::get_Facebook()
extern void FacebookGameObject_get_Facebook_m740762DE179046787FC0A7EA1394E6BB5D0AA509 (void);
// 0x000000BC System.Void Facebook.Unity.FacebookGameObject::set_Facebook(Facebook.Unity.IFacebookImplementation)
extern void FacebookGameObject_set_Facebook_m37C5BF1F3C26C5D1DD9735CC4E078ADAEFC79B1D (void);
// 0x000000BD System.Void Facebook.Unity.FacebookGameObject::Awake()
extern void FacebookGameObject_Awake_m50A6BE0B373BCAEA23B014CD6C22042DEE44A699 (void);
// 0x000000BE System.Void Facebook.Unity.FacebookGameObject::OnInitComplete(System.String)
extern void FacebookGameObject_OnInitComplete_m9C488BB89093D10D70A6DEFB86A95BFBBF6DC892 (void);
// 0x000000BF System.Void Facebook.Unity.FacebookGameObject::OnLoginComplete(System.String)
extern void FacebookGameObject_OnLoginComplete_m9EB8859D2D2E78C1338CEFFD60F6B5963B90D9AA (void);
// 0x000000C0 System.Void Facebook.Unity.FacebookGameObject::OnLogoutComplete(System.String)
extern void FacebookGameObject_OnLogoutComplete_m9EDB8A0D7197A07F1EB4736659990BF8A45EF6BA (void);
// 0x000000C1 System.Void Facebook.Unity.FacebookGameObject::OnGetAppLinkComplete(System.String)
extern void FacebookGameObject_OnGetAppLinkComplete_mD046144760E8377905C12A9F0904F1898864F299 (void);
// 0x000000C2 System.Void Facebook.Unity.FacebookGameObject::OnAppRequestsComplete(System.String)
extern void FacebookGameObject_OnAppRequestsComplete_mFBB2EB6CEABDB9AC0E372DDCD8DD2D9915F8ACDF (void);
// 0x000000C3 System.Void Facebook.Unity.FacebookGameObject::OnShareLinkComplete(System.String)
extern void FacebookGameObject_OnShareLinkComplete_m28884FEE8B037721524CB0C2C791AEC2DA2C886A (void);
// 0x000000C4 System.Void Facebook.Unity.FacebookGameObject::OnAwake()
extern void FacebookGameObject_OnAwake_mBB85C524A54369A5B494CE5A84B31849D58A1936 (void);
// 0x000000C5 System.Void Facebook.Unity.FacebookGameObject::.ctor()
extern void FacebookGameObject__ctor_mE073FEAB06013691F9271009E05F7DE5B5BA057F (void);
// 0x000000C6 System.String Facebook.Unity.FacebookSdkVersion::get_Build()
extern void FacebookSdkVersion_get_Build_mC63F17DA631CAC852C8C7459F8961E2E02A4AFEC (void);
// 0x000000C7 System.Boolean Facebook.Unity.IFacebook::get_LoggedIn()
// 0x000000C8 System.Boolean Facebook.Unity.IFacebook::get_LimitEventUsage()
// 0x000000C9 System.Void Facebook.Unity.IFacebook::set_LimitEventUsage(System.Boolean)
// 0x000000CA System.String Facebook.Unity.IFacebook::get_SDKUserAgent()
// 0x000000CB System.Boolean Facebook.Unity.IFacebook::get_Initialized()
// 0x000000CC System.Void Facebook.Unity.IFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x000000CD System.Void Facebook.Unity.IFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x000000CE System.Void Facebook.Unity.IFacebook::LogOut()
// 0x000000CF System.Void Facebook.Unity.IFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
// 0x000000D0 System.Void Facebook.Unity.IFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x000000D1 System.Void Facebook.Unity.IFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x000000D2 System.Void Facebook.Unity.IFacebook::API(System.String,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x000000D3 System.Void Facebook.Unity.IFacebook::API(System.String,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x000000D4 System.Void Facebook.Unity.IFacebook::ActivateApp(System.String)
// 0x000000D5 System.Void Facebook.Unity.IFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x000000D6 System.Void Facebook.Unity.IFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000D7 System.Void Facebook.Unity.IFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000D8 System.Void Facebook.Unity.IFacebookCallbackHandler::OnInitComplete(System.String)
// 0x000000D9 System.Void Facebook.Unity.IFacebookCallbackHandler::OnLoginComplete(System.String)
// 0x000000DA System.Void Facebook.Unity.IFacebookCallbackHandler::OnAppRequestsComplete(System.String)
// 0x000000DB System.Void Facebook.Unity.IFacebookCallbackHandler::OnShareLinkComplete(System.String)
// 0x000000DC System.Void Facebook.Unity.IFacebookResultHandler::OnInitComplete(Facebook.Unity.ResultContainer)
// 0x000000DD System.Void Facebook.Unity.IFacebookResultHandler::OnLoginComplete(Facebook.Unity.ResultContainer)
// 0x000000DE System.Void Facebook.Unity.IFacebookResultHandler::OnLogoutComplete(Facebook.Unity.ResultContainer)
// 0x000000DF System.Void Facebook.Unity.IFacebookResultHandler::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x000000E0 System.Void Facebook.Unity.IFacebookResultHandler::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
// 0x000000E1 System.Void Facebook.Unity.IFacebookResultHandler::OnShareLinkComplete(Facebook.Unity.ResultContainer)
// 0x000000E2 System.Void Facebook.Unity.IPayFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
// 0x000000E3 System.Void Facebook.Unity.MethodArguments::.ctor()
extern void MethodArguments__ctor_mD508C0DE76D61259DC9275B44C72919DE728BD66 (void);
// 0x000000E4 System.Void Facebook.Unity.MethodArguments::.ctor(Facebook.Unity.MethodArguments)
extern void MethodArguments__ctor_m4FD0284E8DFE50B1516702ED85FE32322490C6E9 (void);
// 0x000000E5 System.Void Facebook.Unity.MethodArguments::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments__ctor_m041528F49BF41576BE25EB8558527844857FB733 (void);
// 0x000000E6 System.Void Facebook.Unity.MethodArguments::AddPrimative(System.String,T)
// 0x000000E7 System.Void Facebook.Unity.MethodArguments::AddNullablePrimitive(System.String,System.Nullable`1<T>)
// 0x000000E8 System.Void Facebook.Unity.MethodArguments::AddString(System.String,System.String)
extern void MethodArguments_AddString_m21242D7EF4204EBE0D732B0A4A8D08AC54CF6042 (void);
// 0x000000E9 System.Void Facebook.Unity.MethodArguments::AddCommaSeparatedList(System.String,System.Collections.Generic.IEnumerable`1<System.String>)
extern void MethodArguments_AddCommaSeparatedList_m8D5BF91A69F43705221FD94CB101A02F250AF478 (void);
// 0x000000EA System.Void Facebook.Unity.MethodArguments::AddDictionary(System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments_AddDictionary_m69CDAACA87D6C7A6B03E1C7EC91B9BA1A8FD8EFD (void);
// 0x000000EB System.Void Facebook.Unity.MethodArguments::AddList(System.String,System.Collections.Generic.IEnumerable`1<T>)
// 0x000000EC System.Void Facebook.Unity.MethodArguments::AddUri(System.String,System.Uri)
extern void MethodArguments_AddUri_m740DF523B5B3EFEB5B0B55FD54A68D241E39B0F4 (void);
// 0x000000ED System.String Facebook.Unity.MethodArguments::ToJsonString()
extern void MethodArguments_ToJsonString_m69074422838A73AB3D489320B9CC01A35D263646 (void);
// 0x000000EE System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.Unity.MethodArguments::ToStringDict(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments_ToStringDict_mB784F8FB8864320D721A8F629E5950413F2E9854 (void);
// 0x000000EF System.Void Facebook.Unity.MethodCall`1::.ctor(Facebook.Unity.FacebookBase,System.String)
// 0x000000F0 System.String Facebook.Unity.MethodCall`1::get_MethodName()
// 0x000000F1 System.Void Facebook.Unity.MethodCall`1::set_MethodName(System.String)
// 0x000000F2 Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1::get_Callback()
// 0x000000F3 System.Void Facebook.Unity.MethodCall`1::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
// 0x000000F4 System.Void Facebook.Unity.MethodCall`1::set_FacebookImpl(Facebook.Unity.FacebookBase)
// 0x000000F5 System.Void Facebook.Unity.MethodCall`1::set_Parameters(Facebook.Unity.MethodArguments)
// 0x000000F6 System.Void Facebook.Unity.MethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x000000F7 System.Void Facebook.Unity.Product::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern void Product__ctor_m23892D597887D74B45ED70677CE4426B94F5A24C (void);
// 0x000000F8 System.String Facebook.Unity.Product::get_Title()
extern void Product_get_Title_m3CCEEC601D3E3FD58085D42D119E2CD72214284B (void);
// 0x000000F9 System.Void Facebook.Unity.Product::set_Title(System.String)
extern void Product_set_Title_mA00BBE60194B8031E8DC9795C5AAD7C478405407 (void);
// 0x000000FA System.String Facebook.Unity.Product::get_ProductID()
extern void Product_get_ProductID_m6D18E88F73E4DAC3CDE6CC39B8F1B95AA1B812DB (void);
// 0x000000FB System.Void Facebook.Unity.Product::set_ProductID(System.String)
extern void Product_set_ProductID_m17F36B75492158330E4B18ABD6A5FED2BE4DD056 (void);
// 0x000000FC System.String Facebook.Unity.Product::get_Description()
extern void Product_get_Description_m6203FA94ECD917A85941F5E2AEFBEB04995B101E (void);
// 0x000000FD System.Void Facebook.Unity.Product::set_Description(System.String)
extern void Product_set_Description_m52B3E12C5CFDB52A82F25733B2CF3A2B297E6AB5 (void);
// 0x000000FE System.String Facebook.Unity.Product::get_ImageURI()
extern void Product_get_ImageURI_mFA357CFE65D3E3B575512089D2E5C96566BC981D (void);
// 0x000000FF System.Void Facebook.Unity.Product::set_ImageURI(System.String)
extern void Product_set_ImageURI_m04B6F48A7F4ADB7287540E1256C7A8DDB504B906 (void);
// 0x00000100 System.String Facebook.Unity.Product::get_Price()
extern void Product_get_Price_mEB2AABDE5B502465B3B2D98DEAC634264DB79860 (void);
// 0x00000101 System.Void Facebook.Unity.Product::set_Price(System.String)
extern void Product_set_Price_m6D902C4B4E14EB609882AB27184349A19EA19013 (void);
// 0x00000102 System.String Facebook.Unity.Product::get_PriceCurrencyCode()
extern void Product_get_PriceCurrencyCode_m4C82A2B515E6F73F138952CF270FF8B34B4F1AB2 (void);
// 0x00000103 System.Void Facebook.Unity.Product::set_PriceCurrencyCode(System.String)
extern void Product_set_PriceCurrencyCode_m2E26A166A9B6FE42E0CAF3A9051AFF42789412EF (void);
// 0x00000104 System.String Facebook.Unity.Product::ToString()
extern void Product_ToString_mE5666C06E1BE83C7D5CB2042E2C6EEF84779F805 (void);
// 0x00000105 System.Void Facebook.Unity.Purchase::.ctor(System.Boolean,System.String,System.String,System.String,System.Int64,System.String,System.String)
extern void Purchase__ctor_m2F6D8D08B0CD45D3097181E71C5CB11FCC68B667 (void);
// 0x00000106 System.Boolean Facebook.Unity.Purchase::get_IsConsumed()
extern void Purchase_get_IsConsumed_mBDF2802687500F1A175DAD081CC0AF850CD6F56F (void);
// 0x00000107 System.String Facebook.Unity.Purchase::get_DeveloperPayload()
extern void Purchase_get_DeveloperPayload_m45989DD105423409B2B38375197D17EDD9425576 (void);
// 0x00000108 System.Void Facebook.Unity.Purchase::set_DeveloperPayload(System.String)
extern void Purchase_set_DeveloperPayload_mA0D6444F2D600A519E3031A773F83E05EDE6385D (void);
// 0x00000109 System.String Facebook.Unity.Purchase::get_PaymentID()
extern void Purchase_get_PaymentID_mFA4AC208C695B7254510E9B7ECEF518D7E55F203 (void);
// 0x0000010A System.Void Facebook.Unity.Purchase::set_PaymentID(System.String)
extern void Purchase_set_PaymentID_mF5980DC233FEFA0428D8CED4F35DDFBC46BF64B4 (void);
// 0x0000010B System.String Facebook.Unity.Purchase::get_ProductID()
extern void Purchase_get_ProductID_mC6DA4ED2B9A12DE3653F1F86F04AA65128DC4163 (void);
// 0x0000010C System.Void Facebook.Unity.Purchase::set_ProductID(System.String)
extern void Purchase_set_ProductID_mB8FADC80738017AB3FE268933F4912F7E4FBAE98 (void);
// 0x0000010D System.DateTime Facebook.Unity.Purchase::get_PurchaseTime()
extern void Purchase_get_PurchaseTime_m0C3548BF414916488397B6AD6A5B8F24EF74DB1D (void);
// 0x0000010E System.Void Facebook.Unity.Purchase::set_PurchaseTime(System.DateTime)
extern void Purchase_set_PurchaseTime_m0FF2CEA8C911631DD04E1F6787D60005E7B178A0 (void);
// 0x0000010F System.String Facebook.Unity.Purchase::get_PurchaseToken()
extern void Purchase_get_PurchaseToken_mB931680FED62380B1EBFBABB2C003C26059B2374 (void);
// 0x00000110 System.Void Facebook.Unity.Purchase::set_PurchaseToken(System.String)
extern void Purchase_set_PurchaseToken_m0869B9434D1A7E7D98A55863BC7F8DC824E438F2 (void);
// 0x00000111 System.String Facebook.Unity.Purchase::get_SignedRequest()
extern void Purchase_get_SignedRequest_mA385E94DC9A29A558111601A4CAA9C3B34D1C1F9 (void);
// 0x00000112 System.Void Facebook.Unity.Purchase::set_SignedRequest(System.String)
extern void Purchase_set_SignedRequest_m743796F39F3443AC9F43B6E84DCAD00E3FE42AD0 (void);
// 0x00000113 System.String Facebook.Unity.Purchase::ToString()
extern void Purchase_ToString_m31D5D3478D351E820B09BAA3851314A9F4F1C70D (void);
// 0x00000114 System.Void Facebook.Unity.AccessTokenRefreshResult::.ctor(Facebook.Unity.ResultContainer)
extern void AccessTokenRefreshResult__ctor_m85D655868C8FF5C2A3336A4007A3FA0186C3BB2A (void);
// 0x00000115 Facebook.Unity.AccessToken Facebook.Unity.AccessTokenRefreshResult::get_AccessToken()
extern void AccessTokenRefreshResult_get_AccessToken_mE6DB4980B770602E587E17E4F9C8995CA5762828 (void);
// 0x00000116 System.Void Facebook.Unity.AccessTokenRefreshResult::set_AccessToken(Facebook.Unity.AccessToken)
extern void AccessTokenRefreshResult_set_AccessToken_mF8AB8ACBE83AAA4A609C913E535E59DBA6ABD24E (void);
// 0x00000117 System.String Facebook.Unity.AccessTokenRefreshResult::ToString()
extern void AccessTokenRefreshResult_ToString_m245B4A745E4282F9517B1957E8D09D3F91A96DC9 (void);
// 0x00000118 System.Void Facebook.Unity.AppLinkResult::.ctor(Facebook.Unity.ResultContainer)
extern void AppLinkResult__ctor_mB11A65A8AB654F69E173C0EA7D93C5B80432F5EE (void);
// 0x00000119 System.String Facebook.Unity.AppLinkResult::get_Url()
extern void AppLinkResult_get_Url_mBA3FBE84A4830A4B2B01E75074436694572678A5 (void);
// 0x0000011A System.Void Facebook.Unity.AppLinkResult::set_Url(System.String)
extern void AppLinkResult_set_Url_mF2F7158EC461D9736B9CAE3E9998621AC7D8B4B7 (void);
// 0x0000011B System.String Facebook.Unity.AppLinkResult::get_TargetUrl()
extern void AppLinkResult_get_TargetUrl_mCA56B7E750E5CBC7F2872750A6010F7246AC9739 (void);
// 0x0000011C System.Void Facebook.Unity.AppLinkResult::set_TargetUrl(System.String)
extern void AppLinkResult_set_TargetUrl_m4C005ED078EF6E4596E17F8A360FA6AF2AD58D23 (void);
// 0x0000011D System.String Facebook.Unity.AppLinkResult::get_Ref()
extern void AppLinkResult_get_Ref_m30B8D8B6DEBC0742580F29FE2CD7538A39A3B58E (void);
// 0x0000011E System.Void Facebook.Unity.AppLinkResult::set_Ref(System.String)
extern void AppLinkResult_set_Ref_m34E37AD58AF1660CC70C39522218B04BC791B1A1 (void);
// 0x0000011F System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.AppLinkResult::get_Extras()
extern void AppLinkResult_get_Extras_m2987F464221039109F018DF9BFD65452DFCEFEC8 (void);
// 0x00000120 System.Void Facebook.Unity.AppLinkResult::set_Extras(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void AppLinkResult_set_Extras_mA2C8B74C9A4C6247E36C2B4AEAE2E69BFA7AA76A (void);
// 0x00000121 System.String Facebook.Unity.AppLinkResult::ToString()
extern void AppLinkResult_ToString_m947B439123C9E5EE87BE3C0947B348C994F04A67 (void);
// 0x00000122 System.Void Facebook.Unity.AppRequestResult::.ctor(Facebook.Unity.ResultContainer)
extern void AppRequestResult__ctor_m80EC540B48D711BA530B8F8BB27622BBC341E426 (void);
// 0x00000123 System.String Facebook.Unity.AppRequestResult::get_RequestID()
extern void AppRequestResult_get_RequestID_m0FB4B77116C02ABF8EC18E3618728BC610F079E8 (void);
// 0x00000124 System.Void Facebook.Unity.AppRequestResult::set_RequestID(System.String)
extern void AppRequestResult_set_RequestID_mE05C9016FD9B06C30F56F814FE0152AF8B4F8607 (void);
// 0x00000125 System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AppRequestResult::get_To()
extern void AppRequestResult_get_To_m37F76214768FC29EEC788C2304073F59F784458B (void);
// 0x00000126 System.Void Facebook.Unity.AppRequestResult::set_To(System.Collections.Generic.IEnumerable`1<System.String>)
extern void AppRequestResult_set_To_m574F932ABB3C52A0E79B9A4B6F4B288C1031A22A (void);
// 0x00000127 System.String Facebook.Unity.AppRequestResult::ToString()
extern void AppRequestResult_ToString_mA4495333BA422CCFCDF8C6D7BEFB382DED578DB8 (void);
// 0x00000128 System.Void Facebook.Unity.CatalogResult::.ctor(Facebook.Unity.ResultContainer)
extern void CatalogResult__ctor_m3BB1C94AB284C7F598285E846E885FB56ADA8E68 (void);
// 0x00000129 System.Collections.Generic.IList`1<Facebook.Unity.Product> Facebook.Unity.CatalogResult::get_Products()
extern void CatalogResult_get_Products_m7182B5FDDFB039C3A65E610E603BEB108AAD4492 (void);
// 0x0000012A System.Void Facebook.Unity.CatalogResult::set_Products(System.Collections.Generic.IList`1<Facebook.Unity.Product>)
extern void CatalogResult_set_Products_m56CFF78280420A4520B2B5493D472E3242FBCA29 (void);
// 0x0000012B System.String Facebook.Unity.CatalogResult::ToString()
extern void CatalogResult_ToString_mCA75D583B8B1EF13F33479E77D45257EA4D6E260 (void);
// 0x0000012C System.Void Facebook.Unity.ConsumePurchaseResult::.ctor(Facebook.Unity.ResultContainer)
extern void ConsumePurchaseResult__ctor_m9B155E16B3DF04C6CD3620E89EB5958B7AE127FA (void);
// 0x0000012D System.Void Facebook.Unity.GamingServicesFriendFinderResult::.ctor(Facebook.Unity.ResultContainer)
extern void GamingServicesFriendFinderResult__ctor_m443BC7ED4BECB5C73952009D056F594EC7C0D35B (void);
// 0x0000012E System.Void Facebook.Unity.GraphResult::.ctor(UnityEngine.WWW)
extern void GraphResult__ctor_mAAEF4E0D08FA61A3206CE4ED59898EECA9F66C32 (void);
// 0x0000012F System.Void Facebook.Unity.GraphResult::set_ResultList(System.Collections.Generic.IList`1<System.Object>)
extern void GraphResult_set_ResultList_m895C92743657296186ADB66CEA4BBC5D487E1408 (void);
// 0x00000130 UnityEngine.Texture2D Facebook.Unity.GraphResult::get_Texture()
extern void GraphResult_get_Texture_m52F3139402C071CB46BC9DC1590FDE6B996AC2D9 (void);
// 0x00000131 System.Void Facebook.Unity.GraphResult::set_Texture(UnityEngine.Texture2D)
extern void GraphResult_set_Texture_m45BF75B17E4FB6EF772BE00CEDD52CC3C45F464E (void);
// 0x00000132 System.Void Facebook.Unity.GraphResult::Init(System.String)
extern void GraphResult_Init_mBF636DC5E6EE0B3FFEB9C04E01DFA1BA54FA3DF8 (void);
// 0x00000133 System.Void Facebook.Unity.IAPReadyResult::.ctor(Facebook.Unity.ResultContainer)
extern void IAPReadyResult__ctor_mCF75DA82298C1C3708B068C8AB53FE87BBACEAC8 (void);
// 0x00000134 UnityEngine.Texture2D Facebook.Unity.IGraphResult::get_Texture()
// 0x00000135 System.Void Facebook.Unity.InitCloudGameResult::.ctor(Facebook.Unity.ResultContainer)
extern void InitCloudGameResult__ctor_mC41318DE39283E2035E2F70FECCD23EC69364602 (void);
// 0x00000136 System.Void Facebook.Unity.InterstitialAdResult::.ctor(Facebook.Unity.ResultContainer)
extern void InterstitialAdResult__ctor_mE071017319C76EB374DE91C402442C371C82D6EC (void);
// 0x00000137 System.String Facebook.Unity.IInternalResult::get_CallbackId()
// 0x00000138 System.String Facebook.Unity.IResult::get_Error()
// 0x00000139 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.IResult::get_ResultDictionary()
// 0x0000013A System.String Facebook.Unity.IResult::get_RawResult()
// 0x0000013B System.Boolean Facebook.Unity.IResult::get_Cancelled()
// 0x0000013C System.Void Facebook.Unity.LoginResult::.ctor(Facebook.Unity.ResultContainer)
extern void LoginResult__ctor_m85276C08E1FB1B5EC6134BED2863F768F9C4FCE8 (void);
// 0x0000013D Facebook.Unity.AccessToken Facebook.Unity.LoginResult::get_AccessToken()
extern void LoginResult_get_AccessToken_mAE248509070F413BD07740D74EFA1AB09C69965E (void);
// 0x0000013E System.Void Facebook.Unity.LoginResult::set_AccessToken(Facebook.Unity.AccessToken)
extern void LoginResult_set_AccessToken_m019741BF9BB4EC4104805DC408D0F0E3437B0D47 (void);
// 0x0000013F Facebook.Unity.AuthenticationToken Facebook.Unity.LoginResult::get_AuthenticationToken()
extern void LoginResult_get_AuthenticationToken_mD59EDC4AC3A0C458D1AC86535E07A971FE1BC02E (void);
// 0x00000140 System.Void Facebook.Unity.LoginResult::set_AuthenticationToken(Facebook.Unity.AuthenticationToken)
extern void LoginResult_set_AuthenticationToken_m8EE993212DBD9D77DA84F75ADC5ED1368E1660BF (void);
// 0x00000141 System.String Facebook.Unity.LoginResult::ToString()
extern void LoginResult_ToString_mC7A43B68C44BCAAED64003998FB58EAA4BCD5353 (void);
// 0x00000142 System.Void Facebook.Unity.LoginResult::.cctor()
extern void LoginResult__cctor_m057A12952360513145B64FCEAFAF4D80D54CE3E4 (void);
// 0x00000143 System.Void Facebook.Unity.LoginStatusResult::.ctor(Facebook.Unity.ResultContainer)
extern void LoginStatusResult__ctor_m71D1B4FDD17BA8174089E50BCABE50B837FC888C (void);
// 0x00000144 System.Boolean Facebook.Unity.LoginStatusResult::get_Failed()
extern void LoginStatusResult_get_Failed_m7E910019B793A2AE97C09E353E07CCAFF5198F06 (void);
// 0x00000145 System.Void Facebook.Unity.LoginStatusResult::set_Failed(System.Boolean)
extern void LoginStatusResult_set_Failed_mAA0F6B0BA9EE37924C64E21610D3ADA9DEC93258 (void);
// 0x00000146 System.String Facebook.Unity.LoginStatusResult::ToString()
extern void LoginStatusResult_ToString_m20597EFF6A48AFF1213CD5A6EA2B40CA6995C008 (void);
// 0x00000147 System.Void Facebook.Unity.LoginStatusResult::.cctor()
extern void LoginStatusResult__cctor_m15D972708D3F6C6688751A309A860102FD46CAB7 (void);
// 0x00000148 System.Void Facebook.Unity.OpenAppStoreResult::.ctor(Facebook.Unity.ResultContainer)
extern void OpenAppStoreResult__ctor_mF5F1196B685895BDB82B57C9D86B57B9C1907E83 (void);
// 0x00000149 System.Void Facebook.Unity.PayloadResult::.ctor(Facebook.Unity.ResultContainer)
extern void PayloadResult__ctor_m691D4495B0FCEF40E7BED3C93F41E55363AB58E2 (void);
// 0x0000014A System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.PayloadResult::get_Payload()
extern void PayloadResult_get_Payload_m45F5E62B679A74AA935056EB36FCE93D2EF679D5 (void);
// 0x0000014B System.Void Facebook.Unity.PayloadResult::set_Payload(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void PayloadResult_set_Payload_mE6BEDB93CCB0B255C47BBC38E1FF1C67BE2DD385 (void);
// 0x0000014C System.String Facebook.Unity.PayloadResult::ToString()
extern void PayloadResult_ToString_mA92958F0A0DB48EFA7999DBAC61BECAED1A50637 (void);
// 0x0000014D System.Void Facebook.Unity.PayResult::.ctor(Facebook.Unity.ResultContainer)
extern void PayResult__ctor_m8E6F08D2B115AB10AF6F43B86CA10607776D2BCD (void);
// 0x0000014E System.Int64 Facebook.Unity.PayResult::get_ErrorCode()
extern void PayResult_get_ErrorCode_m94F77C8859FA562BB0281C0ACC6015184C9BAF4B (void);
// 0x0000014F System.String Facebook.Unity.PayResult::ToString()
extern void PayResult_ToString_mE4B6D445AE6638AF4D1E67FA41FC374160C32E4C (void);
// 0x00000150 System.Void Facebook.Unity.PurchaseResult::.ctor(Facebook.Unity.ResultContainer)
extern void PurchaseResult__ctor_m87B784088DBA83E92FCDD326412C179857598815 (void);
// 0x00000151 Facebook.Unity.Purchase Facebook.Unity.PurchaseResult::get_Purchase()
extern void PurchaseResult_get_Purchase_m227AF577E2F5EAD8C2475FF8C157F503A69F0173 (void);
// 0x00000152 System.Void Facebook.Unity.PurchaseResult::set_Purchase(Facebook.Unity.Purchase)
extern void PurchaseResult_set_Purchase_mF6DC16F48C0BAFC85181E26CB3DAA17DEB3A3EDE (void);
// 0x00000153 System.String Facebook.Unity.PurchaseResult::ToString()
extern void PurchaseResult_ToString_mC6A4CB4DEA9A9B64923A15767276289BCDEE9BE4 (void);
// 0x00000154 System.Void Facebook.Unity.PurchasesResult::.ctor(Facebook.Unity.ResultContainer)
extern void PurchasesResult__ctor_m2F067E525ADD70BC70675729D490631201992AFC (void);
// 0x00000155 System.Collections.Generic.IList`1<Facebook.Unity.Purchase> Facebook.Unity.PurchasesResult::get_Purchases()
extern void PurchasesResult_get_Purchases_m52BA69945FED109ED71BFCC966E7BE9CD533D7AA (void);
// 0x00000156 System.Void Facebook.Unity.PurchasesResult::set_Purchases(System.Collections.Generic.IList`1<Facebook.Unity.Purchase>)
extern void PurchasesResult_set_Purchases_m41CFF14B70FE8C8E855DF245D0859329660E2C66 (void);
// 0x00000157 System.String Facebook.Unity.PurchasesResult::ToString()
extern void PurchasesResult_ToString_mB15039FD72FA69BC768C92979F3C0E8DB2C7FD77 (void);
// 0x00000158 System.Void Facebook.Unity.ResultBase::.ctor(Facebook.Unity.ResultContainer)
extern void ResultBase__ctor_m5E12EFF38E77D6C2618264EF071B9946A07D2FA6 (void);
// 0x00000159 System.Void Facebook.Unity.ResultBase::.ctor(Facebook.Unity.ResultContainer,System.String,System.Boolean)
extern void ResultBase__ctor_m3E05350CF01C733EE78E9910FFF0BC49153248A5 (void);
// 0x0000015A System.String Facebook.Unity.ResultBase::get_Error()
extern void ResultBase_get_Error_mCC0909B7B88BA1EAC4175B6E13C86019F0B65D25 (void);
// 0x0000015B System.Void Facebook.Unity.ResultBase::set_Error(System.String)
extern void ResultBase_set_Error_mDE71B35D7C648815A395ACEACEFB2E1731692482 (void);
// 0x0000015C System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.ResultBase::get_ErrorDictionary()
extern void ResultBase_get_ErrorDictionary_m49CA2E69CD572D72EF9A38656ACF234C2B6BAAE9 (void);
// 0x0000015D System.Void Facebook.Unity.ResultBase::set_ErrorDictionary(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void ResultBase_set_ErrorDictionary_m1CA8FB49C25033B3B263CA80A75ADDEB5BFF1D63 (void);
// 0x0000015E System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultBase::get_ResultDictionary()
extern void ResultBase_get_ResultDictionary_mD20F414B2C7D529F85356ABE2283D2341B7AB5B1 (void);
// 0x0000015F System.Void Facebook.Unity.ResultBase::set_ResultDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_set_ResultDictionary_m4241527675668D7BA49F868F0F9171CA2169A776 (void);
// 0x00000160 System.String Facebook.Unity.ResultBase::get_RawResult()
extern void ResultBase_get_RawResult_mFFF2523AD1CDC61D6A1B22F27B72A42C6309AD2D (void);
// 0x00000161 System.Void Facebook.Unity.ResultBase::set_RawResult(System.String)
extern void ResultBase_set_RawResult_m05E3C92A53BAC1479BF04DE3EF7F8E12C103A65A (void);
// 0x00000162 System.Boolean Facebook.Unity.ResultBase::get_Cancelled()
extern void ResultBase_get_Cancelled_mDD7F37569D6DBF9857CEDF7121022A84AB04DA62 (void);
// 0x00000163 System.Void Facebook.Unity.ResultBase::set_Cancelled(System.Boolean)
extern void ResultBase_set_Cancelled_mBF2A85838F542800DE8CE18B23CD96EEE09926AC (void);
// 0x00000164 System.String Facebook.Unity.ResultBase::get_CallbackId()
extern void ResultBase_get_CallbackId_mD5740195C1CB90B66BF4AD373DE6541FB342D320 (void);
// 0x00000165 System.Void Facebook.Unity.ResultBase::set_CallbackId(System.String)
extern void ResultBase_set_CallbackId_m6BD9FE80163FA42DA2F480244F9FEE7963E715F9 (void);
// 0x00000166 System.Nullable`1<System.Int64> Facebook.Unity.ResultBase::get_CanvasErrorCode()
extern void ResultBase_get_CanvasErrorCode_mA4234ACCBEA60FB65EB4840FA986C5ACEEAE8296 (void);
// 0x00000167 System.Void Facebook.Unity.ResultBase::set_CanvasErrorCode(System.Nullable`1<System.Int64>)
extern void ResultBase_set_CanvasErrorCode_m8F1FA8B0AE7ECB1350BE35CA064DABE9C168E660 (void);
// 0x00000168 System.String Facebook.Unity.ResultBase::ToString()
extern void ResultBase_ToString_m62550CEB891F641D9052A52C5C707679EF5400C2 (void);
// 0x00000169 System.Void Facebook.Unity.ResultBase::Init(Facebook.Unity.ResultContainer,System.String,System.Boolean,System.String)
extern void ResultBase_Init_mEBE2DBDE8088CBFB6E7FB38BB3FE5F5AA9DFA326 (void);
// 0x0000016A System.String Facebook.Unity.ResultBase::GetErrorValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetErrorValue_mA0CCC18CB71A02D745EF87157F946E67BDC7E61F (void);
// 0x0000016B System.Boolean Facebook.Unity.ResultBase::GetCancelledValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetCancelledValue_m97BD8CAA2D2DBBCDC7DA608E0B39EC20BFF736AC (void);
// 0x0000016C System.String Facebook.Unity.ResultBase::GetCallbackId(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetCallbackId_m09BEA21429814FB46C26D48011722151FED9E7CC (void);
// 0x0000016D System.Void Facebook.Unity.ResultContainer::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer__ctor_m2D713E4783B2EE17A3FCE77BC85BD3AE9FE9DAA5 (void);
// 0x0000016E System.Void Facebook.Unity.ResultContainer::.ctor(System.String)
extern void ResultContainer__ctor_mA77410DC3FC49A8E25BEB7A656C7A174826A042E (void);
// 0x0000016F System.String Facebook.Unity.ResultContainer::get_RawResult()
extern void ResultContainer_get_RawResult_mE6B6064A98F862CF47C25572CDA1F841CA543016 (void);
// 0x00000170 System.Void Facebook.Unity.ResultContainer::set_RawResult(System.String)
extern void ResultContainer_set_RawResult_mEDA77372AEB21EDAE82C78089DB9386094EF67F7 (void);
// 0x00000171 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::get_ResultDictionary()
extern void ResultContainer_get_ResultDictionary_m2D22D637E456BBFE03A180CE2F25EEF556698932 (void);
// 0x00000172 System.Void Facebook.Unity.ResultContainer::set_ResultDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer_set_ResultDictionary_m960C623B0C08F561E21E9D2AB45860FB7E841098 (void);
// 0x00000173 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::GetWebFormattedResponseDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer_GetWebFormattedResponseDictionary_mB8B35868B7283489FC8F66254D1CB41659BB890C (void);
// 0x00000174 System.Void Facebook.Unity.RewardedVideoResult::.ctor(Facebook.Unity.ResultContainer)
extern void RewardedVideoResult__ctor_m4CA2DB81B09D68215D3A9AAF40940D18AE55E99D (void);
// 0x00000175 System.Void Facebook.Unity.ScheduleAppToUserNotificationResult::.ctor(Facebook.Unity.ResultContainer)
extern void ScheduleAppToUserNotificationResult__ctor_m813EEB92BCAFF10AF770602CB3E733AB928B99A0 (void);
// 0x00000176 System.Void Facebook.Unity.ShareResult::.ctor(Facebook.Unity.ResultContainer)
extern void ShareResult__ctor_mA64055E46B61193BB87068E62AE2B26270FBDEB3 (void);
// 0x00000177 System.String Facebook.Unity.ShareResult::get_PostId()
extern void ShareResult_get_PostId_mE57DC5964B14931326498A079D9750D3A3BA24AF (void);
// 0x00000178 System.Void Facebook.Unity.ShareResult::set_PostId(System.String)
extern void ShareResult_set_PostId_mE962B736189DBCF1D1DECCF30B661E5A7581BCF1 (void);
// 0x00000179 System.String Facebook.Unity.ShareResult::get_PostIDKey()
extern void ShareResult_get_PostIDKey_m8353405F36BB63AD45B6103BAF312784FAAFF105 (void);
// 0x0000017A System.String Facebook.Unity.ShareResult::ToString()
extern void ShareResult_ToString_mBAB1FFEA0FCBDB2E0D1A4B01583651D59096C213 (void);
// 0x0000017B System.Void Facebook.Unity.AsyncRequestString::Post(System.Uri,System.Collections.Generic.Dictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Post_m3508D6AC1F863897396378699216DF1CD56C08CB (void);
// 0x0000017C System.Void Facebook.Unity.AsyncRequestString::Get(System.Uri,System.Collections.Generic.Dictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Get_m7961B3941B966DC3A6E8BFED4E84A21D299ED3B3 (void);
// 0x0000017D System.Void Facebook.Unity.AsyncRequestString::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Request_mC7D709B6B3285FD732775F1A29A97611D4D94D8C (void);
// 0x0000017E System.Void Facebook.Unity.AsyncRequestString::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Request_m4E8E7362ACAD642147D6503C333AC95771C9600F (void);
// 0x0000017F System.Collections.IEnumerator Facebook.Unity.AsyncRequestString::Start()
extern void AsyncRequestString_Start_mF290990673DEDF249E128201F16A3B149B4B85CB (void);
// 0x00000180 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetUrl(System.Uri)
extern void AsyncRequestString_SetUrl_m6E79537333AE9E7F61EDB0FD48019AA771AFA82F (void);
// 0x00000181 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetMethod(Facebook.Unity.HttpMethod)
extern void AsyncRequestString_SetMethod_m3DC09200EB5EDD98E44C916D099161BC13F7D14D (void);
// 0x00000182 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetFormData(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AsyncRequestString_SetFormData_m5EE6DC961CBEACB166445A9E9D3F97BD66314803 (void);
// 0x00000183 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetQuery(UnityEngine.WWWForm)
extern void AsyncRequestString_SetQuery_mFAE0D2A7594D55C80241D7025302224A613C8A5A (void);
// 0x00000184 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetCallback(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_SetCallback_mD0315B82008E8E523D91DA4B8D3D3E3CFCD4123D (void);
// 0x00000185 System.Void Facebook.Unity.AsyncRequestString::.ctor()
extern void AsyncRequestString__ctor_mD3E8608FE8401CEC81E32FF9979692805F5CA950 (void);
// 0x00000186 System.Void Facebook.Unity.AsyncRequestString/<Start>d__9::.ctor(System.Int32)
extern void U3CStartU3Ed__9__ctor_m91E8453F9A21FA4964B22F7E00DFBDBF320FE4B3 (void);
// 0x00000187 System.Void Facebook.Unity.AsyncRequestString/<Start>d__9::System.IDisposable.Dispose()
extern void U3CStartU3Ed__9_System_IDisposable_Dispose_mAE3163411A60AF021C851814054C5C2CED63C843 (void);
// 0x00000188 System.Boolean Facebook.Unity.AsyncRequestString/<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_m19236F54EC8992E5C785CED4C274183B3119F922 (void);
// 0x00000189 System.Object Facebook.Unity.AsyncRequestString/<Start>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE50330F88F71D02DB3524466A22E02125CCC3A27 (void);
// 0x0000018A System.Void Facebook.Unity.AsyncRequestString/<Start>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m4AD7E69240BB16630A342A950E247C17CA66CA26 (void);
// 0x0000018B System.Object Facebook.Unity.AsyncRequestString/<Start>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m9ACE793E1E1660527DB7FCF8B756ABA8CA6795B2 (void);
// 0x0000018C System.Void Facebook.Unity.FacebookLogger::.cctor()
extern void FacebookLogger__cctor_mD3887F56A7BD2BB21F373B8FB4DCC147E51689A8 (void);
// 0x0000018D Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger::get_Instance()
extern void FacebookLogger_get_Instance_m0A479DAE88A8B5FE53DAC0FCEC98D50EA647CBCF (void);
// 0x0000018E System.Void Facebook.Unity.FacebookLogger::set_Instance(Facebook.Unity.IFacebookLogger)
extern void FacebookLogger_set_Instance_m828EEDA7C96A62FE2D525A006751147505C24264 (void);
// 0x0000018F System.Void Facebook.Unity.FacebookLogger::Log(System.String)
extern void FacebookLogger_Log_m43CAF4597811AC44ABAC437768AC3F4D7A0696D4 (void);
// 0x00000190 System.Void Facebook.Unity.FacebookLogger::Info(System.String)
extern void FacebookLogger_Info_mF7A1BBED758AED0EBE12365426DB0D394424A947 (void);
// 0x00000191 System.Void Facebook.Unity.FacebookLogger::Warn(System.String)
extern void FacebookLogger_Warn_m8F5C4F8D8944929A8A6D550209A207D118A92046 (void);
// 0x00000192 System.Void Facebook.Unity.FacebookLogger::Warn(System.String,System.String[])
extern void FacebookLogger_Warn_m9A0E2D966A5C4A77930B8DFDB927F46E52D24485 (void);
// 0x00000193 System.Void Facebook.Unity.FacebookLogger/DebugLogger::.ctor()
extern void DebugLogger__ctor_m584004A91D03D3110D75C9D052A30C8761E49DD0 (void);
// 0x00000194 System.Void Facebook.Unity.FacebookLogger/DebugLogger::Log(System.String)
extern void DebugLogger_Log_mDDD540D8A0CDA94FE8E628967CAB47976AB2A9F5 (void);
// 0x00000195 System.Void Facebook.Unity.FacebookLogger/DebugLogger::Info(System.String)
extern void DebugLogger_Info_mFB3C3C6AC4027F5AF8F87CD06E044F75F8BE7328 (void);
// 0x00000196 System.Void Facebook.Unity.FacebookLogger/DebugLogger::Warn(System.String)
extern void DebugLogger_Warn_m58A99DF6AD66480BAC6F18D4D91B7AAFACFF62E6 (void);
// 0x00000197 System.Void Facebook.Unity.IFacebookLogger::Log(System.String)
// 0x00000198 System.Void Facebook.Unity.IFacebookLogger::Info(System.String)
// 0x00000199 System.Void Facebook.Unity.IFacebookLogger::Warn(System.String)
// 0x0000019A System.Boolean Facebook.Unity.Utilities::TryGetValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,T&)
// 0x0000019B System.Int64 Facebook.Unity.Utilities::TotalSeconds(System.DateTime)
extern void Utilities_TotalSeconds_mA49A914FAEFFBA5834F1FB7B6EF3E0D67BAE38A5 (void);
// 0x0000019C T Facebook.Unity.Utilities::GetValueOrDefault(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Boolean)
// 0x0000019D System.String Facebook.Unity.Utilities::ToCommaSeparateList(System.Collections.Generic.IEnumerable`1<System.String>)
extern void Utilities_ToCommaSeparateList_m3925082326445282086770A30C42E9CED61FE092 (void);
// 0x0000019E System.String Facebook.Unity.Utilities::AbsoluteUrlOrEmptyString(System.Uri)
extern void Utilities_AbsoluteUrlOrEmptyString_m716B9F8FDA319A4FA6E1E5774D0F9DC5F6FC68E8 (void);
// 0x0000019F System.String Facebook.Unity.Utilities::GetUserAgent(System.String,System.String)
extern void Utilities_GetUserAgent_m675C4889732616346E67C628F97B8AC369FF0587 (void);
// 0x000001A0 System.String Facebook.Unity.Utilities::ToJson(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ToJson_m0F09663A04FA1B2674B2E0D177A132C429D7171E (void);
// 0x000001A1 System.Void Facebook.Unity.Utilities::AddAllKVPFrom(System.Collections.Generic.IDictionary`2<T1,T2>,System.Collections.Generic.IDictionary`2<T1,T2>)
// 0x000001A2 Facebook.Unity.AccessToken Facebook.Unity.Utilities::ParseAccessTokenFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseAccessTokenFromResult_m07AB0E9CC6F0963AA4F9C2EFC1790B7FAFD1FD25 (void);
// 0x000001A3 Facebook.Unity.AuthenticationToken Facebook.Unity.Utilities::ParseAuthenticationTokenFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseAuthenticationTokenFromResult_m021290683967FECFBE7B168598ACB60DAD55136D (void);
// 0x000001A4 System.String Facebook.Unity.Utilities::ToStringNullOk(System.Object)
extern void Utilities_ToStringNullOk_m3894054C9E1D3C06482160DD8AD34E18D478730B (void);
// 0x000001A5 System.String Facebook.Unity.Utilities::FormatToString(System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void Utilities_FormatToString_mA85A14D511A2C0AA815D5046ACF222C2BF1B7388 (void);
// 0x000001A6 System.DateTime Facebook.Unity.Utilities::ParseExpirationDateFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseExpirationDateFromResult_m28AF7A3EEA902C98623138DE37E608F554C7EED2 (void);
// 0x000001A7 System.Nullable`1<System.DateTime> Facebook.Unity.Utilities::ParseLastRefreshFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseLastRefreshFromResult_m1548D2EFFC28EFC662B07D6C1BC88332D9160226 (void);
// 0x000001A8 System.Collections.Generic.ICollection`1<System.String> Facebook.Unity.Utilities::ParsePermissionFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParsePermissionFromResult_mD7E2E68C2D3795CCB744CAD73F41675F5B98DD8A (void);
// 0x000001A9 System.Collections.Generic.IList`1<Facebook.Unity.Product> Facebook.Unity.Utilities::ParseCatalogFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseCatalogFromResult_m9E0D7BE8527F7B6BFF51AA88AD1CD5FA389C287A (void);
// 0x000001AA System.Collections.Generic.IList`1<Facebook.Unity.Purchase> Facebook.Unity.Utilities::ParsePurchasesFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParsePurchasesFromResult_m2BB331D4FA2F57DA465B7D3CE07C3825F22F3893 (void);
// 0x000001AB Facebook.Unity.Purchase Facebook.Unity.Utilities::ParsePurchaseFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParsePurchaseFromResult_mE96C30E2EE9E3B82773184357FEF9AAEF779AD17 (void);
// 0x000001AC System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.Utilities::ParseStringDictionaryFromString(System.String)
extern void Utilities_ParseStringDictionaryFromString_mF9A13BF3A0FCBDB7036721CAE4B925CFE6182AF0 (void);
// 0x000001AD System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.Utilities::ParseInnerStringDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String)
extern void Utilities_ParseInnerStringDictionary_m1CF34CC2930297E03F1922442DE189D70568956D (void);
// 0x000001AE System.DateTime Facebook.Unity.Utilities::FromTimestamp(System.Int32)
extern void Utilities_FromTimestamp_mE8D881324DA6DBCEF2954A79E578F04B84437D81 (void);
// 0x000001AF System.Void Facebook.Unity.Utilities/Callback`1::.ctor(System.Object,System.IntPtr)
// 0x000001B0 System.Void Facebook.Unity.Utilities/Callback`1::Invoke(T)
// 0x000001B1 System.IAsyncResult Facebook.Unity.Utilities/Callback`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000001B2 System.Void Facebook.Unity.Utilities/Callback`1::EndInvoke(System.IAsyncResult)
// 0x000001B3 System.Void Facebook.Unity.Utilities/<>c::.cctor()
extern void U3CU3Ec__cctor_m0FF4B018807197B4DAFC4A47B33BE53CD248FB66 (void);
// 0x000001B4 System.Void Facebook.Unity.Utilities/<>c::.ctor()
extern void U3CU3Ec__ctor_m13112A068F52B52E15586E59A13FB09E6EB7D27D (void);
// 0x000001B5 System.String Facebook.Unity.Utilities/<>c::<ParsePermissionFromResult>b__19_0(System.Object)
extern void U3CU3Ec_U3CParsePermissionFromResultU3Eb__19_0_mC90F5C78EB6264F11F0CFBF2E943C4C78EA9F512 (void);
// 0x000001B6 Facebook.Unity.IAsyncRequestStringWrapper Facebook.Unity.FBUnityUtility::get_AsyncRequestStringWrapper()
extern void FBUnityUtility_get_AsyncRequestStringWrapper_m39D1630078EFD96CD6921AE348F263B373AADDF9 (void);
// 0x000001B7 System.Void Facebook.Unity.AsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestStringWrapper_Request_m40B8CAA59C930106522ABFCACD626FABCEB98286 (void);
// 0x000001B8 System.Void Facebook.Unity.AsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestStringWrapper_Request_mC76363AF290872D609EEF729C07C7E00746DB331 (void);
// 0x000001B9 System.Void Facebook.Unity.AsyncRequestStringWrapper::.ctor()
extern void AsyncRequestStringWrapper__ctor_m2FD885515CC1B65FFF6B6388CC06332B694C77FA (void);
// 0x000001BA System.Void Facebook.Unity.IAsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x000001BB System.Void Facebook.Unity.IAsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x000001BC System.Void Facebook.Unity.FacebookScheduler::Schedule(System.Action,System.Int64)
extern void FacebookScheduler_Schedule_m618AF75D838CF13B9D89105E8760ECF338F29427 (void);
// 0x000001BD System.Collections.IEnumerator Facebook.Unity.FacebookScheduler::DelayEvent(System.Action,System.Int64)
extern void FacebookScheduler_DelayEvent_m2E4BE1C92A4D3BDB717A8431851E9F53A091742F (void);
// 0x000001BE System.Void Facebook.Unity.FacebookScheduler::.ctor()
extern void FacebookScheduler__ctor_m7A9D976933EA4156ADD5D7554C9B8B9848D538A7 (void);
// 0x000001BF System.Void Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::.ctor(System.Int32)
extern void U3CDelayEventU3Ed__1__ctor_m38E0B6E65329104B15319617C622D64F4404F92C (void);
// 0x000001C0 System.Void Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::System.IDisposable.Dispose()
extern void U3CDelayEventU3Ed__1_System_IDisposable_Dispose_mCA96EF4F77E659BC5E04A98B17FA8F5DF9A628BC (void);
// 0x000001C1 System.Boolean Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::MoveNext()
extern void U3CDelayEventU3Ed__1_MoveNext_m8AD01F44CDFE46644DB388E062AC5A2BA6B889EB (void);
// 0x000001C2 System.Object Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayEventU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66F8B4754D0F53AFB18E324FE98F53B2B7AFB418 (void);
// 0x000001C3 System.Void Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::System.Collections.IEnumerator.Reset()
extern void U3CDelayEventU3Ed__1_System_Collections_IEnumerator_Reset_mCC535E07AE97CD37C62153C1E756FE5FD55FDBE8 (void);
// 0x000001C4 System.Object Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CDelayEventU3Ed__1_System_Collections_IEnumerator_get_Current_mF83DB1DF0B887609F322379D06749009AB0BFE56 (void);
// 0x000001C5 System.Void Facebook.Unity.CodelessIAPAutoLog::handlePurchaseCompleted(System.Object)
extern void CodelessIAPAutoLog_handlePurchaseCompleted_m7BFBFC8B4C7EA38AB4A6B7DB14AA3B1565198C90 (void);
// 0x000001C6 System.Void Facebook.Unity.CodelessIAPAutoLog::addListenerToIAPButtons(System.Object)
extern void CodelessIAPAutoLog_addListenerToIAPButtons_mF998CEAA3FC76F58CA9F55DA4A2129ABF98BE931 (void);
// 0x000001C7 System.Void Facebook.Unity.CodelessIAPAutoLog::addListenerToGameObject(UnityEngine.Object,System.Object)
extern void CodelessIAPAutoLog_addListenerToGameObject_mEE84C26CE19440744D6806976E98AE32A5E450DE (void);
// 0x000001C8 System.Type Facebook.Unity.CodelessIAPAutoLog::FindTypeInAssemblies(System.String,System.String)
extern void CodelessIAPAutoLog_FindTypeInAssemblies_m58DF0E54F78158C075E0886AA90D46798F433945 (void);
// 0x000001C9 UnityEngine.Object[] Facebook.Unity.CodelessIAPAutoLog::FindObjectsOfTypeByName(System.String,System.String)
extern void CodelessIAPAutoLog_FindObjectsOfTypeByName_mF943C5AEAC7E1610028544E00002D63A7D2689E0 (void);
// 0x000001CA System.Object Facebook.Unity.CodelessIAPAutoLog::GetField(System.Object,System.String)
extern void CodelessIAPAutoLog_GetField_m2F30B99D73BB5096A60124A9DD9F75990528B9D4 (void);
// 0x000001CB System.Object Facebook.Unity.CodelessIAPAutoLog::GetProperty(System.Object,System.String)
extern void CodelessIAPAutoLog_GetProperty_mE14CD2C5FFA67850F75416B4C0665A1DB68AAD15 (void);
// 0x000001CC System.Void Facebook.Unity.CodelessCrawler::Awake()
extern void CodelessCrawler_Awake_m79E667ACA71DCFCE59C7CC403663C20FE7BB550A (void);
// 0x000001CD System.Void Facebook.Unity.CodelessCrawler::CaptureViewHierarchy(System.String)
extern void CodelessCrawler_CaptureViewHierarchy_m6B4F987BADE0EB6BC0C3ECA1DDC8E0604908A93F (void);
// 0x000001CE System.Collections.IEnumerator Facebook.Unity.CodelessCrawler::GenSnapshot()
extern void CodelessCrawler_GenSnapshot_m117B4518BE22E76B45A71850C0272F0272A440C5 (void);
// 0x000001CF System.Void Facebook.Unity.CodelessCrawler::SendAndroid(System.String)
extern void CodelessCrawler_SendAndroid_m17E9238FD99CCB645F4535AF573DAB72E4DC37C1 (void);
// 0x000001D0 System.Void Facebook.Unity.CodelessCrawler::SendIos(System.String)
extern void CodelessCrawler_SendIos_m850CFDD71622E2E6054817533E5F6FA5C718697A (void);
// 0x000001D1 System.String Facebook.Unity.CodelessCrawler::GenBase64Screenshot()
extern void CodelessCrawler_GenBase64Screenshot_m89669CE2A4967F7436868EF3D09EFCF075EDC4CE (void);
// 0x000001D2 System.String Facebook.Unity.CodelessCrawler::GenViewJson()
extern void CodelessCrawler_GenViewJson_m69C7A401E1659BBF383130167D64BCE1792BCB4C (void);
// 0x000001D3 System.Void Facebook.Unity.CodelessCrawler::GenChild(UnityEngine.GameObject,System.Text.StringBuilder)
extern void CodelessCrawler_GenChild_mEA81BD2454F1E222D1A92A9851AFD4849E7849CE (void);
// 0x000001D4 System.Void Facebook.Unity.CodelessCrawler::onActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void CodelessCrawler_onActiveSceneChanged_m317AE3FD644B3E61D5DA80AA273365E958CB53FF (void);
// 0x000001D5 System.Void Facebook.Unity.CodelessCrawler::updateMainCamera()
extern void CodelessCrawler_updateMainCamera_m8BE3DC22CFB7DE0C9F24293DC580987EE8E024A7 (void);
// 0x000001D6 UnityEngine.Vector2 Facebook.Unity.CodelessCrawler::getScreenCoordinate(UnityEngine.Vector3,UnityEngine.RenderMode)
extern void CodelessCrawler_getScreenCoordinate_mDB8D9B138EC5B37642511256C9E1C4A6B93FDB37 (void);
// 0x000001D7 System.String Facebook.Unity.CodelessCrawler::getClasstypeBitmaskButton()
extern void CodelessCrawler_getClasstypeBitmaskButton_mCF199713431C6260B8BDF3A4B42D4782F2FFA880 (void);
// 0x000001D8 System.String Facebook.Unity.CodelessCrawler::getVisibility(UnityEngine.GameObject)
extern void CodelessCrawler_getVisibility_mB79811B9A8DE402CD51E54B375D8CE22013202EE (void);
// 0x000001D9 System.Void Facebook.Unity.CodelessCrawler::.ctor()
extern void CodelessCrawler__ctor_m1222ABFCEC83334B91A27739101F8B78344E3403 (void);
// 0x000001DA System.Void Facebook.Unity.CodelessCrawler::.cctor()
extern void CodelessCrawler__cctor_mA68B072F4ED51A1B9F3C09027096751928F9CAD3 (void);
// 0x000001DB System.Void Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4::.ctor(System.Int32)
extern void U3CGenSnapshotU3Ed__4__ctor_mC9ECD537808627E62E02D894E4FB96F7E78545F2 (void);
// 0x000001DC System.Void Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4::System.IDisposable.Dispose()
extern void U3CGenSnapshotU3Ed__4_System_IDisposable_Dispose_m80935841B7055D1AF032835FC1BF8DEF0D6867F0 (void);
// 0x000001DD System.Boolean Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4::MoveNext()
extern void U3CGenSnapshotU3Ed__4_MoveNext_m16E0D888991BF7B231BA04C43F44F9780EBCCD4D (void);
// 0x000001DE System.Object Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenSnapshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0C2F1C8A514CA6D3F1E580DD4521E47FF72BB25 (void);
// 0x000001DF System.Void Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4::System.Collections.IEnumerator.Reset()
extern void U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_Reset_m31E52EDA4D8A81C47C4ECED631B7EC48384A7FB2 (void);
// 0x000001E0 System.Object Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_get_Current_m9BB9ADC38539B8FE9900FBCBA85818870B5E2428 (void);
// 0x000001E1 Facebook.Unity.FBSDKEventBindingManager Facebook.Unity.CodelessUIInteractEvent::get_eventBindingManager()
extern void CodelessUIInteractEvent_get_eventBindingManager_m09E20CAC00FEFF3D70AA3A0982A408B0305AD9BB (void);
// 0x000001E2 System.Void Facebook.Unity.CodelessUIInteractEvent::set_eventBindingManager(Facebook.Unity.FBSDKEventBindingManager)
extern void CodelessUIInteractEvent_set_eventBindingManager_mFA2C3761A6E7FF12C7DBD697FFF200459CCDA9A4 (void);
// 0x000001E3 System.Void Facebook.Unity.CodelessUIInteractEvent::Awake()
extern void CodelessUIInteractEvent_Awake_mC7D8CFC60C1B3164D7A858B16215B48634100ABC (void);
// 0x000001E4 System.Void Facebook.Unity.CodelessUIInteractEvent::SetLoggerInitAndroid()
extern void CodelessUIInteractEvent_SetLoggerInitAndroid_mB99B35324357A509641B5FB32AD1BAD363D7194C (void);
// 0x000001E5 System.Void Facebook.Unity.CodelessUIInteractEvent::SetLoggerInitIos()
extern void CodelessUIInteractEvent_SetLoggerInitIos_m869694270A2763A3C106AB554934ADBF89DC7D0F (void);
// 0x000001E6 System.Void Facebook.Unity.CodelessUIInteractEvent::Update()
extern void CodelessUIInteractEvent_Update_m23BB03FA167B66B34927AA6CAE6DBD85966FF75D (void);
// 0x000001E7 System.Void Facebook.Unity.CodelessUIInteractEvent::OnReceiveMapping(System.String)
extern void CodelessUIInteractEvent_OnReceiveMapping_mE4676BA6BA86DB6AA0FC2E3AF721FA77D505C3A8 (void);
// 0x000001E8 System.Void Facebook.Unity.CodelessUIInteractEvent::.ctor()
extern void CodelessUIInteractEvent__ctor_m5F4E625E7C9CD5B9AE3E1670CF8F43F3B51191C5 (void);
// 0x000001E9 System.Boolean Facebook.Unity.FBSDKViewHiearchy::CheckGameObjectMatchPath(UnityEngine.GameObject,System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKViewHiearchy_CheckGameObjectMatchPath_m565090E33BEBD2EA5A1D8823DBB47BB7B19A815A (void);
// 0x000001EA System.Boolean Facebook.Unity.FBSDKViewHiearchy::CheckPathMatchPath(System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>,System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKViewHiearchy_CheckPathMatchPath_m862127A4751D0D5F69DC54099E0D4CA844A345B9 (void);
// 0x000001EB System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKViewHiearchy::GetPath(UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetPath_m135E286F0053F0AD0CE1D60CC49C041281FCF3AB (void);
// 0x000001EC System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKViewHiearchy::GetPath(UnityEngine.GameObject,System.Int32)
extern void FBSDKViewHiearchy_GetPath_m363AB73243E422E012D3917313569CCB86D2E8D1 (void);
// 0x000001ED UnityEngine.GameObject Facebook.Unity.FBSDKViewHiearchy::GetParent(UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetParent_m7ED6F4EFA6D5C600645A34662593385D80C7810F (void);
// 0x000001EE System.Collections.Generic.Dictionary`2<System.String,System.Object> Facebook.Unity.FBSDKViewHiearchy::GetAttribute(UnityEngine.GameObject,UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetAttribute_mEF4401A542B3015E1DAC1AA4C1CAE43BE6E8F125 (void);
// 0x000001EF System.Void Facebook.Unity.FBSDKCodelessPathComponent::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FBSDKCodelessPathComponent__ctor_m67D851D4687D3A2789973A32E6231B20BBECD742 (void);
// 0x000001F0 System.String Facebook.Unity.FBSDKCodelessPathComponent::get_className()
extern void FBSDKCodelessPathComponent_get_className_m54C63AD717ACDC4692ACC6DE94804F877B55DC6E (void);
// 0x000001F1 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_className(System.String)
extern void FBSDKCodelessPathComponent_set_className_m3CD005890C7841B28A175D32951B2CDEB5BE3AEC (void);
// 0x000001F2 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_text(System.String)
extern void FBSDKCodelessPathComponent_set_text_mBA33C1DB8F9456C8071B2199CF5937AD68B48742 (void);
// 0x000001F3 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_hint(System.String)
extern void FBSDKCodelessPathComponent_set_hint_m504833A7DC08EA59526D8E6F37A4FBA8EB23E866 (void);
// 0x000001F4 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_desc(System.String)
extern void FBSDKCodelessPathComponent_set_desc_mECBECABCFA864C2C07BCB75612AF71B9DC374981 (void);
// 0x000001F5 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_tag(System.String)
extern void FBSDKCodelessPathComponent_set_tag_m4712160C1725335A11F346A575FCCCEE2B095F82 (void);
// 0x000001F6 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_index(System.Int64)
extern void FBSDKCodelessPathComponent_set_index_m1F0A3266005D13D0138D95BD9D1277D47BDB0584 (void);
// 0x000001F7 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_section(System.Int64)
extern void FBSDKCodelessPathComponent_set_section_m5C12C24C6D15A68DC696F89881FA07393D4DB5AD (void);
// 0x000001F8 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_row(System.Int64)
extern void FBSDKCodelessPathComponent_set_row_m55AAA375912C9B25A334BE09A078B84F0D75F857 (void);
// 0x000001F9 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_matchBitmask(System.Int64)
extern void FBSDKCodelessPathComponent_set_matchBitmask_mBF4D4DAD0701174DD4657A83807584EC5F16C77C (void);
// 0x000001FA System.Void Facebook.Unity.FBSDKEventBinding::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FBSDKEventBinding__ctor_m9839B6846EE1B62343BB77C5A94559A36BB25604 (void);
// 0x000001FB System.String Facebook.Unity.FBSDKEventBinding::get_eventName()
extern void FBSDKEventBinding_get_eventName_m663422CB21B0DAF91675DE2D6877078B51321884 (void);
// 0x000001FC System.Void Facebook.Unity.FBSDKEventBinding::set_eventName(System.String)
extern void FBSDKEventBinding_set_eventName_m84494EBB6BFFF6C9DC0741E6D10C4040926B476C (void);
// 0x000001FD System.String Facebook.Unity.FBSDKEventBinding::get_eventType()
extern void FBSDKEventBinding_get_eventType_mBB79F9BFE1BE931019D05F072537ED5EFBF522E9 (void);
// 0x000001FE System.Void Facebook.Unity.FBSDKEventBinding::set_eventType(System.String)
extern void FBSDKEventBinding_set_eventType_mE488464891341EC1396E33ECB22F7C33DB1BA9B2 (void);
// 0x000001FF System.String Facebook.Unity.FBSDKEventBinding::get_appVersion()
extern void FBSDKEventBinding_get_appVersion_mBDFA265D6A3311D6D1E18CAC8CFC3130905F45B8 (void);
// 0x00000200 System.Void Facebook.Unity.FBSDKEventBinding::set_appVersion(System.String)
extern void FBSDKEventBinding_set_appVersion_mE6157B6AA510EB7499DE7F53C497A6CB5504CB2D (void);
// 0x00000201 System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKEventBinding::get_path()
extern void FBSDKEventBinding_get_path_mC979F6275E5CFA7F97AB4A63C27FDF4D6F6C89A5 (void);
// 0x00000202 System.Void Facebook.Unity.FBSDKEventBinding::set_path(System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKEventBinding_set_path_m1F283930D25BE4B316173A0595F503AC33B98C61 (void);
// 0x00000203 System.Collections.Generic.List`1<Facebook.Unity.FBSDKEventBinding> Facebook.Unity.FBSDKEventBindingManager::get_eventBindings()
extern void FBSDKEventBindingManager_get_eventBindings_mD03FDCE6DFA0E75C406989DC46F5C75CB653A000 (void);
// 0x00000204 System.Void Facebook.Unity.FBSDKEventBindingManager::set_eventBindings(System.Collections.Generic.List`1<Facebook.Unity.FBSDKEventBinding>)
extern void FBSDKEventBindingManager_set_eventBindings_m27574887FFAC62D99CF0A359B6B691663C7F1249 (void);
// 0x00000205 System.Void Facebook.Unity.FBSDKEventBindingManager::.ctor(System.Collections.Generic.List`1<System.Object>)
extern void FBSDKEventBindingManager__ctor_mBB09B4D710437D1616F3C8890FCBD561CA1BCF60 (void);
// 0x00000206 System.Void Facebook.Unity.MediaUploadResult::.ctor(Facebook.Unity.ResultContainer)
extern void MediaUploadResult__ctor_m682318B1599ABE994FAB654F2CF4E95069A15B14 (void);
// 0x00000207 System.String Facebook.Unity.MediaUploadResult::get_MediaId()
extern void MediaUploadResult_get_MediaId_mB577FF8F14B267504DDE51320F9ECD51EC30CEB6 (void);
// 0x00000208 System.Void Facebook.Unity.MediaUploadResult::set_MediaId(System.String)
extern void MediaUploadResult_set_MediaId_m74AE625BED87E80F418CBB1F34CE134591121CB0 (void);
// 0x00000209 System.String Facebook.Unity.MediaUploadResult::ToString()
extern void MediaUploadResult_ToString_mBAFC619ADF28E3D81E8F1BA90797C646A95E28A8 (void);
// 0x0000020A System.Void Facebook.Unity.SessionScoreResult::.ctor(Facebook.Unity.ResultContainer)
extern void SessionScoreResult__ctor_m5981BD56F2AAF4D85752607BAB658B4A79CEC96C (void);
// 0x0000020B System.Void Facebook.Unity.AuthenticationToken::.ctor(System.String,System.String)
extern void AuthenticationToken__ctor_m582B8C72C50CD0B411857B7570F8AE7A908811B3 (void);
// 0x0000020C System.String Facebook.Unity.AuthenticationToken::get_TokenString()
extern void AuthenticationToken_get_TokenString_m0FF339C87DB816343FB1B683AB499061E98E9749 (void);
// 0x0000020D System.Void Facebook.Unity.AuthenticationToken::set_TokenString(System.String)
extern void AuthenticationToken_set_TokenString_mD671893C5AAAFF41B601435982FA9BC9CAD921C6 (void);
// 0x0000020E System.String Facebook.Unity.AuthenticationToken::get_Nonce()
extern void AuthenticationToken_get_Nonce_m3D2F427FC8C38DFB78B4D8426A47068DC8B66538 (void);
// 0x0000020F System.Void Facebook.Unity.AuthenticationToken::set_Nonce(System.String)
extern void AuthenticationToken_set_Nonce_m9010ECD323D9E5FA397A0092FC63FE3F6DDD2CDB (void);
// 0x00000210 System.String Facebook.Unity.AuthenticationToken::ToString()
extern void AuthenticationToken_ToString_m5D04203FD03220C42D69A9FF7C0D70268EBC4BA6 (void);
// 0x00000211 System.Void Facebook.Unity.Gameroom.GameroomFacebook::.ctor()
extern void GameroomFacebook__ctor_m395C1063B36776E683508D5304802564655AECEA (void);
// 0x00000212 System.Void Facebook.Unity.Gameroom.GameroomFacebook::.ctor(Facebook.Unity.Gameroom.IGameroomWrapper,Facebook.Unity.CallbackManager)
extern void GameroomFacebook__ctor_mEE88A6B713759347370AC9DE8A95A2C46F89E98A (void);
// 0x00000213 System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::get_LimitEventUsage()
extern void GameroomFacebook_get_LimitEventUsage_m983B24C2E6A386AC28B5694C4B7B2AB54EBCC556 (void);
// 0x00000214 System.Void Facebook.Unity.Gameroom.GameroomFacebook::set_LimitEventUsage(System.Boolean)
extern void GameroomFacebook_set_LimitEventUsage_m290ADD7D2C3467390F8F05BC9EBB7E1B07C7B3F8 (void);
// 0x00000215 System.String Facebook.Unity.Gameroom.GameroomFacebook::get_SDKName()
extern void GameroomFacebook_get_SDKName_m75155617153F455C5F10F77B7D7595D26EC92440 (void);
// 0x00000216 System.String Facebook.Unity.Gameroom.GameroomFacebook::get_SDKVersion()
extern void GameroomFacebook_get_SDKVersion_mA236F5B3B26830E98E9CC62FAD00F9FCE9199749 (void);
// 0x00000217 System.Void Facebook.Unity.Gameroom.GameroomFacebook::Init(System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void GameroomFacebook_Init_mF299CD4BBDE79E1B6B203B134EE102E6D6B61D0D (void);
// 0x00000218 System.Void Facebook.Unity.Gameroom.GameroomFacebook::ActivateApp(System.String)
extern void GameroomFacebook_ActivateApp_mCAE26855B3F08C404594CA7F949C76330A359C8D (void);
// 0x00000219 System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void GameroomFacebook_AppEventsLogEvent_m2C48316A206932AE71CF99D6C82BE6DB6698A339 (void);
// 0x0000021A System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void GameroomFacebook_AppEventsLogPurchase_m0785A1AB79989706B17CC44B09BA3E14560C3C87 (void);
// 0x0000021B System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void GameroomFacebook_AppRequest_mECCE93E426EC641229CAEAB2F795B54B3210E451 (void);
// 0x0000021C System.Void Facebook.Unity.Gameroom.GameroomFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void GameroomFacebook_FeedShare_m4ECE4E08B3D267B2B88A33EF7D10B2CA1EB85C25 (void);
// 0x0000021D System.Void Facebook.Unity.Gameroom.GameroomFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void GameroomFacebook_ShareLink_m15C80AF88399C4E3D7FC475F01632BEBFA14BEA8 (void);
// 0x0000021E System.Void Facebook.Unity.Gameroom.GameroomFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void GameroomFacebook_Pay_m7197B0E337E0A110D9436C86D69BF189D0BF4461 (void);
// 0x0000021F System.Void Facebook.Unity.Gameroom.GameroomFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void GameroomFacebook_GetAppLink_mD88E333019C913E87ED7A7D5453A0E3B9B93A405 (void);
// 0x00000220 System.Void Facebook.Unity.Gameroom.GameroomFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LogInWithPublishPermissions_mDA3567F57F869A946BF6D5ED644E57048BB9FC3B (void);
// 0x00000221 System.Void Facebook.Unity.Gameroom.GameroomFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LogInWithReadPermissions_m1554C36F994862C5A1A9EC385F4FC9C75BD0FC07 (void);
// 0x00000222 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnAppRequestsComplete_mD901A737B47B266D5C34B313DF5EDA4D1156CF80 (void);
// 0x00000223 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnGetAppLinkComplete_m3EABE4875EE59194FB16ECCDBDE2EB2CA04979ED (void);
// 0x00000224 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnLoginComplete_m228ABADFAA8B392EFD7C3A793B519142A8BBB5D7 (void);
// 0x00000225 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnShareLinkComplete_m66AAEF0681DB726C57037A577492DF44B265E987 (void);
// 0x00000226 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnPayComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnPayComplete_m69AE7C644152916123B0B1A5ED69EC3ED94749F9 (void);
// 0x00000227 System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::HaveReceivedPipeResponse()
extern void GameroomFacebook_HaveReceivedPipeResponse_mD73834BCEA3A7FBB49592B6A28C9A0C61CC112AD (void);
// 0x00000228 System.String Facebook.Unity.Gameroom.GameroomFacebook::GetPipeResponse(System.String)
extern void GameroomFacebook_GetPipeResponse_mE146FCD5F6A63E588C1BBE407CC4E3BC5249EB1D (void);
// 0x00000229 Facebook.Unity.Gameroom.IGameroomWrapper Facebook.Unity.Gameroom.GameroomFacebook::GetGameroomWrapper()
extern void GameroomFacebook_GetGameroomWrapper_m1DCCC3CBD2EFD09A89D345F5A36A003A113D75EA (void);
// 0x0000022A System.Void Facebook.Unity.Gameroom.GameroomFacebook::PayImpl(System.String,System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void GameroomFacebook_PayImpl_m9D915F7DA5551C3ABE0B771F33179F6702B9A440 (void);
// 0x0000022B System.Void Facebook.Unity.Gameroom.GameroomFacebook::LoginWithPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LoginWithPermissions_m6F1EEC2B916850BC3571DF437812BB61DF3E8CA8 (void);
// 0x0000022C System.Void Facebook.Unity.Gameroom.GameroomFacebook/OnComplete::.ctor(System.Object,System.IntPtr)
extern void OnComplete__ctor_m592C850930D3FB8B8E60074CAFCCFAF39133422D (void);
// 0x0000022D System.Void Facebook.Unity.Gameroom.GameroomFacebook/OnComplete::Invoke(Facebook.Unity.ResultContainer)
extern void OnComplete_Invoke_m7B870E9DEB6798DE433A37102B2F65737DCD9AEB (void);
// 0x0000022E System.IAsyncResult Facebook.Unity.Gameroom.GameroomFacebook/OnComplete::BeginInvoke(Facebook.Unity.ResultContainer,System.AsyncCallback,System.Object)
extern void OnComplete_BeginInvoke_m2F9D8949A8658B37123DDD5405BF08C822688DF7 (void);
// 0x0000022F System.Void Facebook.Unity.Gameroom.GameroomFacebook/OnComplete::EndInvoke(System.IAsyncResult)
extern void OnComplete_EndInvoke_m2BC26756B4FED6B0E54DE0ED5697EFA3CD7B0EAD (void);
// 0x00000230 Facebook.Unity.Gameroom.IGameroomFacebookImplementation Facebook.Unity.Gameroom.GameroomFacebookGameObject::get_GameroomFacebookImpl()
extern void GameroomFacebookGameObject_get_GameroomFacebookImpl_m7588AFEAE6F993114A0FC8985F3E9C35D1C7820B (void);
// 0x00000231 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::WaitForResponse(Facebook.Unity.Gameroom.GameroomFacebook/OnComplete,System.String)
extern void GameroomFacebookGameObject_WaitForResponse_m1BF6EBCCD0ADF829882E9DBE9881EFAD6D84CE6B (void);
// 0x00000232 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::OnAwake()
extern void GameroomFacebookGameObject_OnAwake_mB0FB2026E6A6D7664A0572C1467468E1E1848A5A (void);
// 0x00000233 System.Collections.IEnumerator Facebook.Unity.Gameroom.GameroomFacebookGameObject::WaitForPipeResponse(Facebook.Unity.Gameroom.GameroomFacebook/OnComplete,System.String)
extern void GameroomFacebookGameObject_WaitForPipeResponse_m60E36429F3ECCEA9D2A0595114F4705BD7171F72 (void);
// 0x00000234 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::.ctor()
extern void GameroomFacebookGameObject__ctor_m818C3C48493B7D02E738B4143AD9D5FD61E1220F (void);
// 0x00000235 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::.ctor(System.Int32)
extern void U3CWaitForPipeResponseU3Ed__4__ctor_mB6833EEC4A53FC762AE059C6BF00332A93A417D1 (void);
// 0x00000236 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::System.IDisposable.Dispose()
extern void U3CWaitForPipeResponseU3Ed__4_System_IDisposable_Dispose_mE78F1C9BAA388D0BC289EEBDF468E596E2D2F82F (void);
// 0x00000237 System.Boolean Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::MoveNext()
extern void U3CWaitForPipeResponseU3Ed__4_MoveNext_m78B590361D10E8B1DC95049836A0A0DE022D6A44 (void);
// 0x00000238 System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73EC4ACF1CB79293E941E42E754B5134478B3880 (void);
// 0x00000239 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_Reset_m3E3A3AB0086839F189D4F6BACB48B6C87E4E62B2 (void);
// 0x0000023A System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_get_Current_m3B4DCFA7715371F1A514BCB9A973661D05E977B1 (void);
// 0x0000023B Facebook.Unity.FacebookGameObject Facebook.Unity.Gameroom.GameroomFacebookLoader::get_FBGameObject()
extern void GameroomFacebookLoader_get_FBGameObject_mE8D0F8C640453C4C17857611C0C9C77A03C6D416 (void);
// 0x0000023C System.Void Facebook.Unity.Gameroom.GameroomFacebookLoader::.ctor()
extern void GameroomFacebookLoader__ctor_m5D25161FD90B80E35E65D7CD39F8A7E0F2CEE1A7 (void);
// 0x0000023D System.Boolean Facebook.Unity.Gameroom.IGameroomFacebookImplementation::HaveReceivedPipeResponse()
// 0x0000023E System.String Facebook.Unity.Gameroom.IGameroomFacebookImplementation::GetPipeResponse(System.String)
// 0x0000023F System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.Gameroom.IGameroomWrapper::get_PipeResponse()
// 0x00000240 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::set_PipeResponse(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
// 0x00000241 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::Init(Facebook.Unity.Gameroom.GameroomFacebook/OnComplete)
// 0x00000242 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoLoginRequest(System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook/OnComplete)
// 0x00000243 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoPayRequest(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook/OnComplete)
// 0x00000244 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoFeedShareRequest(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook/OnComplete)
// 0x00000245 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoAppRequestRequest(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook/OnComplete)
// 0x00000246 System.Void Facebook.Unity.Editor.EditorFacebook::.ctor(Facebook.Unity.Editor.IEditorWrapper,Facebook.Unity.CallbackManager)
extern void EditorFacebook__ctor_mC895ECBF5B4D00AC57B7BC4E823FBD35D666FA47 (void);
// 0x00000247 System.Void Facebook.Unity.Editor.EditorFacebook::.ctor()
extern void EditorFacebook__ctor_m2FE82AAB9C00188B8C55713F1052774F0E77B893 (void);
// 0x00000248 System.Boolean Facebook.Unity.Editor.EditorFacebook::get_LimitEventUsage()
extern void EditorFacebook_get_LimitEventUsage_mE539027EAEE9B6EAEF31BA24D1B6A2A69D8F937F (void);
// 0x00000249 System.Void Facebook.Unity.Editor.EditorFacebook::set_LimitEventUsage(System.Boolean)
extern void EditorFacebook_set_LimitEventUsage_mA0FB2C32D5CBEACBF2237C08D140FA71EE18AE98 (void);
// 0x0000024A System.Void Facebook.Unity.Editor.EditorFacebook::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void EditorFacebook_set_ShareDialogMode_mFE8F73284D51D0505B5D18595F77A5FB5F7A803E (void);
// 0x0000024B System.String Facebook.Unity.Editor.EditorFacebook::get_SDKName()
extern void EditorFacebook_get_SDKName_mAEEE49D0CCF640723AE5D21E2B9A0C38F307C85D (void);
// 0x0000024C System.String Facebook.Unity.Editor.EditorFacebook::get_SDKVersion()
extern void EditorFacebook_get_SDKVersion_mC3E3D2A980ADA6817304A2E018357CE0D9088D41 (void);
// 0x0000024D Facebook.Unity.IFacebookCallbackHandler Facebook.Unity.Editor.EditorFacebook::get_EditorGameObject()
extern void EditorFacebook_get_EditorGameObject_mA58F715E4A6CAED7BAD7D6855FD01346A734F0AB (void);
// 0x0000024E System.Void Facebook.Unity.Editor.EditorFacebook::Init(Facebook.Unity.InitDelegate)
extern void EditorFacebook_Init_mB2976CA743D62F2A18A0F616DFF3F060278CD8B6 (void);
// 0x0000024F System.Void Facebook.Unity.Editor.EditorFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void EditorFacebook_LogInWithReadPermissions_mDDCE9AC38C7F6A48DEF4A6BBC9F05C013822F6F4 (void);
// 0x00000250 System.Void Facebook.Unity.Editor.EditorFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void EditorFacebook_LogInWithPublishPermissions_m4FB1364B65BA5E531D0F5C780040AD5E720E4A11 (void);
// 0x00000251 System.Void Facebook.Unity.Editor.EditorFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void EditorFacebook_AppRequest_m46F273516AB3B87DD26016C151DBB06168ABDD1C (void);
// 0x00000252 System.Void Facebook.Unity.Editor.EditorFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void EditorFacebook_ShareLink_m925FCAA68F89FF7150377BD1791AB2EFE34E33B3 (void);
// 0x00000253 System.Void Facebook.Unity.Editor.EditorFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void EditorFacebook_FeedShare_m1CCD4CB562EEF11BA2CFF338C7744C35586ABC08 (void);
// 0x00000254 System.Void Facebook.Unity.Editor.EditorFacebook::ActivateApp(System.String)
extern void EditorFacebook_ActivateApp_mD59EF3CC68C02C05258CB29F9D659D6C79EBD5A3 (void);
// 0x00000255 System.Void Facebook.Unity.Editor.EditorFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void EditorFacebook_GetAppLink_mEF8F98C659A1990D5F6B5C395566F33E35E98BE3 (void);
// 0x00000256 System.Void Facebook.Unity.Editor.EditorFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void EditorFacebook_AppEventsLogEvent_mBB14C3701038A0BCDB8283201AE7B84FA8832853 (void);
// 0x00000257 System.Void Facebook.Unity.Editor.EditorFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void EditorFacebook_AppEventsLogPurchase_mDFFACBB5F5F1D8E6F449E219342B9EE8E968FD58 (void);
// 0x00000258 System.Boolean Facebook.Unity.Editor.EditorFacebook::IsImplicitPurchaseLoggingEnabled()
extern void EditorFacebook_IsImplicitPurchaseLoggingEnabled_m690B471A2E0F8F5DF275EF0C57800A789A303343 (void);
// 0x00000259 System.Void Facebook.Unity.Editor.EditorFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void EditorFacebook_FetchDeferredAppLink_mA5CFF5DDE754C6701E22703AE53E2D973D82C203 (void);
// 0x0000025A System.Void Facebook.Unity.Editor.EditorFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void EditorFacebook_Pay_mC40C763950E7687983A2EBDCC28782A6DC88B25D (void);
// 0x0000025B System.Void Facebook.Unity.Editor.EditorFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern void EditorFacebook_RefreshCurrentAccessToken_m460E7A0F63DE4395F614B476E4B9B9F9D46FDDB9 (void);
// 0x0000025C System.Void Facebook.Unity.Editor.EditorFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnAppRequestsComplete_m78B4742D83B8BBA5BF9697E273CC6D4AE5BD7A43 (void);
// 0x0000025D System.Void Facebook.Unity.Editor.EditorFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnGetAppLinkComplete_mBEBC20C5673274711DDBA664DCF5F9A3E8A18C58 (void);
// 0x0000025E System.Void Facebook.Unity.Editor.EditorFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnLoginComplete_mD4CA8F12364E752EE54627CE82E28377963862C9 (void);
// 0x0000025F System.Void Facebook.Unity.Editor.EditorFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnShareLinkComplete_mC3843A3C0EA1273F35B36FEA6A8C71D337F255CC (void);
// 0x00000260 System.Void Facebook.Unity.Editor.EditorFacebook::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnFetchDeferredAppLinkComplete_m25C10BAC2166C3C67284EAD2DC77827C0271DB04 (void);
// 0x00000261 System.Void Facebook.Unity.Editor.EditorFacebook::OnPayComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnPayComplete_mF25D6608769632C843AB30BEE384EA2AA3EA9877 (void);
// 0x00000262 System.Void Facebook.Unity.Editor.EditorFacebook::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnRefreshCurrentAccessTokenComplete_m497E48E473600186DA0523CEBD925C78935E8DDA (void);
// 0x00000263 System.Void Facebook.Unity.Editor.EditorFacebook::OnFriendFinderComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnFriendFinderComplete_m0CA10896F756D9F0BFF51F8ACFFDDF8534B508EC (void);
// 0x00000264 System.Void Facebook.Unity.Editor.EditorFacebook::OnUploadImageToMediaLibraryComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnUploadImageToMediaLibraryComplete_mF1C4CC30008ED30EE4308AD1D74AEC6CDB044F9D (void);
// 0x00000265 System.Void Facebook.Unity.Editor.EditorFacebook::OnUploadVideoToMediaLibraryComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnUploadVideoToMediaLibraryComplete_mB806E7A01CDD452B1723FBF094B35E08F21BDEF3 (void);
// 0x00000266 System.Void Facebook.Unity.Editor.EditorFacebook::OnOnIAPReadyComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnOnIAPReadyComplete_m892842F21427BFAD91FC82DC32DF7948F5E0445E (void);
// 0x00000267 System.Void Facebook.Unity.Editor.EditorFacebook::OnGetCatalogComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnGetCatalogComplete_mBF069DFAD7556FCAC5A12746DABD9C4C9FFF645F (void);
// 0x00000268 System.Void Facebook.Unity.Editor.EditorFacebook::OnGetPurchasesComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnGetPurchasesComplete_mF6F163D55A69D0B302D9DEBA9DC1EE9B357385F8 (void);
// 0x00000269 System.Void Facebook.Unity.Editor.EditorFacebook::OnPurchaseComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnPurchaseComplete_mFC9B13A3243ABDBC626484CA7793608AAF49DC75 (void);
// 0x0000026A System.Void Facebook.Unity.Editor.EditorFacebook::OnConsumePurchaseComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnConsumePurchaseComplete_m3CD40A4162B0F70D011D6F6B978D9DECACD94888 (void);
// 0x0000026B System.Void Facebook.Unity.Editor.EditorFacebook::OnInitCloudGameComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnInitCloudGameComplete_m932D432B306395B3ACB4644A266336BB5B745379 (void);
// 0x0000026C System.Void Facebook.Unity.Editor.EditorFacebook::OnScheduleAppToUserNotificationComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnScheduleAppToUserNotificationComplete_m90EDD41D3455590EE9B8B779736FAC3CA2D5B3CF (void);
// 0x0000026D System.Void Facebook.Unity.Editor.EditorFacebook::OnLoadInterstitialAdComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnLoadInterstitialAdComplete_m8DED1A00390D7BB9625759531A3B90091841A567 (void);
// 0x0000026E System.Void Facebook.Unity.Editor.EditorFacebook::OnShowInterstitialAdComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnShowInterstitialAdComplete_mD42DF902FAF16BE11480F8797B86C2311B55378F (void);
// 0x0000026F System.Void Facebook.Unity.Editor.EditorFacebook::OnLoadRewardedVideoComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnLoadRewardedVideoComplete_m994AF993F3198EC1EC4314AC0607B958165F84F1 (void);
// 0x00000270 System.Void Facebook.Unity.Editor.EditorFacebook::OnShowRewardedVideoComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnShowRewardedVideoComplete_m19BD197620A16A77ADABB3D8FFA966F9A16EA2AA (void);
// 0x00000271 System.Void Facebook.Unity.Editor.EditorFacebook::OnGetPayloadComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnGetPayloadComplete_m1B586208132A365A51944ED8C19155A225BB30F1 (void);
// 0x00000272 System.Void Facebook.Unity.Editor.EditorFacebook::OnPostSessionScoreComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnPostSessionScoreComplete_m1A9D7A012296D45376AE8B956DCEFC4027D0DF21 (void);
// 0x00000273 System.Void Facebook.Unity.Editor.EditorFacebook::OnOpenAppStoreComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnOpenAppStoreComplete_m293C9F26F6800278DE9190D1C31CB3F5080028CA (void);
// 0x00000274 System.Void Facebook.Unity.Editor.EditorFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void EditorFacebook_OpenFriendFinderDialog_m06223420B105EE32F2F451DECC1E01B921CD385B (void);
// 0x00000275 System.Void Facebook.Unity.Editor.EditorFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void EditorFacebook_UploadImageToMediaLibrary_m45D940444AAB90B1587153E7A2B2EC78D3B72633 (void);
// 0x00000276 System.Void Facebook.Unity.Editor.EditorFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void EditorFacebook_UploadVideoToMediaLibrary_mF89A565B5674CC6582A8DEA98CBF2E8014444F19 (void);
// 0x00000277 System.Void Facebook.Unity.Editor.EditorFacebook::OnIAPReady(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IIAPReadyResult>)
extern void EditorFacebook_OnIAPReady_m296CACA177A58EF5A41D4A93748D229AFBBDF2E8 (void);
// 0x00000278 System.Void Facebook.Unity.Editor.EditorFacebook::GetCatalog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ICatalogResult>)
extern void EditorFacebook_GetCatalog_m5F4C793CAE2EBA62D09684D95AEE512A19AA4026 (void);
// 0x00000279 System.Void Facebook.Unity.Editor.EditorFacebook::GetPurchases(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchasesResult>)
extern void EditorFacebook_GetPurchases_mA89DEEA13BBBFABCF97196DBB98A08E961C4B6AF (void);
// 0x0000027A System.Void Facebook.Unity.Editor.EditorFacebook::Purchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchaseResult>,System.String)
extern void EditorFacebook_Purchase_m27DF85A8128C05E8032EC3AC17C487A570A4528F (void);
// 0x0000027B System.Void Facebook.Unity.Editor.EditorFacebook::ConsumePurchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IConsumePurchaseResult>)
extern void EditorFacebook_ConsumePurchase_mFE8928DED9834E0D8BAE1E792B045FCCB66F3AB8 (void);
// 0x0000027C System.Void Facebook.Unity.Editor.EditorFacebook::InitCloudGame(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInitCloudGameResult>)
extern void EditorFacebook_InitCloudGame_mC9B99D3470B22DE1AC7FBA7EE00E9821FC83463C (void);
// 0x0000027D System.Void Facebook.Unity.Editor.EditorFacebook::ScheduleAppToUserNotification(System.String,System.String,System.Uri,System.Int32,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IScheduleAppToUserNotificationResult>)
extern void EditorFacebook_ScheduleAppToUserNotification_m390D70AE2CE6F3C3AE9BF7F3C8F26A0853DDB7FF (void);
// 0x0000027E System.Void Facebook.Unity.Editor.EditorFacebook::LoadInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
extern void EditorFacebook_LoadInterstitialAd_m770301BC0B48D39A8B9FB9FB00840EE06DE16B0D (void);
// 0x0000027F System.Void Facebook.Unity.Editor.EditorFacebook::ShowInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
extern void EditorFacebook_ShowInterstitialAd_mDD1DE90B5601FBE5022E50D27C74E630A60642C7 (void);
// 0x00000280 System.Void Facebook.Unity.Editor.EditorFacebook::LoadRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
extern void EditorFacebook_LoadRewardedVideo_m5EAA7A6CE71F3B4F2A74CBE93FF03EFB05667C59 (void);
// 0x00000281 System.Void Facebook.Unity.Editor.EditorFacebook::ShowRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
extern void EditorFacebook_ShowRewardedVideo_mBAB7ACCE57765DAD54C4B707B0729BA76EAE6183 (void);
// 0x00000282 System.Void Facebook.Unity.Editor.EditorFacebook::GetPayload(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayloadResult>)
extern void EditorFacebook_GetPayload_mB98598B925C5FC36A0D901B43C3FE13896E8F46D (void);
// 0x00000283 System.Void Facebook.Unity.Editor.EditorFacebook::PostSessionScore(System.Int32,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ISessionScoreResult>)
extern void EditorFacebook_PostSessionScore_m161BD8C601949C06523C2E9653D45284114E502A (void);
// 0x00000284 System.Void Facebook.Unity.Editor.EditorFacebook::OpenAppStore(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IOpenAppStoreResult>)
extern void EditorFacebook_OpenAppStore_mF1E6E16E07CD36E53D695ED18898D2FC1A41C220 (void);
// 0x00000285 System.Void Facebook.Unity.Editor.EditorFacebook::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnFacebookAuthResponseChange_m57A9AADC351896646838E876258F2456857103CA (void);
// 0x00000286 System.Void Facebook.Unity.Editor.EditorFacebook::OnUrlResponse(System.String)
extern void EditorFacebook_OnUrlResponse_mD55F513A73B5E00228FE1D8D704A00E8212AAC6F (void);
// 0x00000287 System.Void Facebook.Unity.Editor.EditorFacebook::OnHideUnity(System.Boolean)
extern void EditorFacebook_OnHideUnity_mE534BA2EBCADD2D90760005A8D454F1436473313 (void);
// 0x00000288 System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnAwake()
extern void EditorFacebookGameObject_OnAwake_m2BF5E0B070B9D35884DB89BFA6474EEA8226B69C (void);
// 0x00000289 System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnEnable()
extern void EditorFacebookGameObject_OnEnable_mB17830D27B22100CB8C1D044743785B0E149236A (void);
// 0x0000028A System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void EditorFacebookGameObject_OnSceneLoaded_m09088F002D47A461FFF8FB25EE8D8853B708150F (void);
// 0x0000028B System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnDisable()
extern void EditorFacebookGameObject_OnDisable_m3F661E00E0ADF66DB2C1E27D8ECFECB1493C2D1A (void);
// 0x0000028C System.Void Facebook.Unity.Editor.EditorFacebookGameObject::onPurchaseCompleteHandler(System.Object)
extern void EditorFacebookGameObject_onPurchaseCompleteHandler_mEB4ADD0AA0BB95CD554F6A3A2A0A37E675B29458 (void);
// 0x0000028D System.Void Facebook.Unity.Editor.EditorFacebookGameObject::.ctor()
extern void EditorFacebookGameObject__ctor_mD5293F581AADCE22CF77F4A19C779C298EB0F6E0 (void);
// 0x0000028E Facebook.Unity.FacebookGameObject Facebook.Unity.Editor.EditorFacebookLoader::get_FBGameObject()
extern void EditorFacebookLoader_get_FBGameObject_m98D9ECB51D29D7EC21E097416938F95B9D891C12 (void);
// 0x0000028F System.Void Facebook.Unity.Editor.EditorFacebookLoader::.ctor()
extern void EditorFacebookLoader__ctor_m892AF329E3C043EA49799F2AF7F1D65DE83947C5 (void);
// 0x00000290 Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Editor.EditorFacebookMockDialog::get_Callback()
extern void EditorFacebookMockDialog_get_Callback_mB980E4C2DE7886FE9AE6A473BD25FB8F09EC593A (void);
// 0x00000291 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_Callback(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>)
extern void EditorFacebookMockDialog_set_Callback_m79EDB0181B2B739751E3F32E8B39063260115BA5 (void);
// 0x00000292 System.String Facebook.Unity.Editor.EditorFacebookMockDialog::get_CallbackID()
extern void EditorFacebookMockDialog_get_CallbackID_m2F061BD27D5E62C9042B4D8FF694157E3BCC7698 (void);
// 0x00000293 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_CallbackID(System.String)
extern void EditorFacebookMockDialog_set_CallbackID_m56FE30D393DFCF716A4E8F8FCC649B23682DB85B (void);
// 0x00000294 System.String Facebook.Unity.Editor.EditorFacebookMockDialog::get_DialogTitle()
// 0x00000295 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::Start()
extern void EditorFacebookMockDialog_Start_m369BC020C1217375866F568D83C79DDFBE06D9DF (void);
// 0x00000296 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUI()
extern void EditorFacebookMockDialog_OnGUI_m6C7CCC61196CF8FB64056DAA1970338D34FF3EE8 (void);
// 0x00000297 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::DoGui()
// 0x00000298 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendSuccessResult()
// 0x00000299 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendCancelResult()
extern void EditorFacebookMockDialog_SendCancelResult_mEBF09B15C0D9B89F73670A1FEB84F6B45B54EF33 (void);
// 0x0000029A System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendErrorResult(System.String)
extern void EditorFacebookMockDialog_SendErrorResult_m49E2BACBF8EF8A6A0EB5B918801E02D7BD7BDE3E (void);
// 0x0000029B System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUIDialog(System.Int32)
extern void EditorFacebookMockDialog_OnGUIDialog_m06B3E9378723D879EBE811403EAB1CD8CB5FBB67 (void);
// 0x0000029C System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::.ctor()
extern void EditorFacebookMockDialog__ctor_m8A46CB5EC10FC2A5C52CC50876C54233BDC1DCD6 (void);
// 0x0000029D System.Void Facebook.Unity.Editor.EditorWrapper::.ctor(Facebook.Unity.IFacebookCallbackHandler)
extern void EditorWrapper__ctor_m28D73B3F264605D24C6AB46013591850382B5AE3 (void);
// 0x0000029E System.Void Facebook.Unity.Editor.EditorWrapper::Init()
extern void EditorWrapper_Init_m86F33B25805654D4B06F2CFE918CB6D208B240FD (void);
// 0x0000029F System.Void Facebook.Unity.Editor.EditorWrapper::ShowLoginMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowLoginMockDialog_m087DC20EA3284A62E4D49331AC83B86A66976C2F (void);
// 0x000002A0 System.Void Facebook.Unity.Editor.EditorWrapper::ShowAppRequestMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern void EditorWrapper_ShowAppRequestMockDialog_mA8E1889FFEE5CD1E7BBED8A2A6871980E7FE2F38 (void);
// 0x000002A1 System.Void Facebook.Unity.Editor.EditorWrapper::ShowPayMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern void EditorWrapper_ShowPayMockDialog_m139D15090785678CE4EDB901CD5068A3AFC7E1C6 (void);
// 0x000002A2 System.Void Facebook.Unity.Editor.EditorWrapper::ShowMockShareDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowMockShareDialog_m7938F185A0910D46FE40CC274D3D580730789F4F (void);
// 0x000002A3 System.Void Facebook.Unity.Editor.EditorWrapper::ShowMockFriendFinderDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowMockFriendFinderDialog_m31A7DC713AB387CA7AB83DB10FB0D299F0275E64 (void);
// 0x000002A4 System.Void Facebook.Unity.Editor.EditorWrapper::ShowEmptyMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowEmptyMockDialog_m792E4B91828419122F29B8C3C27F7E0D40E7CE34 (void);
// 0x000002A5 System.Void Facebook.Unity.Editor.IEditorWrapper::Init()
// 0x000002A6 System.Void Facebook.Unity.Editor.IEditorWrapper::ShowLoginMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
// 0x000002A7 System.Void Facebook.Unity.Editor.IEditorWrapper::ShowAppRequestMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
// 0x000002A8 System.Void Facebook.Unity.Editor.IEditorWrapper::ShowPayMockDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String)
// 0x000002A9 System.Void Facebook.Unity.Editor.IEditorWrapper::ShowMockShareDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
// 0x000002AA System.Void Facebook.Unity.Editor.IEditorWrapper::ShowMockFriendFinderDialog(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
// 0x000002AB System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::get_EmptyDialogTitle()
extern void EmptyMockDialog_get_EmptyDialogTitle_mBED1FE8DF023CBE518BD3B3FF8175EFB8E73B917 (void);
// 0x000002AC System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::set_EmptyDialogTitle(System.String)
extern void EmptyMockDialog_set_EmptyDialogTitle_mFC70B0CB05F99728777D5B1160C9EA23692CC7CB (void);
// 0x000002AD System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::get_DialogTitle()
extern void EmptyMockDialog_get_DialogTitle_m8D4CE5B93823A10FD24392DA2EF87F03582D92C8 (void);
// 0x000002AE System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::DoGui()
extern void EmptyMockDialog_DoGui_mDF02D9CBAC325FC4351F84FA49170362EEF8EB4F (void);
// 0x000002AF System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::SendSuccessResult()
extern void EmptyMockDialog_SendSuccessResult_m74F914A713B334A1AAAE7013FB91CB6A8D30A7EC (void);
// 0x000002B0 System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::.ctor()
extern void EmptyMockDialog__ctor_m22D56D1D778B280D1469EC7D6D8F230130808F3B (void);
// 0x000002B1 System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::get_DialogTitle()
extern void MockLoginDialog_get_DialogTitle_mC4724DC7C102E31F073295F0AFA061F962C3CE6C (void);
// 0x000002B2 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::DoGui()
extern void MockLoginDialog_DoGui_m7E0CFA8B5C29A44E4E805ACA738A9FAC2C7F8DFB (void);
// 0x000002B3 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::SendSuccessResult()
extern void MockLoginDialog_SendSuccessResult_m6C272B967106871686CA2EB795489A1981373C59 (void);
// 0x000002B4 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::.ctor()
extern void MockLoginDialog__ctor_m5A78B4F4A738966F4F21ADF1408FE945B608F220 (void);
// 0x000002B5 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m334657C5725829CE27CC4ED1C0F37D701EF503FE (void);
// 0x000002B6 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_0::<SendSuccessResult>b__0(Facebook.Unity.IGraphResult)
extern void U3CU3Ec__DisplayClass4_0_U3CSendSuccessResultU3Eb__0_m4548AD0B00D23DC7920E53BD24D0962B8E0C7BB8 (void);
// 0x000002B7 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_m933B6982D96CBFE524A0CE9C56A0E48E9724850F (void);
// 0x000002B8 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_1::<SendSuccessResult>b__1(Facebook.Unity.IGraphResult)
extern void U3CU3Ec__DisplayClass4_1_U3CSendSuccessResultU3Eb__1_m3A3F9E05E2DE31629E6D77CAF9E9421D9CC054D7 (void);
// 0x000002B9 System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::get_SubTitle()
extern void MockShareDialog_get_SubTitle_mB9F0FB095CD3B3C5933B84E627CDF5D9EF7F4CAD (void);
// 0x000002BA System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::set_SubTitle(System.String)
extern void MockShareDialog_set_SubTitle_mB4D39F272D2A770A4D657B1B39B0FE5BA6A8431B (void);
// 0x000002BB System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::get_DialogTitle()
extern void MockShareDialog_get_DialogTitle_m14F7665EE1C32E00D998384DEDC1C9F5E1E3A601 (void);
// 0x000002BC System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::DoGui()
extern void MockShareDialog_DoGui_m77A4240FB9B63D73F448649802198EDB3966F0EE (void);
// 0x000002BD System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::SendSuccessResult()
extern void MockShareDialog_SendSuccessResult_m9FC3E55AA2A436E613FF8131D0AFADF51E85B53B (void);
// 0x000002BE System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::SendCancelResult()
extern void MockShareDialog_SendCancelResult_m084AA0EFA215A51C7D79A07FEE1FDBC4F02C5AB7 (void);
// 0x000002BF System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::GenerateFakePostID()
extern void MockShareDialog_GenerateFakePostID_mABC6DC88EB31173960710BD2A57B61001BA42680 (void);
// 0x000002C0 System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::.ctor()
extern void MockShareDialog__ctor_mAD8420899B316E8E94001CF617ED334D4DDB0898 (void);
// 0x000002C1 System.Void Facebook.Unity.Mobile.IMobileFacebook::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
// 0x000002C2 System.Void Facebook.Unity.Mobile.IMobileFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x000002C3 System.Void Facebook.Unity.Mobile.IMobileFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
// 0x000002C4 System.Boolean Facebook.Unity.Mobile.IMobileFacebook::IsImplicitPurchaseLoggingEnabled()
// 0x000002C5 System.Void Facebook.Unity.Mobile.IMobileFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
// 0x000002C6 System.Void Facebook.Unity.Mobile.IMobileFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
// 0x000002C7 System.Void Facebook.Unity.Mobile.IMobileFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
// 0x000002C8 System.Void Facebook.Unity.Mobile.IMobileFacebook::OnIAPReady(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IIAPReadyResult>)
// 0x000002C9 System.Void Facebook.Unity.Mobile.IMobileFacebook::GetCatalog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ICatalogResult>)
// 0x000002CA System.Void Facebook.Unity.Mobile.IMobileFacebook::GetPurchases(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchasesResult>)
// 0x000002CB System.Void Facebook.Unity.Mobile.IMobileFacebook::Purchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchaseResult>,System.String)
// 0x000002CC System.Void Facebook.Unity.Mobile.IMobileFacebook::ConsumePurchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IConsumePurchaseResult>)
// 0x000002CD System.Void Facebook.Unity.Mobile.IMobileFacebook::InitCloudGame(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInitCloudGameResult>)
// 0x000002CE System.Void Facebook.Unity.Mobile.IMobileFacebook::ScheduleAppToUserNotification(System.String,System.String,System.Uri,System.Int32,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IScheduleAppToUserNotificationResult>)
// 0x000002CF System.Void Facebook.Unity.Mobile.IMobileFacebook::LoadInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
// 0x000002D0 System.Void Facebook.Unity.Mobile.IMobileFacebook::ShowInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
// 0x000002D1 System.Void Facebook.Unity.Mobile.IMobileFacebook::LoadRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
// 0x000002D2 System.Void Facebook.Unity.Mobile.IMobileFacebook::ShowRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
// 0x000002D3 System.Void Facebook.Unity.Mobile.IMobileFacebook::GetPayload(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayloadResult>)
// 0x000002D4 System.Void Facebook.Unity.Mobile.IMobileFacebook::PostSessionScore(System.Int32,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ISessionScoreResult>)
// 0x000002D5 System.Void Facebook.Unity.Mobile.IMobileFacebook::OpenAppStore(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IOpenAppStoreResult>)
// 0x000002D6 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x000002D7 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
// 0x000002D8 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnFriendFinderComplete(Facebook.Unity.ResultContainer)
// 0x000002D9 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnUploadImageToMediaLibraryComplete(Facebook.Unity.ResultContainer)
// 0x000002DA System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnUploadVideoToMediaLibraryComplete(Facebook.Unity.ResultContainer)
// 0x000002DB System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnOnIAPReadyComplete(Facebook.Unity.ResultContainer)
// 0x000002DC System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnGetCatalogComplete(Facebook.Unity.ResultContainer)
// 0x000002DD System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnGetPurchasesComplete(Facebook.Unity.ResultContainer)
// 0x000002DE System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnPurchaseComplete(Facebook.Unity.ResultContainer)
// 0x000002DF System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnConsumePurchaseComplete(Facebook.Unity.ResultContainer)
// 0x000002E0 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnInitCloudGameComplete(Facebook.Unity.ResultContainer)
// 0x000002E1 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnScheduleAppToUserNotificationComplete(Facebook.Unity.ResultContainer)
// 0x000002E2 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnLoadInterstitialAdComplete(Facebook.Unity.ResultContainer)
// 0x000002E3 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnShowInterstitialAdComplete(Facebook.Unity.ResultContainer)
// 0x000002E4 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnLoadRewardedVideoComplete(Facebook.Unity.ResultContainer)
// 0x000002E5 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnShowRewardedVideoComplete(Facebook.Unity.ResultContainer)
// 0x000002E6 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnGetPayloadComplete(Facebook.Unity.ResultContainer)
// 0x000002E7 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnPostSessionScoreComplete(Facebook.Unity.ResultContainer)
// 0x000002E8 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnOpenAppStoreComplete(Facebook.Unity.ResultContainer)
// 0x000002E9 System.Void Facebook.Unity.Mobile.MobileFacebook::.ctor(Facebook.Unity.CallbackManager)
extern void MobileFacebook__ctor_m3A59FE7FD3E2006C5160940FC49B3EF5F857066E (void);
// 0x000002EA System.Void Facebook.Unity.Mobile.MobileFacebook::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void MobileFacebook_set_ShareDialogMode_m0C1939A04C4F38E8B8F65827051820C6F4980AA9 (void);
// 0x000002EB Facebook.Unity.AuthenticationToken Facebook.Unity.Mobile.MobileFacebook::CurrentAuthenticationToken()
// 0x000002EC System.Void Facebook.Unity.Mobile.MobileFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x000002ED System.Void Facebook.Unity.Mobile.MobileFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
// 0x000002EE System.Boolean Facebook.Unity.Mobile.MobileFacebook::IsImplicitPurchaseLoggingEnabled()
// 0x000002EF System.Void Facebook.Unity.Mobile.MobileFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnLoginComplete_mC2E1C6514504CABBBA15A90B750226BEA84D6EF6 (void);
// 0x000002F0 System.Void Facebook.Unity.Mobile.MobileFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnGetAppLinkComplete_mE09DF4EDA5F11D23C14809A245D68EC6B0CA1737 (void);
// 0x000002F1 System.Void Facebook.Unity.Mobile.MobileFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnAppRequestsComplete_m4E1708FDB9E2AC680A885F0E81DA1AB04BF16B8F (void);
// 0x000002F2 System.Void Facebook.Unity.Mobile.MobileFacebook::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnFetchDeferredAppLinkComplete_m02E22D30B8B3F6FE7EFFFB786196B091C1D9D25F (void);
// 0x000002F3 System.Void Facebook.Unity.Mobile.MobileFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnShareLinkComplete_m71F8C6C4203BC96679C5460996145BC318D6E67A (void);
// 0x000002F4 System.Void Facebook.Unity.Mobile.MobileFacebook::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnRefreshCurrentAccessTokenComplete_mC5ECAC7647A0A7C2DE7F22B6F9C460B3132DB75D (void);
// 0x000002F5 System.Void Facebook.Unity.Mobile.MobileFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void MobileFacebook_OpenFriendFinderDialog_mA3238547742AB678BAA887351ECF937CDE11EE43 (void);
// 0x000002F6 System.Void Facebook.Unity.Mobile.MobileFacebook::OnFriendFinderComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnFriendFinderComplete_m626C33379170486AEA2E34B59773AAC79A76C212 (void);
// 0x000002F7 System.Void Facebook.Unity.Mobile.MobileFacebook::OnUploadImageToMediaLibraryComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnUploadImageToMediaLibraryComplete_m086A54D5A8DEBB010F0CDCA106B667DEAB6F51FF (void);
// 0x000002F8 System.Void Facebook.Unity.Mobile.MobileFacebook::OnUploadVideoToMediaLibraryComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnUploadVideoToMediaLibraryComplete_mD9CCE6E2A2C459F9B7B71A97A19EC8D0BAF6D468 (void);
// 0x000002F9 System.Void Facebook.Unity.Mobile.MobileFacebook::OnOnIAPReadyComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnOnIAPReadyComplete_m377ACCA6CA40A9C8F4E1BDA978671C04DE08A027 (void);
// 0x000002FA System.Void Facebook.Unity.Mobile.MobileFacebook::OnGetCatalogComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnGetCatalogComplete_m58E624DAEB759FC2745AD3B69414B4653A709273 (void);
// 0x000002FB System.Void Facebook.Unity.Mobile.MobileFacebook::OnGetPurchasesComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnGetPurchasesComplete_m47A229404A1A8FF01900458690F14C3CA7B3F56B (void);
// 0x000002FC System.Void Facebook.Unity.Mobile.MobileFacebook::OnPurchaseComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnPurchaseComplete_m26E970FE60ECAECD7286F389B4BCA0C2063E6D29 (void);
// 0x000002FD System.Void Facebook.Unity.Mobile.MobileFacebook::OnConsumePurchaseComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnConsumePurchaseComplete_m8D2EB7D176B3A2BD8FB56FAFFD393B0F03BB5042 (void);
// 0x000002FE System.Void Facebook.Unity.Mobile.MobileFacebook::OnInitCloudGameComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnInitCloudGameComplete_mC42D55F7D7D0A6B483FC6A993D4AA0303023B701 (void);
// 0x000002FF System.Void Facebook.Unity.Mobile.MobileFacebook::OnScheduleAppToUserNotificationComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnScheduleAppToUserNotificationComplete_m03A02BAF150A4287F5336F698008CD2DCE7D1929 (void);
// 0x00000300 System.Void Facebook.Unity.Mobile.MobileFacebook::OnLoadInterstitialAdComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnLoadInterstitialAdComplete_m19E022189D1630A90488E594D10025DDDDA40C91 (void);
// 0x00000301 System.Void Facebook.Unity.Mobile.MobileFacebook::OnShowInterstitialAdComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnShowInterstitialAdComplete_m58CCF38D37F44B85B3DBDE3E2D6327663AE39807 (void);
// 0x00000302 System.Void Facebook.Unity.Mobile.MobileFacebook::OnLoadRewardedVideoComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnLoadRewardedVideoComplete_m9EFF13380B64589719BDB1C90013733AF69EC23D (void);
// 0x00000303 System.Void Facebook.Unity.Mobile.MobileFacebook::OnShowRewardedVideoComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnShowRewardedVideoComplete_mBC634D8C917CCF287FEC7BF1666AFEE53EC9FE1E (void);
// 0x00000304 System.Void Facebook.Unity.Mobile.MobileFacebook::OnGetPayloadComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnGetPayloadComplete_mFEC699267699E7FC2562BFEC04C5B57D75DC32F4 (void);
// 0x00000305 System.Void Facebook.Unity.Mobile.MobileFacebook::OnPostSessionScoreComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnPostSessionScoreComplete_m450564D3AB404D90F7623570D14BF9866D9EC124 (void);
// 0x00000306 System.Void Facebook.Unity.Mobile.MobileFacebook::OnOpenAppStoreComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnOpenAppStoreComplete_mD2A07711BBDEAB30DF98A9421944F9336FE17858 (void);
// 0x00000307 System.Void Facebook.Unity.Mobile.MobileFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void MobileFacebook_UploadImageToMediaLibrary_mD1322C750B48D7723FEFEF24C29B7C7068D3F585 (void);
// 0x00000308 System.Void Facebook.Unity.Mobile.MobileFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void MobileFacebook_UploadVideoToMediaLibrary_m24D85E29396D40D88D44A0BD4F13BE627B4A4F35 (void);
// 0x00000309 System.Void Facebook.Unity.Mobile.MobileFacebook::OnIAPReady(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IIAPReadyResult>)
extern void MobileFacebook_OnIAPReady_m683832CBD8300205907D50232B35191347537F01 (void);
// 0x0000030A System.Void Facebook.Unity.Mobile.MobileFacebook::GetCatalog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ICatalogResult>)
extern void MobileFacebook_GetCatalog_mC266DE9EFCE0DE278B9713D384BDDD699A8C8340 (void);
// 0x0000030B System.Void Facebook.Unity.Mobile.MobileFacebook::GetPurchases(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchasesResult>)
extern void MobileFacebook_GetPurchases_mF9E6C292DA2CFA014B8FC0FCCEF9BA01384BEB66 (void);
// 0x0000030C System.Void Facebook.Unity.Mobile.MobileFacebook::Purchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchaseResult>,System.String)
extern void MobileFacebook_Purchase_m0A51FB493721DA84D4D7DD552688D983A8F3BEE6 (void);
// 0x0000030D System.Void Facebook.Unity.Mobile.MobileFacebook::ConsumePurchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IConsumePurchaseResult>)
extern void MobileFacebook_ConsumePurchase_m876E0E0E3E5F5696C2985C8505F067235FE5760D (void);
// 0x0000030E System.Void Facebook.Unity.Mobile.MobileFacebook::InitCloudGame(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInitCloudGameResult>)
extern void MobileFacebook_InitCloudGame_m7F549AB5428AE0AC82860B4BE52413F1A5A0C5B9 (void);
// 0x0000030F System.Void Facebook.Unity.Mobile.MobileFacebook::ScheduleAppToUserNotification(System.String,System.String,System.Uri,System.Int32,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IScheduleAppToUserNotificationResult>)
extern void MobileFacebook_ScheduleAppToUserNotification_m60BC9BEF24FFC569D1975FB968F328D18E167FC0 (void);
// 0x00000310 System.Void Facebook.Unity.Mobile.MobileFacebook::LoadInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
extern void MobileFacebook_LoadInterstitialAd_mA872513BFF29238974E533541EC75EE822624491 (void);
// 0x00000311 System.Void Facebook.Unity.Mobile.MobileFacebook::ShowInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
extern void MobileFacebook_ShowInterstitialAd_m20A743A8AB56BCA9AE229117D9F4217D4BF3B7AB (void);
// 0x00000312 System.Void Facebook.Unity.Mobile.MobileFacebook::LoadRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
extern void MobileFacebook_LoadRewardedVideo_mB9257EAE9B89E618AB9F9586793EC8C1C09B8449 (void);
// 0x00000313 System.Void Facebook.Unity.Mobile.MobileFacebook::ShowRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
extern void MobileFacebook_ShowRewardedVideo_m313AB0E5C45FC15F47FC0E175CA701AFECED52E9 (void);
// 0x00000314 System.Void Facebook.Unity.Mobile.MobileFacebook::GetPayload(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayloadResult>)
extern void MobileFacebook_GetPayload_mF110D04F8C577B29C3FD5E86E74C2DC9AB10E6EC (void);
// 0x00000315 System.Void Facebook.Unity.Mobile.MobileFacebook::PostSessionScore(System.Int32,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ISessionScoreResult>)
extern void MobileFacebook_PostSessionScore_m257FA9D84B65D67E0126ABDFDDD836DCE6E6040D (void);
// 0x00000316 System.Void Facebook.Unity.Mobile.MobileFacebook::OpenAppStore(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IOpenAppStoreResult>)
extern void MobileFacebook_OpenAppStore_mFD7C4397E722EC3EC034C29A9D096FB6E478A147 (void);
// 0x00000317 System.Void Facebook.Unity.Mobile.MobileFacebook::SetShareDialogMode(Facebook.Unity.ShareDialogMode)
// 0x00000318 Facebook.Unity.Mobile.IMobileFacebookImplementation Facebook.Unity.Mobile.MobileFacebookGameObject::get_MobileFacebook()
extern void MobileFacebookGameObject_get_MobileFacebook_mA97444E3D77017BA5489E5470A2E121B5CD55D28 (void);
// 0x00000319 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnFetchDeferredAppLinkComplete(System.String)
extern void MobileFacebookGameObject_OnFetchDeferredAppLinkComplete_mE11CC37B0BD5530D16C88F7BFB870F545AAA0382 (void);
// 0x0000031A System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnRefreshCurrentAccessTokenComplete(System.String)
extern void MobileFacebookGameObject_OnRefreshCurrentAccessTokenComplete_mBB94038603B430A43F7CA07FEB641CF35F2FAD4F (void);
// 0x0000031B System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnFriendFinderComplete(System.String)
extern void MobileFacebookGameObject_OnFriendFinderComplete_m1F7496E68F10067E631DDDB5D04FFE9F67E219D5 (void);
// 0x0000031C System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnUploadImageToMediaLibraryComplete(System.String)
extern void MobileFacebookGameObject_OnUploadImageToMediaLibraryComplete_mAB689BA1DD00D712ADF9553FB5CFE451EA845FF5 (void);
// 0x0000031D System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnUploadVideoToMediaLibraryComplete(System.String)
extern void MobileFacebookGameObject_OnUploadVideoToMediaLibraryComplete_m349469B4DCEE43567994CBC3ADB595FC867C5C90 (void);
// 0x0000031E System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnOnIAPReadyComplete(System.String)
extern void MobileFacebookGameObject_OnOnIAPReadyComplete_mF898C209F8E6ED875550118083BF10F3D566D957 (void);
// 0x0000031F System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnGetCatalogComplete(System.String)
extern void MobileFacebookGameObject_OnGetCatalogComplete_m3EAECF41AFB2D3044148F71A9D2FD252249F15D6 (void);
// 0x00000320 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnGetPurchasesComplete(System.String)
extern void MobileFacebookGameObject_OnGetPurchasesComplete_m6D4DAFF42761B0C72386CC3EF17F7C5CEA644CB4 (void);
// 0x00000321 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnPurchaseComplete(System.String)
extern void MobileFacebookGameObject_OnPurchaseComplete_m4B47F45F9936CA9FE84A5FC2BC0B0000165B6390 (void);
// 0x00000322 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnConsumePurchaseComplete(System.String)
extern void MobileFacebookGameObject_OnConsumePurchaseComplete_m8C6E2EB2741A8E1A33692DDFB8214A4C8A1D2E31 (void);
// 0x00000323 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnInitCloudGameComplete(System.String)
extern void MobileFacebookGameObject_OnInitCloudGameComplete_mB44D2AADF3B584138F6A2844A883F828EF0292FB (void);
// 0x00000324 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnScheduleAppToUserNotificationComplete(System.String)
extern void MobileFacebookGameObject_OnScheduleAppToUserNotificationComplete_m396FCC8BC23E30C4CD4CC4D75D8B6A1AD50967AA (void);
// 0x00000325 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnLoadInterstitialAdComplete(System.String)
extern void MobileFacebookGameObject_OnLoadInterstitialAdComplete_m28A22C166D382D8FDD6AAFDEC37C7AFD68FCA37F (void);
// 0x00000326 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnShowInterstitialAdComplete(System.String)
extern void MobileFacebookGameObject_OnShowInterstitialAdComplete_m64902E980B06499086A54198C8D2B4E7FF795174 (void);
// 0x00000327 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnLoadRewardedVideoComplete(System.String)
extern void MobileFacebookGameObject_OnLoadRewardedVideoComplete_m80241068E26FAB35B0685BAEC5CD69A819D3BC51 (void);
// 0x00000328 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnShowRewardedVideoComplete(System.String)
extern void MobileFacebookGameObject_OnShowRewardedVideoComplete_mC5B03B4BFC4A05676C4693F15763AD488932455F (void);
// 0x00000329 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnGetPayloadComplete(System.String)
extern void MobileFacebookGameObject_OnGetPayloadComplete_m27FADDD99C24609F322033481CA5A656B57A5E9B (void);
// 0x0000032A System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnPostSessionScoreComplete(System.String)
extern void MobileFacebookGameObject_OnPostSessionScoreComplete_m8C611B9F486BA8FD81D819B5CAC24093E4C83992 (void);
// 0x0000032B System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnOpenAppStoreComplete(System.String)
extern void MobileFacebookGameObject_OnOpenAppStoreComplete_mC20F40C9A2A527DC42E3145039897695B570C6B5 (void);
// 0x0000032C System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::.ctor()
extern void MobileFacebookGameObject__ctor_mDAB316B7660F03139CF9A9E30545F3B6939A461A (void);
// 0x0000032D System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::Init(System.String,System.Boolean,System.String,System.String)
// 0x0000032E System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogInWithReadPermissions(System.Int32,System.String)
// 0x0000032F System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogInWithPublishPermissions(System.Int32,System.String)
// 0x00000330 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogOut()
// 0x00000331 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::SetShareDialogMode(System.Int32)
// 0x00000332 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::ShareLink(System.Int32,System.String,System.String,System.String,System.String)
// 0x00000333 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
// 0x00000334 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::AppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
// 0x00000335 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FBAppEventsActivateApp()
// 0x00000336 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogAppEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
// 0x00000337 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogPurchaseAppEvent(System.Double,System.String,System.Int32,System.String[],System.String[])
// 0x00000338 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FBAppEventsSetLimitEventUsage(System.Boolean)
// 0x00000339 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::GetAppLink(System.Int32)
// 0x0000033A System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::RefreshCurrentAccessToken(System.Int32)
// 0x0000033B System.String Facebook.Unity.Mobile.IOS.IIOSWrapper::FBSdkVersion()
// 0x0000033C System.String Facebook.Unity.Mobile.IOS.IIOSWrapper::FBGetUserID()
// 0x0000033D System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::OpenFriendFinderDialog(System.Int32)
// 0x0000033E System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::UploadImageToMediaLibrary(System.Int32,System.String,System.String,System.Boolean)
// 0x0000033F System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::UploadVideoToMediaLibrary(System.Int32,System.String,System.String)
// 0x00000340 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FetchDeferredAppLink(System.Int32)
// 0x00000341 Facebook.Unity.AuthenticationToken Facebook.Unity.Mobile.IOS.IIOSWrapper::CurrentAuthenticationToken()
// 0x00000342 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::.ctor()
extern void IOSFacebook__ctor_m67943C9D01260F1DEC7EF2BB3FC93DEE5BEB2177 (void);
// 0x00000343 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::.ctor(Facebook.Unity.Mobile.IOS.IIOSWrapper,Facebook.Unity.CallbackManager)
extern void IOSFacebook__ctor_m7F17D11D3AF8645BA7AA7507D358A4033F41D564 (void);
// 0x00000344 System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::get_LimitEventUsage()
extern void IOSFacebook_get_LimitEventUsage_mBF549BEB68B2BD767FFA6165A78833E7A4551CDB (void);
// 0x00000345 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::set_LimitEventUsage(System.Boolean)
extern void IOSFacebook_set_LimitEventUsage_m60A37224769BC1942981285864DD2CF9B8F4058E (void);
// 0x00000346 System.String Facebook.Unity.Mobile.IOS.IOSFacebook::get_SDKName()
extern void IOSFacebook_get_SDKName_m870BC8F1EE06DBA8C2F731328676BE54120E60AA (void);
// 0x00000347 System.String Facebook.Unity.Mobile.IOS.IOSFacebook::get_SDKVersion()
extern void IOSFacebook_get_SDKVersion_m6752E514687D2D9F15C65EBFE992680E9CCB575C (void);
// 0x00000348 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::Init(System.String,System.Boolean,System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void IOSFacebook_Init_m77C41B0995508935908947E81CFD3E2A0A97A29A (void);
// 0x00000349 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void IOSFacebook_LogInWithReadPermissions_m1633E33F011F266E665AC5D38CD7BEC9993F6BC6 (void);
// 0x0000034A System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void IOSFacebook_LogInWithPublishPermissions_m732FCA79064E4E4884B658208F170857587F05B4 (void);
// 0x0000034B System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogOut()
extern void IOSFacebook_LogOut_m6539436DA8EEC710FA5ED68872517059A473D27C (void);
// 0x0000034C System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::get_LoggedIn()
extern void IOSFacebook_get_LoggedIn_mE2934BB7E9EA4F16B5D18D769F02543263A9EBAE (void);
// 0x0000034D Facebook.Unity.AuthenticationToken Facebook.Unity.Mobile.IOS.IOSFacebook::CurrentAuthenticationToken()
extern void IOSFacebook_CurrentAuthenticationToken_m150F5D259224EDE684BCE337A759C668282333F9 (void);
// 0x0000034E System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void IOSFacebook_AppRequest_mB29DD50E3D9A62867251AB48722C173D32EF294E (void);
// 0x0000034F System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void IOSFacebook_ShareLink_mFF3639DCD597FF14A59DCA98C925D46BE908373A (void);
// 0x00000350 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void IOSFacebook_FeedShare_m3AC5FD0C77703A88D1BDDDB90691512C3B295CC8 (void);
// 0x00000351 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_AppEventsLogEvent_m71CBAD70D6C9E9D48E20D6CF0F5FD6F21632DB45 (void);
// 0x00000352 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_AppEventsLogPurchase_mD32B798B7A73388E830CDA082DE31D0249F89BB9 (void);
// 0x00000353 System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::IsImplicitPurchaseLoggingEnabled()
extern void IOSFacebook_IsImplicitPurchaseLoggingEnabled_mE039213CD95A517BF32699840B73A1846646E245 (void);
// 0x00000354 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::ActivateApp(System.String)
extern void IOSFacebook_ActivateApp_m555BD849D1A35B8726BA8F9619A06604392FE5A0 (void);
// 0x00000355 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void IOSFacebook_FetchDeferredAppLink_m2BFB122C2C092902246B0275369DA3EBEF12DBC0 (void);
// 0x00000356 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void IOSFacebook_GetAppLink_m3412ED4F59A236F9D8518AFE0D38DEE0ADB05347 (void);
// 0x00000357 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void IOSFacebook_OpenFriendFinderDialog_m0CACAA50B6AAE93E32C867A01C5E0BC08E8B6628 (void);
// 0x00000358 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern void IOSFacebook_RefreshCurrentAccessToken_mE94B2165E86149D53632EF4D3C494EFA1CA800F3 (void);
// 0x00000359 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::SetShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void IOSFacebook_SetShareDialogMode_m04FA4185F2321A0E11B8A1BAF03438D0A8A0111F (void);
// 0x0000035A System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void IOSFacebook_UploadImageToMediaLibrary_m4AEA69C1B067B024604F241FB712F54253B7FB28 (void);
// 0x0000035B System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void IOSFacebook_UploadVideoToMediaLibrary_mE0C410BE7FE660F864D10EB1EC91C8E427C0DD5E (void);
// 0x0000035C Facebook.Unity.Mobile.IOS.IIOSWrapper Facebook.Unity.Mobile.IOS.IOSFacebook::GetIOSWrapper()
extern void IOSFacebook_GetIOSWrapper_mE30F0CFFDE57F641649BC4D0EA8F60E139DE2BFA (void);
// 0x0000035D Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict Facebook.Unity.Mobile.IOS.IOSFacebook::MarshallDict(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_MarshallDict_mBB7F8CF57D6649DD0CFF4832886A40D68D49D731 (void);
// 0x0000035E System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook::AddCallback(Facebook.Unity.FacebookDelegate`1<T>)
// 0x0000035F System.Void Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::.ctor()
extern void NativeDict__ctor_m5E29B0F06531EB01D2755928B8BFB4AE60FB248B (void);
// 0x00000360 System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::get_NumEntries()
extern void NativeDict_get_NumEntries_mD08372040D670F3B2E8DAFB666908C69494614E2 (void);
// 0x00000361 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::set_NumEntries(System.Int32)
extern void NativeDict_set_NumEntries_m964DA95E7E5391D1EAA539A665BD8CC3EFF7A187 (void);
// 0x00000362 System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::get_Keys()
extern void NativeDict_get_Keys_m6791D63A565CFEB36B174D049CE45127815BD3C2 (void);
// 0x00000363 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::set_Keys(System.String[])
extern void NativeDict_set_Keys_m8815A402AA77D607854B7056654D93CB32A085F9 (void);
// 0x00000364 System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::get_Values()
extern void NativeDict_get_Values_m52424E4E1B8CAF63C8DA1AA7DF11E1F2D7C522FF (void);
// 0x00000365 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::set_Values(System.String[])
extern void NativeDict_set_Values_m165D1C1EDF6B83713A73DF8BC8E52E958BDF86DA (void);
// 0x00000366 System.Void Facebook.Unity.Mobile.IOS.IOSFacebookGameObject::.ctor()
extern void IOSFacebookGameObject__ctor_mDA597F941109F6DEE3D363F3D3B3EB42E3682CE7 (void);
// 0x00000367 Facebook.Unity.FacebookGameObject Facebook.Unity.Mobile.IOS.IOSFacebookLoader::get_FBGameObject()
extern void IOSFacebookLoader_get_FBGameObject_m4C574D00396A998BCC12A0C730EA6907DDED05D6 (void);
// 0x00000368 System.Void Facebook.Unity.Mobile.IOS.IOSFacebookLoader::.ctor()
extern void IOSFacebookLoader__ctor_m9FCE25D7B9DCAA60BF5E15118FBAE2B01A5403E9 (void);
// 0x00000369 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::.ctor()
extern void AndroidFacebook__ctor_m3670A637434B95F4E24956001C1F3555F1D1F938 (void);
// 0x0000036A System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::.ctor(Facebook.Unity.Mobile.Android.IAndroidWrapper,Facebook.Unity.CallbackManager)
extern void AndroidFacebook__ctor_m8797EBBBD9D5411CCC885AC4BF754A7D8B4FED8D (void);
// 0x0000036B System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::set_KeyHash(System.String)
extern void AndroidFacebook_set_KeyHash_m15258DF6E2CE5A72887805F580CECF7CD737BA01 (void);
// 0x0000036C System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::get_LimitEventUsage()
extern void AndroidFacebook_get_LimitEventUsage_m468C50D9060DC2ACFEED68430BA34B7CDC07EB9A (void);
// 0x0000036D System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::set_LimitEventUsage(System.Boolean)
extern void AndroidFacebook_set_LimitEventUsage_m31117A074D556FC5882BC3935D446E5646D744B6 (void);
// 0x0000036E System.String Facebook.Unity.Mobile.Android.AndroidFacebook::get_SDKName()
extern void AndroidFacebook_get_SDKName_m653D3A561F385535389A2D508A0862AAA0C83252 (void);
// 0x0000036F System.String Facebook.Unity.Mobile.Android.AndroidFacebook::get_SDKVersion()
extern void AndroidFacebook_get_SDKVersion_mA7F1CFB6E4A7C4CE821A970B3D03EB6CD5739D5A (void);
// 0x00000370 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::Init(System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void AndroidFacebook_Init_m59E2FCF14391556A4D8A5691370B082F83EE8896 (void);
// 0x00000371 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void AndroidFacebook_LogInWithReadPermissions_m9DBEB4CA9C4E77D6FD09D414CE11110A809C60E7 (void);
// 0x00000372 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void AndroidFacebook_LogInWithPublishPermissions_mB49269D1824EE89A9FE13FDC4918131FDA37AF20 (void);
// 0x00000373 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogOut()
extern void AndroidFacebook_LogOut_m417B71776C7B3E41941D7FCBEF0ACEC0A2ADAB42 (void);
// 0x00000374 Facebook.Unity.AuthenticationToken Facebook.Unity.Mobile.Android.AndroidFacebook::CurrentAuthenticationToken()
extern void AndroidFacebook_CurrentAuthenticationToken_m67B75339334A3AA452D5750E004BC73970A192E8 (void);
// 0x00000375 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::OnLoginStatusRetrieved(Facebook.Unity.ResultContainer)
extern void AndroidFacebook_OnLoginStatusRetrieved_mA67750D682778DD84E794712229C6D8EE2517439 (void);
// 0x00000376 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void AndroidFacebook_AppRequest_m3002DA04736125A5DD3BEADCB761954E2B2043BB (void);
// 0x00000377 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void AndroidFacebook_ShareLink_m49F30222882F74578C5A532A75C3C359E13275B6 (void);
// 0x00000378 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void AndroidFacebook_FeedShare_m6CD3A7EDDC8ACF5B32B193D257C121508A45EF6C (void);
// 0x00000379 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void AndroidFacebook_GetAppLink_m81177A729A37521E30F139D9E9C45B3C5CD197CA (void);
// 0x0000037A System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AndroidFacebook_AppEventsLogEvent_mBBEB32EDF7F74DB0132C7D91071C788C5F619229 (void);
// 0x0000037B System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AndroidFacebook_AppEventsLogPurchase_m83E3C2978CC496574FF3683E33FA44EB1BFE8665 (void);
// 0x0000037C System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::IsImplicitPurchaseLoggingEnabled()
extern void AndroidFacebook_IsImplicitPurchaseLoggingEnabled_mF59F636DF33CE9E58571E0D80CBFC0A01628E3F9 (void);
// 0x0000037D System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ActivateApp(System.String)
extern void AndroidFacebook_ActivateApp_mE19FF59FA4DB069A44939A19F21E6D9601C64F48 (void);
// 0x0000037E System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void AndroidFacebook_FetchDeferredAppLink_mB222CF5E53BAA394F9049D5CB93C17C49C2384C6 (void);
// 0x0000037F System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern void AndroidFacebook_RefreshCurrentAccessToken_m68A309C8FD782B7AD2C91DB2A661AC54D9D44944 (void);
// 0x00000380 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void AndroidFacebook_OpenFriendFinderDialog_m624F14812094E91A7C5B699941B5677BDF65A81F (void);
// 0x00000381 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void AndroidFacebook_UploadImageToMediaLibrary_m2142BE6C6B6539905191031D42070638DCAB551A (void);
// 0x00000382 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void AndroidFacebook_UploadVideoToMediaLibrary_m2219968A7BEDCF5CFADFF15A44E21992295AA980 (void);
// 0x00000383 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::OnIAPReady(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IIAPReadyResult>)
extern void AndroidFacebook_OnIAPReady_m8EF0424FD603C0AA5075294573B4980AD0AE8E76 (void);
// 0x00000384 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::GetCatalog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ICatalogResult>)
extern void AndroidFacebook_GetCatalog_mD98354DA0554D38655FC82BEBD2E2AD8F782FA1B (void);
// 0x00000385 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::GetPurchases(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchasesResult>)
extern void AndroidFacebook_GetPurchases_m270B1E530CB6B451B13D6EFBE628455DCC82BF04 (void);
// 0x00000386 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::Purchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPurchaseResult>,System.String)
extern void AndroidFacebook_Purchase_mEF18CF6E51EB0E84725D07751B0A21EF3AC0B280 (void);
// 0x00000387 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ConsumePurchase(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IConsumePurchaseResult>)
extern void AndroidFacebook_ConsumePurchase_m7A70C61B1982196160D935E8F85978F549BC010A (void);
// 0x00000388 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::InitCloudGame(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInitCloudGameResult>)
extern void AndroidFacebook_InitCloudGame_mBED6DCD1AEDE1F41139951826E3F1FE2432511FC (void);
// 0x00000389 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ScheduleAppToUserNotification(System.String,System.String,System.Uri,System.Int32,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IScheduleAppToUserNotificationResult>)
extern void AndroidFacebook_ScheduleAppToUserNotification_mA2FEA52C62DD05C4B5A061820DDF88A2857C8232 (void);
// 0x0000038A System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LoadInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
extern void AndroidFacebook_LoadInterstitialAd_mF7E73077959FB2EB2A89FA55300F87371F88535B (void);
// 0x0000038B System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ShowInterstitialAd(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IInterstitialAdResult>)
extern void AndroidFacebook_ShowInterstitialAd_m462A7FBB53C5463BCC8320C6F045C37CDE04605E (void);
// 0x0000038C System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LoadRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
extern void AndroidFacebook_LoadRewardedVideo_m0A5384F94B156292ACAFAAAB23FFA9FDA50CBD29 (void);
// 0x0000038D System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ShowRewardedVideo(System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IRewardedVideoResult>)
extern void AndroidFacebook_ShowRewardedVideo_m4499EB54AEE330ECEEB176263390C96A23757437 (void);
// 0x0000038E System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::GetPayload(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayloadResult>)
extern void AndroidFacebook_GetPayload_m0C3144612071DF508C1ABA72F1FE60BC46EA4AC1 (void);
// 0x0000038F System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::PostSessionScore(System.Int32,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ISessionScoreResult>)
extern void AndroidFacebook_PostSessionScore_m6439E2821372FD2334A380EAE78EC899AFDDB8A7 (void);
// 0x00000390 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::OpenAppStore(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IOpenAppStoreResult>)
extern void AndroidFacebook_OpenAppStore_m8E442AA9203E2A3EFE7077FBE34FB5BAB386E7A6 (void);
// 0x00000391 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::SetShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void AndroidFacebook_SetShareDialogMode_mEA41EE9EDA48CC7DEF99795CAFEABE9D77911B0E (void);
// 0x00000392 Facebook.Unity.Mobile.Android.IAndroidWrapper Facebook.Unity.Mobile.Android.AndroidFacebook::GetAndroidWrapper()
extern void AndroidFacebook_GetAndroidWrapper_m6D70DA270BBA55DE6D43AAA1A11A8464D9148F98 (void);
// 0x00000393 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::CallFB(System.String,System.String)
extern void AndroidFacebook_CallFB_mEFBA9B6E2ADBE5FA52130BFCC8344035071DA4F4 (void);
// 0x00000394 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
// 0x00000395 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x00000396 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnAwake()
extern void AndroidFacebookGameObject_OnAwake_m602A1414C05540213995C7EA5ED223CADB2A1875 (void);
// 0x00000397 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnEnable()
extern void AndroidFacebookGameObject_OnEnable_mB4DBD2CFB073C064ECDB9CCCABF32D93F3515868 (void);
// 0x00000398 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void AndroidFacebookGameObject_OnSceneLoaded_m404AD8E0F1F30AE688D8652FF77995239DA53511 (void);
// 0x00000399 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnDisable()
extern void AndroidFacebookGameObject_OnDisable_m8ADA1051965DCFF709052978D5A77D261A263DFB (void);
// 0x0000039A System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::onPurchaseCompleteHandler(System.Object)
extern void AndroidFacebookGameObject_onPurchaseCompleteHandler_m81DB4C8D88E2182469DCA140C1F6879EF2111B97 (void);
// 0x0000039B System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnLoginStatusRetrieved(System.String)
extern void AndroidFacebookGameObject_OnLoginStatusRetrieved_m0ABD92E79E0A3F6B049BE36FF3BE37AD2871A457 (void);
// 0x0000039C System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::.ctor()
extern void AndroidFacebookGameObject__ctor_mBC46954F350AC6D449BB4CDBED8A6DD85A562022 (void);
// 0x0000039D Facebook.Unity.FacebookGameObject Facebook.Unity.Mobile.Android.AndroidFacebookLoader::get_FBGameObject()
extern void AndroidFacebookLoader_get_FBGameObject_mEFDDF78F9A30BCD5F96A154FA4E92E16D1779FB9 (void);
// 0x0000039E System.Void Facebook.Unity.Mobile.Android.AndroidFacebookLoader::.ctor()
extern void AndroidFacebookLoader__ctor_mDA110EEE32F120E15DBDEAF16971F226BFE65A2E (void);
// 0x0000039F T Facebook.Unity.Mobile.Android.IAndroidWrapper::CallStatic(System.String)
// 0x000003A0 System.Void Facebook.Unity.Mobile.Android.IAndroidWrapper::CallStatic(System.String,System.Object[])
// 0x000003A1 System.Void Facebook.Unity.Canvas.CanvasFacebook::.ctor()
extern void CanvasFacebook__ctor_mF5071236474D18C1EEB1EE4A2632C7E27A023C1E (void);
// 0x000003A2 System.Void Facebook.Unity.Canvas.CanvasFacebook::.ctor(Facebook.Unity.Canvas.ICanvasJSWrapper,Facebook.Unity.CallbackManager)
extern void CanvasFacebook__ctor_m3DEC7ED9BE8ADA51D2E1D7AE8982B798F44800F9 (void);
// 0x000003A3 Facebook.Unity.Canvas.ICanvasJSWrapper Facebook.Unity.Canvas.CanvasFacebook::GetCanvasJSWrapper()
extern void CanvasFacebook_GetCanvasJSWrapper_m050AAD4D4D867F982F790039DDC8B3B351A46659 (void);
// 0x000003A4 System.Boolean Facebook.Unity.Canvas.CanvasFacebook::get_LimitEventUsage()
extern void CanvasFacebook_get_LimitEventUsage_mB265FDF5F0E5215548A9593C43ADAB62142BD53D (void);
// 0x000003A5 System.Void Facebook.Unity.Canvas.CanvasFacebook::set_LimitEventUsage(System.Boolean)
extern void CanvasFacebook_set_LimitEventUsage_m28EFAAF8B4917567E5824048435B3E52E469C8FD (void);
// 0x000003A6 System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKName()
extern void CanvasFacebook_get_SDKName_m6D59E8324BBC43E1A74454DBEF239C46F87664DF (void);
// 0x000003A7 System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKVersion()
extern void CanvasFacebook_get_SDKVersion_mA74BC052CF22EDB90B24DB71FEAFDDA0B975651C (void);
// 0x000003A8 System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKUserAgent()
extern void CanvasFacebook_get_SDKUserAgent_m5F5965391D99B17789D23F8E8A01B66601CF3136 (void);
// 0x000003A9 System.Void Facebook.Unity.Canvas.CanvasFacebook::Init(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.Boolean,System.String,System.Boolean,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void CanvasFacebook_Init_mCD5F942DFDE55A89D30D7A8527661A910FBDF294 (void);
// 0x000003AA System.Void Facebook.Unity.Canvas.CanvasFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void CanvasFacebook_LogInWithPublishPermissions_m54F3508A3B0C643E26ED774E273707D289A0C420 (void);
// 0x000003AB System.Void Facebook.Unity.Canvas.CanvasFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void CanvasFacebook_LogInWithReadPermissions_mC62E0A5F68554005E80772285904CB152E5393E1 (void);
// 0x000003AC System.Void Facebook.Unity.Canvas.CanvasFacebook::LogOut()
extern void CanvasFacebook_LogOut_m0E27A0429F9F83E4BFB7ADC1346FA0528102D520 (void);
// 0x000003AD System.Void Facebook.Unity.Canvas.CanvasFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void CanvasFacebook_AppRequest_mB516DED31E849E005CFBC29A1C56846D70FF2D68 (void);
// 0x000003AE System.Void Facebook.Unity.Canvas.CanvasFacebook::ActivateApp(System.String)
extern void CanvasFacebook_ActivateApp_m888D5E16A44DD6FA3A60E4FCF905531B9A37683C (void);
// 0x000003AF System.Void Facebook.Unity.Canvas.CanvasFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void CanvasFacebook_ShareLink_m071F87BC1FD5342B43F18339315022CF49FAE019 (void);
// 0x000003B0 System.Void Facebook.Unity.Canvas.CanvasFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void CanvasFacebook_FeedShare_m013A8BA1C67BDD84F81538A3D41D53BEA1FF1A21 (void);
// 0x000003B1 System.Void Facebook.Unity.Canvas.CanvasFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void CanvasFacebook_Pay_m2EC43A1BD4F5A9866A98ACABECB777B7BF9A2B07 (void);
// 0x000003B2 System.Void Facebook.Unity.Canvas.CanvasFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void CanvasFacebook_GetAppLink_mC7766021EADBED1B0C7E4ABE8DF1349983E09E9F (void);
// 0x000003B3 System.Void Facebook.Unity.Canvas.CanvasFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void CanvasFacebook_AppEventsLogEvent_m27F5F9190D138214109023FDCB8FDEA19E5CA737 (void);
// 0x000003B4 System.Void Facebook.Unity.Canvas.CanvasFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void CanvasFacebook_AppEventsLogPurchase_mF67D924531E629698173BFB024509315F57F4B90 (void);
// 0x000003B5 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnLoginComplete_m00863213E57C474A2E390D209F34947D418B6F5E (void);
// 0x000003B6 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnGetAppLinkComplete_mE1702A430CE06B64C0F66B3F5C83AEC272C2EFE4 (void);
// 0x000003B7 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnFacebookAuthResponseChange_mD00FEAB1C3EAA1FB37CEEC598FED343BB22EEECA (void);
// 0x000003B8 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnPayComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnPayComplete_m1020BA3E1DC54A647F6793BDDA3A5413362AA0AD (void);
// 0x000003B9 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnAppRequestsComplete_m6EC2528B0F4DDEB2CFB7C71670E697ABE340F5AA (void);
// 0x000003BA System.Void Facebook.Unity.Canvas.CanvasFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnShareLinkComplete_m2D4D2BE7059EF66771028B963472337B07DE241C (void);
// 0x000003BB System.Void Facebook.Unity.Canvas.CanvasFacebook::OnUrlResponse(System.String)
extern void CanvasFacebook_OnUrlResponse_m2B7B05CCA4080B7E8B7A673C64AF50942423A463 (void);
// 0x000003BC System.Void Facebook.Unity.Canvas.CanvasFacebook::OnHideUnity(System.Boolean)
extern void CanvasFacebook_OnHideUnity_m22EF87949270FAEAE36CDEBD8C1460EACFF4A4E0 (void);
// 0x000003BD System.Void Facebook.Unity.Canvas.CanvasFacebook::FormatAuthResponse(Facebook.Unity.ResultContainer,Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>)
extern void CanvasFacebook_FormatAuthResponse_m374ABCB1718388EE260B20A584535D2BD13AFA28 (void);
// 0x000003BE System.Void Facebook.Unity.Canvas.CanvasFacebook::PayImpl(System.String,System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void CanvasFacebook_PayImpl_m41B3F794EF3A77DD8C4EFDC0ECD05FFB9F057646 (void);
// 0x000003BF System.Void Facebook.Unity.Canvas.CanvasFacebook::<OnLoginComplete>b__37_0(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_U3COnLoginCompleteU3Eb__37_0_m4884AECAD1936BCCD5FA80EE08412CD1ABD0843D (void);
// 0x000003C0 System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1::.ctor(Facebook.Unity.Canvas.CanvasFacebook,System.String,System.String)
// 0x000003C1 System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x000003C2 System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1::UI(System.String,Facebook.Unity.MethodArguments,Facebook.Unity.FacebookDelegate`1<T>)
// 0x000003C3 System.Void Facebook.Unity.Canvas.CanvasFacebook/<>c::.cctor()
extern void U3CU3Ec__cctor_mA7E3CE8F0906A85F32F56FD5AE6AFD9E7A7F7B45 (void);
// 0x000003C4 System.Void Facebook.Unity.Canvas.CanvasFacebook/<>c::.ctor()
extern void U3CU3Ec__ctor_m1E8C16CF2D75F07BF97D6DEE7662EF9F25C46246 (void);
// 0x000003C5 System.Void Facebook.Unity.Canvas.CanvasFacebook/<>c::<OnFacebookAuthResponseChange>b__40_0(Facebook.Unity.ResultContainer)
extern void U3CU3Ec_U3COnFacebookAuthResponseChangeU3Eb__40_0_m7E697C1D7FC82EFBC23D9C786801A9AAF56E6616 (void);
// 0x000003C6 System.Void Facebook.Unity.Canvas.CanvasFacebook/<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_m2FDD491E0781244FB3788B379FD9EE2B28E5CB7C (void);
// 0x000003C7 System.Void Facebook.Unity.Canvas.CanvasFacebook/<>c__DisplayClass47_0::<FormatAuthResponse>b__0(Facebook.Unity.IGraphResult)
extern void U3CU3Ec__DisplayClass47_0_U3CFormatAuthResponseU3Eb__0_m0CA3563740B3CEF72EB65CE4778927E747701587 (void);
// 0x000003C8 Facebook.Unity.Canvas.ICanvasFacebookImplementation Facebook.Unity.Canvas.CanvasFacebookGameObject::get_CanvasFacebookImpl()
extern void CanvasFacebookGameObject_get_CanvasFacebookImpl_m7D19E62CC7202410D1F7357B2A388357C65AD1E5 (void);
// 0x000003C9 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnPayComplete(System.String)
extern void CanvasFacebookGameObject_OnPayComplete_mFD9D763BED07E52A6A54F88B2CEAF98A97BF1757 (void);
// 0x000003CA System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnFacebookAuthResponseChange(System.String)
extern void CanvasFacebookGameObject_OnFacebookAuthResponseChange_mA57CDD8B45DB932A3E6B83AA95C93168B5916046 (void);
// 0x000003CB System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnUrlResponse(System.String)
extern void CanvasFacebookGameObject_OnUrlResponse_m2FBDA32B779C5A719F6676494D476F4398252F09 (void);
// 0x000003CC System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnHideUnity(System.Boolean)
extern void CanvasFacebookGameObject_OnHideUnity_m4B6D89636DCC628C9724429666B6499EF1C992B2 (void);
// 0x000003CD System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnAwake()
extern void CanvasFacebookGameObject_OnAwake_mE01DF7BD12ED924891EE4C46EFCFF1DBA1181F08 (void);
// 0x000003CE System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::.ctor()
extern void CanvasFacebookGameObject__ctor_mEEF1E3E0681D37F9260AE8BF89C8574F5BC188FD (void);
// 0x000003CF Facebook.Unity.FacebookGameObject Facebook.Unity.Canvas.CanvasFacebookLoader::get_FBGameObject()
extern void CanvasFacebookLoader_get_FBGameObject_mDE0DA5343A6359539803E21DA59582AC021C72EB (void);
// 0x000003D0 System.Void Facebook.Unity.Canvas.CanvasFacebookLoader::.ctor()
extern void CanvasFacebookLoader__ctor_m51722CD874D3E9AC5AEA30E7B99EFC177D2AC261 (void);
// 0x000003D1 System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnPayComplete(System.String)
// 0x000003D2 System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnFacebookAuthResponseChange(System.String)
// 0x000003D3 System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnUrlResponse(System.String)
// 0x000003D4 System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnHideUnity(System.Boolean)
// 0x000003D5 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnPayComplete(Facebook.Unity.ResultContainer)
// 0x000003D6 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
// 0x000003D7 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnUrlResponse(System.String)
// 0x000003D8 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnHideUnity(System.Boolean)
// 0x000003D9 System.String Facebook.Unity.Canvas.ICanvasJSWrapper::GetSDKVersion()
// 0x000003DA System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::DisableFullScreen()
// 0x000003DB System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Init(System.String,System.String,System.Int32,System.String,System.Int32)
// 0x000003DC System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Login(System.Collections.Generic.IEnumerable`1<System.String>,System.String)
// 0x000003DD System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Logout()
// 0x000003DE System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::ActivateApp()
// 0x000003DF System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::LogAppEvent(System.String,System.Nullable`1<System.Single>,System.String)
// 0x000003E0 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::LogPurchase(System.Single,System.String,System.String)
// 0x000003E1 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Ui(System.String,System.String,System.String)
// 0x000003E2 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::InitScreenPosition()
// 0x000003E3 System.Void Facebook.Unity.Canvas.JsBridge::Start()
extern void JsBridge_Start_mE05B0ADFC1D761CCB0BD6C73B5698E2587683D98 (void);
// 0x000003E4 System.Void Facebook.Unity.Canvas.JsBridge::OnLoginComplete(System.String)
extern void JsBridge_OnLoginComplete_m8B0C69F5F63F92CBA95891F4BFEDA4F136E46846 (void);
// 0x000003E5 System.Void Facebook.Unity.Canvas.JsBridge::OnFacebookAuthResponseChange(System.String)
extern void JsBridge_OnFacebookAuthResponseChange_mD61337B5A7F9678E77B5F09346082E76F8CBB079 (void);
// 0x000003E6 System.Void Facebook.Unity.Canvas.JsBridge::OnPayComplete(System.String)
extern void JsBridge_OnPayComplete_mCFEE1BFE93D3A69C40C59699BDD3B327ADBF125A (void);
// 0x000003E7 System.Void Facebook.Unity.Canvas.JsBridge::OnAppRequestsComplete(System.String)
extern void JsBridge_OnAppRequestsComplete_mF7CE386DCC45C6CA82B46CF9B5FB6DB7E65AABB6 (void);
// 0x000003E8 System.Void Facebook.Unity.Canvas.JsBridge::OnShareLinkComplete(System.String)
extern void JsBridge_OnShareLinkComplete_m54EDE0659C873FDDADE26C560C4DF494F91AC1EA (void);
// 0x000003E9 System.Void Facebook.Unity.Canvas.JsBridge::OnFacebookFocus(System.String)
extern void JsBridge_OnFacebookFocus_mFA58F06C9FD7D5EF62DF7907B2F7D0C341A3C24C (void);
// 0x000003EA System.Void Facebook.Unity.Canvas.JsBridge::OnInitComplete(System.String)
extern void JsBridge_OnInitComplete_m1234396209C8140801BC6471FA4F3B55A7FFEF93 (void);
// 0x000003EB System.Void Facebook.Unity.Canvas.JsBridge::OnUrlResponse(System.String)
extern void JsBridge_OnUrlResponse_m97C0335B24E973EFACB8B5797B5A711F5E352F50 (void);
// 0x000003EC System.Void Facebook.Unity.Canvas.JsBridge::.ctor()
extern void JsBridge__ctor_m3F577D8585FE744992C4940EC0C25C18301FECE4 (void);
static Il2CppMethodPointer s_methodPointers[1004] = 
{
	Json_Deserialize_m8A4873939A88E3C92D498920B3DA32DAB473C380,
	Json_Serialize_m7A5CC1F648D014786ECEB63ADF37FA2F87C1D1C9,
	Json__cctor_m86A124542557ED1615CA91D6B5F7F2A1E7676301,
	Parser__ctor_m3EAE969C4F18DF8C6C138B6EAE74E8847E0B0CFA,
	Parser_get_PeekChar_mB8C2C7F4878CEB598583D806CE0F110317E845C8,
	Parser_get_NextChar_m42CA3391DD63DE468017D36C83D836374DA2A11D,
	Parser_get_NextWord_mB3AD0907F66FD0330F9D3340DD9FB70F580AF4E0,
	Parser_get_NextToken_mC4877723D8402C82A77874CD29FB1A9B25677926,
	Parser_Parse_m38B0EA54936E41916AC7A4806EC7D92429468E81,
	Parser_Dispose_mF9C0AC9941324651E87EB630527F3D1915C6FB83,
	Parser_ParseObject_m49FE9211987CD1CBD7D473A2F25E5A7E52A5177F,
	Parser_ParseArray_m0ECC260EA4F69E154D9C4F428E0DE604D5C7F562,
	Parser_ParseValue_m97042849CCE35B06AF36216F9F269EEB663CC507,
	Parser_ParseByToken_m12B4B3A89B5DA35D60357C70FD9F1CD53B720F9F,
	Parser_ParseString_m4BA16C7B543BDB7E19825C33F5332A411D34E2B7,
	Parser_ParseNumber_m88A7B428BA2107FA5598FAF365CF8BEEF1F5CBC7,
	Parser_EatWhitespace_m06A3D69A6B90246196E2133ABF213E3A3F2C2387,
	Serializer__ctor_m4640B8CDB217BFF63F603993504CBB614490D094,
	Serializer_Serialize_m7EEFD2C8400A6682457C734866767E501A34E064,
	Serializer_SerializeValue_mB3DE0F17E69D42DE7E12C920098A03544067ADF3,
	Serializer_SerializeObject_m8BEA0ED5BA09CF5A40CE90BEDD925CE76B24EEF5,
	Serializer_SerializeArray_mD5CAD8CB4055BD0433F23F0AF75A95E50DDB3B17,
	Serializer_SerializeString_mD28BF565B12B827B5A303E60C59A05A178F02976,
	Serializer_SerializeOther_m6F0F9EB4C95A4F2A0F2856148CF0610B4955896C,
	AccessToken__ctor_m868A26BB81086128A8008C972BC9E9D68CDAF147,
	AccessToken_get_CurrentAccessToken_m68F8FBAE40A2C05C8085CA3DF3B4A30C2D59BB39,
	AccessToken_set_CurrentAccessToken_m63FA7CCAF4D748635020E08AB401E7350235F9B7,
	AccessToken_get_TokenString_m42882F2AE3BB424792602A331ED69BC42F7F9DCE,
	AccessToken_set_TokenString_m8574AD2AC5B429CFD539E1E880981E7113FB47EF,
	AccessToken_get_ExpirationTime_m57DC46687C401A994FD08179DE31115BF421965C,
	AccessToken_set_ExpirationTime_m7228C202A7E60D4C74FF7662139FF9B93561BCBF,
	AccessToken_get_Permissions_m22CD07DF3993F1890B400D017D8A251152DEC3AD,
	AccessToken_set_Permissions_m1C6A4706BE77DC950E06EA93890F407B71850936,
	AccessToken_get_UserId_m00A570C3A2C9E244DECCFD4EDB3319AF27B7F7CD,
	AccessToken_set_UserId_m3C7C0021EBB9F4E6890B24EFF97D9B614042F7D5,
	AccessToken_get_LastRefresh_mC60959BF2FAA8D09EDDE6D41C720DA3A0E4FAC75,
	AccessToken_set_LastRefresh_m4291CCBCAD1D47C532BE824BC98DD760F1E2E8B6,
	AccessToken_get_GraphDomain_mD2C34BB0FE21303125493F9DB5DB8B8409606186,
	AccessToken_set_GraphDomain_m8412CF6CE3A0DC3AA532985C523047169B5E3C78,
	AccessToken_ToString_m5C8986D79621CBFF34C2452F504442F117530B14,
	AccessToken_ToJson_m04EFC8D86152E0A4DA6FB9492049382F927643CE,
	NULL,
	CallbackManager_OnFacebookResponse_mD758F6BA1AFA4B9EBFED12C56E2A88B7F7484A97,
	CallbackManager_CallCallback_mA8C8B2E25F093F96C5E1E0B48A72B59D32FDB013,
	NULL,
	CallbackManager__ctor_m763C66B56D10819458B7FAAAA56F0609835CA615,
	ComponentFactory_get_FacebookGameObject_m5CBDF2CF11D38884E02214311AE4064A80A992CC,
	NULL,
	NULL,
	Constants_get_GraphUrl_mBDCB7D0383D19AD0992E4A3BC0B67CC39F51EEB4,
	Constants_get_GraphApiUserAgent_m580B293788FB75973B68E7D8833D70692DF82FB3,
	Constants_get_IsMobile_m186265C246D711A4AEBA58833C656492567572BF,
	Constants_get_IsEditor_mB569B724E025232BCB4C8994E10EB7DF4DA98111,
	Constants_get_IsWeb_m4029C384BFAAA99F8BF8F5C669C720EC9D6E1205,
	Constants_get_IsGameroom_mE062F17073791A2BBD8F3E93A4DD51DBF3F3192B,
	Constants_get_UnitySDKUserAgentSuffixLegacy_m4D71B001D94DBD109D9DDF4BD537D89206508D9E,
	Constants_get_UnitySDKUserAgent_m00D799BB1B54747B8D89677B8F367EAD5B14ABD2,
	Constants_get_DebugMode_m523E57985E18A348C01D589A2CD387AD945E7A99,
	Constants_get_CurrentPlatform_m79DC42780CD59233CFBF88661B0F3E93980CFA42,
	Constants_GetCurrentPlatform_mA9C24D84007393900A66F5F5A60B9F2AD4B711EA,
	FB_get_AppId_mDC50F9EEABA4ECAE2810B8DF3CF41A82D965F995,
	FB_set_AppId_m41A5BBE82AC85E920D1E25C8BC42CBFA8DF2980D,
	FB_get_ClientToken_m4548A64C0BD9CA828B448C84D86032BDC60C79C0,
	FB_set_ClientToken_mBFB85D294FF628674A38EB8094CE9E9665835063,
	FB_get_GraphApiVersion_mB9418431A4CD2C22072A25C63ED38910D5366E3E,
	FB_set_GraphApiVersion_m708B5205194016FDE8655351D923639F96689700,
	FB_get_IsLoggedIn_m1C3F0E0A4D2D8715E88D703D8B14856CE38C4B32,
	FB_get_IsInitialized_m226A5FACF50DCF9DD52D226118E653728C2B5DC7,
	FB_get_LimitAppEventUsage_mEA61CF1E4FC84AD3E5429876F9F6449CA4079F7C,
	FB_set_LimitAppEventUsage_m08C030622EEDB6B941BC39F5D494BD3942311579,
	FB_get_FacebookImpl_m9DDD32735B11AF92957E015A4FB4A70C79DA8B23,
	FB_set_FacebookImpl_m9ECE851AD12B9C3B950293000AAFF57B66239E85,
	FB_get_FacebookDomain_m48ED648280D35BF95A9163267EA26C9532928797,
	FB_set_FacebookDomain_m2805CB9A186C2F039ED32226079918081EBC9245,
	FB_get_OnDLLLoadedDelegate_m68AFB315121C26792E9F3289F97F5BFE10BC8378,
	FB_set_OnDLLLoadedDelegate_m40A415D10186935193B5E0EC343A4FE470CC78C6,
	FB_Init_m5A02A3E5BC5D5F6A4C62B8D263A38124EBF9A098,
	FB_Init_mEE0F3EC6A526B7E25CC022E753EB4F47DD73CA79,
	FB_LogInWithPublishPermissions_mD1592B197BA920DA4F2E3349FDB6724C62844D5D,
	FB_LogInWithReadPermissions_m96613A9D72165192B22C0F82BC1143EE927210C9,
	FB_LogOut_m0D25B168F57A13462B6AAAC91D7356FF4D87C788,
	FB_AppRequest_mC44637F293AC033A9BFD9F3ED87C7EA00FE4F137,
	FB_AppRequest_m3FE0AA3F6490736DB6B3CA06908FB7AE01D562E4,
	FB_AppRequest_m43363842AC368438E1D9C8EE6F91F04B989D205C,
	FB_ShareLink_m99C29A3AE0A5E08BEA9732E53EBC93928881BB81,
	FB_FeedShare_m4FC571ABEA1D35B5FCB3714DEE6902130D71B0A5,
	FB_API_m65BFFC9E9D451EFB81538A26FA221CCA2B6E2649,
	FB_API_m1B6845C837D836D59337ED16D723E00746AEA547,
	FB_ActivateApp_m1ADB1549DED591571736095BE961B44CECBC01CB,
	FB_GetAppLink_mA04CDA4C0EBD32B49888F2CD02DFE4118E55B183,
	FB_ClearAppLink_mF46B63A9C8A001A0B20400B94328A22F184A4E46,
	FB_LogAppEvent_m2D94462AAA8AEF7277BED399335572FC5BD439FA,
	FB_LogPurchase_mD3114E2A71063653FA968C36D30F5610D91B5592,
	FB_LogPurchase_m623F36D67FC9B24FFAA8270DF86010138A37B7AD,
	FB_LogVersion_m782E87C23A5C914B171A76951C2AF89BFAD625D9,
	FB__ctor_m1FEE8CECF7AB4EE3E3BE7DDDA5C5BEAB73355BC6,
	FB__cctor_m1A31BC451AE86EEDF2615C5631983EA5DACD834A,
	OnDLLLoaded__ctor_m72B76C3B929B38EB8184944717FD701C15DE3B11,
	OnDLLLoaded_Invoke_m3E5BDEB9570ED39708B6A06A680A4257963E4BCB,
	OnDLLLoaded_BeginInvoke_m11340471648103A288D7FC4587363355872F5240,
	OnDLLLoaded_EndInvoke_mAF6D67493AA53FF2BF91B900CA94E3E6539E0660,
	Canvas_get_FacebookPayImpl_mBA4900DC0F60567D8A7B9914628908A4FF678F61,
	Canvas_Pay_mC50F771C4A54D295AFD8F0FA62076E1767825841,
	Mobile_set_ShareDialogMode_m104137D266702C132CE01F700A6A67A780060CF0,
	Mobile_get_MobileFacebookImpl_m5E6A8CC1A63B65EC4C2244390E29F8E331193653,
	Mobile_FetchDeferredAppLinkData_mD21280CA395985D45F257025E76CB39AEDB2150F,
	Mobile_RefreshCurrentAccessToken_m8CFC22E8C68E570666309A0C18A55C786F852D60,
	Mobile_IsImplicitPurchaseLoggingEnabled_mA927ACFFF7E5BEF2DC35B4671E9ABB76FBF0300A,
	NULL,
	CompiledFacebookLoader_Start_m423FD3259B28B14A2D6D212098ACE84171D3E802,
	CompiledFacebookLoader__ctor_m68C8597BA48F90FB3D4AFFD66F9E8A4245D6762D,
	U3CU3Ec__DisplayClass36_0__ctor_m4F699BB647DFEBEA39ECA67ECB2E4518D0AEAB85,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__0_mBC4A9768979E3164490158FCA938EF8F31F7027C,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__1_m46947DCD1D39783A33A2F2AAE042941E508C02EA,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__2_m7849F2FE7E358D938290AF87AD77849111DCB928,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__3_m98E511AB15B5BF687F83644796C871A83407425B,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__4_m232FD2A6A1F1D12CEE743B12063B1C3C3AFEE4D6,
	FBGamingServices_OpenFriendFinderDialog_m5DD6C0955411778CF3E0D74878C25CB2BBFAF510,
	FBGamingServices_UploadImageToMediaLibrary_m3EE26DB2D18DFBD5849F5D70902D937A4468B0F9,
	FBGamingServices_UploadVideoToMediaLibrary_m269C3798B983ECFA1BEC25103D31E28767D70E01,
	FBGamingServices_OnIAPReady_m8813CE330E24341C9B743E4C5B1CD31A21574B50,
	FBGamingServices_GetCatalog_m2B3E4C7F66F56EEE476953FBEE411FBC9E89AE92,
	FBGamingServices_GetPurchases_mCB9F083309D7FF911BB4CC32537FDAB1BEE63210,
	FBGamingServices_Purchase_mD639AE3DC7AD7111805A9FE941F75C3010151EB1,
	FBGamingServices_ConsumePurchase_m282F1B8E82F9B65C3BDDC71D9E1CA5C9054CD6DF,
	FBGamingServices_InitCloudGame_m7AEDF5833930F5CEE740E0A7D943D1270FB5C319,
	FBGamingServices_ScheduleAppToUserNotification_m28595A8F944CA66A0F752EF954EDAE4CDC73A021,
	FBGamingServices_LoadInterstitialAd_m640EB91774097945028192F7F9BA6AB3022B8F90,
	FBGamingServices_ShowInterstitialAd_mADFF84923F9E66DC6EDFE4C0792B39E11B424327,
	FBGamingServices_LoadRewardedVideo_m18E8D0B4A9E423B1B125DCE18CAFCD2C9B6587D7,
	FBGamingServices_ShowRewardedVideo_m5F887E64B1564CA465EEE123F62EDC8D2C5555CA,
	FBGamingServices_GetPayload_m66F3087E3831BB2C39C209FDCFDECE58AAD3619F,
	FBGamingServices_PostSessionScore_m4BDABA0390EA639A0BA07016833A95B8F89973A4,
	FBGamingServices_OpenAppStore_mEDFD19A576679F0DBD00B3048FDD007875119DCD,
	FBGamingServices_get_MobileFacebookImpl_m5A8D530741D37D39BA4633292FF0922BAEE482EC,
	FBGamingServices__ctor_m3A77F49C599439C907F372B2A8FEFE727DD0DDFC,
	FacebookBase__ctor_m5F5458781CCBA9AFC2921FE6A22B33C9B0AF0E49,
	NULL,
	NULL,
	NULL,
	NULL,
	FacebookBase_get_SDKUserAgent_mDDCD16907E906D2DFF381CBF2FB06FE6F76DDE37,
	FacebookBase_get_LoggedIn_m33A7F0BA33E2577AB9E0E28790C9C53A9DD1C909,
	FacebookBase_get_Initialized_m5DC69D32E618C8B600A6FB619F1B6BEBC39C94C2,
	FacebookBase_set_Initialized_mBC6032B47F78047FFA549EA661CB0B4AB53D582E,
	FacebookBase_get_CallbackManager_m3A96D4B6632C96E2AD4CE94F6681DE16E2006599,
	FacebookBase_set_CallbackManager_m057CF6DD5BA2C75C269FB80148F40DCD76BBD967,
	FacebookBase_Init_m4B5F140C94F0F0FD114695EEA2D3FC4EBA09CC65,
	NULL,
	NULL,
	FacebookBase_LogOut_m9F8B5EF7A5B3D8C9980D74B03FE46F0E18EE5866,
	NULL,
	NULL,
	NULL,
	FacebookBase_API_m732AA2837BDA43339DC49987F08F6A7880B87CD9,
	FacebookBase_API_m5B6F889AB5586F55164655D5007A15676A0AD449,
	NULL,
	NULL,
	NULL,
	NULL,
	FacebookBase_OnInitComplete_m5EB5BFE3254C6419AC1D6D2B5B7186DB3534B126,
	NULL,
	FacebookBase_OnLogoutComplete_m6BC78C7BEEFC2B1CD5969B0AA413E0534D81B946,
	NULL,
	NULL,
	NULL,
	FacebookBase_ValidateAppRequestArgs_m2731AD5CE8FE6AC86F65BA6321F934A5E3AD0327,
	FacebookBase_OnAuthResponse_mD467E7FB31E8C0F26D9FC9357ADB8ED7A632E6FA,
	FacebookBase_CopyByValue_mD91FE265F3A6AA6644EB7C55D0AC6D4D6904BD08,
	FacebookBase_GetGraphUrl_m148C245B755FD97DBA264D3BF718BFB3DBCBA1BB,
	FacebookBase_U3COnInitCompleteU3Eb__35_0_mBCF8E6AFAB3EA568481D9333EAD1D5DE0E827713,
	U3CU3Ec__cctor_m3FD929F62DFC2F903AD7DEEB1729B094194E373F,
	U3CU3Ec__ctor_m8E17DA27E2486957C417694CF1C85C57C892AE20,
	U3CU3Ec_U3CValidateAppRequestArgsU3Eb__41_0_m358DE4B56E8939DF48480C0F177D8CB7E35FB9A9,
	InitDelegate__ctor_mB3E00CB4B2CEEC79F308B18FAB5E33E8AC545D1B,
	InitDelegate_Invoke_m5E0ED52D4D85D78000E3058FE7272076F04BD437,
	InitDelegate_BeginInvoke_m764FAE5D802F83DB32EA08055AADA71E48076986,
	InitDelegate_EndInvoke_m6FF9B83F97E46505746CDBE3EEEB30A1D8E0892B,
	NULL,
	NULL,
	NULL,
	NULL,
	HideUnityDelegate__ctor_m184BF575CF91A178A3FB5347E8210C294930DC49,
	HideUnityDelegate_Invoke_mB19B711AA04813ECBAF5F1B3BC233610774035E8,
	HideUnityDelegate_BeginInvoke_mF0552DBDC4CE2268B6ECA0531E6931BEF1925793,
	HideUnityDelegate_EndInvoke_m7120D8E66259DD68549E47534AE7B05CA4DB8DA3,
	FacebookGameObject_get_Facebook_m740762DE179046787FC0A7EA1394E6BB5D0AA509,
	FacebookGameObject_set_Facebook_m37C5BF1F3C26C5D1DD9735CC4E078ADAEFC79B1D,
	FacebookGameObject_Awake_m50A6BE0B373BCAEA23B014CD6C22042DEE44A699,
	FacebookGameObject_OnInitComplete_m9C488BB89093D10D70A6DEFB86A95BFBBF6DC892,
	FacebookGameObject_OnLoginComplete_m9EB8859D2D2E78C1338CEFFD60F6B5963B90D9AA,
	FacebookGameObject_OnLogoutComplete_m9EDB8A0D7197A07F1EB4736659990BF8A45EF6BA,
	FacebookGameObject_OnGetAppLinkComplete_mD046144760E8377905C12A9F0904F1898864F299,
	FacebookGameObject_OnAppRequestsComplete_mFBB2EB6CEABDB9AC0E372DDCD8DD2D9915F8ACDF,
	FacebookGameObject_OnShareLinkComplete_m28884FEE8B037721524CB0C2C791AEC2DA2C886A,
	FacebookGameObject_OnAwake_mBB85C524A54369A5B494CE5A84B31849D58A1936,
	FacebookGameObject__ctor_mE073FEAB06013691F9271009E05F7DE5B5BA057F,
	FacebookSdkVersion_get_Build_mC63F17DA631CAC852C8C7459F8961E2E02A4AFEC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MethodArguments__ctor_mD508C0DE76D61259DC9275B44C72919DE728BD66,
	MethodArguments__ctor_m4FD0284E8DFE50B1516702ED85FE32322490C6E9,
	MethodArguments__ctor_m041528F49BF41576BE25EB8558527844857FB733,
	NULL,
	NULL,
	MethodArguments_AddString_m21242D7EF4204EBE0D732B0A4A8D08AC54CF6042,
	MethodArguments_AddCommaSeparatedList_m8D5BF91A69F43705221FD94CB101A02F250AF478,
	MethodArguments_AddDictionary_m69CDAACA87D6C7A6B03E1C7EC91B9BA1A8FD8EFD,
	NULL,
	MethodArguments_AddUri_m740DF523B5B3EFEB5B0B55FD54A68D241E39B0F4,
	MethodArguments_ToJsonString_m69074422838A73AB3D489320B9CC01A35D263646,
	MethodArguments_ToStringDict_mB784F8FB8864320D721A8F629E5950413F2E9854,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Product__ctor_m23892D597887D74B45ED70677CE4426B94F5A24C,
	Product_get_Title_m3CCEEC601D3E3FD58085D42D119E2CD72214284B,
	Product_set_Title_mA00BBE60194B8031E8DC9795C5AAD7C478405407,
	Product_get_ProductID_m6D18E88F73E4DAC3CDE6CC39B8F1B95AA1B812DB,
	Product_set_ProductID_m17F36B75492158330E4B18ABD6A5FED2BE4DD056,
	Product_get_Description_m6203FA94ECD917A85941F5E2AEFBEB04995B101E,
	Product_set_Description_m52B3E12C5CFDB52A82F25733B2CF3A2B297E6AB5,
	Product_get_ImageURI_mFA357CFE65D3E3B575512089D2E5C96566BC981D,
	Product_set_ImageURI_m04B6F48A7F4ADB7287540E1256C7A8DDB504B906,
	Product_get_Price_mEB2AABDE5B502465B3B2D98DEAC634264DB79860,
	Product_set_Price_m6D902C4B4E14EB609882AB27184349A19EA19013,
	Product_get_PriceCurrencyCode_m4C82A2B515E6F73F138952CF270FF8B34B4F1AB2,
	Product_set_PriceCurrencyCode_m2E26A166A9B6FE42E0CAF3A9051AFF42789412EF,
	Product_ToString_mE5666C06E1BE83C7D5CB2042E2C6EEF84779F805,
	Purchase__ctor_m2F6D8D08B0CD45D3097181E71C5CB11FCC68B667,
	Purchase_get_IsConsumed_mBDF2802687500F1A175DAD081CC0AF850CD6F56F,
	Purchase_get_DeveloperPayload_m45989DD105423409B2B38375197D17EDD9425576,
	Purchase_set_DeveloperPayload_mA0D6444F2D600A519E3031A773F83E05EDE6385D,
	Purchase_get_PaymentID_mFA4AC208C695B7254510E9B7ECEF518D7E55F203,
	Purchase_set_PaymentID_mF5980DC233FEFA0428D8CED4F35DDFBC46BF64B4,
	Purchase_get_ProductID_mC6DA4ED2B9A12DE3653F1F86F04AA65128DC4163,
	Purchase_set_ProductID_mB8FADC80738017AB3FE268933F4912F7E4FBAE98,
	Purchase_get_PurchaseTime_m0C3548BF414916488397B6AD6A5B8F24EF74DB1D,
	Purchase_set_PurchaseTime_m0FF2CEA8C911631DD04E1F6787D60005E7B178A0,
	Purchase_get_PurchaseToken_mB931680FED62380B1EBFBABB2C003C26059B2374,
	Purchase_set_PurchaseToken_m0869B9434D1A7E7D98A55863BC7F8DC824E438F2,
	Purchase_get_SignedRequest_mA385E94DC9A29A558111601A4CAA9C3B34D1C1F9,
	Purchase_set_SignedRequest_m743796F39F3443AC9F43B6E84DCAD00E3FE42AD0,
	Purchase_ToString_m31D5D3478D351E820B09BAA3851314A9F4F1C70D,
	AccessTokenRefreshResult__ctor_m85D655868C8FF5C2A3336A4007A3FA0186C3BB2A,
	AccessTokenRefreshResult_get_AccessToken_mE6DB4980B770602E587E17E4F9C8995CA5762828,
	AccessTokenRefreshResult_set_AccessToken_mF8AB8ACBE83AAA4A609C913E535E59DBA6ABD24E,
	AccessTokenRefreshResult_ToString_m245B4A745E4282F9517B1957E8D09D3F91A96DC9,
	AppLinkResult__ctor_mB11A65A8AB654F69E173C0EA7D93C5B80432F5EE,
	AppLinkResult_get_Url_mBA3FBE84A4830A4B2B01E75074436694572678A5,
	AppLinkResult_set_Url_mF2F7158EC461D9736B9CAE3E9998621AC7D8B4B7,
	AppLinkResult_get_TargetUrl_mCA56B7E750E5CBC7F2872750A6010F7246AC9739,
	AppLinkResult_set_TargetUrl_m4C005ED078EF6E4596E17F8A360FA6AF2AD58D23,
	AppLinkResult_get_Ref_m30B8D8B6DEBC0742580F29FE2CD7538A39A3B58E,
	AppLinkResult_set_Ref_m34E37AD58AF1660CC70C39522218B04BC791B1A1,
	AppLinkResult_get_Extras_m2987F464221039109F018DF9BFD65452DFCEFEC8,
	AppLinkResult_set_Extras_mA2C8B74C9A4C6247E36C2B4AEAE2E69BFA7AA76A,
	AppLinkResult_ToString_m947B439123C9E5EE87BE3C0947B348C994F04A67,
	AppRequestResult__ctor_m80EC540B48D711BA530B8F8BB27622BBC341E426,
	AppRequestResult_get_RequestID_m0FB4B77116C02ABF8EC18E3618728BC610F079E8,
	AppRequestResult_set_RequestID_mE05C9016FD9B06C30F56F814FE0152AF8B4F8607,
	AppRequestResult_get_To_m37F76214768FC29EEC788C2304073F59F784458B,
	AppRequestResult_set_To_m574F932ABB3C52A0E79B9A4B6F4B288C1031A22A,
	AppRequestResult_ToString_mA4495333BA422CCFCDF8C6D7BEFB382DED578DB8,
	CatalogResult__ctor_m3BB1C94AB284C7F598285E846E885FB56ADA8E68,
	CatalogResult_get_Products_m7182B5FDDFB039C3A65E610E603BEB108AAD4492,
	CatalogResult_set_Products_m56CFF78280420A4520B2B5493D472E3242FBCA29,
	CatalogResult_ToString_mCA75D583B8B1EF13F33479E77D45257EA4D6E260,
	ConsumePurchaseResult__ctor_m9B155E16B3DF04C6CD3620E89EB5958B7AE127FA,
	GamingServicesFriendFinderResult__ctor_m443BC7ED4BECB5C73952009D056F594EC7C0D35B,
	GraphResult__ctor_mAAEF4E0D08FA61A3206CE4ED59898EECA9F66C32,
	GraphResult_set_ResultList_m895C92743657296186ADB66CEA4BBC5D487E1408,
	GraphResult_get_Texture_m52F3139402C071CB46BC9DC1590FDE6B996AC2D9,
	GraphResult_set_Texture_m45BF75B17E4FB6EF772BE00CEDD52CC3C45F464E,
	GraphResult_Init_mBF636DC5E6EE0B3FFEB9C04E01DFA1BA54FA3DF8,
	IAPReadyResult__ctor_mCF75DA82298C1C3708B068C8AB53FE87BBACEAC8,
	NULL,
	InitCloudGameResult__ctor_mC41318DE39283E2035E2F70FECCD23EC69364602,
	InterstitialAdResult__ctor_mE071017319C76EB374DE91C402442C371C82D6EC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LoginResult__ctor_m85276C08E1FB1B5EC6134BED2863F768F9C4FCE8,
	LoginResult_get_AccessToken_mAE248509070F413BD07740D74EFA1AB09C69965E,
	LoginResult_set_AccessToken_m019741BF9BB4EC4104805DC408D0F0E3437B0D47,
	LoginResult_get_AuthenticationToken_mD59EDC4AC3A0C458D1AC86535E07A971FE1BC02E,
	LoginResult_set_AuthenticationToken_m8EE993212DBD9D77DA84F75ADC5ED1368E1660BF,
	LoginResult_ToString_mC7A43B68C44BCAAED64003998FB58EAA4BCD5353,
	LoginResult__cctor_m057A12952360513145B64FCEAFAF4D80D54CE3E4,
	LoginStatusResult__ctor_m71D1B4FDD17BA8174089E50BCABE50B837FC888C,
	LoginStatusResult_get_Failed_m7E910019B793A2AE97C09E353E07CCAFF5198F06,
	LoginStatusResult_set_Failed_mAA0F6B0BA9EE37924C64E21610D3ADA9DEC93258,
	LoginStatusResult_ToString_m20597EFF6A48AFF1213CD5A6EA2B40CA6995C008,
	LoginStatusResult__cctor_m15D972708D3F6C6688751A309A860102FD46CAB7,
	OpenAppStoreResult__ctor_mF5F1196B685895BDB82B57C9D86B57B9C1907E83,
	PayloadResult__ctor_m691D4495B0FCEF40E7BED3C93F41E55363AB58E2,
	PayloadResult_get_Payload_m45F5E62B679A74AA935056EB36FCE93D2EF679D5,
	PayloadResult_set_Payload_mE6BEDB93CCB0B255C47BBC38E1FF1C67BE2DD385,
	PayloadResult_ToString_mA92958F0A0DB48EFA7999DBAC61BECAED1A50637,
	PayResult__ctor_m8E6F08D2B115AB10AF6F43B86CA10607776D2BCD,
	PayResult_get_ErrorCode_m94F77C8859FA562BB0281C0ACC6015184C9BAF4B,
	PayResult_ToString_mE4B6D445AE6638AF4D1E67FA41FC374160C32E4C,
	PurchaseResult__ctor_m87B784088DBA83E92FCDD326412C179857598815,
	PurchaseResult_get_Purchase_m227AF577E2F5EAD8C2475FF8C157F503A69F0173,
	PurchaseResult_set_Purchase_mF6DC16F48C0BAFC85181E26CB3DAA17DEB3A3EDE,
	PurchaseResult_ToString_mC6A4CB4DEA9A9B64923A15767276289BCDEE9BE4,
	PurchasesResult__ctor_m2F067E525ADD70BC70675729D490631201992AFC,
	PurchasesResult_get_Purchases_m52BA69945FED109ED71BFCC966E7BE9CD533D7AA,
	PurchasesResult_set_Purchases_m41CFF14B70FE8C8E855DF245D0859329660E2C66,
	PurchasesResult_ToString_mB15039FD72FA69BC768C92979F3C0E8DB2C7FD77,
	ResultBase__ctor_m5E12EFF38E77D6C2618264EF071B9946A07D2FA6,
	ResultBase__ctor_m3E05350CF01C733EE78E9910FFF0BC49153248A5,
	ResultBase_get_Error_mCC0909B7B88BA1EAC4175B6E13C86019F0B65D25,
	ResultBase_set_Error_mDE71B35D7C648815A395ACEACEFB2E1731692482,
	ResultBase_get_ErrorDictionary_m49CA2E69CD572D72EF9A38656ACF234C2B6BAAE9,
	ResultBase_set_ErrorDictionary_m1CA8FB49C25033B3B263CA80A75ADDEB5BFF1D63,
	ResultBase_get_ResultDictionary_mD20F414B2C7D529F85356ABE2283D2341B7AB5B1,
	ResultBase_set_ResultDictionary_m4241527675668D7BA49F868F0F9171CA2169A776,
	ResultBase_get_RawResult_mFFF2523AD1CDC61D6A1B22F27B72A42C6309AD2D,
	ResultBase_set_RawResult_m05E3C92A53BAC1479BF04DE3EF7F8E12C103A65A,
	ResultBase_get_Cancelled_mDD7F37569D6DBF9857CEDF7121022A84AB04DA62,
	ResultBase_set_Cancelled_mBF2A85838F542800DE8CE18B23CD96EEE09926AC,
	ResultBase_get_CallbackId_mD5740195C1CB90B66BF4AD373DE6541FB342D320,
	ResultBase_set_CallbackId_m6BD9FE80163FA42DA2F480244F9FEE7963E715F9,
	ResultBase_get_CanvasErrorCode_mA4234ACCBEA60FB65EB4840FA986C5ACEEAE8296,
	ResultBase_set_CanvasErrorCode_m8F1FA8B0AE7ECB1350BE35CA064DABE9C168E660,
	ResultBase_ToString_m62550CEB891F641D9052A52C5C707679EF5400C2,
	ResultBase_Init_mEBE2DBDE8088CBFB6E7FB38BB3FE5F5AA9DFA326,
	ResultBase_GetErrorValue_mA0CCC18CB71A02D745EF87157F946E67BDC7E61F,
	ResultBase_GetCancelledValue_m97BD8CAA2D2DBBCDC7DA608E0B39EC20BFF736AC,
	ResultBase_GetCallbackId_m09BEA21429814FB46C26D48011722151FED9E7CC,
	ResultContainer__ctor_m2D713E4783B2EE17A3FCE77BC85BD3AE9FE9DAA5,
	ResultContainer__ctor_mA77410DC3FC49A8E25BEB7A656C7A174826A042E,
	ResultContainer_get_RawResult_mE6B6064A98F862CF47C25572CDA1F841CA543016,
	ResultContainer_set_RawResult_mEDA77372AEB21EDAE82C78089DB9386094EF67F7,
	ResultContainer_get_ResultDictionary_m2D22D637E456BBFE03A180CE2F25EEF556698932,
	ResultContainer_set_ResultDictionary_m960C623B0C08F561E21E9D2AB45860FB7E841098,
	ResultContainer_GetWebFormattedResponseDictionary_mB8B35868B7283489FC8F66254D1CB41659BB890C,
	RewardedVideoResult__ctor_m4CA2DB81B09D68215D3A9AAF40940D18AE55E99D,
	ScheduleAppToUserNotificationResult__ctor_m813EEB92BCAFF10AF770602CB3E733AB928B99A0,
	ShareResult__ctor_mA64055E46B61193BB87068E62AE2B26270FBDEB3,
	ShareResult_get_PostId_mE57DC5964B14931326498A079D9750D3A3BA24AF,
	ShareResult_set_PostId_mE962B736189DBCF1D1DECCF30B661E5A7581BCF1,
	ShareResult_get_PostIDKey_m8353405F36BB63AD45B6103BAF312784FAAFF105,
	ShareResult_ToString_mBAB1FFEA0FCBDB2E0D1A4B01583651D59096C213,
	AsyncRequestString_Post_m3508D6AC1F863897396378699216DF1CD56C08CB,
	AsyncRequestString_Get_m7961B3941B966DC3A6E8BFED4E84A21D299ED3B3,
	AsyncRequestString_Request_mC7D709B6B3285FD732775F1A29A97611D4D94D8C,
	AsyncRequestString_Request_m4E8E7362ACAD642147D6503C333AC95771C9600F,
	AsyncRequestString_Start_mF290990673DEDF249E128201F16A3B149B4B85CB,
	AsyncRequestString_SetUrl_m6E79537333AE9E7F61EDB0FD48019AA771AFA82F,
	AsyncRequestString_SetMethod_m3DC09200EB5EDD98E44C916D099161BC13F7D14D,
	AsyncRequestString_SetFormData_m5EE6DC961CBEACB166445A9E9D3F97BD66314803,
	AsyncRequestString_SetQuery_mFAE0D2A7594D55C80241D7025302224A613C8A5A,
	AsyncRequestString_SetCallback_mD0315B82008E8E523D91DA4B8D3D3E3CFCD4123D,
	AsyncRequestString__ctor_mD3E8608FE8401CEC81E32FF9979692805F5CA950,
	U3CStartU3Ed__9__ctor_m91E8453F9A21FA4964B22F7E00DFBDBF320FE4B3,
	U3CStartU3Ed__9_System_IDisposable_Dispose_mAE3163411A60AF021C851814054C5C2CED63C843,
	U3CStartU3Ed__9_MoveNext_m19236F54EC8992E5C785CED4C274183B3119F922,
	U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE50330F88F71D02DB3524466A22E02125CCC3A27,
	U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m4AD7E69240BB16630A342A950E247C17CA66CA26,
	U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m9ACE793E1E1660527DB7FCF8B756ABA8CA6795B2,
	FacebookLogger__cctor_mD3887F56A7BD2BB21F373B8FB4DCC147E51689A8,
	FacebookLogger_get_Instance_m0A479DAE88A8B5FE53DAC0FCEC98D50EA647CBCF,
	FacebookLogger_set_Instance_m828EEDA7C96A62FE2D525A006751147505C24264,
	FacebookLogger_Log_m43CAF4597811AC44ABAC437768AC3F4D7A0696D4,
	FacebookLogger_Info_mF7A1BBED758AED0EBE12365426DB0D394424A947,
	FacebookLogger_Warn_m8F5C4F8D8944929A8A6D550209A207D118A92046,
	FacebookLogger_Warn_m9A0E2D966A5C4A77930B8DFDB927F46E52D24485,
	DebugLogger__ctor_m584004A91D03D3110D75C9D052A30C8761E49DD0,
	DebugLogger_Log_mDDD540D8A0CDA94FE8E628967CAB47976AB2A9F5,
	DebugLogger_Info_mFB3C3C6AC4027F5AF8F87CD06E044F75F8BE7328,
	DebugLogger_Warn_m58A99DF6AD66480BAC6F18D4D91B7AAFACFF62E6,
	NULL,
	NULL,
	NULL,
	NULL,
	Utilities_TotalSeconds_mA49A914FAEFFBA5834F1FB7B6EF3E0D67BAE38A5,
	NULL,
	Utilities_ToCommaSeparateList_m3925082326445282086770A30C42E9CED61FE092,
	Utilities_AbsoluteUrlOrEmptyString_m716B9F8FDA319A4FA6E1E5774D0F9DC5F6FC68E8,
	Utilities_GetUserAgent_m675C4889732616346E67C628F97B8AC369FF0587,
	Utilities_ToJson_m0F09663A04FA1B2674B2E0D177A132C429D7171E,
	NULL,
	Utilities_ParseAccessTokenFromResult_m07AB0E9CC6F0963AA4F9C2EFC1790B7FAFD1FD25,
	Utilities_ParseAuthenticationTokenFromResult_m021290683967FECFBE7B168598ACB60DAD55136D,
	Utilities_ToStringNullOk_m3894054C9E1D3C06482160DD8AD34E18D478730B,
	Utilities_FormatToString_mA85A14D511A2C0AA815D5046ACF222C2BF1B7388,
	Utilities_ParseExpirationDateFromResult_m28AF7A3EEA902C98623138DE37E608F554C7EED2,
	Utilities_ParseLastRefreshFromResult_m1548D2EFFC28EFC662B07D6C1BC88332D9160226,
	Utilities_ParsePermissionFromResult_mD7E2E68C2D3795CCB744CAD73F41675F5B98DD8A,
	Utilities_ParseCatalogFromResult_m9E0D7BE8527F7B6BFF51AA88AD1CD5FA389C287A,
	Utilities_ParsePurchasesFromResult_m2BB331D4FA2F57DA465B7D3CE07C3825F22F3893,
	Utilities_ParsePurchaseFromResult_mE96C30E2EE9E3B82773184357FEF9AAEF779AD17,
	Utilities_ParseStringDictionaryFromString_mF9A13BF3A0FCBDB7036721CAE4B925CFE6182AF0,
	Utilities_ParseInnerStringDictionary_m1CF34CC2930297E03F1922442DE189D70568956D,
	Utilities_FromTimestamp_mE8D881324DA6DBCEF2954A79E578F04B84437D81,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m0FF4B018807197B4DAFC4A47B33BE53CD248FB66,
	U3CU3Ec__ctor_m13112A068F52B52E15586E59A13FB09E6EB7D27D,
	U3CU3Ec_U3CParsePermissionFromResultU3Eb__19_0_mC90F5C78EB6264F11F0CFBF2E943C4C78EA9F512,
	FBUnityUtility_get_AsyncRequestStringWrapper_m39D1630078EFD96CD6921AE348F263B373AADDF9,
	AsyncRequestStringWrapper_Request_m40B8CAA59C930106522ABFCACD626FABCEB98286,
	AsyncRequestStringWrapper_Request_mC76363AF290872D609EEF729C07C7E00746DB331,
	AsyncRequestStringWrapper__ctor_m2FD885515CC1B65FFF6B6388CC06332B694C77FA,
	NULL,
	NULL,
	FacebookScheduler_Schedule_m618AF75D838CF13B9D89105E8760ECF338F29427,
	FacebookScheduler_DelayEvent_m2E4BE1C92A4D3BDB717A8431851E9F53A091742F,
	FacebookScheduler__ctor_m7A9D976933EA4156ADD5D7554C9B8B9848D538A7,
	U3CDelayEventU3Ed__1__ctor_m38E0B6E65329104B15319617C622D64F4404F92C,
	U3CDelayEventU3Ed__1_System_IDisposable_Dispose_mCA96EF4F77E659BC5E04A98B17FA8F5DF9A628BC,
	U3CDelayEventU3Ed__1_MoveNext_m8AD01F44CDFE46644DB388E062AC5A2BA6B889EB,
	U3CDelayEventU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66F8B4754D0F53AFB18E324FE98F53B2B7AFB418,
	U3CDelayEventU3Ed__1_System_Collections_IEnumerator_Reset_mCC535E07AE97CD37C62153C1E756FE5FD55FDBE8,
	U3CDelayEventU3Ed__1_System_Collections_IEnumerator_get_Current_mF83DB1DF0B887609F322379D06749009AB0BFE56,
	CodelessIAPAutoLog_handlePurchaseCompleted_m7BFBFC8B4C7EA38AB4A6B7DB14AA3B1565198C90,
	CodelessIAPAutoLog_addListenerToIAPButtons_mF998CEAA3FC76F58CA9F55DA4A2129ABF98BE931,
	CodelessIAPAutoLog_addListenerToGameObject_mEE84C26CE19440744D6806976E98AE32A5E450DE,
	CodelessIAPAutoLog_FindTypeInAssemblies_m58DF0E54F78158C075E0886AA90D46798F433945,
	CodelessIAPAutoLog_FindObjectsOfTypeByName_mF943C5AEAC7E1610028544E00002D63A7D2689E0,
	CodelessIAPAutoLog_GetField_m2F30B99D73BB5096A60124A9DD9F75990528B9D4,
	CodelessIAPAutoLog_GetProperty_mE14CD2C5FFA67850F75416B4C0665A1DB68AAD15,
	CodelessCrawler_Awake_m79E667ACA71DCFCE59C7CC403663C20FE7BB550A,
	CodelessCrawler_CaptureViewHierarchy_m6B4F987BADE0EB6BC0C3ECA1DDC8E0604908A93F,
	CodelessCrawler_GenSnapshot_m117B4518BE22E76B45A71850C0272F0272A440C5,
	CodelessCrawler_SendAndroid_m17E9238FD99CCB645F4535AF573DAB72E4DC37C1,
	CodelessCrawler_SendIos_m850CFDD71622E2E6054817533E5F6FA5C718697A,
	CodelessCrawler_GenBase64Screenshot_m89669CE2A4967F7436868EF3D09EFCF075EDC4CE,
	CodelessCrawler_GenViewJson_m69C7A401E1659BBF383130167D64BCE1792BCB4C,
	CodelessCrawler_GenChild_mEA81BD2454F1E222D1A92A9851AFD4849E7849CE,
	CodelessCrawler_onActiveSceneChanged_m317AE3FD644B3E61D5DA80AA273365E958CB53FF,
	CodelessCrawler_updateMainCamera_m8BE3DC22CFB7DE0C9F24293DC580987EE8E024A7,
	CodelessCrawler_getScreenCoordinate_mDB8D9B138EC5B37642511256C9E1C4A6B93FDB37,
	CodelessCrawler_getClasstypeBitmaskButton_mCF199713431C6260B8BDF3A4B42D4782F2FFA880,
	CodelessCrawler_getVisibility_mB79811B9A8DE402CD51E54B375D8CE22013202EE,
	CodelessCrawler__ctor_m1222ABFCEC83334B91A27739101F8B78344E3403,
	CodelessCrawler__cctor_mA68B072F4ED51A1B9F3C09027096751928F9CAD3,
	U3CGenSnapshotU3Ed__4__ctor_mC9ECD537808627E62E02D894E4FB96F7E78545F2,
	U3CGenSnapshotU3Ed__4_System_IDisposable_Dispose_m80935841B7055D1AF032835FC1BF8DEF0D6867F0,
	U3CGenSnapshotU3Ed__4_MoveNext_m16E0D888991BF7B231BA04C43F44F9780EBCCD4D,
	U3CGenSnapshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0C2F1C8A514CA6D3F1E580DD4521E47FF72BB25,
	U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_Reset_m31E52EDA4D8A81C47C4ECED631B7EC48384A7FB2,
	U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_get_Current_m9BB9ADC38539B8FE9900FBCBA85818870B5E2428,
	CodelessUIInteractEvent_get_eventBindingManager_m09E20CAC00FEFF3D70AA3A0982A408B0305AD9BB,
	CodelessUIInteractEvent_set_eventBindingManager_mFA2C3761A6E7FF12C7DBD697FFF200459CCDA9A4,
	CodelessUIInteractEvent_Awake_mC7D8CFC60C1B3164D7A858B16215B48634100ABC,
	CodelessUIInteractEvent_SetLoggerInitAndroid_mB99B35324357A509641B5FB32AD1BAD363D7194C,
	CodelessUIInteractEvent_SetLoggerInitIos_m869694270A2763A3C106AB554934ADBF89DC7D0F,
	CodelessUIInteractEvent_Update_m23BB03FA167B66B34927AA6CAE6DBD85966FF75D,
	CodelessUIInteractEvent_OnReceiveMapping_mE4676BA6BA86DB6AA0FC2E3AF721FA77D505C3A8,
	CodelessUIInteractEvent__ctor_m5F4E625E7C9CD5B9AE3E1670CF8F43F3B51191C5,
	FBSDKViewHiearchy_CheckGameObjectMatchPath_m565090E33BEBD2EA5A1D8823DBB47BB7B19A815A,
	FBSDKViewHiearchy_CheckPathMatchPath_m862127A4751D0D5F69DC54099E0D4CA844A345B9,
	FBSDKViewHiearchy_GetPath_m135E286F0053F0AD0CE1D60CC49C041281FCF3AB,
	FBSDKViewHiearchy_GetPath_m363AB73243E422E012D3917313569CCB86D2E8D1,
	FBSDKViewHiearchy_GetParent_m7ED6F4EFA6D5C600645A34662593385D80C7810F,
	FBSDKViewHiearchy_GetAttribute_mEF4401A542B3015E1DAC1AA4C1CAE43BE6E8F125,
	FBSDKCodelessPathComponent__ctor_m67D851D4687D3A2789973A32E6231B20BBECD742,
	FBSDKCodelessPathComponent_get_className_m54C63AD717ACDC4692ACC6DE94804F877B55DC6E,
	FBSDKCodelessPathComponent_set_className_m3CD005890C7841B28A175D32951B2CDEB5BE3AEC,
	FBSDKCodelessPathComponent_set_text_mBA33C1DB8F9456C8071B2199CF5937AD68B48742,
	FBSDKCodelessPathComponent_set_hint_m504833A7DC08EA59526D8E6F37A4FBA8EB23E866,
	FBSDKCodelessPathComponent_set_desc_mECBECABCFA864C2C07BCB75612AF71B9DC374981,
	FBSDKCodelessPathComponent_set_tag_m4712160C1725335A11F346A575FCCCEE2B095F82,
	FBSDKCodelessPathComponent_set_index_m1F0A3266005D13D0138D95BD9D1277D47BDB0584,
	FBSDKCodelessPathComponent_set_section_m5C12C24C6D15A68DC696F89881FA07393D4DB5AD,
	FBSDKCodelessPathComponent_set_row_m55AAA375912C9B25A334BE09A078B84F0D75F857,
	FBSDKCodelessPathComponent_set_matchBitmask_mBF4D4DAD0701174DD4657A83807584EC5F16C77C,
	FBSDKEventBinding__ctor_m9839B6846EE1B62343BB77C5A94559A36BB25604,
	FBSDKEventBinding_get_eventName_m663422CB21B0DAF91675DE2D6877078B51321884,
	FBSDKEventBinding_set_eventName_m84494EBB6BFFF6C9DC0741E6D10C4040926B476C,
	FBSDKEventBinding_get_eventType_mBB79F9BFE1BE931019D05F072537ED5EFBF522E9,
	FBSDKEventBinding_set_eventType_mE488464891341EC1396E33ECB22F7C33DB1BA9B2,
	FBSDKEventBinding_get_appVersion_mBDFA265D6A3311D6D1E18CAC8CFC3130905F45B8,
	FBSDKEventBinding_set_appVersion_mE6157B6AA510EB7499DE7F53C497A6CB5504CB2D,
	FBSDKEventBinding_get_path_mC979F6275E5CFA7F97AB4A63C27FDF4D6F6C89A5,
	FBSDKEventBinding_set_path_m1F283930D25BE4B316173A0595F503AC33B98C61,
	FBSDKEventBindingManager_get_eventBindings_mD03FDCE6DFA0E75C406989DC46F5C75CB653A000,
	FBSDKEventBindingManager_set_eventBindings_m27574887FFAC62D99CF0A359B6B691663C7F1249,
	FBSDKEventBindingManager__ctor_mBB09B4D710437D1616F3C8890FCBD561CA1BCF60,
	MediaUploadResult__ctor_m682318B1599ABE994FAB654F2CF4E95069A15B14,
	MediaUploadResult_get_MediaId_mB577FF8F14B267504DDE51320F9ECD51EC30CEB6,
	MediaUploadResult_set_MediaId_m74AE625BED87E80F418CBB1F34CE134591121CB0,
	MediaUploadResult_ToString_mBAFC619ADF28E3D81E8F1BA90797C646A95E28A8,
	SessionScoreResult__ctor_m5981BD56F2AAF4D85752607BAB658B4A79CEC96C,
	AuthenticationToken__ctor_m582B8C72C50CD0B411857B7570F8AE7A908811B3,
	AuthenticationToken_get_TokenString_m0FF339C87DB816343FB1B683AB499061E98E9749,
	AuthenticationToken_set_TokenString_mD671893C5AAAFF41B601435982FA9BC9CAD921C6,
	AuthenticationToken_get_Nonce_m3D2F427FC8C38DFB78B4D8426A47068DC8B66538,
	AuthenticationToken_set_Nonce_m9010ECD323D9E5FA397A0092FC63FE3F6DDD2CDB,
	AuthenticationToken_ToString_m5D04203FD03220C42D69A9FF7C0D70268EBC4BA6,
	GameroomFacebook__ctor_m395C1063B36776E683508D5304802564655AECEA,
	GameroomFacebook__ctor_mEE88A6B713759347370AC9DE8A95A2C46F89E98A,
	GameroomFacebook_get_LimitEventUsage_m983B24C2E6A386AC28B5694C4B7B2AB54EBCC556,
	GameroomFacebook_set_LimitEventUsage_m290ADD7D2C3467390F8F05BC9EBB7E1B07C7B3F8,
	GameroomFacebook_get_SDKName_m75155617153F455C5F10F77B7D7595D26EC92440,
	GameroomFacebook_get_SDKVersion_mA236F5B3B26830E98E9CC62FAD00F9FCE9199749,
	GameroomFacebook_Init_mF299CD4BBDE79E1B6B203B134EE102E6D6B61D0D,
	GameroomFacebook_ActivateApp_mCAE26855B3F08C404594CA7F949C76330A359C8D,
	GameroomFacebook_AppEventsLogEvent_m2C48316A206932AE71CF99D6C82BE6DB6698A339,
	GameroomFacebook_AppEventsLogPurchase_m0785A1AB79989706B17CC44B09BA3E14560C3C87,
	GameroomFacebook_AppRequest_mECCE93E426EC641229CAEAB2F795B54B3210E451,
	GameroomFacebook_FeedShare_m4ECE4E08B3D267B2B88A33EF7D10B2CA1EB85C25,
	GameroomFacebook_ShareLink_m15C80AF88399C4E3D7FC475F01632BEBFA14BEA8,
	GameroomFacebook_Pay_m7197B0E337E0A110D9436C86D69BF189D0BF4461,
	GameroomFacebook_GetAppLink_mD88E333019C913E87ED7A7D5453A0E3B9B93A405,
	GameroomFacebook_LogInWithPublishPermissions_mDA3567F57F869A946BF6D5ED644E57048BB9FC3B,
	GameroomFacebook_LogInWithReadPermissions_m1554C36F994862C5A1A9EC385F4FC9C75BD0FC07,
	GameroomFacebook_OnAppRequestsComplete_mD901A737B47B266D5C34B313DF5EDA4D1156CF80,
	GameroomFacebook_OnGetAppLinkComplete_m3EABE4875EE59194FB16ECCDBDE2EB2CA04979ED,
	GameroomFacebook_OnLoginComplete_m228ABADFAA8B392EFD7C3A793B519142A8BBB5D7,
	GameroomFacebook_OnShareLinkComplete_m66AAEF0681DB726C57037A577492DF44B265E987,
	GameroomFacebook_OnPayComplete_m69AE7C644152916123B0B1A5ED69EC3ED94749F9,
	GameroomFacebook_HaveReceivedPipeResponse_mD73834BCEA3A7FBB49592B6A28C9A0C61CC112AD,
	GameroomFacebook_GetPipeResponse_mE146FCD5F6A63E588C1BBE407CC4E3BC5249EB1D,
	GameroomFacebook_GetGameroomWrapper_m1DCCC3CBD2EFD09A89D345F5A36A003A113D75EA,
	GameroomFacebook_PayImpl_m9D915F7DA5551C3ABE0B771F33179F6702B9A440,
	GameroomFacebook_LoginWithPermissions_m6F1EEC2B916850BC3571DF437812BB61DF3E8CA8,
	OnComplete__ctor_m592C850930D3FB8B8E60074CAFCCFAF39133422D,
	OnComplete_Invoke_m7B870E9DEB6798DE433A37102B2F65737DCD9AEB,
	OnComplete_BeginInvoke_m2F9D8949A8658B37123DDD5405BF08C822688DF7,
	OnComplete_EndInvoke_m2BC26756B4FED6B0E54DE0ED5697EFA3CD7B0EAD,
	GameroomFacebookGameObject_get_GameroomFacebookImpl_m7588AFEAE6F993114A0FC8985F3E9C35D1C7820B,
	GameroomFacebookGameObject_WaitForResponse_m1BF6EBCCD0ADF829882E9DBE9881EFAD6D84CE6B,
	GameroomFacebookGameObject_OnAwake_mB0FB2026E6A6D7664A0572C1467468E1E1848A5A,
	GameroomFacebookGameObject_WaitForPipeResponse_m60E36429F3ECCEA9D2A0595114F4705BD7171F72,
	GameroomFacebookGameObject__ctor_m818C3C48493B7D02E738B4143AD9D5FD61E1220F,
	U3CWaitForPipeResponseU3Ed__4__ctor_mB6833EEC4A53FC762AE059C6BF00332A93A417D1,
	U3CWaitForPipeResponseU3Ed__4_System_IDisposable_Dispose_mE78F1C9BAA388D0BC289EEBDF468E596E2D2F82F,
	U3CWaitForPipeResponseU3Ed__4_MoveNext_m78B590361D10E8B1DC95049836A0A0DE022D6A44,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73EC4ACF1CB79293E941E42E754B5134478B3880,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_Reset_m3E3A3AB0086839F189D4F6BACB48B6C87E4E62B2,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_get_Current_m3B4DCFA7715371F1A514BCB9A973661D05E977B1,
	GameroomFacebookLoader_get_FBGameObject_mE8D0F8C640453C4C17857611C0C9C77A03C6D416,
	GameroomFacebookLoader__ctor_m5D25161FD90B80E35E65D7CD39F8A7E0F2CEE1A7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EditorFacebook__ctor_mC895ECBF5B4D00AC57B7BC4E823FBD35D666FA47,
	EditorFacebook__ctor_m2FE82AAB9C00188B8C55713F1052774F0E77B893,
	EditorFacebook_get_LimitEventUsage_mE539027EAEE9B6EAEF31BA24D1B6A2A69D8F937F,
	EditorFacebook_set_LimitEventUsage_mA0FB2C32D5CBEACBF2237C08D140FA71EE18AE98,
	EditorFacebook_set_ShareDialogMode_mFE8F73284D51D0505B5D18595F77A5FB5F7A803E,
	EditorFacebook_get_SDKName_mAEEE49D0CCF640723AE5D21E2B9A0C38F307C85D,
	EditorFacebook_get_SDKVersion_mC3E3D2A980ADA6817304A2E018357CE0D9088D41,
	EditorFacebook_get_EditorGameObject_mA58F715E4A6CAED7BAD7D6855FD01346A734F0AB,
	EditorFacebook_Init_mB2976CA743D62F2A18A0F616DFF3F060278CD8B6,
	EditorFacebook_LogInWithReadPermissions_mDDCE9AC38C7F6A48DEF4A6BBC9F05C013822F6F4,
	EditorFacebook_LogInWithPublishPermissions_m4FB1364B65BA5E531D0F5C780040AD5E720E4A11,
	EditorFacebook_AppRequest_m46F273516AB3B87DD26016C151DBB06168ABDD1C,
	EditorFacebook_ShareLink_m925FCAA68F89FF7150377BD1791AB2EFE34E33B3,
	EditorFacebook_FeedShare_m1CCD4CB562EEF11BA2CFF338C7744C35586ABC08,
	EditorFacebook_ActivateApp_mD59EF3CC68C02C05258CB29F9D659D6C79EBD5A3,
	EditorFacebook_GetAppLink_mEF8F98C659A1990D5F6B5C395566F33E35E98BE3,
	EditorFacebook_AppEventsLogEvent_mBB14C3701038A0BCDB8283201AE7B84FA8832853,
	EditorFacebook_AppEventsLogPurchase_mDFFACBB5F5F1D8E6F449E219342B9EE8E968FD58,
	EditorFacebook_IsImplicitPurchaseLoggingEnabled_m690B471A2E0F8F5DF275EF0C57800A789A303343,
	EditorFacebook_FetchDeferredAppLink_mA5CFF5DDE754C6701E22703AE53E2D973D82C203,
	EditorFacebook_Pay_mC40C763950E7687983A2EBDCC28782A6DC88B25D,
	EditorFacebook_RefreshCurrentAccessToken_m460E7A0F63DE4395F614B476E4B9B9F9D46FDDB9,
	EditorFacebook_OnAppRequestsComplete_m78B4742D83B8BBA5BF9697E273CC6D4AE5BD7A43,
	EditorFacebook_OnGetAppLinkComplete_mBEBC20C5673274711DDBA664DCF5F9A3E8A18C58,
	EditorFacebook_OnLoginComplete_mD4CA8F12364E752EE54627CE82E28377963862C9,
	EditorFacebook_OnShareLinkComplete_mC3843A3C0EA1273F35B36FEA6A8C71D337F255CC,
	EditorFacebook_OnFetchDeferredAppLinkComplete_m25C10BAC2166C3C67284EAD2DC77827C0271DB04,
	EditorFacebook_OnPayComplete_mF25D6608769632C843AB30BEE384EA2AA3EA9877,
	EditorFacebook_OnRefreshCurrentAccessTokenComplete_m497E48E473600186DA0523CEBD925C78935E8DDA,
	EditorFacebook_OnFriendFinderComplete_m0CA10896F756D9F0BFF51F8ACFFDDF8534B508EC,
	EditorFacebook_OnUploadImageToMediaLibraryComplete_mF1C4CC30008ED30EE4308AD1D74AEC6CDB044F9D,
	EditorFacebook_OnUploadVideoToMediaLibraryComplete_mB806E7A01CDD452B1723FBF094B35E08F21BDEF3,
	EditorFacebook_OnOnIAPReadyComplete_m892842F21427BFAD91FC82DC32DF7948F5E0445E,
	EditorFacebook_OnGetCatalogComplete_mBF069DFAD7556FCAC5A12746DABD9C4C9FFF645F,
	EditorFacebook_OnGetPurchasesComplete_mF6F163D55A69D0B302D9DEBA9DC1EE9B357385F8,
	EditorFacebook_OnPurchaseComplete_mFC9B13A3243ABDBC626484CA7793608AAF49DC75,
	EditorFacebook_OnConsumePurchaseComplete_m3CD40A4162B0F70D011D6F6B978D9DECACD94888,
	EditorFacebook_OnInitCloudGameComplete_m932D432B306395B3ACB4644A266336BB5B745379,
	EditorFacebook_OnScheduleAppToUserNotificationComplete_m90EDD41D3455590EE9B8B779736FAC3CA2D5B3CF,
	EditorFacebook_OnLoadInterstitialAdComplete_m8DED1A00390D7BB9625759531A3B90091841A567,
	EditorFacebook_OnShowInterstitialAdComplete_mD42DF902FAF16BE11480F8797B86C2311B55378F,
	EditorFacebook_OnLoadRewardedVideoComplete_m994AF993F3198EC1EC4314AC0607B958165F84F1,
	EditorFacebook_OnShowRewardedVideoComplete_m19BD197620A16A77ADABB3D8FFA966F9A16EA2AA,
	EditorFacebook_OnGetPayloadComplete_m1B586208132A365A51944ED8C19155A225BB30F1,
	EditorFacebook_OnPostSessionScoreComplete_m1A9D7A012296D45376AE8B956DCEFC4027D0DF21,
	EditorFacebook_OnOpenAppStoreComplete_m293C9F26F6800278DE9190D1C31CB3F5080028CA,
	EditorFacebook_OpenFriendFinderDialog_m06223420B105EE32F2F451DECC1E01B921CD385B,
	EditorFacebook_UploadImageToMediaLibrary_m45D940444AAB90B1587153E7A2B2EC78D3B72633,
	EditorFacebook_UploadVideoToMediaLibrary_mF89A565B5674CC6582A8DEA98CBF2E8014444F19,
	EditorFacebook_OnIAPReady_m296CACA177A58EF5A41D4A93748D229AFBBDF2E8,
	EditorFacebook_GetCatalog_m5F4C793CAE2EBA62D09684D95AEE512A19AA4026,
	EditorFacebook_GetPurchases_mA89DEEA13BBBFABCF97196DBB98A08E961C4B6AF,
	EditorFacebook_Purchase_m27DF85A8128C05E8032EC3AC17C487A570A4528F,
	EditorFacebook_ConsumePurchase_mFE8928DED9834E0D8BAE1E792B045FCCB66F3AB8,
	EditorFacebook_InitCloudGame_mC9B99D3470B22DE1AC7FBA7EE00E9821FC83463C,
	EditorFacebook_ScheduleAppToUserNotification_m390D70AE2CE6F3C3AE9BF7F3C8F26A0853DDB7FF,
	EditorFacebook_LoadInterstitialAd_m770301BC0B48D39A8B9FB9FB00840EE06DE16B0D,
	EditorFacebook_ShowInterstitialAd_mDD1DE90B5601FBE5022E50D27C74E630A60642C7,
	EditorFacebook_LoadRewardedVideo_m5EAA7A6CE71F3B4F2A74CBE93FF03EFB05667C59,
	EditorFacebook_ShowRewardedVideo_mBAB7ACCE57765DAD54C4B707B0729BA76EAE6183,
	EditorFacebook_GetPayload_mB98598B925C5FC36A0D901B43C3FE13896E8F46D,
	EditorFacebook_PostSessionScore_m161BD8C601949C06523C2E9653D45284114E502A,
	EditorFacebook_OpenAppStore_mF1E6E16E07CD36E53D695ED18898D2FC1A41C220,
	EditorFacebook_OnFacebookAuthResponseChange_m57A9AADC351896646838E876258F2456857103CA,
	EditorFacebook_OnUrlResponse_mD55F513A73B5E00228FE1D8D704A00E8212AAC6F,
	EditorFacebook_OnHideUnity_mE534BA2EBCADD2D90760005A8D454F1436473313,
	EditorFacebookGameObject_OnAwake_m2BF5E0B070B9D35884DB89BFA6474EEA8226B69C,
	EditorFacebookGameObject_OnEnable_mB17830D27B22100CB8C1D044743785B0E149236A,
	EditorFacebookGameObject_OnSceneLoaded_m09088F002D47A461FFF8FB25EE8D8853B708150F,
	EditorFacebookGameObject_OnDisable_m3F661E00E0ADF66DB2C1E27D8ECFECB1493C2D1A,
	EditorFacebookGameObject_onPurchaseCompleteHandler_mEB4ADD0AA0BB95CD554F6A3A2A0A37E675B29458,
	EditorFacebookGameObject__ctor_mD5293F581AADCE22CF77F4A19C779C298EB0F6E0,
	EditorFacebookLoader_get_FBGameObject_m98D9ECB51D29D7EC21E097416938F95B9D891C12,
	EditorFacebookLoader__ctor_m892AF329E3C043EA49799F2AF7F1D65DE83947C5,
	EditorFacebookMockDialog_get_Callback_mB980E4C2DE7886FE9AE6A473BD25FB8F09EC593A,
	EditorFacebookMockDialog_set_Callback_m79EDB0181B2B739751E3F32E8B39063260115BA5,
	EditorFacebookMockDialog_get_CallbackID_m2F061BD27D5E62C9042B4D8FF694157E3BCC7698,
	EditorFacebookMockDialog_set_CallbackID_m56FE30D393DFCF716A4E8F8FCC649B23682DB85B,
	NULL,
	EditorFacebookMockDialog_Start_m369BC020C1217375866F568D83C79DDFBE06D9DF,
	EditorFacebookMockDialog_OnGUI_m6C7CCC61196CF8FB64056DAA1970338D34FF3EE8,
	NULL,
	NULL,
	EditorFacebookMockDialog_SendCancelResult_mEBF09B15C0D9B89F73670A1FEB84F6B45B54EF33,
	EditorFacebookMockDialog_SendErrorResult_m49E2BACBF8EF8A6A0EB5B918801E02D7BD7BDE3E,
	EditorFacebookMockDialog_OnGUIDialog_m06B3E9378723D879EBE811403EAB1CD8CB5FBB67,
	EditorFacebookMockDialog__ctor_m8A46CB5EC10FC2A5C52CC50876C54233BDC1DCD6,
	EditorWrapper__ctor_m28D73B3F264605D24C6AB46013591850382B5AE3,
	EditorWrapper_Init_m86F33B25805654D4B06F2CFE918CB6D208B240FD,
	EditorWrapper_ShowLoginMockDialog_m087DC20EA3284A62E4D49331AC83B86A66976C2F,
	EditorWrapper_ShowAppRequestMockDialog_mA8E1889FFEE5CD1E7BBED8A2A6871980E7FE2F38,
	EditorWrapper_ShowPayMockDialog_m139D15090785678CE4EDB901CD5068A3AFC7E1C6,
	EditorWrapper_ShowMockShareDialog_m7938F185A0910D46FE40CC274D3D580730789F4F,
	EditorWrapper_ShowMockFriendFinderDialog_m31A7DC713AB387CA7AB83DB10FB0D299F0275E64,
	EditorWrapper_ShowEmptyMockDialog_m792E4B91828419122F29B8C3C27F7E0D40E7CE34,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmptyMockDialog_get_EmptyDialogTitle_mBED1FE8DF023CBE518BD3B3FF8175EFB8E73B917,
	EmptyMockDialog_set_EmptyDialogTitle_mFC70B0CB05F99728777D5B1160C9EA23692CC7CB,
	EmptyMockDialog_get_DialogTitle_m8D4CE5B93823A10FD24392DA2EF87F03582D92C8,
	EmptyMockDialog_DoGui_mDF02D9CBAC325FC4351F84FA49170362EEF8EB4F,
	EmptyMockDialog_SendSuccessResult_m74F914A713B334A1AAAE7013FB91CB6A8D30A7EC,
	EmptyMockDialog__ctor_m22D56D1D778B280D1469EC7D6D8F230130808F3B,
	MockLoginDialog_get_DialogTitle_mC4724DC7C102E31F073295F0AFA061F962C3CE6C,
	MockLoginDialog_DoGui_m7E0CFA8B5C29A44E4E805ACA738A9FAC2C7F8DFB,
	MockLoginDialog_SendSuccessResult_m6C272B967106871686CA2EB795489A1981373C59,
	MockLoginDialog__ctor_m5A78B4F4A738966F4F21ADF1408FE945B608F220,
	U3CU3Ec__DisplayClass4_0__ctor_m334657C5725829CE27CC4ED1C0F37D701EF503FE,
	U3CU3Ec__DisplayClass4_0_U3CSendSuccessResultU3Eb__0_m4548AD0B00D23DC7920E53BD24D0962B8E0C7BB8,
	U3CU3Ec__DisplayClass4_1__ctor_m933B6982D96CBFE524A0CE9C56A0E48E9724850F,
	U3CU3Ec__DisplayClass4_1_U3CSendSuccessResultU3Eb__1_m3A3F9E05E2DE31629E6D77CAF9E9421D9CC054D7,
	MockShareDialog_get_SubTitle_mB9F0FB095CD3B3C5933B84E627CDF5D9EF7F4CAD,
	MockShareDialog_set_SubTitle_mB4D39F272D2A770A4D657B1B39B0FE5BA6A8431B,
	MockShareDialog_get_DialogTitle_m14F7665EE1C32E00D998384DEDC1C9F5E1E3A601,
	MockShareDialog_DoGui_m77A4240FB9B63D73F448649802198EDB3966F0EE,
	MockShareDialog_SendSuccessResult_m9FC3E55AA2A436E613FF8131D0AFADF51E85B53B,
	MockShareDialog_SendCancelResult_m084AA0EFA215A51C7D79A07FEE1FDBC4F02C5AB7,
	MockShareDialog_GenerateFakePostID_mABC6DC88EB31173960710BD2A57B61001BA42680,
	MockShareDialog__ctor_mAD8420899B316E8E94001CF617ED334D4DDB0898,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MobileFacebook__ctor_m3A59FE7FD3E2006C5160940FC49B3EF5F857066E,
	MobileFacebook_set_ShareDialogMode_m0C1939A04C4F38E8B8F65827051820C6F4980AA9,
	NULL,
	NULL,
	NULL,
	NULL,
	MobileFacebook_OnLoginComplete_mC2E1C6514504CABBBA15A90B750226BEA84D6EF6,
	MobileFacebook_OnGetAppLinkComplete_mE09DF4EDA5F11D23C14809A245D68EC6B0CA1737,
	MobileFacebook_OnAppRequestsComplete_m4E1708FDB9E2AC680A885F0E81DA1AB04BF16B8F,
	MobileFacebook_OnFetchDeferredAppLinkComplete_m02E22D30B8B3F6FE7EFFFB786196B091C1D9D25F,
	MobileFacebook_OnShareLinkComplete_m71F8C6C4203BC96679C5460996145BC318D6E67A,
	MobileFacebook_OnRefreshCurrentAccessTokenComplete_mC5ECAC7647A0A7C2DE7F22B6F9C460B3132DB75D,
	MobileFacebook_OpenFriendFinderDialog_mA3238547742AB678BAA887351ECF937CDE11EE43,
	MobileFacebook_OnFriendFinderComplete_m626C33379170486AEA2E34B59773AAC79A76C212,
	MobileFacebook_OnUploadImageToMediaLibraryComplete_m086A54D5A8DEBB010F0CDCA106B667DEAB6F51FF,
	MobileFacebook_OnUploadVideoToMediaLibraryComplete_mD9CCE6E2A2C459F9B7B71A97A19EC8D0BAF6D468,
	MobileFacebook_OnOnIAPReadyComplete_m377ACCA6CA40A9C8F4E1BDA978671C04DE08A027,
	MobileFacebook_OnGetCatalogComplete_m58E624DAEB759FC2745AD3B69414B4653A709273,
	MobileFacebook_OnGetPurchasesComplete_m47A229404A1A8FF01900458690F14C3CA7B3F56B,
	MobileFacebook_OnPurchaseComplete_m26E970FE60ECAECD7286F389B4BCA0C2063E6D29,
	MobileFacebook_OnConsumePurchaseComplete_m8D2EB7D176B3A2BD8FB56FAFFD393B0F03BB5042,
	MobileFacebook_OnInitCloudGameComplete_mC42D55F7D7D0A6B483FC6A993D4AA0303023B701,
	MobileFacebook_OnScheduleAppToUserNotificationComplete_m03A02BAF150A4287F5336F698008CD2DCE7D1929,
	MobileFacebook_OnLoadInterstitialAdComplete_m19E022189D1630A90488E594D10025DDDDA40C91,
	MobileFacebook_OnShowInterstitialAdComplete_m58CCF38D37F44B85B3DBDE3E2D6327663AE39807,
	MobileFacebook_OnLoadRewardedVideoComplete_m9EFF13380B64589719BDB1C90013733AF69EC23D,
	MobileFacebook_OnShowRewardedVideoComplete_mBC634D8C917CCF287FEC7BF1666AFEE53EC9FE1E,
	MobileFacebook_OnGetPayloadComplete_mFEC699267699E7FC2562BFEC04C5B57D75DC32F4,
	MobileFacebook_OnPostSessionScoreComplete_m450564D3AB404D90F7623570D14BF9866D9EC124,
	MobileFacebook_OnOpenAppStoreComplete_mD2A07711BBDEAB30DF98A9421944F9336FE17858,
	MobileFacebook_UploadImageToMediaLibrary_mD1322C750B48D7723FEFEF24C29B7C7068D3F585,
	MobileFacebook_UploadVideoToMediaLibrary_m24D85E29396D40D88D44A0BD4F13BE627B4A4F35,
	MobileFacebook_OnIAPReady_m683832CBD8300205907D50232B35191347537F01,
	MobileFacebook_GetCatalog_mC266DE9EFCE0DE278B9713D384BDDD699A8C8340,
	MobileFacebook_GetPurchases_mF9E6C292DA2CFA014B8FC0FCCEF9BA01384BEB66,
	MobileFacebook_Purchase_m0A51FB493721DA84D4D7DD552688D983A8F3BEE6,
	MobileFacebook_ConsumePurchase_m876E0E0E3E5F5696C2985C8505F067235FE5760D,
	MobileFacebook_InitCloudGame_m7F549AB5428AE0AC82860B4BE52413F1A5A0C5B9,
	MobileFacebook_ScheduleAppToUserNotification_m60BC9BEF24FFC569D1975FB968F328D18E167FC0,
	MobileFacebook_LoadInterstitialAd_mA872513BFF29238974E533541EC75EE822624491,
	MobileFacebook_ShowInterstitialAd_m20A743A8AB56BCA9AE229117D9F4217D4BF3B7AB,
	MobileFacebook_LoadRewardedVideo_mB9257EAE9B89E618AB9F9586793EC8C1C09B8449,
	MobileFacebook_ShowRewardedVideo_m313AB0E5C45FC15F47FC0E175CA701AFECED52E9,
	MobileFacebook_GetPayload_mF110D04F8C577B29C3FD5E86E74C2DC9AB10E6EC,
	MobileFacebook_PostSessionScore_m257FA9D84B65D67E0126ABDFDDD836DCE6E6040D,
	MobileFacebook_OpenAppStore_mFD7C4397E722EC3EC034C29A9D096FB6E478A147,
	NULL,
	MobileFacebookGameObject_get_MobileFacebook_mA97444E3D77017BA5489E5470A2E121B5CD55D28,
	MobileFacebookGameObject_OnFetchDeferredAppLinkComplete_mE11CC37B0BD5530D16C88F7BFB870F545AAA0382,
	MobileFacebookGameObject_OnRefreshCurrentAccessTokenComplete_mBB94038603B430A43F7CA07FEB641CF35F2FAD4F,
	MobileFacebookGameObject_OnFriendFinderComplete_m1F7496E68F10067E631DDDB5D04FFE9F67E219D5,
	MobileFacebookGameObject_OnUploadImageToMediaLibraryComplete_mAB689BA1DD00D712ADF9553FB5CFE451EA845FF5,
	MobileFacebookGameObject_OnUploadVideoToMediaLibraryComplete_m349469B4DCEE43567994CBC3ADB595FC867C5C90,
	MobileFacebookGameObject_OnOnIAPReadyComplete_mF898C209F8E6ED875550118083BF10F3D566D957,
	MobileFacebookGameObject_OnGetCatalogComplete_m3EAECF41AFB2D3044148F71A9D2FD252249F15D6,
	MobileFacebookGameObject_OnGetPurchasesComplete_m6D4DAFF42761B0C72386CC3EF17F7C5CEA644CB4,
	MobileFacebookGameObject_OnPurchaseComplete_m4B47F45F9936CA9FE84A5FC2BC0B0000165B6390,
	MobileFacebookGameObject_OnConsumePurchaseComplete_m8C6E2EB2741A8E1A33692DDFB8214A4C8A1D2E31,
	MobileFacebookGameObject_OnInitCloudGameComplete_mB44D2AADF3B584138F6A2844A883F828EF0292FB,
	MobileFacebookGameObject_OnScheduleAppToUserNotificationComplete_m396FCC8BC23E30C4CD4CC4D75D8B6A1AD50967AA,
	MobileFacebookGameObject_OnLoadInterstitialAdComplete_m28A22C166D382D8FDD6AAFDEC37C7AFD68FCA37F,
	MobileFacebookGameObject_OnShowInterstitialAdComplete_m64902E980B06499086A54198C8D2B4E7FF795174,
	MobileFacebookGameObject_OnLoadRewardedVideoComplete_m80241068E26FAB35B0685BAEC5CD69A819D3BC51,
	MobileFacebookGameObject_OnShowRewardedVideoComplete_mC5B03B4BFC4A05676C4693F15763AD488932455F,
	MobileFacebookGameObject_OnGetPayloadComplete_m27FADDD99C24609F322033481CA5A656B57A5E9B,
	MobileFacebookGameObject_OnPostSessionScoreComplete_m8C611B9F486BA8FD81D819B5CAC24093E4C83992,
	MobileFacebookGameObject_OnOpenAppStoreComplete_mC20F40C9A2A527DC42E3145039897695B570C6B5,
	MobileFacebookGameObject__ctor_mDAB316B7660F03139CF9A9E30545F3B6939A461A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IOSFacebook__ctor_m67943C9D01260F1DEC7EF2BB3FC93DEE5BEB2177,
	IOSFacebook__ctor_m7F17D11D3AF8645BA7AA7507D358A4033F41D564,
	IOSFacebook_get_LimitEventUsage_mBF549BEB68B2BD767FFA6165A78833E7A4551CDB,
	IOSFacebook_set_LimitEventUsage_m60A37224769BC1942981285864DD2CF9B8F4058E,
	IOSFacebook_get_SDKName_m870BC8F1EE06DBA8C2F731328676BE54120E60AA,
	IOSFacebook_get_SDKVersion_m6752E514687D2D9F15C65EBFE992680E9CCB575C,
	IOSFacebook_Init_m77C41B0995508935908947E81CFD3E2A0A97A29A,
	IOSFacebook_LogInWithReadPermissions_m1633E33F011F266E665AC5D38CD7BEC9993F6BC6,
	IOSFacebook_LogInWithPublishPermissions_m732FCA79064E4E4884B658208F170857587F05B4,
	IOSFacebook_LogOut_m6539436DA8EEC710FA5ED68872517059A473D27C,
	IOSFacebook_get_LoggedIn_mE2934BB7E9EA4F16B5D18D769F02543263A9EBAE,
	IOSFacebook_CurrentAuthenticationToken_m150F5D259224EDE684BCE337A759C668282333F9,
	IOSFacebook_AppRequest_mB29DD50E3D9A62867251AB48722C173D32EF294E,
	IOSFacebook_ShareLink_mFF3639DCD597FF14A59DCA98C925D46BE908373A,
	IOSFacebook_FeedShare_m3AC5FD0C77703A88D1BDDDB90691512C3B295CC8,
	IOSFacebook_AppEventsLogEvent_m71CBAD70D6C9E9D48E20D6CF0F5FD6F21632DB45,
	IOSFacebook_AppEventsLogPurchase_mD32B798B7A73388E830CDA082DE31D0249F89BB9,
	IOSFacebook_IsImplicitPurchaseLoggingEnabled_mE039213CD95A517BF32699840B73A1846646E245,
	IOSFacebook_ActivateApp_m555BD849D1A35B8726BA8F9619A06604392FE5A0,
	IOSFacebook_FetchDeferredAppLink_m2BFB122C2C092902246B0275369DA3EBEF12DBC0,
	IOSFacebook_GetAppLink_m3412ED4F59A236F9D8518AFE0D38DEE0ADB05347,
	IOSFacebook_OpenFriendFinderDialog_m0CACAA50B6AAE93E32C867A01C5E0BC08E8B6628,
	IOSFacebook_RefreshCurrentAccessToken_mE94B2165E86149D53632EF4D3C494EFA1CA800F3,
	IOSFacebook_SetShareDialogMode_m04FA4185F2321A0E11B8A1BAF03438D0A8A0111F,
	IOSFacebook_UploadImageToMediaLibrary_m4AEA69C1B067B024604F241FB712F54253B7FB28,
	IOSFacebook_UploadVideoToMediaLibrary_mE0C410BE7FE660F864D10EB1EC91C8E427C0DD5E,
	IOSFacebook_GetIOSWrapper_mE30F0CFFDE57F641649BC4D0EA8F60E139DE2BFA,
	IOSFacebook_MarshallDict_mBB7F8CF57D6649DD0CFF4832886A40D68D49D731,
	NULL,
	NativeDict__ctor_m5E29B0F06531EB01D2755928B8BFB4AE60FB248B,
	NativeDict_get_NumEntries_mD08372040D670F3B2E8DAFB666908C69494614E2,
	NativeDict_set_NumEntries_m964DA95E7E5391D1EAA539A665BD8CC3EFF7A187,
	NativeDict_get_Keys_m6791D63A565CFEB36B174D049CE45127815BD3C2,
	NativeDict_set_Keys_m8815A402AA77D607854B7056654D93CB32A085F9,
	NativeDict_get_Values_m52424E4E1B8CAF63C8DA1AA7DF11E1F2D7C522FF,
	NativeDict_set_Values_m165D1C1EDF6B83713A73DF8BC8E52E958BDF86DA,
	IOSFacebookGameObject__ctor_mDA597F941109F6DEE3D363F3D3B3EB42E3682CE7,
	IOSFacebookLoader_get_FBGameObject_m4C574D00396A998BCC12A0C730EA6907DDED05D6,
	IOSFacebookLoader__ctor_m9FCE25D7B9DCAA60BF5E15118FBAE2B01A5403E9,
	AndroidFacebook__ctor_m3670A637434B95F4E24956001C1F3555F1D1F938,
	AndroidFacebook__ctor_m8797EBBBD9D5411CCC885AC4BF754A7D8B4FED8D,
	AndroidFacebook_set_KeyHash_m15258DF6E2CE5A72887805F580CECF7CD737BA01,
	AndroidFacebook_get_LimitEventUsage_m468C50D9060DC2ACFEED68430BA34B7CDC07EB9A,
	AndroidFacebook_set_LimitEventUsage_m31117A074D556FC5882BC3935D446E5646D744B6,
	AndroidFacebook_get_SDKName_m653D3A561F385535389A2D508A0862AAA0C83252,
	AndroidFacebook_get_SDKVersion_mA7F1CFB6E4A7C4CE821A970B3D03EB6CD5739D5A,
	AndroidFacebook_Init_m59E2FCF14391556A4D8A5691370B082F83EE8896,
	AndroidFacebook_LogInWithReadPermissions_m9DBEB4CA9C4E77D6FD09D414CE11110A809C60E7,
	AndroidFacebook_LogInWithPublishPermissions_mB49269D1824EE89A9FE13FDC4918131FDA37AF20,
	AndroidFacebook_LogOut_m417B71776C7B3E41941D7FCBEF0ACEC0A2ADAB42,
	AndroidFacebook_CurrentAuthenticationToken_m67B75339334A3AA452D5750E004BC73970A192E8,
	AndroidFacebook_OnLoginStatusRetrieved_mA67750D682778DD84E794712229C6D8EE2517439,
	AndroidFacebook_AppRequest_m3002DA04736125A5DD3BEADCB761954E2B2043BB,
	AndroidFacebook_ShareLink_m49F30222882F74578C5A532A75C3C359E13275B6,
	AndroidFacebook_FeedShare_m6CD3A7EDDC8ACF5B32B193D257C121508A45EF6C,
	AndroidFacebook_GetAppLink_m81177A729A37521E30F139D9E9C45B3C5CD197CA,
	AndroidFacebook_AppEventsLogEvent_mBBEB32EDF7F74DB0132C7D91071C788C5F619229,
	AndroidFacebook_AppEventsLogPurchase_m83E3C2978CC496574FF3683E33FA44EB1BFE8665,
	AndroidFacebook_IsImplicitPurchaseLoggingEnabled_mF59F636DF33CE9E58571E0D80CBFC0A01628E3F9,
	AndroidFacebook_ActivateApp_mE19FF59FA4DB069A44939A19F21E6D9601C64F48,
	AndroidFacebook_FetchDeferredAppLink_mB222CF5E53BAA394F9049D5CB93C17C49C2384C6,
	AndroidFacebook_RefreshCurrentAccessToken_m68A309C8FD782B7AD2C91DB2A661AC54D9D44944,
	AndroidFacebook_OpenFriendFinderDialog_m624F14812094E91A7C5B699941B5677BDF65A81F,
	AndroidFacebook_UploadImageToMediaLibrary_m2142BE6C6B6539905191031D42070638DCAB551A,
	AndroidFacebook_UploadVideoToMediaLibrary_m2219968A7BEDCF5CFADFF15A44E21992295AA980,
	AndroidFacebook_OnIAPReady_m8EF0424FD603C0AA5075294573B4980AD0AE8E76,
	AndroidFacebook_GetCatalog_mD98354DA0554D38655FC82BEBD2E2AD8F782FA1B,
	AndroidFacebook_GetPurchases_m270B1E530CB6B451B13D6EFBE628455DCC82BF04,
	AndroidFacebook_Purchase_mEF18CF6E51EB0E84725D07751B0A21EF3AC0B280,
	AndroidFacebook_ConsumePurchase_m7A70C61B1982196160D935E8F85978F549BC010A,
	AndroidFacebook_InitCloudGame_mBED6DCD1AEDE1F41139951826E3F1FE2432511FC,
	AndroidFacebook_ScheduleAppToUserNotification_mA2FEA52C62DD05C4B5A061820DDF88A2857C8232,
	AndroidFacebook_LoadInterstitialAd_mF7E73077959FB2EB2A89FA55300F87371F88535B,
	AndroidFacebook_ShowInterstitialAd_m462A7FBB53C5463BCC8320C6F045C37CDE04605E,
	AndroidFacebook_LoadRewardedVideo_m0A5384F94B156292ACAFAAAB23FFA9FDA50CBD29,
	AndroidFacebook_ShowRewardedVideo_m4499EB54AEE330ECEEB176263390C96A23757437,
	AndroidFacebook_GetPayload_m0C3144612071DF508C1ABA72F1FE60BC46EA4AC1,
	AndroidFacebook_PostSessionScore_m6439E2821372FD2334A380EAE78EC899AFDDB8A7,
	AndroidFacebook_OpenAppStore_m8E442AA9203E2A3EFE7077FBE34FB5BAB386E7A6,
	AndroidFacebook_SetShareDialogMode_mEA41EE9EDA48CC7DEF99795CAFEABE9D77911B0E,
	AndroidFacebook_GetAndroidWrapper_m6D70DA270BBA55DE6D43AAA1A11A8464D9148F98,
	AndroidFacebook_CallFB_mEFBA9B6E2ADBE5FA52130BFCC8344035071DA4F4,
	NULL,
	NULL,
	AndroidFacebookGameObject_OnAwake_m602A1414C05540213995C7EA5ED223CADB2A1875,
	AndroidFacebookGameObject_OnEnable_mB4DBD2CFB073C064ECDB9CCCABF32D93F3515868,
	AndroidFacebookGameObject_OnSceneLoaded_m404AD8E0F1F30AE688D8652FF77995239DA53511,
	AndroidFacebookGameObject_OnDisable_m8ADA1051965DCFF709052978D5A77D261A263DFB,
	AndroidFacebookGameObject_onPurchaseCompleteHandler_m81DB4C8D88E2182469DCA140C1F6879EF2111B97,
	AndroidFacebookGameObject_OnLoginStatusRetrieved_m0ABD92E79E0A3F6B049BE36FF3BE37AD2871A457,
	AndroidFacebookGameObject__ctor_mBC46954F350AC6D449BB4CDBED8A6DD85A562022,
	AndroidFacebookLoader_get_FBGameObject_mEFDDF78F9A30BCD5F96A154FA4E92E16D1779FB9,
	AndroidFacebookLoader__ctor_mDA110EEE32F120E15DBDEAF16971F226BFE65A2E,
	NULL,
	NULL,
	CanvasFacebook__ctor_mF5071236474D18C1EEB1EE4A2632C7E27A023C1E,
	CanvasFacebook__ctor_m3DEC7ED9BE8ADA51D2E1D7AE8982B798F44800F9,
	CanvasFacebook_GetCanvasJSWrapper_m050AAD4D4D867F982F790039DDC8B3B351A46659,
	CanvasFacebook_get_LimitEventUsage_mB265FDF5F0E5215548A9593C43ADAB62142BD53D,
	CanvasFacebook_set_LimitEventUsage_m28EFAAF8B4917567E5824048435B3E52E469C8FD,
	CanvasFacebook_get_SDKName_m6D59E8324BBC43E1A74454DBEF239C46F87664DF,
	CanvasFacebook_get_SDKVersion_mA74BC052CF22EDB90B24DB71FEAFDDA0B975651C,
	CanvasFacebook_get_SDKUserAgent_m5F5965391D99B17789D23F8E8A01B66601CF3136,
	CanvasFacebook_Init_mCD5F942DFDE55A89D30D7A8527661A910FBDF294,
	CanvasFacebook_LogInWithPublishPermissions_m54F3508A3B0C643E26ED774E273707D289A0C420,
	CanvasFacebook_LogInWithReadPermissions_mC62E0A5F68554005E80772285904CB152E5393E1,
	CanvasFacebook_LogOut_m0E27A0429F9F83E4BFB7ADC1346FA0528102D520,
	CanvasFacebook_AppRequest_mB516DED31E849E005CFBC29A1C56846D70FF2D68,
	CanvasFacebook_ActivateApp_m888D5E16A44DD6FA3A60E4FCF905531B9A37683C,
	CanvasFacebook_ShareLink_m071F87BC1FD5342B43F18339315022CF49FAE019,
	CanvasFacebook_FeedShare_m013A8BA1C67BDD84F81538A3D41D53BEA1FF1A21,
	CanvasFacebook_Pay_m2EC43A1BD4F5A9866A98ACABECB777B7BF9A2B07,
	CanvasFacebook_GetAppLink_mC7766021EADBED1B0C7E4ABE8DF1349983E09E9F,
	CanvasFacebook_AppEventsLogEvent_m27F5F9190D138214109023FDCB8FDEA19E5CA737,
	CanvasFacebook_AppEventsLogPurchase_mF67D924531E629698173BFB024509315F57F4B90,
	CanvasFacebook_OnLoginComplete_m00863213E57C474A2E390D209F34947D418B6F5E,
	CanvasFacebook_OnGetAppLinkComplete_mE1702A430CE06B64C0F66B3F5C83AEC272C2EFE4,
	CanvasFacebook_OnFacebookAuthResponseChange_mD00FEAB1C3EAA1FB37CEEC598FED343BB22EEECA,
	CanvasFacebook_OnPayComplete_m1020BA3E1DC54A647F6793BDDA3A5413362AA0AD,
	CanvasFacebook_OnAppRequestsComplete_m6EC2528B0F4DDEB2CFB7C71670E697ABE340F5AA,
	CanvasFacebook_OnShareLinkComplete_m2D4D2BE7059EF66771028B963472337B07DE241C,
	CanvasFacebook_OnUrlResponse_m2B7B05CCA4080B7E8B7A673C64AF50942423A463,
	CanvasFacebook_OnHideUnity_m22EF87949270FAEAE36CDEBD8C1460EACFF4A4E0,
	CanvasFacebook_FormatAuthResponse_m374ABCB1718388EE260B20A584535D2BD13AFA28,
	CanvasFacebook_PayImpl_m41B3F794EF3A77DD8C4EFDC0ECD05FFB9F057646,
	CanvasFacebook_U3COnLoginCompleteU3Eb__37_0_m4884AECAD1936BCCD5FA80EE08412CD1ABD0843D,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_mA7E3CE8F0906A85F32F56FD5AE6AFD9E7A7F7B45,
	U3CU3Ec__ctor_m1E8C16CF2D75F07BF97D6DEE7662EF9F25C46246,
	U3CU3Ec_U3COnFacebookAuthResponseChangeU3Eb__40_0_m7E697C1D7FC82EFBC23D9C786801A9AAF56E6616,
	U3CU3Ec__DisplayClass47_0__ctor_m2FDD491E0781244FB3788B379FD9EE2B28E5CB7C,
	U3CU3Ec__DisplayClass47_0_U3CFormatAuthResponseU3Eb__0_m0CA3563740B3CEF72EB65CE4778927E747701587,
	CanvasFacebookGameObject_get_CanvasFacebookImpl_m7D19E62CC7202410D1F7357B2A388357C65AD1E5,
	CanvasFacebookGameObject_OnPayComplete_mFD9D763BED07E52A6A54F88B2CEAF98A97BF1757,
	CanvasFacebookGameObject_OnFacebookAuthResponseChange_mA57CDD8B45DB932A3E6B83AA95C93168B5916046,
	CanvasFacebookGameObject_OnUrlResponse_m2FBDA32B779C5A719F6676494D476F4398252F09,
	CanvasFacebookGameObject_OnHideUnity_m4B6D89636DCC628C9724429666B6499EF1C992B2,
	CanvasFacebookGameObject_OnAwake_mE01DF7BD12ED924891EE4C46EFCFF1DBA1181F08,
	CanvasFacebookGameObject__ctor_mEEF1E3E0681D37F9260AE8BF89C8574F5BC188FD,
	CanvasFacebookLoader_get_FBGameObject_mDE0DA5343A6359539803E21DA59582AC021C72EB,
	CanvasFacebookLoader__ctor_m51722CD874D3E9AC5AEA30E7B99EFC177D2AC261,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsBridge_Start_mE05B0ADFC1D761CCB0BD6C73B5698E2587683D98,
	JsBridge_OnLoginComplete_m8B0C69F5F63F92CBA95891F4BFEDA4F136E46846,
	JsBridge_OnFacebookAuthResponseChange_mD61337B5A7F9678E77B5F09346082E76F8CBB079,
	JsBridge_OnPayComplete_mCFEE1BFE93D3A69C40C59699BDD3B327ADBF125A,
	JsBridge_OnAppRequestsComplete_mF7CE386DCC45C6CA82B46CF9B5FB6DB7E65AABB6,
	JsBridge_OnShareLinkComplete_m54EDE0659C873FDDADE26C560C4DF494F91AC1EA,
	JsBridge_OnFacebookFocus_mFA58F06C9FD7D5EF62DF7907B2F7D0C341A3C24C,
	JsBridge_OnInitComplete_m1234396209C8140801BC6471FA4F3B55A7FFEF93,
	JsBridge_OnUrlResponse_m97C0335B24E973EFACB8B5797B5A711F5E352F50,
	JsBridge__ctor_m3F577D8585FE744992C4940EC0C25C18301FECE4,
};
static const int32_t s_InvokerIndices[1004] = 
{
	2447,
	2447,
	2547,
	1323,
	1519,
	1519,
	1532,
	1520,
	2447,
	1575,
	1532,
	1532,
	1532,
	1037,
	1532,
	1532,
	1575,
	1575,
	2447,
	1323,
	1323,
	1323,
	1323,
	1323,
	115,
	2531,
	2506,
	1532,
	1323,
	1502,
	1293,
	1532,
	1323,
	1532,
	1323,
	1478,
	1271,
	1532,
	1323,
	1532,
	1532,
	-1,
	1323,
	2338,
	-1,
	1575,
	2531,
	-1,
	-1,
	2531,
	2531,
	2538,
	2538,
	2538,
	2538,
	2531,
	2531,
	2538,
	2526,
	2526,
	2531,
	2506,
	2531,
	2506,
	2531,
	2506,
	2538,
	2538,
	2538,
	2508,
	2531,
	2506,
	2531,
	2506,
	2531,
	2506,
	2114,
	1600,
	2338,
	2338,
	2547,
	1650,
	1610,
	1628,
	1785,
	1629,
	1921,
	1921,
	2547,
	2506,
	2547,
	2101,
	2078,
	2123,
	2547,
	1575,
	2547,
	823,
	1575,
	617,
	1323,
	2531,
	1611,
	2504,
	2531,
	2506,
	2506,
	2538,
	1532,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	2506,
	1928,
	2114,
	2506,
	2506,
	2506,
	2114,
	2338,
	2506,
	1701,
	2338,
	2338,
	2338,
	2338,
	2506,
	2329,
	2506,
	2531,
	1575,
	1323,
	1552,
	1340,
	1532,
	1532,
	1532,
	1552,
	1552,
	1340,
	1532,
	1323,
	1323,
	824,
	824,
	1575,
	16,
	180,
	47,
	334,
	334,
	1323,
	1323,
	513,
	551,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	16,
	1323,
	1040,
	1040,
	1323,
	2547,
	1575,
	1152,
	823,
	1575,
	617,
	1323,
	-1,
	-1,
	-1,
	-1,
	823,
	1340,
	433,
	1323,
	1532,
	1323,
	1575,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1575,
	1575,
	2531,
	1552,
	1552,
	1340,
	1532,
	1552,
	824,
	824,
	1575,
	16,
	180,
	47,
	334,
	334,
	1323,
	1323,
	513,
	551,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	24,
	1575,
	1323,
	1323,
	-1,
	-1,
	824,
	824,
	824,
	-1,
	824,
	1532,
	2447,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	122,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	78,
	1552,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1502,
	1293,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1323,
	1323,
	1323,
	1532,
	1323,
	1323,
	1323,
	1532,
	1323,
	1323,
	1532,
	1532,
	1532,
	1532,
	1552,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	2547,
	1323,
	1552,
	1340,
	1532,
	2547,
	1323,
	1323,
	1532,
	1323,
	1532,
	1323,
	1521,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	530,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1552,
	1340,
	1532,
	1323,
	1480,
	1272,
	1532,
	351,
	2447,
	2470,
	2447,
	1323,
	1323,
	1532,
	1323,
	1532,
	1323,
	1040,
	1323,
	1323,
	1323,
	1532,
	1323,
	2531,
	1532,
	2114,
	2114,
	1921,
	1921,
	1532,
	1040,
	1037,
	1040,
	1040,
	1040,
	1575,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	2547,
	2531,
	2506,
	2506,
	2506,
	2506,
	2338,
	1575,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	-1,
	2410,
	-1,
	2447,
	2447,
	2212,
	2447,
	-1,
	2447,
	2447,
	2447,
	2012,
	2368,
	2354,
	2447,
	2447,
	2447,
	2447,
	2447,
	2212,
	2366,
	-1,
	-1,
	-1,
	-1,
	2547,
	1575,
	1040,
	2531,
	334,
	334,
	1575,
	334,
	334,
	822,
	616,
	1575,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	2506,
	2506,
	2338,
	2212,
	2212,
	2212,
	2212,
	1575,
	1323,
	1532,
	2506,
	2506,
	2531,
	2531,
	2338,
	836,
	2547,
	2311,
	2531,
	2447,
	1575,
	2547,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1532,
	1323,
	1575,
	2547,
	2547,
	1575,
	1323,
	1575,
	2267,
	2267,
	2447,
	2208,
	2447,
	2212,
	1323,
	1532,
	1323,
	1323,
	1323,
	1323,
	1323,
	1314,
	1314,
	1314,
	1314,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1323,
	1323,
	1532,
	1323,
	1532,
	1323,
	824,
	1532,
	1323,
	1532,
	1323,
	1532,
	1575,
	824,
	1552,
	1340,
	1532,
	1532,
	529,
	1323,
	513,
	551,
	16,
	47,
	180,
	24,
	1323,
	824,
	824,
	1323,
	1323,
	1323,
	1323,
	1323,
	1552,
	1040,
	2531,
	12,
	824,
	823,
	1323,
	424,
	1323,
	1532,
	824,
	1575,
	617,
	1575,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1532,
	1575,
	1552,
	1040,
	1532,
	1323,
	1323,
	347,
	3,
	19,
	7,
	824,
	1575,
	1552,
	1340,
	1313,
	1532,
	1532,
	2531,
	1323,
	824,
	824,
	16,
	180,
	47,
	1323,
	1323,
	513,
	551,
	1552,
	1323,
	24,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	351,
	529,
	1323,
	1323,
	1323,
	529,
	824,
	1323,
	118,
	824,
	824,
	824,
	824,
	1323,
	765,
	1323,
	1323,
	1323,
	1340,
	1575,
	1575,
	835,
	1575,
	1323,
	1575,
	1532,
	1575,
	1532,
	1323,
	1532,
	1323,
	1532,
	1575,
	1575,
	1575,
	1575,
	1575,
	1323,
	1313,
	1575,
	1323,
	1575,
	529,
	824,
	824,
	529,
	529,
	529,
	1575,
	529,
	824,
	824,
	529,
	529,
	1532,
	1323,
	1532,
	1575,
	1575,
	1575,
	1532,
	1575,
	1575,
	1575,
	1575,
	1323,
	1575,
	1323,
	1532,
	1323,
	1532,
	1575,
	1575,
	1575,
	1532,
	1575,
	1313,
	1323,
	1323,
	1552,
	1323,
	351,
	529,
	1323,
	1323,
	1323,
	529,
	824,
	1323,
	118,
	824,
	824,
	824,
	824,
	1323,
	765,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1313,
	1532,
	1323,
	1323,
	1552,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	351,
	529,
	1323,
	1323,
	1323,
	529,
	824,
	1323,
	118,
	824,
	824,
	824,
	824,
	1323,
	765,
	1323,
	1313,
	1532,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1575,
	354,
	765,
	765,
	1575,
	1313,
	167,
	43,
	4,
	1575,
	170,
	157,
	1340,
	1313,
	1313,
	1532,
	1532,
	1313,
	325,
	504,
	1313,
	1532,
	1575,
	824,
	1552,
	1340,
	1532,
	1532,
	185,
	824,
	824,
	1575,
	1552,
	1532,
	16,
	180,
	47,
	513,
	551,
	1552,
	1323,
	1323,
	1323,
	1323,
	1323,
	1313,
	351,
	529,
	2531,
	2447,
	-1,
	1575,
	1520,
	1313,
	1532,
	1323,
	1532,
	1323,
	1575,
	1532,
	1575,
	1575,
	824,
	1323,
	1552,
	1340,
	1532,
	1532,
	529,
	824,
	824,
	1575,
	1532,
	1323,
	16,
	180,
	47,
	1323,
	513,
	551,
	1552,
	1323,
	1323,
	1323,
	1323,
	351,
	529,
	1323,
	1323,
	1323,
	529,
	824,
	1323,
	118,
	824,
	824,
	824,
	824,
	1323,
	765,
	1323,
	1313,
	2531,
	824,
	-1,
	-1,
	1575,
	1575,
	835,
	1575,
	1323,
	1323,
	1575,
	1532,
	1575,
	-1,
	824,
	1575,
	824,
	2531,
	1552,
	1340,
	1532,
	1532,
	1532,
	8,
	824,
	824,
	1575,
	16,
	1323,
	180,
	47,
	24,
	1323,
	513,
	551,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1340,
	2338,
	12,
	1323,
	-1,
	-1,
	-1,
	2547,
	1575,
	1323,
	1575,
	1323,
	1532,
	1323,
	1323,
	1323,
	1340,
	1575,
	1575,
	1532,
	1575,
	1323,
	1323,
	1323,
	1340,
	1323,
	1323,
	1323,
	1340,
	1532,
	1575,
	178,
	824,
	1575,
	1575,
	513,
	551,
	529,
	1575,
	1575,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1323,
	1575,
};
static const Il2CppTokenRangePair s_rgctxIndices[12] = 
{
	{ 0x02000020, { 11, 3 } },
	{ 0x0200008C, { 20, 5 } },
	{ 0x02000091, { 25, 6 } },
	{ 0x0600002D, { 0, 3 } },
	{ 0x06000030, { 3, 3 } },
	{ 0x06000031, { 6, 1 } },
	{ 0x060000E6, { 7, 1 } },
	{ 0x060000E7, { 8, 3 } },
	{ 0x0600019A, { 14, 1 } },
	{ 0x0600019C, { 15, 1 } },
	{ 0x060001A1, { 16, 3 } },
	{ 0x0600035E, { 19, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[31] = 
{
	{ (Il2CppRGCTXDataType)2, 833 },
	{ (Il2CppRGCTXDataType)2, 76 },
	{ (Il2CppRGCTXDataType)3, 3217 },
	{ (Il2CppRGCTXDataType)3, 9390 },
	{ (Il2CppRGCTXDataType)2, 91 },
	{ (Il2CppRGCTXDataType)3, 9359 },
	{ (Il2CppRGCTXDataType)3, 9358 },
	{ (Il2CppRGCTXDataType)2, 156 },
	{ (Il2CppRGCTXDataType)3, 6355 },
	{ (Il2CppRGCTXDataType)3, 6356 },
	{ (Il2CppRGCTXDataType)2, 155 },
	{ (Il2CppRGCTXDataType)3, 6264 },
	{ (Il2CppRGCTXDataType)3, 6262 },
	{ (Il2CppRGCTXDataType)3, 6263 },
	{ (Il2CppRGCTXDataType)2, 187 },
	{ (Il2CppRGCTXDataType)3, 9701 },
	{ (Il2CppRGCTXDataType)2, 1079 },
	{ (Il2CppRGCTXDataType)2, 1111 },
	{ (Il2CppRGCTXDataType)2, 1171 },
	{ (Il2CppRGCTXDataType)3, 9121 },
	{ (Il2CppRGCTXDataType)3, 6265 },
	{ (Il2CppRGCTXDataType)2, 1701 },
	{ (Il2CppRGCTXDataType)3, 6266 },
	{ (Il2CppRGCTXDataType)3, 9122 },
	{ (Il2CppRGCTXDataType)3, 6267 },
	{ (Il2CppRGCTXDataType)3, 6268 },
	{ (Il2CppRGCTXDataType)2, 1702 },
	{ (Il2CppRGCTXDataType)3, 6270 },
	{ (Il2CppRGCTXDataType)3, 6269 },
	{ (Il2CppRGCTXDataType)3, 580 },
	{ (Il2CppRGCTXDataType)3, 9123 },
};
extern const CustomAttributesCacheGenerator g_Facebook_Unity_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Facebook_Unity_CodeGenModule;
const Il2CppCodeGenModule g_Facebook_Unity_CodeGenModule = 
{
	"Facebook.Unity.dll",
	1004,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	12,
	s_rgctxIndices,
	31,
	s_rgctxValues,
	NULL,
	g_Facebook_Unity_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
