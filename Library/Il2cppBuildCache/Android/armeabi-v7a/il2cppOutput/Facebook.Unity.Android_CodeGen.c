﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 T Facebook.Unity.Android.AndroidWrapper::CallStatic(System.String)
// 0x00000002 System.Void Facebook.Unity.Android.AndroidWrapper::CallStatic(System.String,System.Object[])
extern void AndroidWrapper_CallStatic_m0FF454A5A689F8738AB6FFFC58AB457A9A986F83 (void);
// 0x00000003 System.Void Facebook.Unity.Android.AndroidWrapper::.ctor()
extern void AndroidWrapper__ctor_mB48C99C8272385BDC19407A3862BA1C7A6ABAFB8 (void);
static Il2CppMethodPointer s_methodPointers[3] = 
{
	NULL,
	AndroidWrapper_CallStatic_m0FF454A5A689F8738AB6FFFC58AB457A9A986F83,
	AndroidWrapper__ctor_mB48C99C8272385BDC19407A3862BA1C7A6ABAFB8,
};
static const int32_t s_InvokerIndices[3] = 
{
	-1,
	824,
	1575,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000001, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)3, 7770 },
};
extern const CustomAttributesCacheGenerator g_Facebook_Unity_Android_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Facebook_Unity_Android_CodeGenModule;
const Il2CppCodeGenModule g_Facebook_Unity_Android_CodeGenModule = 
{
	"Facebook.Unity.Android.dll",
	3,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_Facebook_Unity_Android_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
