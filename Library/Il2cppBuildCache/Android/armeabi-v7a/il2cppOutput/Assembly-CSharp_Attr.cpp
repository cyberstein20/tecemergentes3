﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct  CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct  HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct  RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void QuizGenerator_t9E7DEDF0AF1781A05B5194A6B880BAD3C0B47753_CustomAttributesCacheGenerator_QuizGenerator_SettingOffInSeconds_mB24CD6BB965B1ECA68F17AE6C04A74A2F793918E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_0_0_0_var), NULL);
	}
}
static void U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47__ctor_mF675BBFAB0FDA1AE944BCCB7291C8B04F405BBB3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47_System_IDisposable_Dispose_m5E06BCFBAE84B00A2AAC0136295914272EEF8DEF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57CF15CED2AAC5B8AFD0322CBC7B16F66F9594B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47_System_Collections_IEnumerator_Reset_m691AEF82CE7137FF2505BDA572289DA3CD9DF50A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47_System_Collections_IEnumerator_get_Current_m5C6780BB0A8B2C7F50A67698D7B70A2709A3DA13(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator_musicsGame(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x20\x6D\x75\x73\x69\x63\x20\x63\x6C\x69\x70\x20\x77\x68\x65\x6E\x20\x73\x74\x61\x72\x74"), NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator_musicsGameVolume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator_soundClick(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x63\x65\x20\x74\x68\x65\x20\x73\x6F\x75\x6E\x64\x20\x69\x6E\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x63\x61\x6C\x6C\x20\x69\x74\x20\x69\x6E\x20\x61\x6E\x6F\x74\x68\x65\x72\x20\x73\x63\x72\x69\x70\x74\x20\x62\x79\x3A\x20\x53\x6F\x75\x6E\x64\x4D\x61\x6E\x61\x67\x65\x72\x2E\x50\x6C\x61\x79\x53\x66\x78\x28\x73\x6F\x75\x6E\x64\x6E\x61\x6D\x65\x29\x3B"), NULL);
	}
}
static void BaseScreen_tE46EE1506694B0997ECFC3BC161B4131788C35DF_CustomAttributesCacheGenerator_Canvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseScreen_tE46EE1506694B0997ECFC3BC161B4131788C35DF_CustomAttributesCacheGenerator_BaseScreen_OpenPopupAsync_m795A427B453AF7173D5990DAAE6C22FEB41D7E3D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_0_0_0_var), NULL);
	}
}
static void BaseScreen_tE46EE1506694B0997ECFC3BC161B4131788C35DF_CustomAttributesCacheGenerator_BaseScreen_FadeIn_m7548331997F6688B1A63FACEF2FDCDE6A7D25B81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_0_0_0_var), NULL);
	}
}
static void BaseScreen_tE46EE1506694B0997ECFC3BC161B4131788C35DF_CustomAttributesCacheGenerator_BaseScreen_FadeOut_mA5D73509584E28F54356E92D73F7CE6E993BB31F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t9A291F99B73BA8C0B5AC2EB224B016F8777828BD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1__ctor_m4449234025FEA3D85E962633F8CC83E13B47FB37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1_System_IDisposable_Dispose_m9FD6083604DC4A71AC34B1CA6499B4E97567B8DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m266449A3818ACA68FAE787CC703FB3232346DE93(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1_System_Collections_IEnumerator_Reset_m4E11A473FECF06FAE9BE67A01AFBB0478D63F2F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1_System_Collections_IEnumerator_get_Current_m4ED573E226A08D079AA4FC9B0AF47191FC0DA18A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6__ctor_m3AF82D0DB0670D96D6DEBBD91DFBEED13AFFDACF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6_System_IDisposable_Dispose_mC413CD4A23A7A97325D7B8F6AC45209820EC895C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F65DA37CD06796A81596AE0783C36D604771A59(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6_System_Collections_IEnumerator_Reset_m6A703D2007F95D1087C06B5CB7458B4AF7315A5A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6_System_Collections_IEnumerator_get_Current_m35E8DB4D3348E057AB5C84B3594715DFEE572127(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7__ctor_m64E38BA229295767CB4CDB535A4C5FE3686F7C92(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7_System_IDisposable_Dispose_m3EA85C292A6691FC50731F9E1AC0B0AFE3C4F215(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4180985992B7D598CE132F467634A82634D78CE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7_System_Collections_IEnumerator_Reset_m664FA32A3FF8C6EFA0EC0B99475FEB9EF697FE6E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7_System_Collections_IEnumerator_get_Current_m17EA0E60F0612B947F0B46C5B64747354E591B70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ListShuffle_t62568330E3A41D86EA5E85F9B75D3AA3F4A65CED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListShuffle_t62568330E3A41D86EA5E85F9B75D3AA3F4A65CED_CustomAttributesCacheGenerator_ListShuffle_Shuffle_mBC741192F2D54324FF8ED5C372503DA0A6BC3BCE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Popup_t8092CB4519A673C23C0C2DDBA839084C6DB1D8D4_CustomAttributesCacheGenerator_ParentScreen(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Popup_t8092CB4519A673C23C0C2DDBA839084C6DB1D8D4_CustomAttributesCacheGenerator_Popup_DestroyPopup_mF22233DF7742B12343C8D5D69BFC8CC8FD8E1C30(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_0_0_0_var), NULL);
	}
}
static void U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7__ctor_mF5C0BD3B6ED00CD38EF0F406EDB560795AE59141(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7_System_IDisposable_Dispose_m73FA6EB3AA01FD75A4663F3D0EB005AABF6966AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD75D1019AE9556F0D97B8A0E9841D0F127579651(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7_System_Collections_IEnumerator_Reset_m4378C21E7218CE13B00BF98161B49904EF6DC2CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7_System_Collections_IEnumerator_get_Current_m1021DF0E92D847245CCA35FB28744C1340DB19B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpriteSwapper_t7A9270C35E88023AD9FA07F8796420D646796FF5_CustomAttributesCacheGenerator_enabledSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpriteSwapper_t7A9270C35E88023AD9FA07F8796420D646796FF5_CustomAttributesCacheGenerator_disabledSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameConfiguration_t451F2E31A0960B452E3863D888D62E484050091E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x76\x69\x61\x20\x51\x75\x69\x7A\x20\x4B\x69\x74\x2F\x47\x61\x6D\x65\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 1LL, NULL);
	}
}
static void QuestionPack_t0D4D69905D7F102FA8274F8B690CDB00E4E32BB1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x51\x75\x65\x73\x74\x69\x6F\x6E\x50\x61\x63\x6B"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x76\x69\x61\x20\x51\x75\x69\x7A\x20\x4B\x69\x74\x2F\x51\x75\x65\x73\x74\x69\x6F\x6E\x20\x70\x61\x63\x6B"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 2LL, NULL);
	}
}
static void QuestionPackSet_tB0D38C9C0BADE6F40AAF095B92FE2F5B7BC39E6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x51\x75\x65\x73\x74\x69\x6F\x6E\x50\x61\x63\x6B\x53\x65\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x76\x69\x61\x20\x51\x75\x69\x7A\x20\x4B\x69\x74\x2F\x51\x75\x65\x73\x74\x69\x6F\x6E\x20\x70\x61\x63\x6B\x20\x73\x65\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 3LL, NULL);
	}
}
static void AlertPopup_t4EDD4750C06145A223C2C4FDBF5B9BC11794EEE3_CustomAttributesCacheGenerator_Text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_TrophyImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_ConfettiParticles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_RainParticles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_CompletedText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_ScoreText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_HighScoreText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProfilePopup_t4AD55E54A1DF966B10B03AED39515CA876E8A45E_CustomAttributesCacheGenerator_AvatarImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProfilePopup_t4AD55E54A1DF966B10B03AED39515CA876E8A45E_CustomAttributesCacheGenerator_QuestionTypeText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProfilePopup_t4AD55E54A1DF966B10B03AED39515CA876E8A45E_CustomAttributesCacheGenerator_CategoryScrollContent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProfilePopup_t4AD55E54A1DF966B10B03AED39515CA876E8A45E_CustomAttributesCacheGenerator_CategoryScrollItemPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SettingsPopup_t0B0F052DC0754DE50107473411687F3B7C111D0F_CustomAttributesCacheGenerator_musicSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SettingsPopup_t0B0F052DC0754DE50107473411687F3B7C111D0F_CustomAttributesCacheGenerator_soundSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t1B8912DEA3D0FCD970A0F0796D7FA4CE18B2CF7F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t3E68B7413E83AE636ECB3F6E169ADE8D09AEFE3C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameScreen_t3A25066DEB4793659252E89F6B9E82E50B4393F5_CustomAttributesCacheGenerator_GameScreen_SelectRandomQuestionAsync_m5C277593F010CAFD112CBDF42F6995117713FCED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_0_0_0_var), NULL);
	}
}
static void GameScreen_t3A25066DEB4793659252E89F6B9E82E50B4393F5_CustomAttributesCacheGenerator_GameScreen_OpenGameFinishedPopupAsync_mDDE57F29D5312E3377D12C7BAF15883A4840FD0D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_0_0_0_var), NULL);
	}
}
static void U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48__ctor_mDD37123FC127E790D7CE66592D688D7FCB08F4EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48_System_IDisposable_Dispose_m186687311CE86ACB62F2C2DAB46A2D19D096C3D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m369D1C6C107A0124D6D006237E39DA39FA8312E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_IEnumerator_Reset_mFC4635B6D4DB5A757FA62E55D63F3B6B6DE6FC7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_IEnumerator_get_Current_m0CD6CF2630BC66C203F5AF4F109DC4054D31272C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass49_0_t4BCAAFB5F8E9D3763BFF9082A75CBBFA4BBE5804_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49__ctor_mDE9A51FCCC24ECE8581480F4F25BCA1B61058984(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49_System_IDisposable_Dispose_mE050E9F7D72D00202AB3D6B9A44430CBA75C2F60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FF145A247ED7369892CAE203D6BEE9784BF013B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_IEnumerator_Reset_m1F1766A314E68317CE556A1912D696084B7EF3C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_IEnumerator_get_Current_m4DEF2131620704154BFF8F8D75FD73E620928170(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HomeScreen_t92FB5187D3D6C238188EFBFAEEECC6524E323175_CustomAttributesCacheGenerator_AvatarImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t23E2727C31295BB81FE5440E5BDD5F18F524E939_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t093FE8219B3AB827300061E17523213CBF538A6F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_1_t14560150DDC04C2A078843E34CF617AB60E834FF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeIn_m40D67B72B348528063F141133B4E26A4140CBE9F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_0_0_0_var), NULL);
	}
}
static void QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeOut_mDA24F36316B5DA1706DF958607DB3CAD61B58AB6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_0_0_0_var), NULL);
	}
}
static void QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeInImage_m88CE5B21BCE8FB6C1DD23369B40A49618CAA12B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_0_0_0_var), NULL);
	}
}
static void QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeOutImage_m296752235987311BA65F4025F255C89F6B6ED522(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_0_0_0_var), NULL);
	}
}
static void QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeInText_mAE52FB41F32DECB053B7A384FC3DD979E727719C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_0_0_0_var), NULL);
	}
}
static void QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeOutText_m1F38DCB813084B1E242F5A5ED1A00D2985BFB661(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_0_0_0_var), NULL);
	}
}
static void U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5__ctor_m74D9B0013114CBD245700700C752CA6F9D4C54D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5_System_IDisposable_Dispose_m426637519063328CC571E171185CC9C3B22F2D31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m746822E4F1F69DA7A0938EC028972D37331C657B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5_System_Collections_IEnumerator_Reset_mC1BC7936550ED852E6166A9DEDFC90B6D2B097D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5_System_Collections_IEnumerator_get_Current_m67C9FCF94FC33DCB98447834DF9914647C4EEDA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6__ctor_mC4DC2218F477AC844A4644E3E926540ADF7F7F62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6_System_IDisposable_Dispose_m5163A6DD6CBA5506F8FE350E00ECF455462756FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE77C57F22C55C3F6DA732834830044D44C6F16A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6_System_Collections_IEnumerator_Reset_mDA2ABCD14D54F3D886AD5759E2081F106168507A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6_System_Collections_IEnumerator_get_Current_mF36B05915D2AE4BC3CAFFC3303A515251245AA3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7__ctor_m0234ABA131890578D682DE3F2387A077CFEA400C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7_System_IDisposable_Dispose_m99AA1279F2ADA34145D797F14E50A47CB7D30A9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE90EB29B462BEDAC582A8683F14919F38D87D423(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7_System_Collections_IEnumerator_Reset_mE2BB5EF1DA866BBC590A0E150D8BE467836078F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7_System_Collections_IEnumerator_get_Current_mAF0AEA8623EE1CC511CD14E5823CB494CC339CD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8__ctor_m9B38F76689716961CA361C4DDBDFBA39284C870D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8_System_IDisposable_Dispose_m76E85D1BE791F9FFFF071A3833350405667D0B23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50748FB3FEDF7D8B29D1C14B62C9ABA352B0EF6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8_System_Collections_IEnumerator_Reset_m4D193083D83A5855B8C421D35C1ED3AED0041716(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8_System_Collections_IEnumerator_get_Current_m30774CD6115F99D85A67DF56206CEF2F90CF6B9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9__ctor_mFC16E5EAD976D5FF732873A44177E70D9863E456(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9_System_IDisposable_Dispose_mDE254A21FA9A8AB7E100020985280E49B76FF4D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C72DE065EF204138607968499DB71937EA857C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9_System_Collections_IEnumerator_Reset_m5FE33FA8B881ABE276980EE13DF8F779D1BD7CCA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9_System_Collections_IEnumerator_get_Current_m8FA4E1605670A9C3071476E6B47DD1A8CD613FD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10__ctor_m04D3968ABEAACEDD769B3FE3A401C3272DE08ECE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10_System_IDisposable_Dispose_m120BC1F80CB95B13361726FE1074AE0A2925FF84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6488A6CB058E4D52C1FE7618D48809BF472FECE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10_System_Collections_IEnumerator_Reset_m4A39887766D710ADA09D04FD4F5941AB2C94A1F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10_System_Collections_IEnumerator_get_Current_m225D35B848312EAE28F8622F0F92ABF9CECA14C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t275DADBA57EF4DB2D7BE81F62F4072B3ED079DC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_1_t2D7E516CEA847FC40263AD83D212B15497B4C564_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t6775C3ADD92FC000DAFF6B34BD31F73C64AABBFE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConsoleBase_t09D8DD8CE927FB3C3E979DCE284C8BDF0EDD37BF_CustomAttributesCacheGenerator_U3CLastResponseTextureU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConsoleBase_t09D8DD8CE927FB3C3E979DCE284C8BDF0EDD37BF_CustomAttributesCacheGenerator_ConsoleBase_get_LastResponseTexture_m6D4AB4A8E2B4906C8D20DB62B10DDE0D812630BD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConsoleBase_t09D8DD8CE927FB3C3E979DCE284C8BDF0EDD37BF_CustomAttributesCacheGenerator_ConsoleBase_set_LastResponseTexture_m9495BB5A6A1C0F6C2CA39B3818B06565B1CBC2E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphRequest_tDD89F08E9D528FC5B23088BA47E602FD21B5AB78_CustomAttributesCacheGenerator_GraphRequest_TakeScreenshot_m6E8E3B5A0BBBBAE2E4124A05607205420B89A904(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_0_0_0_var), NULL);
	}
}
static void U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4__ctor_mFDE73BDF39095D5CB04C52943F3F749FC1D96B3A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_mB4A7976DB6C72A46D7BD842E606BA59D639EEA6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BFEAC3397C268E3CE931454E7DDCAF4AB248BC8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m7BF49F82A5EFF7F32B3FA82AB48554E368DCF674(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m48CB53297F5D6E3CC52272F3E48E27F03C346A6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[138] = 
{
	U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t9A291F99B73BA8C0B5AC2EB224B016F8777828BD_CustomAttributesCacheGenerator,
	U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator,
	U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator,
	U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator,
	ListShuffle_t62568330E3A41D86EA5E85F9B75D3AA3F4A65CED_CustomAttributesCacheGenerator,
	U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator,
	GameConfiguration_t451F2E31A0960B452E3863D888D62E484050091E_CustomAttributesCacheGenerator,
	QuestionPack_t0D4D69905D7F102FA8274F8B690CDB00E4E32BB1_CustomAttributesCacheGenerator,
	QuestionPackSet_tB0D38C9C0BADE6F40AAF095B92FE2F5B7BC39E6A_CustomAttributesCacheGenerator,
	U3CU3Ec_t1B8912DEA3D0FCD970A0F0796D7FA4CE18B2CF7F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t3E68B7413E83AE636ECB3F6E169ADE8D09AEFE3C_CustomAttributesCacheGenerator,
	U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass49_0_t4BCAAFB5F8E9D3763BFF9082A75CBBFA4BBE5804_CustomAttributesCacheGenerator,
	U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator,
	U3CU3Ec_t23E2727C31295BB81FE5440E5BDD5F18F524E939_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t093FE8219B3AB827300061E17523213CBF538A6F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_1_t14560150DDC04C2A078843E34CF617AB60E834FF_CustomAttributesCacheGenerator,
	U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator,
	U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator,
	U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator,
	U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator,
	U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator,
	U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t275DADBA57EF4DB2D7BE81F62F4072B3ED079DC4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_1_t2D7E516CEA847FC40263AD83D212B15497B4C564_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t6775C3ADD92FC000DAFF6B34BD31F73C64AABBFE_CustomAttributesCacheGenerator,
	U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator_musicsGame,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator_musicsGameVolume,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator_soundClick,
	BaseScreen_tE46EE1506694B0997ECFC3BC161B4131788C35DF_CustomAttributesCacheGenerator_Canvas,
	Popup_t8092CB4519A673C23C0C2DDBA839084C6DB1D8D4_CustomAttributesCacheGenerator_ParentScreen,
	SpriteSwapper_t7A9270C35E88023AD9FA07F8796420D646796FF5_CustomAttributesCacheGenerator_enabledSprite,
	SpriteSwapper_t7A9270C35E88023AD9FA07F8796420D646796FF5_CustomAttributesCacheGenerator_disabledSprite,
	AlertPopup_t4EDD4750C06145A223C2C4FDBF5B9BC11794EEE3_CustomAttributesCacheGenerator_Text,
	GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_TrophyImage,
	GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_ConfettiParticles,
	GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_RainParticles,
	GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_CompletedText,
	GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_ScoreText,
	GameFinishedPopup_tBF17AA67CFD6353A9BB88E4BAD3D74E136DFFEF8_CustomAttributesCacheGenerator_HighScoreText,
	ProfilePopup_t4AD55E54A1DF966B10B03AED39515CA876E8A45E_CustomAttributesCacheGenerator_AvatarImage,
	ProfilePopup_t4AD55E54A1DF966B10B03AED39515CA876E8A45E_CustomAttributesCacheGenerator_QuestionTypeText,
	ProfilePopup_t4AD55E54A1DF966B10B03AED39515CA876E8A45E_CustomAttributesCacheGenerator_CategoryScrollContent,
	ProfilePopup_t4AD55E54A1DF966B10B03AED39515CA876E8A45E_CustomAttributesCacheGenerator_CategoryScrollItemPrefab,
	SettingsPopup_t0B0F052DC0754DE50107473411687F3B7C111D0F_CustomAttributesCacheGenerator_musicSlider,
	SettingsPopup_t0B0F052DC0754DE50107473411687F3B7C111D0F_CustomAttributesCacheGenerator_soundSlider,
	HomeScreen_t92FB5187D3D6C238188EFBFAEEECC6524E323175_CustomAttributesCacheGenerator_AvatarImage,
	ConsoleBase_t09D8DD8CE927FB3C3E979DCE284C8BDF0EDD37BF_CustomAttributesCacheGenerator_U3CLastResponseTextureU3Ek__BackingField,
	QuizGenerator_t9E7DEDF0AF1781A05B5194A6B880BAD3C0B47753_CustomAttributesCacheGenerator_QuizGenerator_SettingOffInSeconds_mB24CD6BB965B1ECA68F17AE6C04A74A2F793918E,
	U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47__ctor_mF675BBFAB0FDA1AE944BCCB7291C8B04F405BBB3,
	U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47_System_IDisposable_Dispose_m5E06BCFBAE84B00A2AAC0136295914272EEF8DEF,
	U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57CF15CED2AAC5B8AFD0322CBC7B16F66F9594B6,
	U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47_System_Collections_IEnumerator_Reset_m691AEF82CE7137FF2505BDA572289DA3CD9DF50A,
	U3CSettingOffInSecondsU3Ed__47_tDD481B5D754F3827AD693D2318944795F9C403F0_CustomAttributesCacheGenerator_U3CSettingOffInSecondsU3Ed__47_System_Collections_IEnumerator_get_Current_m5C6780BB0A8B2C7F50A67698D7B70A2709A3DA13,
	BaseScreen_tE46EE1506694B0997ECFC3BC161B4131788C35DF_CustomAttributesCacheGenerator_BaseScreen_OpenPopupAsync_m795A427B453AF7173D5990DAAE6C22FEB41D7E3D,
	BaseScreen_tE46EE1506694B0997ECFC3BC161B4131788C35DF_CustomAttributesCacheGenerator_BaseScreen_FadeIn_m7548331997F6688B1A63FACEF2FDCDE6A7D25B81,
	BaseScreen_tE46EE1506694B0997ECFC3BC161B4131788C35DF_CustomAttributesCacheGenerator_BaseScreen_FadeOut_mA5D73509584E28F54356E92D73F7CE6E993BB31F,
	U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1__ctor_m4449234025FEA3D85E962633F8CC83E13B47FB37,
	U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1_System_IDisposable_Dispose_m9FD6083604DC4A71AC34B1CA6499B4E97567B8DD,
	U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m266449A3818ACA68FAE787CC703FB3232346DE93,
	U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1_System_Collections_IEnumerator_Reset_m4E11A473FECF06FAE9BE67A01AFBB0478D63F2F3,
	U3COpenPopupAsyncU3Ed__5_1_t62C4CF5C103A69F2B5A428CF9881D0AE155FF6ED_CustomAttributesCacheGenerator_U3COpenPopupAsyncU3Ed__5_1_System_Collections_IEnumerator_get_Current_m4ED573E226A08D079AA4FC9B0AF47191FC0DA18A,
	U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6__ctor_m3AF82D0DB0670D96D6DEBBD91DFBEED13AFFDACF,
	U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6_System_IDisposable_Dispose_mC413CD4A23A7A97325D7B8F6AC45209820EC895C,
	U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F65DA37CD06796A81596AE0783C36D604771A59,
	U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6_System_Collections_IEnumerator_Reset_m6A703D2007F95D1087C06B5CB7458B4AF7315A5A,
	U3CFadeInU3Ed__6_t58798F664E6645A306083B24B4E33B7750AC4E14_CustomAttributesCacheGenerator_U3CFadeInU3Ed__6_System_Collections_IEnumerator_get_Current_m35E8DB4D3348E057AB5C84B3594715DFEE572127,
	U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7__ctor_m64E38BA229295767CB4CDB535A4C5FE3686F7C92,
	U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7_System_IDisposable_Dispose_m3EA85C292A6691FC50731F9E1AC0B0AFE3C4F215,
	U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4180985992B7D598CE132F467634A82634D78CE2,
	U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7_System_Collections_IEnumerator_Reset_m664FA32A3FF8C6EFA0EC0B99475FEB9EF697FE6E,
	U3CFadeOutU3Ed__7_t9AE162198E3A7F9407A356B7F2131640FAB72602_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__7_System_Collections_IEnumerator_get_Current_m17EA0E60F0612B947F0B46C5B64747354E591B70,
	ListShuffle_t62568330E3A41D86EA5E85F9B75D3AA3F4A65CED_CustomAttributesCacheGenerator_ListShuffle_Shuffle_mBC741192F2D54324FF8ED5C372503DA0A6BC3BCE,
	Popup_t8092CB4519A673C23C0C2DDBA839084C6DB1D8D4_CustomAttributesCacheGenerator_Popup_DestroyPopup_mF22233DF7742B12343C8D5D69BFC8CC8FD8E1C30,
	U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7__ctor_mF5C0BD3B6ED00CD38EF0F406EDB560795AE59141,
	U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7_System_IDisposable_Dispose_m73FA6EB3AA01FD75A4663F3D0EB005AABF6966AD,
	U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD75D1019AE9556F0D97B8A0E9841D0F127579651,
	U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7_System_Collections_IEnumerator_Reset_m4378C21E7218CE13B00BF98161B49904EF6DC2CB,
	U3CDestroyPopupU3Ed__7_tC3B33AFF0DB351BDD6EF34722C871610D689737A_CustomAttributesCacheGenerator_U3CDestroyPopupU3Ed__7_System_Collections_IEnumerator_get_Current_m1021DF0E92D847245CCA35FB28744C1340DB19B1,
	GameScreen_t3A25066DEB4793659252E89F6B9E82E50B4393F5_CustomAttributesCacheGenerator_GameScreen_SelectRandomQuestionAsync_m5C277593F010CAFD112CBDF42F6995117713FCED,
	GameScreen_t3A25066DEB4793659252E89F6B9E82E50B4393F5_CustomAttributesCacheGenerator_GameScreen_OpenGameFinishedPopupAsync_mDDE57F29D5312E3377D12C7BAF15883A4840FD0D,
	U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48__ctor_mDD37123FC127E790D7CE66592D688D7FCB08F4EA,
	U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48_System_IDisposable_Dispose_m186687311CE86ACB62F2C2DAB46A2D19D096C3D4,
	U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m369D1C6C107A0124D6D006237E39DA39FA8312E8,
	U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_IEnumerator_Reset_mFC4635B6D4DB5A757FA62E55D63F3B6B6DE6FC7A,
	U3CSelectRandomQuestionAsyncU3Ed__48_t84D21CFECE6F7726DEF54B3C4DBDF630B31FD41A_CustomAttributesCacheGenerator_U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_IEnumerator_get_Current_m0CD6CF2630BC66C203F5AF4F109DC4054D31272C,
	U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49__ctor_mDE9A51FCCC24ECE8581480F4F25BCA1B61058984,
	U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49_System_IDisposable_Dispose_mE050E9F7D72D00202AB3D6B9A44430CBA75C2F60,
	U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FF145A247ED7369892CAE203D6BEE9784BF013B,
	U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_IEnumerator_Reset_m1F1766A314E68317CE556A1912D696084B7EF3C8,
	U3COpenGameFinishedPopupAsyncU3Ed__49_t9D79BD79AD6E4FF1609EE23E68EFA4CA57220D4A_CustomAttributesCacheGenerator_U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_IEnumerator_get_Current_m4DEF2131620704154BFF8F8D75FD73E620928170,
	QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeIn_m40D67B72B348528063F141133B4E26A4140CBE9F,
	QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeOut_mDA24F36316B5DA1706DF958607DB3CAD61B58AB6,
	QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeInImage_m88CE5B21BCE8FB6C1DD23369B40A49618CAA12B3,
	QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeOutImage_m296752235987311BA65F4025F255C89F6B6ED522,
	QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeInText_mAE52FB41F32DECB053B7A384FC3DD979E727719C,
	QuestionResultUi_t31D2E6BFD4A812D70E510890B2DA51C7D06DC26C_CustomAttributesCacheGenerator_QuestionResultUi_FadeOutText_m1F38DCB813084B1E242F5A5ED1A00D2985BFB661,
	U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5__ctor_m74D9B0013114CBD245700700C752CA6F9D4C54D2,
	U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5_System_IDisposable_Dispose_m426637519063328CC571E171185CC9C3B22F2D31,
	U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m746822E4F1F69DA7A0938EC028972D37331C657B,
	U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5_System_Collections_IEnumerator_Reset_mC1BC7936550ED852E6166A9DEDFC90B6D2B097D8,
	U3CFadeInU3Ed__5_tA24F7723DEC38C9430D50397955B058B1DF62190_CustomAttributesCacheGenerator_U3CFadeInU3Ed__5_System_Collections_IEnumerator_get_Current_m67C9FCF94FC33DCB98447834DF9914647C4EEDA3,
	U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6__ctor_mC4DC2218F477AC844A4644E3E926540ADF7F7F62,
	U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6_System_IDisposable_Dispose_m5163A6DD6CBA5506F8FE350E00ECF455462756FB,
	U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE77C57F22C55C3F6DA732834830044D44C6F16A3,
	U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6_System_Collections_IEnumerator_Reset_mDA2ABCD14D54F3D886AD5759E2081F106168507A,
	U3CFadeOutU3Ed__6_tA2B1A43D695252E7545BF07390D6CD0F98432320_CustomAttributesCacheGenerator_U3CFadeOutU3Ed__6_System_Collections_IEnumerator_get_Current_mF36B05915D2AE4BC3CAFFC3303A515251245AA3B,
	U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7__ctor_m0234ABA131890578D682DE3F2387A077CFEA400C,
	U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7_System_IDisposable_Dispose_m99AA1279F2ADA34145D797F14E50A47CB7D30A9E,
	U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE90EB29B462BEDAC582A8683F14919F38D87D423,
	U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7_System_Collections_IEnumerator_Reset_mE2BB5EF1DA866BBC590A0E150D8BE467836078F1,
	U3CFadeInImageU3Ed__7_t66AD85B5CDA9ACC1486E48C4FB023A8FC6D12E00_CustomAttributesCacheGenerator_U3CFadeInImageU3Ed__7_System_Collections_IEnumerator_get_Current_mAF0AEA8623EE1CC511CD14E5823CB494CC339CD3,
	U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8__ctor_m9B38F76689716961CA361C4DDBDFBA39284C870D,
	U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8_System_IDisposable_Dispose_m76E85D1BE791F9FFFF071A3833350405667D0B23,
	U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50748FB3FEDF7D8B29D1C14B62C9ABA352B0EF6A,
	U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8_System_Collections_IEnumerator_Reset_m4D193083D83A5855B8C421D35C1ED3AED0041716,
	U3CFadeOutImageU3Ed__8_t74518DC9BDDAD0205AD25DE6BAC0224D713A170F_CustomAttributesCacheGenerator_U3CFadeOutImageU3Ed__8_System_Collections_IEnumerator_get_Current_m30774CD6115F99D85A67DF56206CEF2F90CF6B9C,
	U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9__ctor_mFC16E5EAD976D5FF732873A44177E70D9863E456,
	U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9_System_IDisposable_Dispose_mDE254A21FA9A8AB7E100020985280E49B76FF4D6,
	U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C72DE065EF204138607968499DB71937EA857C3,
	U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9_System_Collections_IEnumerator_Reset_m5FE33FA8B881ABE276980EE13DF8F779D1BD7CCA,
	U3CFadeInTextU3Ed__9_t57029A4FC8166F8AC55284B75CC1BE3B6FB55132_CustomAttributesCacheGenerator_U3CFadeInTextU3Ed__9_System_Collections_IEnumerator_get_Current_m8FA4E1605670A9C3071476E6B47DD1A8CD613FD5,
	U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10__ctor_m04D3968ABEAACEDD769B3FE3A401C3272DE08ECE,
	U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10_System_IDisposable_Dispose_m120BC1F80CB95B13361726FE1074AE0A2925FF84,
	U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6488A6CB058E4D52C1FE7618D48809BF472FECE6,
	U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10_System_Collections_IEnumerator_Reset_m4A39887766D710ADA09D04FD4F5941AB2C94A1F0,
	U3CFadeOutTextU3Ed__10_tF6BD7F200D1D9D456881E8AA1815B205D72E9CC5_CustomAttributesCacheGenerator_U3CFadeOutTextU3Ed__10_System_Collections_IEnumerator_get_Current_m225D35B848312EAE28F8622F0F92ABF9CECA14C7,
	ConsoleBase_t09D8DD8CE927FB3C3E979DCE284C8BDF0EDD37BF_CustomAttributesCacheGenerator_ConsoleBase_get_LastResponseTexture_m6D4AB4A8E2B4906C8D20DB62B10DDE0D812630BD,
	ConsoleBase_t09D8DD8CE927FB3C3E979DCE284C8BDF0EDD37BF_CustomAttributesCacheGenerator_ConsoleBase_set_LastResponseTexture_m9495BB5A6A1C0F6C2CA39B3818B06565B1CBC2E9,
	GraphRequest_tDD89F08E9D528FC5B23088BA47E602FD21B5AB78_CustomAttributesCacheGenerator_GraphRequest_TakeScreenshot_m6E8E3B5A0BBBBAE2E4124A05607205420B89A904,
	U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4__ctor_mFDE73BDF39095D5CB04C52943F3F749FC1D96B3A,
	U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_mB4A7976DB6C72A46D7BD842E606BA59D639EEA6B,
	U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BFEAC3397C268E3CE931454E7DDCAF4AB248BC8,
	U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m7BF49F82A5EFF7F32B3FA82AB48554E368DCF674,
	U3CTakeScreenshotU3Ed__4_t641AE41102E7259031A1427F40F2A20FA494FE7D_CustomAttributesCacheGenerator_U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m48CB53297F5D6E3CC52272F3E48E27F03C346A6B,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
