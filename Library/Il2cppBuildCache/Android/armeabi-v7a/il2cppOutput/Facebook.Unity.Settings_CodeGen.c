﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
extern void FacebookSettings_get_SelectedAppIndex_mD1184BAB88CC164E39929F0C7D23817BF957C2AD (void);
// 0x00000002 System.Void Facebook.Unity.Settings.FacebookSettings::set_SelectedAppIndex(System.Int32)
extern void FacebookSettings_set_SelectedAppIndex_m28F647F0F7FAB79DFED2B93EB70A2958213E5435 (void);
// 0x00000003 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
extern void FacebookSettings_get_AppIds_m23292AF4C70A2F16EF253C33796547D18B95A930 (void);
// 0x00000004 System.Void Facebook.Unity.Settings.FacebookSettings::set_AppIds(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_AppIds_m752E2013829105D0AC207A4EFC3E12E5045B7D14 (void);
// 0x00000005 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppLabels()
extern void FacebookSettings_get_AppLabels_mC8296B31F2FB08025F3CC2605DE5358E2209D737 (void);
// 0x00000006 System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLabels(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_AppLabels_mF7B410F53DA07D2F5FB8F3DC0F039DDEA58D309C (void);
// 0x00000007 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
extern void FacebookSettings_get_ClientTokens_m07B3D9E8EF88579C198B4E0F8F5AEF58EA6530CB (void);
// 0x00000008 System.Void Facebook.Unity.Settings.FacebookSettings::set_ClientTokens(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_ClientTokens_mEE165306704C4CCE5A326F7E80C494EBFC783978 (void);
// 0x00000009 System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
extern void FacebookSettings_get_AppId_m0BF08B1DBEEA55EE9DB9908B3D0B37377F837C2E (void);
// 0x0000000A System.String Facebook.Unity.Settings.FacebookSettings::get_ClientToken()
extern void FacebookSettings_get_ClientToken_mD7A7153CA46CCED350E36461FDC2D60E771DD96E (void);
// 0x0000000B System.Boolean Facebook.Unity.Settings.FacebookSettings::get_IsValidAppId()
extern void FacebookSettings_get_IsValidAppId_m38C2628E701D390B5E05A8FE93681A2288B422DA (void);
// 0x0000000C System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Cookie()
extern void FacebookSettings_get_Cookie_m0934E3B820FCCE2B599B24E5EA3300A6629F8792 (void);
// 0x0000000D System.Void Facebook.Unity.Settings.FacebookSettings::set_Cookie(System.Boolean)
extern void FacebookSettings_set_Cookie_m90A36F305AEF7E0B3D26BD27F4F73A33DD574AC7 (void);
// 0x0000000E System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Logging()
extern void FacebookSettings_get_Logging_m0A7E8654D34FD8314E2695563FBED738513DAF76 (void);
// 0x0000000F System.Void Facebook.Unity.Settings.FacebookSettings::set_Logging(System.Boolean)
extern void FacebookSettings_set_Logging_m96E5D1D0886F406209146D8517E6F0560919CFAF (void);
// 0x00000010 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Status()
extern void FacebookSettings_get_Status_mD75339FB8D94F27B96B556BBE2E87C52772A88BB (void);
// 0x00000011 System.Void Facebook.Unity.Settings.FacebookSettings::set_Status(System.Boolean)
extern void FacebookSettings_set_Status_m8CAFF734CA87E8A4E4BEA7A279899C53EE62B578 (void);
// 0x00000012 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Xfbml()
extern void FacebookSettings_get_Xfbml_m2BC9361CD2D372EAD4F12CA6E39B20AA85BD974F (void);
// 0x00000013 System.Void Facebook.Unity.Settings.FacebookSettings::set_Xfbml(System.Boolean)
extern void FacebookSettings_set_Xfbml_m61C488351F4E8CB27F55CDBBBE948E6B17EB8815 (void);
// 0x00000014 System.String Facebook.Unity.Settings.FacebookSettings::get_AndroidKeystorePath()
extern void FacebookSettings_get_AndroidKeystorePath_mAC8B9D8E33914B1B15C3969EEC7E0BD5B5CA6D40 (void);
// 0x00000015 System.Void Facebook.Unity.Settings.FacebookSettings::set_AndroidKeystorePath(System.String)
extern void FacebookSettings_set_AndroidKeystorePath_mE484D7C905C1DACD9AB848ED53165E0E56EFD9EB (void);
// 0x00000016 System.String Facebook.Unity.Settings.FacebookSettings::get_IosURLSuffix()
extern void FacebookSettings_get_IosURLSuffix_mFD7069CDCA4450410060FE64780C0ADFB7A6752F (void);
// 0x00000017 System.Void Facebook.Unity.Settings.FacebookSettings::set_IosURLSuffix(System.String)
extern void FacebookSettings_set_IosURLSuffix_m68CA9082DE506F0A711F2602B1727D5A7E28118C (void);
// 0x00000018 System.String Facebook.Unity.Settings.FacebookSettings::get_ChannelUrl()
extern void FacebookSettings_get_ChannelUrl_m9A4AE36709677ED17724F6AB9AD75E1130C4246D (void);
// 0x00000019 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_FrictionlessRequests()
extern void FacebookSettings_get_FrictionlessRequests_mA023B0C49EAC3590311FE88889092BBC7BFD5421 (void);
// 0x0000001A System.Void Facebook.Unity.Settings.FacebookSettings::set_FrictionlessRequests(System.Boolean)
extern void FacebookSettings_set_FrictionlessRequests_m6901FA823D37FD6E7738088A0144E7A769013F7E (void);
// 0x0000001B System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes> Facebook.Unity.Settings.FacebookSettings::get_AppLinkSchemes()
extern void FacebookSettings_get_AppLinkSchemes_mEF2C984313272F032B0E4B33DE9514413258C3B2 (void);
// 0x0000001C System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLinkSchemes(System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>)
extern void FacebookSettings_set_AppLinkSchemes_mC6FEEBE0E55AF8C98FFFC71E7B3CEC7064E543AA (void);
// 0x0000001D System.String Facebook.Unity.Settings.FacebookSettings::get_UploadAccessToken()
extern void FacebookSettings_get_UploadAccessToken_mC29C781F9B7CD825B1ED04E5FA9E5E95FC17B16D (void);
// 0x0000001E System.Void Facebook.Unity.Settings.FacebookSettings::set_UploadAccessToken(System.String)
extern void FacebookSettings_set_UploadAccessToken_m0A26737AF797C2DDCDC164C81E30D4CAFC70D9AD (void);
// 0x0000001F System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AutoLogAppEventsEnabled()
extern void FacebookSettings_get_AutoLogAppEventsEnabled_m040C830C93A71D071D7493DBEDE95763845CB557 (void);
// 0x00000020 System.Void Facebook.Unity.Settings.FacebookSettings::set_AutoLogAppEventsEnabled(System.Boolean)
extern void FacebookSettings_set_AutoLogAppEventsEnabled_m7064F4A781BDE33CC773233C5BD1ECDA6142B899 (void);
// 0x00000021 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AdvertiserIDCollectionEnabled()
extern void FacebookSettings_get_AdvertiserIDCollectionEnabled_mE46D06ED553C4E6FAD65597DD6DDD9E4598CC27E (void);
// 0x00000022 System.Void Facebook.Unity.Settings.FacebookSettings::set_AdvertiserIDCollectionEnabled(System.Boolean)
extern void FacebookSettings_set_AdvertiserIDCollectionEnabled_m7A19A71234DFF2D4520F0E7DBF7E5DE0A64C6D2D (void);
// 0x00000023 Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
extern void FacebookSettings_get_Instance_mCAE97526EA3E1E726D3910B02294FFEDD8CDDDEE (void);
// 0x00000024 Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
extern void FacebookSettings_get_NullableInstance_m6181BAC36F37158CE5702BE624E49D7855192F44 (void);
// 0x00000025 System.Void Facebook.Unity.Settings.FacebookSettings::RegisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings/OnChangeCallback)
extern void FacebookSettings_RegisterChangeEventCallback_m5ECB51CE8C8F9052F38A3CAB73F1A539F63629FD (void);
// 0x00000026 System.Void Facebook.Unity.Settings.FacebookSettings::UnregisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings/OnChangeCallback)
extern void FacebookSettings_UnregisterChangeEventCallback_m3D4B67C1C05B615618B0C5C2A0BEC825D97995EA (void);
// 0x00000027 System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
extern void FacebookSettings_SettingsChanged_m6313F8116EC22A4042BD2D1847DBD5CB5641D262 (void);
// 0x00000028 System.Void Facebook.Unity.Settings.FacebookSettings::.ctor()
extern void FacebookSettings__ctor_mBC158BC6856CFE4F0F4E2825DF55465671489CAE (void);
// 0x00000029 System.Void Facebook.Unity.Settings.FacebookSettings::.cctor()
extern void FacebookSettings__cctor_mB24D0E4F450F891A9A6B6884F4EA7F1B6F0CB40C (void);
// 0x0000002A System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::.ctor(System.Object,System.IntPtr)
extern void OnChangeCallback__ctor_mEAB8B87C326157955942D71112B10F11A4A1E9C5 (void);
// 0x0000002B System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::Invoke()
extern void OnChangeCallback_Invoke_m5B489E62F675A2006A3AF52507A606A862D27011 (void);
// 0x0000002C System.IAsyncResult Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnChangeCallback_BeginInvoke_mD6CF8990CF73746BD74EB5A7CC54A420C566F6DF (void);
// 0x0000002D System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::EndInvoke(System.IAsyncResult)
extern void OnChangeCallback_EndInvoke_m6ADA64DFEC0F801D4B387A7F4221937635380F27 (void);
// 0x0000002E System.Void Facebook.Unity.Settings.FacebookSettings/UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
extern void UrlSchemes__ctor_m4C46C1E31E0E1E8F0E562E7870B6543E8F8BD543 (void);
// 0x0000002F System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings/UrlSchemes::get_Schemes()
extern void UrlSchemes_get_Schemes_mB8EDC548CF98394145E1823B9628397F7C5E95B3 (void);
// 0x00000030 System.Void Facebook.Unity.Settings.FacebookSettings/UrlSchemes::set_Schemes(System.Collections.Generic.List`1<System.String>)
extern void UrlSchemes_set_Schemes_m470793C5D9931FEB434EEA531341C1D17AC36F2C (void);
// 0x00000031 System.Void Facebook.Unity.Settings.FacebookSettings/<>c::.cctor()
extern void U3CU3Ec__cctor_m6231713E6DF00C5312C032B164974EAE9A9E7303 (void);
// 0x00000032 System.Void Facebook.Unity.Settings.FacebookSettings/<>c::.ctor()
extern void U3CU3Ec__ctor_m9B78EA27C6DB210BC608E764D8A043823931B479 (void);
// 0x00000033 System.Void Facebook.Unity.Settings.FacebookSettings/<>c::<SettingsChanged>b__80_0(Facebook.Unity.Settings.FacebookSettings/OnChangeCallback)
extern void U3CU3Ec_U3CSettingsChangedU3Eb__80_0_m56B9AD79B2AFB2BD50D787DE1DBBC8951951BDC6 (void);
static Il2CppMethodPointer s_methodPointers[51] = 
{
	FacebookSettings_get_SelectedAppIndex_mD1184BAB88CC164E39929F0C7D23817BF957C2AD,
	FacebookSettings_set_SelectedAppIndex_m28F647F0F7FAB79DFED2B93EB70A2958213E5435,
	FacebookSettings_get_AppIds_m23292AF4C70A2F16EF253C33796547D18B95A930,
	FacebookSettings_set_AppIds_m752E2013829105D0AC207A4EFC3E12E5045B7D14,
	FacebookSettings_get_AppLabels_mC8296B31F2FB08025F3CC2605DE5358E2209D737,
	FacebookSettings_set_AppLabels_mF7B410F53DA07D2F5FB8F3DC0F039DDEA58D309C,
	FacebookSettings_get_ClientTokens_m07B3D9E8EF88579C198B4E0F8F5AEF58EA6530CB,
	FacebookSettings_set_ClientTokens_mEE165306704C4CCE5A326F7E80C494EBFC783978,
	FacebookSettings_get_AppId_m0BF08B1DBEEA55EE9DB9908B3D0B37377F837C2E,
	FacebookSettings_get_ClientToken_mD7A7153CA46CCED350E36461FDC2D60E771DD96E,
	FacebookSettings_get_IsValidAppId_m38C2628E701D390B5E05A8FE93681A2288B422DA,
	FacebookSettings_get_Cookie_m0934E3B820FCCE2B599B24E5EA3300A6629F8792,
	FacebookSettings_set_Cookie_m90A36F305AEF7E0B3D26BD27F4F73A33DD574AC7,
	FacebookSettings_get_Logging_m0A7E8654D34FD8314E2695563FBED738513DAF76,
	FacebookSettings_set_Logging_m96E5D1D0886F406209146D8517E6F0560919CFAF,
	FacebookSettings_get_Status_mD75339FB8D94F27B96B556BBE2E87C52772A88BB,
	FacebookSettings_set_Status_m8CAFF734CA87E8A4E4BEA7A279899C53EE62B578,
	FacebookSettings_get_Xfbml_m2BC9361CD2D372EAD4F12CA6E39B20AA85BD974F,
	FacebookSettings_set_Xfbml_m61C488351F4E8CB27F55CDBBBE948E6B17EB8815,
	FacebookSettings_get_AndroidKeystorePath_mAC8B9D8E33914B1B15C3969EEC7E0BD5B5CA6D40,
	FacebookSettings_set_AndroidKeystorePath_mE484D7C905C1DACD9AB848ED53165E0E56EFD9EB,
	FacebookSettings_get_IosURLSuffix_mFD7069CDCA4450410060FE64780C0ADFB7A6752F,
	FacebookSettings_set_IosURLSuffix_m68CA9082DE506F0A711F2602B1727D5A7E28118C,
	FacebookSettings_get_ChannelUrl_m9A4AE36709677ED17724F6AB9AD75E1130C4246D,
	FacebookSettings_get_FrictionlessRequests_mA023B0C49EAC3590311FE88889092BBC7BFD5421,
	FacebookSettings_set_FrictionlessRequests_m6901FA823D37FD6E7738088A0144E7A769013F7E,
	FacebookSettings_get_AppLinkSchemes_mEF2C984313272F032B0E4B33DE9514413258C3B2,
	FacebookSettings_set_AppLinkSchemes_mC6FEEBE0E55AF8C98FFFC71E7B3CEC7064E543AA,
	FacebookSettings_get_UploadAccessToken_mC29C781F9B7CD825B1ED04E5FA9E5E95FC17B16D,
	FacebookSettings_set_UploadAccessToken_m0A26737AF797C2DDCDC164C81E30D4CAFC70D9AD,
	FacebookSettings_get_AutoLogAppEventsEnabled_m040C830C93A71D071D7493DBEDE95763845CB557,
	FacebookSettings_set_AutoLogAppEventsEnabled_m7064F4A781BDE33CC773233C5BD1ECDA6142B899,
	FacebookSettings_get_AdvertiserIDCollectionEnabled_mE46D06ED553C4E6FAD65597DD6DDD9E4598CC27E,
	FacebookSettings_set_AdvertiserIDCollectionEnabled_m7A19A71234DFF2D4520F0E7DBF7E5DE0A64C6D2D,
	FacebookSettings_get_Instance_mCAE97526EA3E1E726D3910B02294FFEDD8CDDDEE,
	FacebookSettings_get_NullableInstance_m6181BAC36F37158CE5702BE624E49D7855192F44,
	FacebookSettings_RegisterChangeEventCallback_m5ECB51CE8C8F9052F38A3CAB73F1A539F63629FD,
	FacebookSettings_UnregisterChangeEventCallback_m3D4B67C1C05B615618B0C5C2A0BEC825D97995EA,
	FacebookSettings_SettingsChanged_m6313F8116EC22A4042BD2D1847DBD5CB5641D262,
	FacebookSettings__ctor_mBC158BC6856CFE4F0F4E2825DF55465671489CAE,
	FacebookSettings__cctor_mB24D0E4F450F891A9A6B6884F4EA7F1B6F0CB40C,
	OnChangeCallback__ctor_mEAB8B87C326157955942D71112B10F11A4A1E9C5,
	OnChangeCallback_Invoke_m5B489E62F675A2006A3AF52507A606A862D27011,
	OnChangeCallback_BeginInvoke_mD6CF8990CF73746BD74EB5A7CC54A420C566F6DF,
	OnChangeCallback_EndInvoke_m6ADA64DFEC0F801D4B387A7F4221937635380F27,
	UrlSchemes__ctor_m4C46C1E31E0E1E8F0E562E7870B6543E8F8BD543,
	UrlSchemes_get_Schemes_mB8EDC548CF98394145E1823B9628397F7C5E95B3,
	UrlSchemes_set_Schemes_m470793C5D9931FEB434EEA531341C1D17AC36F2C,
	U3CU3Ec__cctor_m6231713E6DF00C5312C032B164974EAE9A9E7303,
	U3CU3Ec__ctor_m9B78EA27C6DB210BC608E764D8A043823931B479,
	U3CU3Ec_U3CSettingsChangedU3Eb__80_0_m56B9AD79B2AFB2BD50D787DE1DBBC8951951BDC6,
};
static const int32_t s_InvokerIndices[51] = 
{
	2526,
	2504,
	2531,
	2506,
	2531,
	2506,
	2531,
	2506,
	2531,
	2531,
	2538,
	2538,
	2508,
	2538,
	2508,
	2538,
	2508,
	2538,
	2508,
	2531,
	2506,
	2531,
	2506,
	2531,
	2538,
	2508,
	2531,
	2506,
	2531,
	2506,
	2538,
	2508,
	2538,
	2508,
	2531,
	2531,
	2506,
	2506,
	2547,
	1575,
	2547,
	823,
	1575,
	617,
	1323,
	1323,
	1532,
	1323,
	2547,
	1575,
	1323,
};
extern const CustomAttributesCacheGenerator g_Facebook_Unity_Settings_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Facebook_Unity_Settings_CodeGenModule;
const Il2CppCodeGenModule g_Facebook_Unity_Settings_CodeGenModule = 
{
	"Facebook.Unity.Settings.dll",
	51,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Facebook_Unity_Settings_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
