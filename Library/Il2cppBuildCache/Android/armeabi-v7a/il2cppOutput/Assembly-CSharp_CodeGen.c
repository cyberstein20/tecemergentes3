﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Counter::Start()
extern void Counter_Start_m77A2B23760AA7DA12C9BBEE742221AC01B67AFB3 (void);
// 0x00000002 System.Void Counter::Update()
extern void Counter_Update_m750B99E43903C32501752EDB091B76D4D8591063 (void);
// 0x00000003 System.Void Counter::RestartCounter()
extern void Counter_RestartCounter_m226B4BBCD884BFC697FFA4BA63BAC375D9C9CAC9 (void);
// 0x00000004 System.Single Counter::GetDefaultTime()
extern void Counter_GetDefaultTime_mC6BC8239D56E1AA83C0F428B7FBAA341717C4F75 (void);
// 0x00000005 System.Void Counter::.ctor()
extern void Counter__ctor_m12F27AA8C9EDE176E9543076DB214639246AAC73 (void);
// 0x00000006 System.Void FacebookAnalytics::Start()
extern void FacebookAnalytics_Start_m1C6767570A1730DFE50A772384B64C19A7493B63 (void);
// 0x00000007 System.Void FacebookAnalytics::OnStartedApp()
extern void FacebookAnalytics_OnStartedApp_m4B856A59C0F662118A676ACC6DC8E28E48B5E575 (void);
// 0x00000008 System.Void FacebookAnalytics::OnInitComplete()
extern void FacebookAnalytics_OnInitComplete_mFD47C43545BC9C656005C5DD181B2462D13BE826 (void);
// 0x00000009 System.Void FacebookAnalytics::SendFacebookEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FacebookAnalytics_SendFacebookEvent_mCC928CBBE2E72847613AB2D374AAAA2CDED26F9B (void);
// 0x0000000A System.Void FacebookAnalytics::OnHideUnity()
extern void FacebookAnalytics_OnHideUnity_m5714AC3123F1E74E548BA146347D63E1E50119A3 (void);
// 0x0000000B System.Void FacebookAnalytics::.ctor()
extern void FacebookAnalytics__ctor_mEE760AF0BADC7F272DCCE241723775BD6EF1ACCE (void);
// 0x0000000C System.Void Quiz::.ctor()
extern void Quiz__ctor_m4925290BE223072E849F0E68926E5B86AD0CE4C7 (void);
// 0x0000000D System.Void QuizGenerator::Start()
extern void QuizGenerator_Start_mC660B3307FBEF3D6E38689D8A0F38F4731CB5BE7 (void);
// 0x0000000E System.Void QuizGenerator::Update()
extern void QuizGenerator_Update_m8B36A52483CCFE7AB94E44F93C0722C36E38A164 (void);
// 0x0000000F System.Void QuizGenerator::LoadQuiz()
extern void QuizGenerator_LoadQuiz_mBB3C8F9208789E26AF2D93841DA949AB3B69CF90 (void);
// 0x00000010 System.Void QuizGenerator::CompareAnswers(System.Int32)
extern void QuizGenerator_CompareAnswers_mBF5CCBFDBC03EF595C505A733C5043EB5D9B1223 (void);
// 0x00000011 System.Void QuizGenerator::ShowAnswers(System.Boolean)
extern void QuizGenerator_ShowAnswers_m5ECC3EA497B6EA13654C04F739B0D5B3BBBFE9EF (void);
// 0x00000012 System.Collections.IEnumerator QuizGenerator::SettingOffInSeconds()
extern void QuizGenerator_SettingOffInSeconds_mB24CD6BB965B1ECA68F17AE6C04A74A2F793918E (void);
// 0x00000013 System.Void QuizGenerator::ResetValues()
extern void QuizGenerator_ResetValues_m53957B427324F5D598ADD73F6753BF38998777C4 (void);
// 0x00000014 System.Void QuizGenerator::SwapObjects()
extern void QuizGenerator_SwapObjects_m4D87B199F3232D0960276C5C1DB25EC7426E482F (void);
// 0x00000015 System.Void QuizGenerator::.ctor()
extern void QuizGenerator__ctor_m97028B7BC244EDFC5CF85D9FD07749640DC1CDAA (void);
// 0x00000016 System.Void QuizGenerator/<SettingOffInSeconds>d__47::.ctor(System.Int32)
extern void U3CSettingOffInSecondsU3Ed__47__ctor_mF675BBFAB0FDA1AE944BCCB7291C8B04F405BBB3 (void);
// 0x00000017 System.Void QuizGenerator/<SettingOffInSeconds>d__47::System.IDisposable.Dispose()
extern void U3CSettingOffInSecondsU3Ed__47_System_IDisposable_Dispose_m5E06BCFBAE84B00A2AAC0136295914272EEF8DEF (void);
// 0x00000018 System.Boolean QuizGenerator/<SettingOffInSeconds>d__47::MoveNext()
extern void U3CSettingOffInSecondsU3Ed__47_MoveNext_m448E22D3B7650AE001E04A554E46BE2736E69362 (void);
// 0x00000019 System.Object QuizGenerator/<SettingOffInSeconds>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSettingOffInSecondsU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57CF15CED2AAC5B8AFD0322CBC7B16F66F9594B6 (void);
// 0x0000001A System.Void QuizGenerator/<SettingOffInSeconds>d__47::System.Collections.IEnumerator.Reset()
extern void U3CSettingOffInSecondsU3Ed__47_System_Collections_IEnumerator_Reset_m691AEF82CE7137FF2505BDA572289DA3CD9DF50A (void);
// 0x0000001B System.Object QuizGenerator/<SettingOffInSeconds>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CSettingOffInSecondsU3Ed__47_System_Collections_IEnumerator_get_Current_m5C6780BB0A8B2C7F50A67698D7B70A2709A3DA13 (void);
// 0x0000001C System.Void RotateOnAxis::Start()
extern void RotateOnAxis_Start_m492B0401395AD778FCD31DBBD5654DD5BD2D268B (void);
// 0x0000001D System.Void RotateOnAxis::Update()
extern void RotateOnAxis_Update_m6C79790B9E4E994B71B014FBB7C766F5AB4E4C40 (void);
// 0x0000001E System.Void RotateOnAxis::.ctor()
extern void RotateOnAxis__ctor_m0E5BF4821EBF602F027D95D5A28594D254472359 (void);
// 0x0000001F System.Void SoundManager::set_MusicVolume(System.Single)
extern void SoundManager_set_MusicVolume_m90A77FD5CCE8CA920F30DA582B2D7E7E238C8F75 (void);
// 0x00000020 System.Single SoundManager::get_MusicVolume()
extern void SoundManager_get_MusicVolume_m25A521EC19EC22F3416BBF23F8257FC6535869B5 (void);
// 0x00000021 System.Void SoundManager::set_SoundVolume(System.Single)
extern void SoundManager_set_SoundVolume_m223100DD55017CE6CB41C3EAF4AC1DFC2D4B5402 (void);
// 0x00000022 System.Single SoundManager::get_SoundVolume()
extern void SoundManager_get_SoundVolume_m990DD0261A123A99D457C3F5712195D38AC6C7F4 (void);
// 0x00000023 System.Void SoundManager::Awake()
extern void SoundManager_Awake_m78F953F39CFB3F539240E1226D04270B793B1A76 (void);
// 0x00000024 System.Void SoundManager::Start()
extern void SoundManager_Start_mE4854722D4248D4CCD33E4202389B3F187C51303 (void);
// 0x00000025 System.Void SoundManager::MomentUpdateSoundFxVolume(System.Single)
extern void SoundManager_MomentUpdateSoundFxVolume_mBE0654C61D7BBC558A4C5AE780DD9724054CE3DD (void);
// 0x00000026 System.Void SoundManager::MomentUpdateMusicVolume(System.Single)
extern void SoundManager_MomentUpdateMusicVolume_m1105A3AA5756BB15D2D9AFF8B106B0DE87C7EED2 (void);
// 0x00000027 System.Void SoundManager::UpdateSoundFxVolume(System.Single)
extern void SoundManager_UpdateSoundFxVolume_m7ABCBD129F252143544D58C90938A6A4EA944A1B (void);
// 0x00000028 System.Void SoundManager::UpdateMusicVolume(System.Single)
extern void SoundManager_UpdateMusicVolume_mFAC1E7817353B4B9D9EED5804902D8815563DF47 (void);
// 0x00000029 System.Void SoundManager::PlaySfxFromScene(UnityEngine.AudioClip)
extern void SoundManager_PlaySfxFromScene_mD35F5CABD56747912F96290214A266E5AEB59195 (void);
// 0x0000002A System.Void SoundManager::PlaySfx(UnityEngine.AudioClip)
extern void SoundManager_PlaySfx_m6425348A8C7F351D6F2E984E302DDDB217902A56 (void);
// 0x0000002B System.Void SoundManager::PlaySfx(UnityEngine.AudioClip,System.Single)
extern void SoundManager_PlaySfx_m032E597366D39EFA0170EE48A169CBE9A36E1F1C (void);
// 0x0000002C System.Void SoundManager::PlayMusic(UnityEngine.AudioClip)
extern void SoundManager_PlayMusic_m3264968E597EA6ECE72CBBD20DB8DB907DA2F9E0 (void);
// 0x0000002D System.Void SoundManager::PlayMusic(UnityEngine.AudioClip,System.Single)
extern void SoundManager_PlayMusic_mE58B9C264B87AF9D0BD6252A830005565127D998 (void);
// 0x0000002E System.Void SoundManager::PlaySound(UnityEngine.AudioClip,UnityEngine.AudioSource)
extern void SoundManager_PlaySound_mCFFF1085F6EB1132234B9276F0EF18F4E67C49F8 (void);
// 0x0000002F System.Void SoundManager::PlaySound(UnityEngine.AudioClip,UnityEngine.AudioSource,System.Single)
extern void SoundManager_PlaySound_m3E84C8B2AE269DD7F076709F07F7F0336BE1534B (void);
// 0x00000030 System.Void SoundManager::PlaySoundClick()
extern void SoundManager_PlaySoundClick_mC489671AAA75CAC5AAEB0E93507A0A4887E98820 (void);
// 0x00000031 System.Void SoundManager::.ctor()
extern void SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE (void);
// 0x00000032 System.Void UA::Start()
extern void UA_Start_mC6FD095022B81B808C56E1113F0506834AE655F1 (void);
// 0x00000033 System.Void UA::Update()
extern void UA_Update_m05E5D9DC1F4557AF9660C8597007414D8C16D3EA (void);
// 0x00000034 System.Void UA::SendBasicEvent(System.String)
extern void UA_SendBasicEvent_m15DA4ADDAB510E5C243ECF42BA5F2E8757102E45 (void);
// 0x00000035 System.Void UA::SendAdvancedEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void UA_SendAdvancedEvent_m21F20DBE8DAD4FF2CA7B3824E4DE84E96845D251 (void);
// 0x00000036 System.Void UA::.ctor()
extern void UA__ctor_mF366F28535CEB5AA0EAA0B813509822CA4C77741 (void);
// 0x00000037 System.Void TriviaQuizKit.BackgroundMusic::Awake()
extern void BackgroundMusic_Awake_m4CAD4EC802EBE49CBB1D3017CA8C07975A1116F4 (void);
// 0x00000038 System.Void TriviaQuizKit.BackgroundMusic::Start()
extern void BackgroundMusic_Start_mB74FEB54CA0C53089D70E64E529F372AFB887D9D (void);
// 0x00000039 System.Void TriviaQuizKit.BackgroundMusic::.ctor()
extern void BackgroundMusic__ctor_m09D013A512092A19A4E1F025697015981BE2D7E7 (void);
// 0x0000003A System.Void TriviaQuizKit.BaseScreen::OpenPopup(System.String,System.Action`1<T>)
// 0x0000003B System.Void TriviaQuizKit.BaseScreen::ClosePopup()
extern void BaseScreen_ClosePopup_mFB343341D2CD06B1CA2620B5D650CBCF53FBDFB8 (void);
// 0x0000003C System.Collections.IEnumerator TriviaQuizKit.BaseScreen::OpenPopupAsync(System.String,System.Action`1<T>)
// 0x0000003D System.Collections.IEnumerator TriviaQuizKit.BaseScreen::FadeIn(UnityEngine.UI.Graphic,System.Single)
extern void BaseScreen_FadeIn_m7548331997F6688B1A63FACEF2FDCDE6A7D25B81 (void);
// 0x0000003E System.Collections.IEnumerator TriviaQuizKit.BaseScreen::FadeOut(UnityEngine.UI.Graphic,System.Single,System.Action)
extern void BaseScreen_FadeOut_mA5D73509584E28F54356E92D73F7CE6E993BB31F (void);
// 0x0000003F System.Void TriviaQuizKit.BaseScreen::.ctor()
extern void BaseScreen__ctor_mEE7278500381B0A89C10D36030401EE1660C5625 (void);
// 0x00000040 System.Void TriviaQuizKit.BaseScreen/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m0861B6FDC3BCF2E5D561EF840ED348F2EF95C9CB (void);
// 0x00000041 System.Void TriviaQuizKit.BaseScreen/<>c__DisplayClass4_0::<ClosePopup>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CClosePopupU3Eb__0_m98BA085A6348E9F6EF6EFB1CDBAEEB8B4A85591C (void);
// 0x00000042 System.Void TriviaQuizKit.BaseScreen/<OpenPopupAsync>d__5`1::.ctor(System.Int32)
// 0x00000043 System.Void TriviaQuizKit.BaseScreen/<OpenPopupAsync>d__5`1::System.IDisposable.Dispose()
// 0x00000044 System.Boolean TriviaQuizKit.BaseScreen/<OpenPopupAsync>d__5`1::MoveNext()
// 0x00000045 System.Object TriviaQuizKit.BaseScreen/<OpenPopupAsync>d__5`1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x00000046 System.Void TriviaQuizKit.BaseScreen/<OpenPopupAsync>d__5`1::System.Collections.IEnumerator.Reset()
// 0x00000047 System.Object TriviaQuizKit.BaseScreen/<OpenPopupAsync>d__5`1::System.Collections.IEnumerator.get_Current()
// 0x00000048 System.Void TriviaQuizKit.BaseScreen/<FadeIn>d__6::.ctor(System.Int32)
extern void U3CFadeInU3Ed__6__ctor_m3AF82D0DB0670D96D6DEBBD91DFBEED13AFFDACF (void);
// 0x00000049 System.Void TriviaQuizKit.BaseScreen/<FadeIn>d__6::System.IDisposable.Dispose()
extern void U3CFadeInU3Ed__6_System_IDisposable_Dispose_mC413CD4A23A7A97325D7B8F6AC45209820EC895C (void);
// 0x0000004A System.Boolean TriviaQuizKit.BaseScreen/<FadeIn>d__6::MoveNext()
extern void U3CFadeInU3Ed__6_MoveNext_mECB8DADE96CA6545A40C2BEB82F482C4A814FD4A (void);
// 0x0000004B System.Object TriviaQuizKit.BaseScreen/<FadeIn>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeInU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F65DA37CD06796A81596AE0783C36D604771A59 (void);
// 0x0000004C System.Void TriviaQuizKit.BaseScreen/<FadeIn>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFadeInU3Ed__6_System_Collections_IEnumerator_Reset_m6A703D2007F95D1087C06B5CB7458B4AF7315A5A (void);
// 0x0000004D System.Object TriviaQuizKit.BaseScreen/<FadeIn>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFadeInU3Ed__6_System_Collections_IEnumerator_get_Current_m35E8DB4D3348E057AB5C84B3594715DFEE572127 (void);
// 0x0000004E System.Void TriviaQuizKit.BaseScreen/<FadeOut>d__7::.ctor(System.Int32)
extern void U3CFadeOutU3Ed__7__ctor_m64E38BA229295767CB4CDB535A4C5FE3686F7C92 (void);
// 0x0000004F System.Void TriviaQuizKit.BaseScreen/<FadeOut>d__7::System.IDisposable.Dispose()
extern void U3CFadeOutU3Ed__7_System_IDisposable_Dispose_m3EA85C292A6691FC50731F9E1AC0B0AFE3C4F215 (void);
// 0x00000050 System.Boolean TriviaQuizKit.BaseScreen/<FadeOut>d__7::MoveNext()
extern void U3CFadeOutU3Ed__7_MoveNext_m39ED71A4E2BFCFF691CA82402815D9FB688652D6 (void);
// 0x00000051 System.Object TriviaQuizKit.BaseScreen/<FadeOut>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4180985992B7D598CE132F467634A82634D78CE2 (void);
// 0x00000052 System.Void TriviaQuizKit.BaseScreen/<FadeOut>d__7::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutU3Ed__7_System_Collections_IEnumerator_Reset_m664FA32A3FF8C6EFA0EC0B99475FEB9EF697FE6E (void);
// 0x00000053 System.Object TriviaQuizKit.BaseScreen/<FadeOut>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutU3Ed__7_System_Collections_IEnumerator_get_Current_m17EA0E60F0612B947F0B46C5B64747354E591B70 (void);
// 0x00000054 System.String TriviaQuizKit.EditorUtils::GetEditorFriendlyText(System.String,System.Int32)
extern void EditorUtils_GetEditorFriendlyText_mAEA742290469004E6FDA91F7255D7B8F3C149CFA (void);
// 0x00000055 System.Void TriviaQuizKit.FlatButton::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void FlatButton_OnPointerEnter_mB866DE2531A865B611090E36574F37AA9E661FE5 (void);
// 0x00000056 System.Void TriviaQuizKit.FlatButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void FlatButton_OnPointerExit_mAD9354840CFFDF1D08FE84D94742C467D63E1A54 (void);
// 0x00000057 System.Void TriviaQuizKit.FlatButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FlatButton_OnPointerDown_m339D8786CC5D253BCA12812065F0AF9E661A3E54 (void);
// 0x00000058 System.Void TriviaQuizKit.FlatButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FlatButton_OnPointerUp_m85FC2BF03C81D7C1943A40F11FE91E2130C3DC4C (void);
// 0x00000059 System.Void TriviaQuizKit.FlatButton::.ctor()
extern void FlatButton__ctor_mF2427B7E73948E7E154529CAFD4EDAA03600EE2B (void);
// 0x0000005A System.Void TriviaQuizKit.Initialization::Awake()
extern void Initialization_Awake_mF78C27CEB63177E43C4F128BEDFD4C70742815BB (void);
// 0x0000005B System.Void TriviaQuizKit.Initialization::.ctor()
extern void Initialization__ctor_mC0D2B0EE4BBCE51F3D5FCE8DD99B5880AB29810F (void);
// 0x0000005C System.Void TriviaQuizKit.ListShuffle::Shuffle(System.Collections.Generic.IList`1<T>)
// 0x0000005D System.Void TriviaQuizKit.ListShuffle::.cctor()
extern void ListShuffle__cctor_m709A5C74189055FADE45CB0B5477C9EC8C34D5A1 (void);
// 0x0000005E System.Void TriviaQuizKit.ObjectPool::Awake()
extern void ObjectPool_Awake_m11291A5E655A45F64639C4E06D88E849E11DF4FA (void);
// 0x0000005F System.Void TriviaQuizKit.ObjectPool::Start()
extern void ObjectPool_Start_mD873EF2EF022B57E3A7C647F56BE319340091634 (void);
// 0x00000060 UnityEngine.GameObject TriviaQuizKit.ObjectPool::GetObject()
extern void ObjectPool_GetObject_m5401D10AFC1440C3EC86BABB5F0507664AE71BFF (void);
// 0x00000061 System.Void TriviaQuizKit.ObjectPool::ReturnObject(UnityEngine.GameObject)
extern void ObjectPool_ReturnObject_mBFDA7B62D6A15AFC432EDD5B223790BE676E313D (void);
// 0x00000062 System.Void TriviaQuizKit.ObjectPool::Reset()
extern void ObjectPool_Reset_m5DAB45765BBFD13AC2FDC8B7A1CBA9BD7F9ED6D2 (void);
// 0x00000063 UnityEngine.GameObject TriviaQuizKit.ObjectPool::CreateInstance()
extern void ObjectPool_CreateInstance_m1633AA63A2E8E9E8104544F155C87898B5F26FB0 (void);
// 0x00000064 System.Void TriviaQuizKit.ObjectPool::.ctor()
extern void ObjectPool__ctor_m05686ACC18AFF85D5A729D1F349CF32B0CEA4DE4 (void);
// 0x00000065 System.Void TriviaQuizKit.PooledObject::.ctor()
extern void PooledObject__ctor_mA3B1413D9FFE2C0B7178CE76B622109320AB60AF (void);
// 0x00000066 System.Void TriviaQuizKit.PlaySound::Play(System.String)
extern void PlaySound_Play_mCB2F79F470A715AE0A1DA330144C49211FF68A47 (void);
// 0x00000067 System.Void TriviaQuizKit.PlaySound::.ctor()
extern void PlaySound__ctor_m1700BC274CCE4D31FE18CB40B1F68AEB0FFA791A (void);
// 0x00000068 System.Void TriviaQuizKit.Popup::Awake()
extern void Popup_Awake_m2AB2892B941A7AC659F7A54BB43284C058A50F74 (void);
// 0x00000069 System.Void TriviaQuizKit.Popup::Start()
extern void Popup_Start_m40956E739B20D1054876AA5257E6E55915F25CE7 (void);
// 0x0000006A System.Void TriviaQuizKit.Popup::Close()
extern void Popup_Close_mC675E3C5E8F7E2780F4481505E92CB52E09F80D3 (void);
// 0x0000006B System.Collections.IEnumerator TriviaQuizKit.Popup::DestroyPopup()
extern void Popup_DestroyPopup_mF22233DF7742B12343C8D5D69BFC8CC8FD8E1C30 (void);
// 0x0000006C System.Void TriviaQuizKit.Popup::.ctor()
extern void Popup__ctor_m3D05A12D022EC52308B8E4367BFC18F5314F48A3 (void);
// 0x0000006D System.Void TriviaQuizKit.Popup/<DestroyPopup>d__7::.ctor(System.Int32)
extern void U3CDestroyPopupU3Ed__7__ctor_mF5C0BD3B6ED00CD38EF0F406EDB560795AE59141 (void);
// 0x0000006E System.Void TriviaQuizKit.Popup/<DestroyPopup>d__7::System.IDisposable.Dispose()
extern void U3CDestroyPopupU3Ed__7_System_IDisposable_Dispose_m73FA6EB3AA01FD75A4663F3D0EB005AABF6966AD (void);
// 0x0000006F System.Boolean TriviaQuizKit.Popup/<DestroyPopup>d__7::MoveNext()
extern void U3CDestroyPopupU3Ed__7_MoveNext_m2F22DD9CA32ED62695AC1CA3383E515DFB1E2270 (void);
// 0x00000070 System.Object TriviaQuizKit.Popup/<DestroyPopup>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyPopupU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD75D1019AE9556F0D97B8A0E9841D0F127579651 (void);
// 0x00000071 System.Void TriviaQuizKit.Popup/<DestroyPopup>d__7::System.Collections.IEnumerator.Reset()
extern void U3CDestroyPopupU3Ed__7_System_Collections_IEnumerator_Reset_m4378C21E7218CE13B00BF98161B49904EF6DC2CB (void);
// 0x00000072 System.Object TriviaQuizKit.Popup/<DestroyPopup>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyPopupU3Ed__7_System_Collections_IEnumerator_get_Current_m1021DF0E92D847245CCA35FB28744C1340DB19B1 (void);
// 0x00000073 System.Void TriviaQuizKit.SceneTransition::TravelToScene(System.String)
extern void SceneTransition_TravelToScene_m2DE78FF0393ED726DA307C204BC6B186D2CE9A06 (void);
// 0x00000074 System.Void TriviaQuizKit.SceneTransition::.ctor()
extern void SceneTransition__ctor_m4A9E1A606C4F7A17B8E9DA785807890D9DF2489E (void);
// 0x00000075 System.Void TriviaQuizKit.SoundFx::Awake()
extern void SoundFx_Awake_m9C47BC94B4C96791755C2B4582F17D14B99A03EF (void);
// 0x00000076 System.Void TriviaQuizKit.SoundFx::Play(UnityEngine.AudioClip,System.Boolean)
extern void SoundFx_Play_m55978E0E95876051C8F298CAF7D6043865B4A66B (void);
// 0x00000077 System.Void TriviaQuizKit.SoundFx::DisableSoundFx()
extern void SoundFx_DisableSoundFx_m4F64B856737C814E9E59F448D985EAF4B766842B (void);
// 0x00000078 System.Void TriviaQuizKit.SoundFx::.ctor()
extern void SoundFx__ctor_m4796FD05F79C2F3780769AD71E3C6E136A013FC0 (void);
// 0x00000079 System.Void TriviaQuizKit.SoundManager::Awake()
extern void SoundManager_Awake_mBD44EAFC434C683D54B0A32C1D83E63F8C616883 (void);
// 0x0000007A System.Void TriviaQuizKit.SoundManager::Start()
extern void SoundManager_Start_m4D44D361B57491668B1CCF83DEBA11F08A482055 (void);
// 0x0000007B System.Void TriviaQuizKit.SoundManager::AddSounds(System.Collections.Generic.List`1<UnityEngine.AudioClip>)
extern void SoundManager_AddSounds_mDFC34452D26D4C677C26CA33BB31BA9F9F922B34 (void);
// 0x0000007C System.Void TriviaQuizKit.SoundManager::RemoveSounds(System.Collections.Generic.List`1<UnityEngine.AudioClip>)
extern void SoundManager_RemoveSounds_m3D34330165A23B11ABCDB77A46B356E90B6931EE (void);
// 0x0000007D System.Void TriviaQuizKit.SoundManager::PlaySound(UnityEngine.AudioClip,System.Boolean)
extern void SoundManager_PlaySound_mC8FB5A0479DF1F5D80B21012446F762F848340D7 (void);
// 0x0000007E System.Void TriviaQuizKit.SoundManager::PlaySound(System.String,System.Boolean)
extern void SoundManager_PlaySound_m2066AB50FD4B0DF83B42351FAC7A9CEBB9337D79 (void);
// 0x0000007F System.Void TriviaQuizKit.SoundManager::SetSoundEnabled(System.Boolean)
extern void SoundManager_SetSoundEnabled_mC5D28DEFF38EE6A2CFB95DF564BD03014F511C3D (void);
// 0x00000080 System.Void TriviaQuizKit.SoundManager::SetMusicEnabled(System.Boolean)
extern void SoundManager_SetMusicEnabled_mFBBBF622A886723BC6EE6A74CC19E518B1F7B456 (void);
// 0x00000081 System.Void TriviaQuizKit.SoundManager::ToggleSound()
extern void SoundManager_ToggleSound_mC7C04FFAFC375410645589EB1726D278B83E70E4 (void);
// 0x00000082 System.Void TriviaQuizKit.SoundManager::ToggleMusic()
extern void SoundManager_ToggleMusic_m4526F4E7FEAD0DD2F448E77F777FDCD786C8D6A8 (void);
// 0x00000083 System.Void TriviaQuizKit.SoundManager::.ctor()
extern void SoundManager__ctor_m77FD5CDD532E5DFC0719F1023FF2F86A68D66AB3 (void);
// 0x00000084 System.Void TriviaQuizKit.SpriteSwapper::Awake()
extern void SpriteSwapper_Awake_m8AC81381468B8383EC49F5CAFD96AD080E6AC81C (void);
// 0x00000085 System.Void TriviaQuizKit.SpriteSwapper::SwapSprite()
extern void SpriteSwapper_SwapSprite_m1766E4CFE9746516ECE989AE4852E5D9DB4729B0 (void);
// 0x00000086 System.Void TriviaQuizKit.SpriteSwapper::SetEnabled(System.Boolean)
extern void SpriteSwapper_SetEnabled_m4178F54C57FE1C64F08E3BE7B5BF16338CEB1DC5 (void);
// 0x00000087 System.Void TriviaQuizKit.SpriteSwapper::.ctor()
extern void SpriteSwapper__ctor_m2D41D64B00E3065CD3796D10A6FE2CEC0AB86A06 (void);
// 0x00000088 System.Void TriviaQuizKit.ToggleButton::Awake()
extern void ToggleButton_Awake_mBA7A855EE37F2E4D067A51EDF01DD54DFE074255 (void);
// 0x00000089 System.Void TriviaQuizKit.ToggleButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ToggleButton_OnPointerDown_m71E1FE60739125F6B444931440785BAB661F57CC (void);
// 0x0000008A System.Void TriviaQuizKit.ToggleButton::SetToggled(System.Boolean)
extern void ToggleButton_SetToggled_mBAB2B690A3A9D27928DF8D6686A8EC2EC42A4179 (void);
// 0x0000008B System.Void TriviaQuizKit.ToggleButton::.ctor()
extern void ToggleButton__ctor_m23FFE637945801AC0280B56F4D7CC6EA86677218 (void);
// 0x0000008C System.Void TriviaQuizKit.ToggleButtonGroup::SetToggle(TriviaQuizKit.ToggleButton)
extern void ToggleButtonGroup_SetToggle_m0CAFD6030F9B6F8550CF400A3131EEAAD89192D9 (void);
// 0x0000008D System.Void TriviaQuizKit.ToggleButtonGroup::SetToggle(System.Int32)
extern void ToggleButtonGroup_SetToggle_m41C6964A02080591947711F0433C67A01661738E (void);
// 0x0000008E System.Void TriviaQuizKit.ToggleButtonGroup::.ctor()
extern void ToggleButtonGroup__ctor_m2EAE4D72228BFBD9D1B217BE54415127561770C5 (void);
// 0x0000008F System.Void TriviaQuizKit.BaseQuestion::.ctor()
extern void BaseQuestion__ctor_m06917283B849CD521435459D24AB9385430F8A13 (void);
// 0x00000090 System.Void TriviaQuizKit.Category::.ctor()
extern void Category__ctor_mC4B9DE26A6C18F91D98F2946A1452D391A20AE85 (void);
// 0x00000091 System.Void TriviaQuizKit.GameConfiguration::.ctor()
extern void GameConfiguration__ctor_mFC877ECA7DAC83EB5A3081877251525ADB24D71C (void);
// 0x00000092 TriviaQuizKit.GameConfiguration TriviaQuizKit.GameConfigurationLoader::LoadGameConfiguration(System.String)
extern void GameConfigurationLoader_LoadGameConfiguration_m6DE9A89EBF0D34D79F6F110662D9448B566C1A3B (void);
// 0x00000093 System.Void TriviaQuizKit.MultipleChoiceQuestion::OnEnable()
extern void MultipleChoiceQuestion_OnEnable_m09D573752ECE9F278FA437F637CDFC98120C4D38 (void);
// 0x00000094 System.Void TriviaQuizKit.MultipleChoiceQuestion::.ctor()
extern void MultipleChoiceQuestion__ctor_m06F45A59AFC0FBD2E33CB9755EA4DD9809898240 (void);
// 0x00000095 System.Void TriviaQuizKit.QuestionPack::.ctor()
extern void QuestionPack__ctor_mB1472CBDD8C5EF32C9F736E424BC3DA3DDB1AF02 (void);
// 0x00000096 System.Collections.Generic.List`1<TriviaQuizKit.BaseQuestion> TriviaQuizKit.QuestionPackLoader::LoadQuestions(System.String)
extern void QuestionPackLoader_LoadQuestions_m843EA894AC9C3EF3E996F8BCFB2B42A28E2E3F28 (void);
// 0x00000097 System.Collections.Generic.List`1<TriviaQuizKit.BaseQuestion> TriviaQuizKit.QuestionPackLoader::LoadAllQuestions(TriviaQuizKit.GameConfiguration)
extern void QuestionPackLoader_LoadAllQuestions_mCC0F0C1DC5C957111E7EA08595F8F4E90B3119B6 (void);
// 0x00000098 System.Void TriviaQuizKit.QuestionPackSet::.ctor()
extern void QuestionPackSet__ctor_m250016998FE9D002A97CDEBD82B184406A64EA19 (void);
// 0x00000099 System.Void TriviaQuizKit.SingleChoiceQuestion::OnEnable()
extern void SingleChoiceQuestion_OnEnable_mC845C0B082F844E59AC000246349F86D810895F5 (void);
// 0x0000009A System.Void TriviaQuizKit.SingleChoiceQuestion::.ctor()
extern void SingleChoiceQuestion__ctor_m3759974FD6AE90CF8DA6AEC19C8761CFFC4445FE (void);
// 0x0000009B System.Void TriviaQuizKit.TrueFalseQuestion::.ctor()
extern void TrueFalseQuestion__ctor_mC930034CA43333114950633424B1D75B7053E579 (void);
// 0x0000009C System.Void TriviaQuizKit.AlertPopup::OnCloseButtonPressed()
extern void AlertPopup_OnCloseButtonPressed_m360CFEA80632C2F2E1ED5CB6ACA9C65DE8684F18 (void);
// 0x0000009D System.Void TriviaQuizKit.AlertPopup::SetText(System.String)
extern void AlertPopup_SetText_mFD9816494BEF4C3DA848595AD577E43782921A48 (void);
// 0x0000009E System.Void TriviaQuizKit.AlertPopup::.ctor()
extern void AlertPopup__ctor_m86C162AF375760FF968E175316BC98AD0FC98ED2 (void);
// 0x0000009F System.Void TriviaQuizKit.GameFinishedPopup::OnReplayButtonPressed()
extern void GameFinishedPopup_OnReplayButtonPressed_m1E7420E7D4AF0BB6D2871789D879DC88671F68C9 (void);
// 0x000000A0 System.Void TriviaQuizKit.GameFinishedPopup::OnQuitButtonPressed()
extern void GameFinishedPopup_OnQuitButtonPressed_mD7D2D49EEEDF2A8610ACEEEC3897B768E69F104E (void);
// 0x000000A1 System.Void TriviaQuizKit.GameFinishedPopup::SetTrophy(System.Int32,System.Int32,System.Int32)
extern void GameFinishedPopup_SetTrophy_mD74997C20F79D09C5CA1642D4CA8CC5AF07F3F83 (void);
// 0x000000A2 System.Void TriviaQuizKit.GameFinishedPopup::.ctor()
extern void GameFinishedPopup__ctor_mFB5744F5A8D897AD51DA62F679A7CCDB899A8FFB (void);
// 0x000000A3 System.Void TriviaQuizKit.ProfilePopup::Start()
extern void ProfilePopup_Start_m6924C5830F1EE4C220EB5FB796FE13803F484F23 (void);
// 0x000000A4 System.Void TriviaQuizKit.ProfilePopup::OnCloseButtonPressed()
extern void ProfilePopup_OnCloseButtonPressed_m4DDA0B6F8E0DB54E0352DDEE02406CE4D52C7B8C (void);
// 0x000000A5 System.Void TriviaQuizKit.ProfilePopup::OnAvatarButtonPressed()
extern void ProfilePopup_OnAvatarButtonPressed_mEE723AAE346C9F1B2BE98FBE7D829B0BBA599B15 (void);
// 0x000000A6 System.Void TriviaQuizKit.ProfilePopup::OnPrevButtonPressed()
extern void ProfilePopup_OnPrevButtonPressed_m4EB277D87CC824301C91E08DEDF8C866930E148C (void);
// 0x000000A7 System.Void TriviaQuizKit.ProfilePopup::OnNextButtonPressed()
extern void ProfilePopup_OnNextButtonPressed_m4902B2DE2B2102BFFC8FF0193932778E5E5327B6 (void);
// 0x000000A8 System.Void TriviaQuizKit.ProfilePopup::OnResetProgressButtonPressed()
extern void ProfilePopup_OnResetProgressButtonPressed_m28A1329CE32D51CD36FC9FC21C4918D711FCE983 (void);
// 0x000000A9 System.Void TriviaQuizKit.ProfilePopup::SetAvatar()
extern void ProfilePopup_SetAvatar_m8CC94D2C6259BF89F32FDE94C3D64FA242B3B123 (void);
// 0x000000AA System.Void TriviaQuizKit.ProfilePopup::SetQuestionTypeText()
extern void ProfilePopup_SetQuestionTypeText_m24DB6DDFD4F3D4606642387368B8B196AD2E5BA2 (void);
// 0x000000AB System.Void TriviaQuizKit.ProfilePopup::CreateCategories()
extern void ProfilePopup_CreateCategories_m6E4CC0CE606CEE124691238188BB9866911F9147 (void);
// 0x000000AC System.Void TriviaQuizKit.ProfilePopup::LoadCategoryInfo()
extern void ProfilePopup_LoadCategoryInfo_mF63E36BE9CD64F607534B615D3E0B0B16770281A (void);
// 0x000000AD System.Void TriviaQuizKit.ProfilePopup::.ctor()
extern void ProfilePopup__ctor_m8B71B5B0268FA1C4AD53B0AB674DC1FFF9F9ACF1 (void);
// 0x000000AE System.Void TriviaQuizKit.QuitGamePopup::OnYesButtonPressed()
extern void QuitGamePopup_OnYesButtonPressed_m828CC59734D69EFA0BAC13CDDC65053BDB6B090A (void);
// 0x000000AF System.Void TriviaQuizKit.QuitGamePopup::OnNoButtonPressed()
extern void QuitGamePopup_OnNoButtonPressed_m86CE276CFE9860233F6B92FF96A397990ADC9AA5 (void);
// 0x000000B0 System.Void TriviaQuizKit.QuitGamePopup::.ctor()
extern void QuitGamePopup__ctor_m65D001FC2FFF36522CD053562FA66A311B7DCC0A (void);
// 0x000000B1 System.Void TriviaQuizKit.SettingsPopup::Start()
extern void SettingsPopup_Start_m1FE30A67DD716E06EF08A9BFEAE663A23974E3C4 (void);
// 0x000000B2 System.Void TriviaQuizKit.SettingsPopup::OnInfoButtonPressed()
extern void SettingsPopup_OnInfoButtonPressed_m3A40C1D524A11E37D648CBE2CD1C68D006597F7F (void);
// 0x000000B3 System.Void TriviaQuizKit.SettingsPopup::OnHelpButtonPressed()
extern void SettingsPopup_OnHelpButtonPressed_m221C6886AC2876034BF1FB2CA15B34427B07F04C (void);
// 0x000000B4 System.Void TriviaQuizKit.SettingsPopup::OnCloseButtonPressed()
extern void SettingsPopup_OnCloseButtonPressed_m856994413671A1B482E0F1F5F938FCD00599A77A (void);
// 0x000000B5 System.Void TriviaQuizKit.SettingsPopup::OnMusicSliderValueChanged()
extern void SettingsPopup_OnMusicSliderValueChanged_m32974702DD8EC0ACBAD825302F56A46FEBF771E5 (void);
// 0x000000B6 System.Void TriviaQuizKit.SettingsPopup::OnSoundSliderValueChanged()
extern void SettingsPopup_OnSoundSliderValueChanged_mA49E3E987F7B0502839EC011C36520D3E7F85161 (void);
// 0x000000B7 System.Void TriviaQuizKit.SettingsPopup::.ctor()
extern void SettingsPopup__ctor_m046A32E1B7E0A33F283C17713767F2C431BE4224 (void);
// 0x000000B8 System.Void TriviaQuizKit.SettingsPopup/<>c::.cctor()
extern void U3CU3Ec__cctor_m1290BD6C61D03A40966D1085E59D07EEAB7AC27D (void);
// 0x000000B9 System.Void TriviaQuizKit.SettingsPopup/<>c::.ctor()
extern void U3CU3Ec__ctor_m833C8EE04413BEDDDE23B2EF009DD26F30C91882 (void);
// 0x000000BA System.Void TriviaQuizKit.SettingsPopup/<>c::<OnInfoButtonPressed>b__5_0(TriviaQuizKit.AlertPopup)
extern void U3CU3Ec_U3COnInfoButtonPressedU3Eb__5_0_m9C1CFC1CEA3720C44C7E7098D56B651A32293848 (void);
// 0x000000BB System.Void TriviaQuizKit.SettingsPopup/<>c::<OnHelpButtonPressed>b__6_0(TriviaQuizKit.AlertPopup)
extern void U3CU3Ec_U3COnHelpButtonPressedU3Eb__6_0_m43426447931F4629F73DEEA5A0A5321C39776D48 (void);
// 0x000000BC System.Void TriviaQuizKit.CategorySelectionScreen::Start()
extern void CategorySelectionScreen_Start_m00E1118B57AEBC39808441A371CEC1AC7FD7BC4A (void);
// 0x000000BD System.Void TriviaQuizKit.CategorySelectionScreen::SetCategory(System.Int32)
extern void CategorySelectionScreen_SetCategory_m0AEDC02215253C74A4279E984739AC79BDA6A07E (void);
// 0x000000BE System.Void TriviaQuizKit.CategorySelectionScreen::OnBackButtonPressed()
extern void CategorySelectionScreen_OnBackButtonPressed_m338A2D984E75B356642675E21FA18181D33261CE (void);
// 0x000000BF System.Void TriviaQuizKit.CategorySelectionScreen::OnNextButtonPressed()
extern void CategorySelectionScreen_OnNextButtonPressed_m4C8875D0E2402C6D34EC921FAE4D5F750FEA8593 (void);
// 0x000000C0 TriviaQuizKit.CategoryToggleWrapper TriviaQuizKit.CategorySelectionScreen::CreateCategoryToggle(System.String,UnityEngine.Sprite,System.Int32)
extern void CategorySelectionScreen_CreateCategoryToggle_mD03FB1B4B0A587DA6633491EF78F48CB1C6B5B7C (void);
// 0x000000C1 System.Void TriviaQuizKit.CategorySelectionScreen::.ctor()
extern void CategorySelectionScreen__ctor_mFA166C076572AC946A6346799C9AC857391D61CE (void);
// 0x000000C2 System.Void TriviaQuizKit.CategorySelectionScreen/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m1FF9E6137BA3307FE1F8318B6BE96EBCEA11B331 (void);
// 0x000000C3 System.Void TriviaQuizKit.CategorySelectionScreen/<>c__DisplayClass8_0::<CreateCategoryToggle>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CCreateCategoryToggleU3Eb__0_m7AE9903196CCEADE7B9C610D8B7A30DE629FB88B (void);
// 0x000000C4 System.Void TriviaQuizKit.GameModeSelectionScreen::Start()
extern void GameModeSelectionScreen_Start_m9FAF4AC95A1455DA698A34AEA0D34A7A8FD73FF2 (void);
// 0x000000C5 System.Void TriviaQuizKit.GameModeSelectionScreen::SetQuestionType(System.Int32)
extern void GameModeSelectionScreen_SetQuestionType_m71E48A71369873D20D94A9751C264805CFF66639 (void);
// 0x000000C6 System.Void TriviaQuizKit.GameModeSelectionScreen::SetTimeMode(System.Int32)
extern void GameModeSelectionScreen_SetTimeMode_m58DEC7D31844AE55F8C2CE8E833645BFD69FFA28 (void);
// 0x000000C7 System.Void TriviaQuizKit.GameModeSelectionScreen::StartGame()
extern void GameModeSelectionScreen_StartGame_m258ED967CAF7138D4A259ED66B11ADE667A0831E (void);
// 0x000000C8 System.Void TriviaQuizKit.GameModeSelectionScreen::.ctor()
extern void GameModeSelectionScreen__ctor_m826B2CB495CCCDA8F84BA63E31BCBF6169366EB3 (void);
// 0x000000C9 System.Void TriviaQuizKit.GameScreen::Start()
extern void GameScreen_Start_mAB8E0638092C15D6A1D482FE63FEE3D5129265DB (void);
// 0x000000CA System.Void TriviaQuizKit.GameScreen::LoadSingleChoiceQuestionUi()
extern void GameScreen_LoadSingleChoiceQuestionUi_mECF27CB4D40507220973D6A10EA07E92C2D1E894 (void);
// 0x000000CB System.Void TriviaQuizKit.GameScreen::LoadMultipleChoiceQuestionUi()
extern void GameScreen_LoadMultipleChoiceQuestionUi_m420E8B2A50D8CD038286943B464C0D8EA01220CC (void);
// 0x000000CC System.Void TriviaQuizKit.GameScreen::LoadTrueFalseQuestionUi()
extern void GameScreen_LoadTrueFalseQuestionUi_m20C401F7DDA6839154235416C2B08F3CB08381FD (void);
// 0x000000CD System.Void TriviaQuizKit.GameScreen::Update()
extern void GameScreen_Update_m52C37CE6E6C81A136008AC63F3612E887BCE1D3A (void);
// 0x000000CE System.Void TriviaQuizKit.GameScreen::LoadQuestions()
extern void GameScreen_LoadQuestions_mAEA9B65BCAC40F6828FE87687E843C057D279D02 (void);
// 0x000000CF System.Void TriviaQuizKit.GameScreen::StartGame()
extern void GameScreen_StartGame_mBCC608A22FBC144ED17EF8FD04ABA4A28081DD43 (void);
// 0x000000D0 System.Void TriviaQuizKit.GameScreen::SelectRandomQuestion()
extern void GameScreen_SelectRandomQuestion_mA564577304847914CA9168DF46D04FF39F898BEF (void);
// 0x000000D1 System.Void TriviaQuizKit.GameScreen::LoadQuestion(TriviaQuizKit.BaseQuestion)
extern void GameScreen_LoadQuestion_m3AFAD0FD637FA45A6B09419FE5FF700D7D3BE3B8 (void);
// 0x000000D2 System.Void TriviaQuizKit.GameScreen::OnPlayerAnswered(System.Int32)
extern void GameScreen_OnPlayerAnswered_m59D4AF05456C2A61943071C7BAA52885D564BF42 (void);
// 0x000000D3 System.Void TriviaQuizKit.GameScreen::AdvanceToNextQuestion()
extern void GameScreen_AdvanceToNextQuestion_m88ED87D72CD56CC79D4BDA1DD9ED8CDDA2C1A30F (void);
// 0x000000D4 System.Collections.IEnumerator TriviaQuizKit.GameScreen::SelectRandomQuestionAsync()
extern void GameScreen_SelectRandomQuestionAsync_m5C277593F010CAFD112CBDF42F6995117713FCED (void);
// 0x000000D5 System.Collections.IEnumerator TriviaQuizKit.GameScreen::OpenGameFinishedPopupAsync()
extern void GameScreen_OpenGameFinishedPopupAsync_mDDE57F29D5312E3377D12C7BAF15883A4840FD0D (void);
// 0x000000D6 System.Void TriviaQuizKit.GameScreen::StartCountdown()
extern void GameScreen_StartCountdown_mE157DEDB79FC3A467655101B6B7C463A558308D5 (void);
// 0x000000D7 System.Void TriviaQuizKit.GameScreen::StopCountdown()
extern void GameScreen_StopCountdown_m581C9F6232CBAE4F70E1B4BC1845FC7B751B33A7 (void);
// 0x000000D8 System.Void TriviaQuizKit.GameScreen::Restart()
extern void GameScreen_Restart_m0BEAA1F14CD8612D29E3965CDAD8DFD832A9E85E (void);
// 0x000000D9 System.Void TriviaQuizKit.GameScreen::OnQuitButtonPressed()
extern void GameScreen_OnQuitButtonPressed_m7D6E361FE30FEEB49FDDEB498B242E4972D8B2C2 (void);
// 0x000000DA System.Void TriviaQuizKit.GameScreen::OnSelectButtonPressed()
extern void GameScreen_OnSelectButtonPressed_m601045A375457144CF21E72BBE29361CABE71893 (void);
// 0x000000DB System.Void TriviaQuizKit.GameScreen::.ctor()
extern void GameScreen__ctor_mD947A94C370DA3B2BC5E33FFACCDFF78711951A5 (void);
// 0x000000DC System.Void TriviaQuizKit.GameScreen/<SelectRandomQuestionAsync>d__48::.ctor(System.Int32)
extern void U3CSelectRandomQuestionAsyncU3Ed__48__ctor_mDD37123FC127E790D7CE66592D688D7FCB08F4EA (void);
// 0x000000DD System.Void TriviaQuizKit.GameScreen/<SelectRandomQuestionAsync>d__48::System.IDisposable.Dispose()
extern void U3CSelectRandomQuestionAsyncU3Ed__48_System_IDisposable_Dispose_m186687311CE86ACB62F2C2DAB46A2D19D096C3D4 (void);
// 0x000000DE System.Boolean TriviaQuizKit.GameScreen/<SelectRandomQuestionAsync>d__48::MoveNext()
extern void U3CSelectRandomQuestionAsyncU3Ed__48_MoveNext_mE0AF7B6528FFD235459E9F818A92283F38560A5A (void);
// 0x000000DF System.Object TriviaQuizKit.GameScreen/<SelectRandomQuestionAsync>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m369D1C6C107A0124D6D006237E39DA39FA8312E8 (void);
// 0x000000E0 System.Void TriviaQuizKit.GameScreen/<SelectRandomQuestionAsync>d__48::System.Collections.IEnumerator.Reset()
extern void U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_IEnumerator_Reset_mFC4635B6D4DB5A757FA62E55D63F3B6B6DE6FC7A (void);
// 0x000000E1 System.Object TriviaQuizKit.GameScreen/<SelectRandomQuestionAsync>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_IEnumerator_get_Current_m0CD6CF2630BC66C203F5AF4F109DC4054D31272C (void);
// 0x000000E2 System.Void TriviaQuizKit.GameScreen/<>c__DisplayClass49_0::.ctor()
extern void U3CU3Ec__DisplayClass49_0__ctor_m5B02A9AE24ECF393CD8DBE08EB2975BE5986D581 (void);
// 0x000000E3 System.Void TriviaQuizKit.GameScreen/<>c__DisplayClass49_0::<OpenGameFinishedPopupAsync>b__0(TriviaQuizKit.GameFinishedPopup)
extern void U3CU3Ec__DisplayClass49_0_U3COpenGameFinishedPopupAsyncU3Eb__0_mEF8FEA25949068832D9BB141E5ECD9E6282F041F (void);
// 0x000000E4 System.Void TriviaQuizKit.GameScreen/<OpenGameFinishedPopupAsync>d__49::.ctor(System.Int32)
extern void U3COpenGameFinishedPopupAsyncU3Ed__49__ctor_mDE9A51FCCC24ECE8581480F4F25BCA1B61058984 (void);
// 0x000000E5 System.Void TriviaQuizKit.GameScreen/<OpenGameFinishedPopupAsync>d__49::System.IDisposable.Dispose()
extern void U3COpenGameFinishedPopupAsyncU3Ed__49_System_IDisposable_Dispose_mE050E9F7D72D00202AB3D6B9A44430CBA75C2F60 (void);
// 0x000000E6 System.Boolean TriviaQuizKit.GameScreen/<OpenGameFinishedPopupAsync>d__49::MoveNext()
extern void U3COpenGameFinishedPopupAsyncU3Ed__49_MoveNext_m2B9A92E43DC8A40BF3CAF69C306092C63F4B9807 (void);
// 0x000000E7 System.Object TriviaQuizKit.GameScreen/<OpenGameFinishedPopupAsync>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FF145A247ED7369892CAE203D6BEE9784BF013B (void);
// 0x000000E8 System.Void TriviaQuizKit.GameScreen/<OpenGameFinishedPopupAsync>d__49::System.Collections.IEnumerator.Reset()
extern void U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_IEnumerator_Reset_m1F1766A314E68317CE556A1912D696084B7EF3C8 (void);
// 0x000000E9 System.Object TriviaQuizKit.GameScreen/<OpenGameFinishedPopupAsync>d__49::System.Collections.IEnumerator.get_Current()
extern void U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_IEnumerator_get_Current_m4DEF2131620704154BFF8F8D75FD73E620928170 (void);
// 0x000000EA System.Void TriviaQuizKit.HomeScreen::Start()
extern void HomeScreen_Start_m317B23F1AFC9541E0F0501ACF4E6521F693E93D0 (void);
// 0x000000EB System.Void TriviaQuizKit.HomeScreen::OnAvatarButtonPressed()
extern void HomeScreen_OnAvatarButtonPressed_m306D7CA00AA9F3F663D3DB12900BDDE62D6F1974 (void);
// 0x000000EC System.Void TriviaQuizKit.HomeScreen::OnSettingsButtonPressed()
extern void HomeScreen_OnSettingsButtonPressed_m1E281C64F73A5C12F2975D7F07D8488E8E398270 (void);
// 0x000000ED System.Void TriviaQuizKit.HomeScreen::SetAvatar(System.Int32)
extern void HomeScreen_SetAvatar_m984975C0AB1459C3834F9F4572CDA0213C31E882 (void);
// 0x000000EE System.Void TriviaQuizKit.HomeScreen::.ctor()
extern void HomeScreen__ctor_mD0B37B46C153242A1FB9AB23B5CF55337C8AD020 (void);
// 0x000000EF System.Void TriviaQuizKit.HomeScreen/<>c::.cctor()
extern void U3CU3Ec__cctor_m6965652C8F23346BF2CA29AC2116C9586F3431E3 (void);
// 0x000000F0 System.Void TriviaQuizKit.HomeScreen/<>c::.ctor()
extern void U3CU3Ec__ctor_m54C16D5362F087382BDFCCB05004EA31D1BDA444 (void);
// 0x000000F1 System.Void TriviaQuizKit.HomeScreen/<>c::<OnSettingsButtonPressed>b__4_0(TriviaQuizKit.SettingsPopup)
extern void U3CU3Ec_U3COnSettingsButtonPressedU3Eb__4_0_mFB7407331C22A79AF287E4FDEB52DAB0424CEC61 (void);
// 0x000000F2 System.Void TriviaQuizKit.CategoryScrollItem::.ctor()
extern void CategoryScrollItem__ctor_mA5414D69BCD83D51BCC192860C4FE1408093236C (void);
// 0x000000F3 System.Void TriviaQuizKit.CategoryToggle::.ctor()
extern void CategoryToggle__ctor_m761AC1BD3E16D551689900CDFFEC31BE38667744 (void);
// 0x000000F4 System.Void TriviaQuizKit.CategoryToggleWrapper::.ctor()
extern void CategoryToggleWrapper__ctor_m394281D5CE78E2119708F1F82F405252B373A059 (void);
// 0x000000F5 System.Void TriviaQuizKit.MultipleChoiceQuestionUi::OnQuestionLoaded(TriviaQuizKit.GameScreen,TriviaQuizKit.BaseQuestion)
extern void MultipleChoiceQuestionUi_OnQuestionLoaded_m34C4A9EE8AE26FCEDBD6168F672B6FBE06723DA5 (void);
// 0x000000F6 System.Boolean TriviaQuizKit.MultipleChoiceQuestionUi::OnQuestionAnswered(System.Int32)
extern void MultipleChoiceQuestionUi_OnQuestionAnswered_m025C563D275723ABF6E74B2396A26F075A536B42 (void);
// 0x000000F7 System.Void TriviaQuizKit.MultipleChoiceQuestionUi::HighlightCorrectAnswer()
extern void MultipleChoiceQuestionUi_HighlightCorrectAnswer_m6A449A754912187CAF7C3DAB6D97DB98523F0A55 (void);
// 0x000000F8 System.Void TriviaQuizKit.MultipleChoiceQuestionUi::HighlightWrongAnswer(System.Int32)
extern void MultipleChoiceQuestionUi_HighlightWrongAnswer_mD5E51D1C158E48FEBD2D05CBF49935C3A93E3B44 (void);
// 0x000000F9 System.Void TriviaQuizKit.MultipleChoiceQuestionUi::LockUi()
extern void MultipleChoiceQuestionUi_LockUi_mD6C1B82C58F2043FAE7DF56F2087F30B6DEC1307 (void);
// 0x000000FA System.Void TriviaQuizKit.MultipleChoiceQuestionUi::UnlockUi()
extern void MultipleChoiceQuestionUi_UnlockUi_m980CE5580B5F85EE0D5137A53D3F821FF8E1059C (void);
// 0x000000FB System.Void TriviaQuizKit.MultipleChoiceQuestionUi::OnSelectButtonPressed()
extern void MultipleChoiceQuestionUi_OnSelectButtonPressed_m7A3FA5C1053299F11982A720798C4DE9AAB6E490 (void);
// 0x000000FC System.Void TriviaQuizKit.MultipleChoiceQuestionUi::.ctor()
extern void MultipleChoiceQuestionUi__ctor_m824535313EE6B5708D48C8DE478931645D49B9E3 (void);
// 0x000000FD System.Void TriviaQuizKit.MultipleChoiceQuestionUi/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mE9A31FD190056385614CC8F8DBC25B1D949162C7 (void);
// 0x000000FE System.Boolean TriviaQuizKit.MultipleChoiceQuestionUi/<>c__DisplayClass15_0::<OnQuestionLoaded>b__0(System.String)
extern void U3CU3Ec__DisplayClass15_0_U3COnQuestionLoadedU3Eb__0_mC4F50DD45F61F8AC3DC83AA9DA286792B999D241 (void);
// 0x000000FF System.Void TriviaQuizKit.MultipleChoiceQuestionUi/<>c__DisplayClass15_1::.ctor()
extern void U3CU3Ec__DisplayClass15_1__ctor_m5BB46BA874C58FA4F63CCE0B584099539D3EA3E6 (void);
// 0x00000100 System.Void TriviaQuizKit.MultipleChoiceQuestionUi/<>c__DisplayClass15_1::<OnQuestionLoaded>b__1()
extern void U3CU3Ec__DisplayClass15_1_U3COnQuestionLoadedU3Eb__1_mD63F3B39CA0891FCD6123E5CDCA101470AA9D6A6 (void);
// 0x00000101 System.Void TriviaQuizKit.QuestionResultUi::OnDisable()
extern void QuestionResultUi_OnDisable_m3CB93DE74545DC4EF3DF19CEA44C836C8D56ADBF (void);
// 0x00000102 System.Void TriviaQuizKit.QuestionResultUi::Hide()
extern void QuestionResultUi_Hide_m00A22B4911F2A7A9573F12D32814671B408A9CE1 (void);
// 0x00000103 System.Collections.IEnumerator TriviaQuizKit.QuestionResultUi::FadeIn(System.Boolean,System.Single)
extern void QuestionResultUi_FadeIn_m40D67B72B348528063F141133B4E26A4140CBE9F (void);
// 0x00000104 System.Collections.IEnumerator TriviaQuizKit.QuestionResultUi::FadeOut(System.Boolean,System.Single)
extern void QuestionResultUi_FadeOut_mDA24F36316B5DA1706DF958607DB3CAD61B58AB6 (void);
// 0x00000105 System.Collections.IEnumerator TriviaQuizKit.QuestionResultUi::FadeInImage(System.Boolean,System.Single)
extern void QuestionResultUi_FadeInImage_m88CE5B21BCE8FB6C1DD23369B40A49618CAA12B3 (void);
// 0x00000106 System.Collections.IEnumerator TriviaQuizKit.QuestionResultUi::FadeOutImage(System.Single)
extern void QuestionResultUi_FadeOutImage_m296752235987311BA65F4025F255C89F6B6ED522 (void);
// 0x00000107 System.Collections.IEnumerator TriviaQuizKit.QuestionResultUi::FadeInText(System.Boolean,System.Single)
extern void QuestionResultUi_FadeInText_mAE52FB41F32DECB053B7A384FC3DD979E727719C (void);
// 0x00000108 System.Collections.IEnumerator TriviaQuizKit.QuestionResultUi::FadeOutText(System.Boolean,System.Single)
extern void QuestionResultUi_FadeOutText_m1F38DCB813084B1E242F5A5ED1A00D2985BFB661 (void);
// 0x00000109 System.Void TriviaQuizKit.QuestionResultUi::.ctor()
extern void QuestionResultUi__ctor_m82D8BD3B8F8A61B13C983166634C9A9F6E530E74 (void);
// 0x0000010A System.Void TriviaQuizKit.QuestionResultUi/<FadeIn>d__5::.ctor(System.Int32)
extern void U3CFadeInU3Ed__5__ctor_m74D9B0013114CBD245700700C752CA6F9D4C54D2 (void);
// 0x0000010B System.Void TriviaQuizKit.QuestionResultUi/<FadeIn>d__5::System.IDisposable.Dispose()
extern void U3CFadeInU3Ed__5_System_IDisposable_Dispose_m426637519063328CC571E171185CC9C3B22F2D31 (void);
// 0x0000010C System.Boolean TriviaQuizKit.QuestionResultUi/<FadeIn>d__5::MoveNext()
extern void U3CFadeInU3Ed__5_MoveNext_m1CF9D2C7EC388F9BDEE15D12CAC80651C72D7641 (void);
// 0x0000010D System.Object TriviaQuizKit.QuestionResultUi/<FadeIn>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeInU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m746822E4F1F69DA7A0938EC028972D37331C657B (void);
// 0x0000010E System.Void TriviaQuizKit.QuestionResultUi/<FadeIn>d__5::System.Collections.IEnumerator.Reset()
extern void U3CFadeInU3Ed__5_System_Collections_IEnumerator_Reset_mC1BC7936550ED852E6166A9DEDFC90B6D2B097D8 (void);
// 0x0000010F System.Object TriviaQuizKit.QuestionResultUi/<FadeIn>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CFadeInU3Ed__5_System_Collections_IEnumerator_get_Current_m67C9FCF94FC33DCB98447834DF9914647C4EEDA3 (void);
// 0x00000110 System.Void TriviaQuizKit.QuestionResultUi/<FadeOut>d__6::.ctor(System.Int32)
extern void U3CFadeOutU3Ed__6__ctor_mC4DC2218F477AC844A4644E3E926540ADF7F7F62 (void);
// 0x00000111 System.Void TriviaQuizKit.QuestionResultUi/<FadeOut>d__6::System.IDisposable.Dispose()
extern void U3CFadeOutU3Ed__6_System_IDisposable_Dispose_m5163A6DD6CBA5506F8FE350E00ECF455462756FB (void);
// 0x00000112 System.Boolean TriviaQuizKit.QuestionResultUi/<FadeOut>d__6::MoveNext()
extern void U3CFadeOutU3Ed__6_MoveNext_mCE2D9774F285917F3563AA865B4F34007CBD8A66 (void);
// 0x00000113 System.Object TriviaQuizKit.QuestionResultUi/<FadeOut>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE77C57F22C55C3F6DA732834830044D44C6F16A3 (void);
// 0x00000114 System.Void TriviaQuizKit.QuestionResultUi/<FadeOut>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutU3Ed__6_System_Collections_IEnumerator_Reset_mDA2ABCD14D54F3D886AD5759E2081F106168507A (void);
// 0x00000115 System.Object TriviaQuizKit.QuestionResultUi/<FadeOut>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutU3Ed__6_System_Collections_IEnumerator_get_Current_mF36B05915D2AE4BC3CAFFC3303A515251245AA3B (void);
// 0x00000116 System.Void TriviaQuizKit.QuestionResultUi/<FadeInImage>d__7::.ctor(System.Int32)
extern void U3CFadeInImageU3Ed__7__ctor_m0234ABA131890578D682DE3F2387A077CFEA400C (void);
// 0x00000117 System.Void TriviaQuizKit.QuestionResultUi/<FadeInImage>d__7::System.IDisposable.Dispose()
extern void U3CFadeInImageU3Ed__7_System_IDisposable_Dispose_m99AA1279F2ADA34145D797F14E50A47CB7D30A9E (void);
// 0x00000118 System.Boolean TriviaQuizKit.QuestionResultUi/<FadeInImage>d__7::MoveNext()
extern void U3CFadeInImageU3Ed__7_MoveNext_m4FF110A95A93CCEDCE9EEE27C41BA75458AAA6E2 (void);
// 0x00000119 System.Object TriviaQuizKit.QuestionResultUi/<FadeInImage>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeInImageU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE90EB29B462BEDAC582A8683F14919F38D87D423 (void);
// 0x0000011A System.Void TriviaQuizKit.QuestionResultUi/<FadeInImage>d__7::System.Collections.IEnumerator.Reset()
extern void U3CFadeInImageU3Ed__7_System_Collections_IEnumerator_Reset_mE2BB5EF1DA866BBC590A0E150D8BE467836078F1 (void);
// 0x0000011B System.Object TriviaQuizKit.QuestionResultUi/<FadeInImage>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CFadeInImageU3Ed__7_System_Collections_IEnumerator_get_Current_mAF0AEA8623EE1CC511CD14E5823CB494CC339CD3 (void);
// 0x0000011C System.Void TriviaQuizKit.QuestionResultUi/<FadeOutImage>d__8::.ctor(System.Int32)
extern void U3CFadeOutImageU3Ed__8__ctor_m9B38F76689716961CA361C4DDBDFBA39284C870D (void);
// 0x0000011D System.Void TriviaQuizKit.QuestionResultUi/<FadeOutImage>d__8::System.IDisposable.Dispose()
extern void U3CFadeOutImageU3Ed__8_System_IDisposable_Dispose_m76E85D1BE791F9FFFF071A3833350405667D0B23 (void);
// 0x0000011E System.Boolean TriviaQuizKit.QuestionResultUi/<FadeOutImage>d__8::MoveNext()
extern void U3CFadeOutImageU3Ed__8_MoveNext_m9619D9C3DA166059C25E0F2A24A376F33C005363 (void);
// 0x0000011F System.Object TriviaQuizKit.QuestionResultUi/<FadeOutImage>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutImageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50748FB3FEDF7D8B29D1C14B62C9ABA352B0EF6A (void);
// 0x00000120 System.Void TriviaQuizKit.QuestionResultUi/<FadeOutImage>d__8::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutImageU3Ed__8_System_Collections_IEnumerator_Reset_m4D193083D83A5855B8C421D35C1ED3AED0041716 (void);
// 0x00000121 System.Object TriviaQuizKit.QuestionResultUi/<FadeOutImage>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutImageU3Ed__8_System_Collections_IEnumerator_get_Current_m30774CD6115F99D85A67DF56206CEF2F90CF6B9C (void);
// 0x00000122 System.Void TriviaQuizKit.QuestionResultUi/<FadeInText>d__9::.ctor(System.Int32)
extern void U3CFadeInTextU3Ed__9__ctor_mFC16E5EAD976D5FF732873A44177E70D9863E456 (void);
// 0x00000123 System.Void TriviaQuizKit.QuestionResultUi/<FadeInText>d__9::System.IDisposable.Dispose()
extern void U3CFadeInTextU3Ed__9_System_IDisposable_Dispose_mDE254A21FA9A8AB7E100020985280E49B76FF4D6 (void);
// 0x00000124 System.Boolean TriviaQuizKit.QuestionResultUi/<FadeInText>d__9::MoveNext()
extern void U3CFadeInTextU3Ed__9_MoveNext_m43AD986AF751E4B70BCC755B0E81A55A0A86B4E9 (void);
// 0x00000125 System.Object TriviaQuizKit.QuestionResultUi/<FadeInText>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeInTextU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C72DE065EF204138607968499DB71937EA857C3 (void);
// 0x00000126 System.Void TriviaQuizKit.QuestionResultUi/<FadeInText>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFadeInTextU3Ed__9_System_Collections_IEnumerator_Reset_m5FE33FA8B881ABE276980EE13DF8F779D1BD7CCA (void);
// 0x00000127 System.Object TriviaQuizKit.QuestionResultUi/<FadeInText>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFadeInTextU3Ed__9_System_Collections_IEnumerator_get_Current_m8FA4E1605670A9C3071476E6B47DD1A8CD613FD5 (void);
// 0x00000128 System.Void TriviaQuizKit.QuestionResultUi/<FadeOutText>d__10::.ctor(System.Int32)
extern void U3CFadeOutTextU3Ed__10__ctor_m04D3968ABEAACEDD769B3FE3A401C3272DE08ECE (void);
// 0x00000129 System.Void TriviaQuizKit.QuestionResultUi/<FadeOutText>d__10::System.IDisposable.Dispose()
extern void U3CFadeOutTextU3Ed__10_System_IDisposable_Dispose_m120BC1F80CB95B13361726FE1074AE0A2925FF84 (void);
// 0x0000012A System.Boolean TriviaQuizKit.QuestionResultUi/<FadeOutText>d__10::MoveNext()
extern void U3CFadeOutTextU3Ed__10_MoveNext_m18E186973E300914CE272F043F9779E597C86DD4 (void);
// 0x0000012B System.Object TriviaQuizKit.QuestionResultUi/<FadeOutText>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutTextU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6488A6CB058E4D52C1FE7618D48809BF472FECE6 (void);
// 0x0000012C System.Void TriviaQuizKit.QuestionResultUi/<FadeOutText>d__10::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutTextU3Ed__10_System_Collections_IEnumerator_Reset_m4A39887766D710ADA09D04FD4F5941AB2C94A1F0 (void);
// 0x0000012D System.Object TriviaQuizKit.QuestionResultUi/<FadeOutText>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutTextU3Ed__10_System_Collections_IEnumerator_get_Current_m225D35B848312EAE28F8622F0F92ABF9CECA14C7 (void);
// 0x0000012E System.Void TriviaQuizKit.QuestionUi::OnQuestionLoaded(TriviaQuizKit.GameScreen,TriviaQuizKit.BaseQuestion)
// 0x0000012F System.Boolean TriviaQuizKit.QuestionUi::OnQuestionAnswered(System.Int32)
// 0x00000130 System.Void TriviaQuizKit.QuestionUi::HighlightCorrectAnswer()
// 0x00000131 System.Void TriviaQuizKit.QuestionUi::HighlightWrongAnswer(System.Int32)
// 0x00000132 System.Void TriviaQuizKit.QuestionUi::LockUi()
// 0x00000133 System.Void TriviaQuizKit.QuestionUi::UnlockUi()
// 0x00000134 System.Void TriviaQuizKit.QuestionUi::OnSelectButtonPressed()
// 0x00000135 System.Void TriviaQuizKit.QuestionUi::.ctor()
extern void QuestionUi__ctor_m9A5B761E8EC4F23C71E2EFB0DC738CDAECEA59A4 (void);
// 0x00000136 System.Void TriviaQuizKit.SingleChoiceQuestionUi::OnQuestionLoaded(TriviaQuizKit.GameScreen,TriviaQuizKit.BaseQuestion)
extern void SingleChoiceQuestionUi_OnQuestionLoaded_m23CE17FCA382E53BA42258677680AA261915765A (void);
// 0x00000137 System.Boolean TriviaQuizKit.SingleChoiceQuestionUi::OnQuestionAnswered(System.Int32)
extern void SingleChoiceQuestionUi_OnQuestionAnswered_m5B35FF71E5368BAA35983B18E782631FB82C3CDA (void);
// 0x00000138 System.Void TriviaQuizKit.SingleChoiceQuestionUi::HighlightCorrectAnswer()
extern void SingleChoiceQuestionUi_HighlightCorrectAnswer_m80499FE86B02A1261F482B9FA1B233D3260F3E72 (void);
// 0x00000139 System.Void TriviaQuizKit.SingleChoiceQuestionUi::HighlightWrongAnswer(System.Int32)
extern void SingleChoiceQuestionUi_HighlightWrongAnswer_m4F8076F69CB69CD3BD4A5FB5C345124F8E4F96D0 (void);
// 0x0000013A System.Void TriviaQuizKit.SingleChoiceQuestionUi::LockUi()
extern void SingleChoiceQuestionUi_LockUi_m57ABA2BA5A3C0DBECADD9806F95860DD8E2AF927 (void);
// 0x0000013B System.Void TriviaQuizKit.SingleChoiceQuestionUi::UnlockUi()
extern void SingleChoiceQuestionUi_UnlockUi_mACE16C265780FF43A557F472EB055CD714899D01 (void);
// 0x0000013C System.Void TriviaQuizKit.SingleChoiceQuestionUi::OnSelectButtonPressed()
extern void SingleChoiceQuestionUi_OnSelectButtonPressed_mF1422D575B5F825FE40C574F8D88D4B5CC9F7957 (void);
// 0x0000013D System.Void TriviaQuizKit.SingleChoiceQuestionUi::.ctor()
extern void SingleChoiceQuestionUi__ctor_mFE55450DAE9813237C583EEA8E1A81ECEC520E9F (void);
// 0x0000013E System.Void TriviaQuizKit.SingleChoiceQuestionUi/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m0B459CAE5F8A90276A80D5839E1586321305A16C (void);
// 0x0000013F System.Boolean TriviaQuizKit.SingleChoiceQuestionUi/<>c__DisplayClass10_0::<OnQuestionLoaded>b__0(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3COnQuestionLoadedU3Eb__0_m6B57688D3CBDF59838673A1B2F65E2576C6A46F5 (void);
// 0x00000140 System.Void TriviaQuizKit.SingleChoiceQuestionUi/<>c__DisplayClass10_1::.ctor()
extern void U3CU3Ec__DisplayClass10_1__ctor_mB839E865810EE85566BA4C6F95C9E02F1E55784B (void);
// 0x00000141 System.Void TriviaQuizKit.SingleChoiceQuestionUi/<>c__DisplayClass10_1::<OnQuestionLoaded>b__1()
extern void U3CU3Ec__DisplayClass10_1_U3COnQuestionLoadedU3Eb__1_m464246BFDE8E8215D99FB546CF9082F700680FE5 (void);
// 0x00000142 System.Void TriviaQuizKit.TrueFalseQuestionUi::OnQuestionLoaded(TriviaQuizKit.GameScreen,TriviaQuizKit.BaseQuestion)
extern void TrueFalseQuestionUi_OnQuestionLoaded_m090D2C4917BF50156FB8335605A5FB17366DE8CD (void);
// 0x00000143 System.Boolean TriviaQuizKit.TrueFalseQuestionUi::OnQuestionAnswered(System.Int32)
extern void TrueFalseQuestionUi_OnQuestionAnswered_mE535A7DA3936D003A83D16EC7BB77D1E6479D83C (void);
// 0x00000144 System.Void TriviaQuizKit.TrueFalseQuestionUi::HighlightCorrectAnswer()
extern void TrueFalseQuestionUi_HighlightCorrectAnswer_mBE4DC3853BD0F85246EDE5E9036FF5E39AF7B5BC (void);
// 0x00000145 System.Void TriviaQuizKit.TrueFalseQuestionUi::HighlightWrongAnswer(System.Int32)
extern void TrueFalseQuestionUi_HighlightWrongAnswer_m9764636AE6B3DAB746BB9FDCA2BEFBD845424181 (void);
// 0x00000146 System.Void TriviaQuizKit.TrueFalseQuestionUi::LockUi()
extern void TrueFalseQuestionUi_LockUi_m0F0480562236BF05A0A986317AE0920ABBF72277 (void);
// 0x00000147 System.Void TriviaQuizKit.TrueFalseQuestionUi::UnlockUi()
extern void TrueFalseQuestionUi_UnlockUi_m70D0AB02850C1860C4F67F06D62626EC42FCFE46 (void);
// 0x00000148 System.Void TriviaQuizKit.TrueFalseQuestionUi::OnSelectButtonPressed()
extern void TrueFalseQuestionUi_OnSelectButtonPressed_mDB20396A98352BCC889427FDFC04BD70E95B497B (void);
// 0x00000149 System.Void TriviaQuizKit.TrueFalseQuestionUi::.ctor()
extern void TrueFalseQuestionUi__ctor_m13E0973C9B34CC1686393742C7C45A1814BAD451 (void);
// 0x0000014A System.Void TriviaQuizKit.TrueFalseQuestionUi/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mFC2E0510E6B6BB39637C5B2DC25929909615121E (void);
// 0x0000014B System.Void TriviaQuizKit.TrueFalseQuestionUi/<>c__DisplayClass7_0::<OnQuestionLoaded>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3COnQuestionLoadedU3Eb__0_m53EF451F78C414A3F5DC548BA4F58BB52A0C1D86 (void);
// 0x0000014C System.Void TriviaQuizKit.TrueFalseQuestionUi/<>c__DisplayClass7_0::<OnQuestionLoaded>b__1()
extern void U3CU3Ec__DisplayClass7_0_U3COnQuestionLoadedU3Eb__1_mB6466485443D56D6E7D2ED238EC61A4AB65471FC (void);
// 0x0000014D System.Int32 Facebook.Unity.Example.ConsoleBase::get_ButtonHeight()
extern void ConsoleBase_get_ButtonHeight_mCA27289A3F104543F20DC06B91A31AB5B6086CBF (void);
// 0x0000014E System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowWidth()
extern void ConsoleBase_get_MainWindowWidth_mD5DF934DE521C4BAC2D8E5D192C99F671CE9A932 (void);
// 0x0000014F System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowFullWidth()
extern void ConsoleBase_get_MainWindowFullWidth_m34B6E4A7E5F46D4BA5C4F6776BE41F81A6EC0708 (void);
// 0x00000150 System.Int32 Facebook.Unity.Example.ConsoleBase::get_MarginFix()
extern void ConsoleBase_get_MarginFix_m62D65A386A3E082B094188A8085C0E70EB1C7E30 (void);
// 0x00000151 System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::get_MenuStack()
extern void ConsoleBase_get_MenuStack_mDC8955F1DC5D449DEEC9E2B9BCC9EDC1ADACB8EC (void);
// 0x00000152 System.Void Facebook.Unity.Example.ConsoleBase::set_MenuStack(System.Collections.Generic.Stack`1<System.String>)
extern void ConsoleBase_set_MenuStack_m51033E1F07C530521C27F170233FD177D22D45C0 (void);
// 0x00000153 System.String Facebook.Unity.Example.ConsoleBase::get_Status()
extern void ConsoleBase_get_Status_m63CCCED1FBDF986534230A863D3CB49D3D19B644 (void);
// 0x00000154 System.Void Facebook.Unity.Example.ConsoleBase::set_Status(System.String)
extern void ConsoleBase_set_Status_m395D6D86AE7C62F043B9B4AB438CF39F7216CFC3 (void);
// 0x00000155 UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::get_LastResponseTexture()
extern void ConsoleBase_get_LastResponseTexture_m6D4AB4A8E2B4906C8D20DB62B10DDE0D812630BD (void);
// 0x00000156 System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponseTexture(UnityEngine.Texture2D)
extern void ConsoleBase_set_LastResponseTexture_m9495BB5A6A1C0F6C2CA39B3818B06565B1CBC2E9 (void);
// 0x00000157 System.String Facebook.Unity.Example.ConsoleBase::get_LastResponse()
extern void ConsoleBase_get_LastResponse_m681ADA0CE897B024487F8F4C50260F71772D9386 (void);
// 0x00000158 System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponse(System.String)
extern void ConsoleBase_set_LastResponse_mA472606A78F4CA3BA65B94644F4B5DD7F70C532A (void);
// 0x00000159 UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::get_ScrollPosition()
extern void ConsoleBase_get_ScrollPosition_mF3756B3BDEB874C262A7EB7178080C045F4CF084 (void);
// 0x0000015A System.Void Facebook.Unity.Example.ConsoleBase::set_ScrollPosition(UnityEngine.Vector2)
extern void ConsoleBase_set_ScrollPosition_m896C935B5961450A7221C74985844A8A685D716C (void);
// 0x0000015B System.Single Facebook.Unity.Example.ConsoleBase::get_ScaleFactor()
extern void ConsoleBase_get_ScaleFactor_mBD5B4AFF1CB11190D92499E2E4FC849F30A36CA5 (void);
// 0x0000015C System.Int32 Facebook.Unity.Example.ConsoleBase::get_FontSize()
extern void ConsoleBase_get_FontSize_m5324E58BF0BA3C20E0F0207DF52ABDAC5836DECB (void);
// 0x0000015D UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextStyle()
extern void ConsoleBase_get_TextStyle_m42A89F1195B33C7B6EB0AC5B21CF8DCE0B8F24E2 (void);
// 0x0000015E UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_ButtonStyle()
extern void ConsoleBase_get_ButtonStyle_m9FE82D58E7A4DA3A588A0285A160C95CDB18BB1B (void);
// 0x0000015F UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextInputStyle()
extern void ConsoleBase_get_TextInputStyle_m4EF1BA3E08B87166B3719AA787380B60D61D185F (void);
// 0x00000160 UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_LabelStyle()
extern void ConsoleBase_get_LabelStyle_mCC3768907195DE623404701909A36C7A81D5F070 (void);
// 0x00000161 System.Void Facebook.Unity.Example.ConsoleBase::Awake()
extern void ConsoleBase_Awake_mF527AF51A14B16534EE6F207FE429271F30DFCE6 (void);
// 0x00000162 System.Boolean Facebook.Unity.Example.ConsoleBase::Button(System.String)
extern void ConsoleBase_Button_m227EA5755011461613164FC2E652AF490057B2C5 (void);
// 0x00000163 System.Void Facebook.Unity.Example.ConsoleBase::LabelAndTextField(System.String,System.String&)
extern void ConsoleBase_LabelAndTextField_m51ED01D9C65067D9BAD30A37EB346E45E9C71151 (void);
// 0x00000164 System.Boolean Facebook.Unity.Example.ConsoleBase::IsHorizontalLayout()
extern void ConsoleBase_IsHorizontalLayout_m7AF2417167256558F464D95D8E37519E2C9ED9C1 (void);
// 0x00000165 System.Void Facebook.Unity.Example.ConsoleBase::SwitchMenu(System.Type)
extern void ConsoleBase_SwitchMenu_m722B511130E38BEB0B82ABEA06EFEF7A8FCA5AAD (void);
// 0x00000166 System.Void Facebook.Unity.Example.ConsoleBase::GoBack()
extern void ConsoleBase_GoBack_m2F521BD6668698924A6ADE7B8A33638C79FA0685 (void);
// 0x00000167 System.Void Facebook.Unity.Example.ConsoleBase::.ctor()
extern void ConsoleBase__ctor_m6A9701FD37DF150A0944E6D6011C8AF2F1D01224 (void);
// 0x00000168 System.Void Facebook.Unity.Example.ConsoleBase::.cctor()
extern void ConsoleBase__cctor_m638237AA15700C2E8B101F9D9F60E12C0A4E0734 (void);
// 0x00000169 System.Void Facebook.Unity.Example.LogView::AddLog(System.String)
extern void LogView_AddLog_mD1DEEAD86B0AAF388524DD3C301212D187094FEE (void);
// 0x0000016A System.Void Facebook.Unity.Example.LogView::OnGUI()
extern void LogView_OnGUI_m09AF694B32089C3DE63F8AF0579666B99080CD87 (void);
// 0x0000016B System.Void Facebook.Unity.Example.LogView::.ctor()
extern void LogView__ctor_m3D72776C6D2FF8290D85E3DC714F7F63120A1FB4 (void);
// 0x0000016C System.Void Facebook.Unity.Example.LogView::.cctor()
extern void LogView__cctor_m4F29B617139DDAA7AA6B4E0E65200A239709F313 (void);
// 0x0000016D System.Void Facebook.Unity.Example.MenuBase::GetGui()
// 0x0000016E System.Boolean Facebook.Unity.Example.MenuBase::ShowDialogModeSelector()
extern void MenuBase_ShowDialogModeSelector_m894AD0F5A55FD7C40E8EB1D5C119133B94B7DAD6 (void);
// 0x0000016F System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton()
extern void MenuBase_ShowBackButton_m8DD5929D9B8E6A122D061AD13032F284619890FB (void);
// 0x00000170 System.Void Facebook.Unity.Example.MenuBase::HandleResult(Facebook.Unity.IResult)
extern void MenuBase_HandleResult_m5B0AD1ECB3F5776D34528CB77C81FFDDA7E5AF3C (void);
// 0x00000171 System.Void Facebook.Unity.Example.MenuBase::OnGUI()
extern void MenuBase_OnGUI_mE9F148A8B87F4A735309EECFC1EF7BFD30966C00 (void);
// 0x00000172 System.Void Facebook.Unity.Example.MenuBase::AddStatus()
extern void MenuBase_AddStatus_m3877A7CBA333AB0EFA6FFE5B92820AC6013167D8 (void);
// 0x00000173 System.Void Facebook.Unity.Example.MenuBase::AddBackButton()
extern void MenuBase_AddBackButton_m4DB356E0DC7A6DB5A383D1A2F12AE90E2F2B875A (void);
// 0x00000174 System.Void Facebook.Unity.Example.MenuBase::AddLogButton()
extern void MenuBase_AddLogButton_mAE4A3F4F817922DEBACB7CD1CEC37EB60736EB5F (void);
// 0x00000175 System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButtons()
extern void MenuBase_AddDialogModeButtons_mA91FE27EE4BB71509169F09212E6C11A4CFB390C (void);
// 0x00000176 System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButton(Facebook.Unity.ShareDialogMode)
extern void MenuBase_AddDialogModeButton_mFAAB203118721B880409F7BD23F7CD59FC64BB04 (void);
// 0x00000177 System.Void Facebook.Unity.Example.MenuBase::.ctor()
extern void MenuBase__ctor_mF263A4EB96A4DDE5C9B1EC63E949FC693D41DCBF (void);
// 0x00000178 System.Void Facebook.Unity.Example.AccessTokenMenu::GetGui()
extern void AccessTokenMenu_GetGui_mE1DC403055C5043F64F350888C92C0178311F26E (void);
// 0x00000179 System.Void Facebook.Unity.Example.AccessTokenMenu::.ctor()
extern void AccessTokenMenu__ctor_m6B70B5C4239CF6ECBADA3DF33BA8AB560E4A21FB (void);
// 0x0000017A System.Void Facebook.Unity.Example.AppEvents::GetGui()
extern void AppEvents_GetGui_m3FE779C3A3BD7049EF051CF07CEEA3C065FE83B8 (void);
// 0x0000017B System.Void Facebook.Unity.Example.AppEvents::.ctor()
extern void AppEvents__ctor_mAC847A17BF516137EA59B65BE56FBB8A7B2B9C7A (void);
// 0x0000017C System.Void Facebook.Unity.Example.AppLinks::GetGui()
extern void AppLinks_GetGui_m95A9E96EBB6D94C066E4962BAB3354C897750129 (void);
// 0x0000017D System.Void Facebook.Unity.Example.AppLinks::.ctor()
extern void AppLinks__ctor_m53275ACDB57B9C32663DB8213897900E3B6321EA (void);
// 0x0000017E System.Void Facebook.Unity.Example.AppRequests::GetGui()
extern void AppRequests_GetGui_m64FAC11162FA76E52EAFF4FA458F391369B31BE4 (void);
// 0x0000017F System.Nullable`1<Facebook.Unity.OGActionType> Facebook.Unity.Example.AppRequests::GetSelectedOGActionType()
extern void AppRequests_GetSelectedOGActionType_m1197DC4A0249B09AEAFF25BFFF8EA2FD9C3BB813 (void);
// 0x00000180 System.Void Facebook.Unity.Example.AppRequests::.ctor()
extern void AppRequests__ctor_m66CE340D69DB0439EDF947359A0E8ED191D90818 (void);
// 0x00000181 System.Boolean Facebook.Unity.Example.DialogShare::ShowDialogModeSelector()
extern void DialogShare_ShowDialogModeSelector_m4A94F284A7AA03D20D7EF06817ADDA4F4D22BC7A (void);
// 0x00000182 System.Void Facebook.Unity.Example.DialogShare::GetGui()
extern void DialogShare_GetGui_m86B39867A307890A53383C8B1BF7B9724A81384A (void);
// 0x00000183 System.Void Facebook.Unity.Example.DialogShare::.ctor()
extern void DialogShare__ctor_mF2E81A32E740BE382EA48FD100DBFB068B400142 (void);
// 0x00000184 System.Void Facebook.Unity.Example.GraphRequest::GetGui()
extern void GraphRequest_GetGui_mC5B2D657033092B6A9AA2B7F8A3BFDD433397E2E (void);
// 0x00000185 System.Void Facebook.Unity.Example.GraphRequest::ProfilePhotoCallback(Facebook.Unity.IGraphResult)
extern void GraphRequest_ProfilePhotoCallback_mC9E8B21A2A5EDDDCB2BBC83851077A147D889409 (void);
// 0x00000186 System.Collections.IEnumerator Facebook.Unity.Example.GraphRequest::TakeScreenshot()
extern void GraphRequest_TakeScreenshot_m6E8E3B5A0BBBBAE2E4124A05607205420B89A904 (void);
// 0x00000187 System.Void Facebook.Unity.Example.GraphRequest::.ctor()
extern void GraphRequest__ctor_m2E200B7AF18E01CA23EA4442A4533773D9C8AA08 (void);
// 0x00000188 System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::.ctor(System.Int32)
extern void U3CTakeScreenshotU3Ed__4__ctor_mFDE73BDF39095D5CB04C52943F3F749FC1D96B3A (void);
// 0x00000189 System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.IDisposable.Dispose()
extern void U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_mB4A7976DB6C72A46D7BD842E606BA59D639EEA6B (void);
// 0x0000018A System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::MoveNext()
extern void U3CTakeScreenshotU3Ed__4_MoveNext_m5CC1B979FCBFA45CC4DE44556BFA5DE5FF8E5C47 (void);
// 0x0000018B System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BFEAC3397C268E3CE931454E7DDCAF4AB248BC8 (void);
// 0x0000018C System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.IEnumerator.Reset()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m7BF49F82A5EFF7F32B3FA82AB48554E368DCF674 (void);
// 0x0000018D System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m48CB53297F5D6E3CC52272F3E48E27F03C346A6B (void);
// 0x0000018E System.Boolean Facebook.Unity.Example.MainMenu::ShowBackButton()
extern void MainMenu_ShowBackButton_m255ED4296523851CCDD84E6D431C2F4FB95298D8 (void);
// 0x0000018F System.Void Facebook.Unity.Example.MainMenu::GetGui()
extern void MainMenu_GetGui_m10B78924EFA55AAB5117C14F7D3BD9E29AF3C68C (void);
// 0x00000190 System.Void Facebook.Unity.Example.MainMenu::CallFBLogin()
extern void MainMenu_CallFBLogin_m1B0369D4D6E9E584DD0757FE1EE3614F782138EC (void);
// 0x00000191 System.Void Facebook.Unity.Example.MainMenu::CallFBLoginForPublish()
extern void MainMenu_CallFBLoginForPublish_m3CC64106CA0E41CBF6955979D91795068EE5050A (void);
// 0x00000192 System.Void Facebook.Unity.Example.MainMenu::CallFBLogout()
extern void MainMenu_CallFBLogout_m3B980F09EC74BF2E909272DBD335DF47672F1596 (void);
// 0x00000193 System.Void Facebook.Unity.Example.MainMenu::OnInitComplete()
extern void MainMenu_OnInitComplete_m3E577DB4E98D84D3102B6C4ABECB5EF469191359 (void);
// 0x00000194 System.Void Facebook.Unity.Example.MainMenu::OnHideUnity(System.Boolean)
extern void MainMenu_OnHideUnity_m3977B8A15B9DD23E5E143E0D499FB66506A77606 (void);
// 0x00000195 System.Void Facebook.Unity.Example.MainMenu::.ctor()
extern void MainMenu__ctor_m395A9D3C110C6F3E6A0671389C35A6B8B79F3F32 (void);
// 0x00000196 System.Void Facebook.Unity.Example.Pay::GetGui()
extern void Pay_GetGui_mCB2EB9638D21C60BEC97A2BE50BB8C652170BC70 (void);
// 0x00000197 System.Void Facebook.Unity.Example.Pay::CallFBPay()
extern void Pay_CallFBPay_m7C6E3F32EA8A70293A14F8F4F8E8D51777F31F5C (void);
// 0x00000198 System.Void Facebook.Unity.Example.Pay::.ctor()
extern void Pay__ctor_m022913AB693A521C412E05FE2782B5D07A6FD689 (void);
static Il2CppMethodPointer s_methodPointers[408] = 
{
	Counter_Start_m77A2B23760AA7DA12C9BBEE742221AC01B67AFB3,
	Counter_Update_m750B99E43903C32501752EDB091B76D4D8591063,
	Counter_RestartCounter_m226B4BBCD884BFC697FFA4BA63BAC375D9C9CAC9,
	Counter_GetDefaultTime_mC6BC8239D56E1AA83C0F428B7FBAA341717C4F75,
	Counter__ctor_m12F27AA8C9EDE176E9543076DB214639246AAC73,
	FacebookAnalytics_Start_m1C6767570A1730DFE50A772384B64C19A7493B63,
	FacebookAnalytics_OnStartedApp_m4B856A59C0F662118A676ACC6DC8E28E48B5E575,
	FacebookAnalytics_OnInitComplete_mFD47C43545BC9C656005C5DD181B2462D13BE826,
	FacebookAnalytics_SendFacebookEvent_mCC928CBBE2E72847613AB2D374AAAA2CDED26F9B,
	FacebookAnalytics_OnHideUnity_m5714AC3123F1E74E548BA146347D63E1E50119A3,
	FacebookAnalytics__ctor_mEE760AF0BADC7F272DCCE241723775BD6EF1ACCE,
	Quiz__ctor_m4925290BE223072E849F0E68926E5B86AD0CE4C7,
	QuizGenerator_Start_mC660B3307FBEF3D6E38689D8A0F38F4731CB5BE7,
	QuizGenerator_Update_m8B36A52483CCFE7AB94E44F93C0722C36E38A164,
	QuizGenerator_LoadQuiz_mBB3C8F9208789E26AF2D93841DA949AB3B69CF90,
	QuizGenerator_CompareAnswers_mBF5CCBFDBC03EF595C505A733C5043EB5D9B1223,
	QuizGenerator_ShowAnswers_m5ECC3EA497B6EA13654C04F739B0D5B3BBBFE9EF,
	QuizGenerator_SettingOffInSeconds_mB24CD6BB965B1ECA68F17AE6C04A74A2F793918E,
	QuizGenerator_ResetValues_m53957B427324F5D598ADD73F6753BF38998777C4,
	QuizGenerator_SwapObjects_m4D87B199F3232D0960276C5C1DB25EC7426E482F,
	QuizGenerator__ctor_m97028B7BC244EDFC5CF85D9FD07749640DC1CDAA,
	U3CSettingOffInSecondsU3Ed__47__ctor_mF675BBFAB0FDA1AE944BCCB7291C8B04F405BBB3,
	U3CSettingOffInSecondsU3Ed__47_System_IDisposable_Dispose_m5E06BCFBAE84B00A2AAC0136295914272EEF8DEF,
	U3CSettingOffInSecondsU3Ed__47_MoveNext_m448E22D3B7650AE001E04A554E46BE2736E69362,
	U3CSettingOffInSecondsU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57CF15CED2AAC5B8AFD0322CBC7B16F66F9594B6,
	U3CSettingOffInSecondsU3Ed__47_System_Collections_IEnumerator_Reset_m691AEF82CE7137FF2505BDA572289DA3CD9DF50A,
	U3CSettingOffInSecondsU3Ed__47_System_Collections_IEnumerator_get_Current_m5C6780BB0A8B2C7F50A67698D7B70A2709A3DA13,
	RotateOnAxis_Start_m492B0401395AD778FCD31DBBD5654DD5BD2D268B,
	RotateOnAxis_Update_m6C79790B9E4E994B71B014FBB7C766F5AB4E4C40,
	RotateOnAxis__ctor_m0E5BF4821EBF602F027D95D5A28594D254472359,
	SoundManager_set_MusicVolume_m90A77FD5CCE8CA920F30DA582B2D7E7E238C8F75,
	SoundManager_get_MusicVolume_m25A521EC19EC22F3416BBF23F8257FC6535869B5,
	SoundManager_set_SoundVolume_m223100DD55017CE6CB41C3EAF4AC1DFC2D4B5402,
	SoundManager_get_SoundVolume_m990DD0261A123A99D457C3F5712195D38AC6C7F4,
	SoundManager_Awake_m78F953F39CFB3F539240E1226D04270B793B1A76,
	SoundManager_Start_mE4854722D4248D4CCD33E4202389B3F187C51303,
	SoundManager_MomentUpdateSoundFxVolume_mBE0654C61D7BBC558A4C5AE780DD9724054CE3DD,
	SoundManager_MomentUpdateMusicVolume_m1105A3AA5756BB15D2D9AFF8B106B0DE87C7EED2,
	SoundManager_UpdateSoundFxVolume_m7ABCBD129F252143544D58C90938A6A4EA944A1B,
	SoundManager_UpdateMusicVolume_mFAC1E7817353B4B9D9EED5804902D8815563DF47,
	SoundManager_PlaySfxFromScene_mD35F5CABD56747912F96290214A266E5AEB59195,
	SoundManager_PlaySfx_m6425348A8C7F351D6F2E984E302DDDB217902A56,
	SoundManager_PlaySfx_m032E597366D39EFA0170EE48A169CBE9A36E1F1C,
	SoundManager_PlayMusic_m3264968E597EA6ECE72CBBD20DB8DB907DA2F9E0,
	SoundManager_PlayMusic_mE58B9C264B87AF9D0BD6252A830005565127D998,
	SoundManager_PlaySound_mCFFF1085F6EB1132234B9276F0EF18F4E67C49F8,
	SoundManager_PlaySound_m3E84C8B2AE269DD7F076709F07F7F0336BE1534B,
	SoundManager_PlaySoundClick_mC489671AAA75CAC5AAEB0E93507A0A4887E98820,
	SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE,
	UA_Start_mC6FD095022B81B808C56E1113F0506834AE655F1,
	UA_Update_m05E5D9DC1F4557AF9660C8597007414D8C16D3EA,
	UA_SendBasicEvent_m15DA4ADDAB510E5C243ECF42BA5F2E8757102E45,
	UA_SendAdvancedEvent_m21F20DBE8DAD4FF2CA7B3824E4DE84E96845D251,
	UA__ctor_mF366F28535CEB5AA0EAA0B813509822CA4C77741,
	BackgroundMusic_Awake_m4CAD4EC802EBE49CBB1D3017CA8C07975A1116F4,
	BackgroundMusic_Start_mB74FEB54CA0C53089D70E64E529F372AFB887D9D,
	BackgroundMusic__ctor_m09D013A512092A19A4E1F025697015981BE2D7E7,
	NULL,
	BaseScreen_ClosePopup_mFB343341D2CD06B1CA2620B5D650CBCF53FBDFB8,
	NULL,
	BaseScreen_FadeIn_m7548331997F6688B1A63FACEF2FDCDE6A7D25B81,
	BaseScreen_FadeOut_mA5D73509584E28F54356E92D73F7CE6E993BB31F,
	BaseScreen__ctor_mEE7278500381B0A89C10D36030401EE1660C5625,
	U3CU3Ec__DisplayClass4_0__ctor_m0861B6FDC3BCF2E5D561EF840ED348F2EF95C9CB,
	U3CU3Ec__DisplayClass4_0_U3CClosePopupU3Eb__0_m98BA085A6348E9F6EF6EFB1CDBAEEB8B4A85591C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CFadeInU3Ed__6__ctor_m3AF82D0DB0670D96D6DEBBD91DFBEED13AFFDACF,
	U3CFadeInU3Ed__6_System_IDisposable_Dispose_mC413CD4A23A7A97325D7B8F6AC45209820EC895C,
	U3CFadeInU3Ed__6_MoveNext_mECB8DADE96CA6545A40C2BEB82F482C4A814FD4A,
	U3CFadeInU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F65DA37CD06796A81596AE0783C36D604771A59,
	U3CFadeInU3Ed__6_System_Collections_IEnumerator_Reset_m6A703D2007F95D1087C06B5CB7458B4AF7315A5A,
	U3CFadeInU3Ed__6_System_Collections_IEnumerator_get_Current_m35E8DB4D3348E057AB5C84B3594715DFEE572127,
	U3CFadeOutU3Ed__7__ctor_m64E38BA229295767CB4CDB535A4C5FE3686F7C92,
	U3CFadeOutU3Ed__7_System_IDisposable_Dispose_m3EA85C292A6691FC50731F9E1AC0B0AFE3C4F215,
	U3CFadeOutU3Ed__7_MoveNext_m39ED71A4E2BFCFF691CA82402815D9FB688652D6,
	U3CFadeOutU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4180985992B7D598CE132F467634A82634D78CE2,
	U3CFadeOutU3Ed__7_System_Collections_IEnumerator_Reset_m664FA32A3FF8C6EFA0EC0B99475FEB9EF697FE6E,
	U3CFadeOutU3Ed__7_System_Collections_IEnumerator_get_Current_m17EA0E60F0612B947F0B46C5B64747354E591B70,
	EditorUtils_GetEditorFriendlyText_mAEA742290469004E6FDA91F7255D7B8F3C149CFA,
	FlatButton_OnPointerEnter_mB866DE2531A865B611090E36574F37AA9E661FE5,
	FlatButton_OnPointerExit_mAD9354840CFFDF1D08FE84D94742C467D63E1A54,
	FlatButton_OnPointerDown_m339D8786CC5D253BCA12812065F0AF9E661A3E54,
	FlatButton_OnPointerUp_m85FC2BF03C81D7C1943A40F11FE91E2130C3DC4C,
	FlatButton__ctor_mF2427B7E73948E7E154529CAFD4EDAA03600EE2B,
	Initialization_Awake_mF78C27CEB63177E43C4F128BEDFD4C70742815BB,
	Initialization__ctor_mC0D2B0EE4BBCE51F3D5FCE8DD99B5880AB29810F,
	NULL,
	ListShuffle__cctor_m709A5C74189055FADE45CB0B5477C9EC8C34D5A1,
	ObjectPool_Awake_m11291A5E655A45F64639C4E06D88E849E11DF4FA,
	ObjectPool_Start_mD873EF2EF022B57E3A7C647F56BE319340091634,
	ObjectPool_GetObject_m5401D10AFC1440C3EC86BABB5F0507664AE71BFF,
	ObjectPool_ReturnObject_mBFDA7B62D6A15AFC432EDD5B223790BE676E313D,
	ObjectPool_Reset_m5DAB45765BBFD13AC2FDC8B7A1CBA9BD7F9ED6D2,
	ObjectPool_CreateInstance_m1633AA63A2E8E9E8104544F155C87898B5F26FB0,
	ObjectPool__ctor_m05686ACC18AFF85D5A729D1F349CF32B0CEA4DE4,
	PooledObject__ctor_mA3B1413D9FFE2C0B7178CE76B622109320AB60AF,
	PlaySound_Play_mCB2F79F470A715AE0A1DA330144C49211FF68A47,
	PlaySound__ctor_m1700BC274CCE4D31FE18CB40B1F68AEB0FFA791A,
	Popup_Awake_m2AB2892B941A7AC659F7A54BB43284C058A50F74,
	Popup_Start_m40956E739B20D1054876AA5257E6E55915F25CE7,
	Popup_Close_mC675E3C5E8F7E2780F4481505E92CB52E09F80D3,
	Popup_DestroyPopup_mF22233DF7742B12343C8D5D69BFC8CC8FD8E1C30,
	Popup__ctor_m3D05A12D022EC52308B8E4367BFC18F5314F48A3,
	U3CDestroyPopupU3Ed__7__ctor_mF5C0BD3B6ED00CD38EF0F406EDB560795AE59141,
	U3CDestroyPopupU3Ed__7_System_IDisposable_Dispose_m73FA6EB3AA01FD75A4663F3D0EB005AABF6966AD,
	U3CDestroyPopupU3Ed__7_MoveNext_m2F22DD9CA32ED62695AC1CA3383E515DFB1E2270,
	U3CDestroyPopupU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD75D1019AE9556F0D97B8A0E9841D0F127579651,
	U3CDestroyPopupU3Ed__7_System_Collections_IEnumerator_Reset_m4378C21E7218CE13B00BF98161B49904EF6DC2CB,
	U3CDestroyPopupU3Ed__7_System_Collections_IEnumerator_get_Current_m1021DF0E92D847245CCA35FB28744C1340DB19B1,
	SceneTransition_TravelToScene_m2DE78FF0393ED726DA307C204BC6B186D2CE9A06,
	SceneTransition__ctor_m4A9E1A606C4F7A17B8E9DA785807890D9DF2489E,
	SoundFx_Awake_m9C47BC94B4C96791755C2B4582F17D14B99A03EF,
	SoundFx_Play_m55978E0E95876051C8F298CAF7D6043865B4A66B,
	SoundFx_DisableSoundFx_m4F64B856737C814E9E59F448D985EAF4B766842B,
	SoundFx__ctor_m4796FD05F79C2F3780769AD71E3C6E136A013FC0,
	SoundManager_Awake_mBD44EAFC434C683D54B0A32C1D83E63F8C616883,
	SoundManager_Start_m4D44D361B57491668B1CCF83DEBA11F08A482055,
	SoundManager_AddSounds_mDFC34452D26D4C677C26CA33BB31BA9F9F922B34,
	SoundManager_RemoveSounds_m3D34330165A23B11ABCDB77A46B356E90B6931EE,
	SoundManager_PlaySound_mC8FB5A0479DF1F5D80B21012446F762F848340D7,
	SoundManager_PlaySound_m2066AB50FD4B0DF83B42351FAC7A9CEBB9337D79,
	SoundManager_SetSoundEnabled_mC5D28DEFF38EE6A2CFB95DF564BD03014F511C3D,
	SoundManager_SetMusicEnabled_mFBBBF622A886723BC6EE6A74CC19E518B1F7B456,
	SoundManager_ToggleSound_mC7C04FFAFC375410645589EB1726D278B83E70E4,
	SoundManager_ToggleMusic_m4526F4E7FEAD0DD2F448E77F777FDCD786C8D6A8,
	SoundManager__ctor_m77FD5CDD532E5DFC0719F1023FF2F86A68D66AB3,
	SpriteSwapper_Awake_m8AC81381468B8383EC49F5CAFD96AD080E6AC81C,
	SpriteSwapper_SwapSprite_m1766E4CFE9746516ECE989AE4852E5D9DB4729B0,
	SpriteSwapper_SetEnabled_m4178F54C57FE1C64F08E3BE7B5BF16338CEB1DC5,
	SpriteSwapper__ctor_m2D41D64B00E3065CD3796D10A6FE2CEC0AB86A06,
	ToggleButton_Awake_mBA7A855EE37F2E4D067A51EDF01DD54DFE074255,
	ToggleButton_OnPointerDown_m71E1FE60739125F6B444931440785BAB661F57CC,
	ToggleButton_SetToggled_mBAB2B690A3A9D27928DF8D6686A8EC2EC42A4179,
	ToggleButton__ctor_m23FFE637945801AC0280B56F4D7CC6EA86677218,
	ToggleButtonGroup_SetToggle_m0CAFD6030F9B6F8550CF400A3131EEAAD89192D9,
	ToggleButtonGroup_SetToggle_m41C6964A02080591947711F0433C67A01661738E,
	ToggleButtonGroup__ctor_m2EAE4D72228BFBD9D1B217BE54415127561770C5,
	BaseQuestion__ctor_m06917283B849CD521435459D24AB9385430F8A13,
	Category__ctor_mC4B9DE26A6C18F91D98F2946A1452D391A20AE85,
	GameConfiguration__ctor_mFC877ECA7DAC83EB5A3081877251525ADB24D71C,
	GameConfigurationLoader_LoadGameConfiguration_m6DE9A89EBF0D34D79F6F110662D9448B566C1A3B,
	MultipleChoiceQuestion_OnEnable_m09D573752ECE9F278FA437F637CDFC98120C4D38,
	MultipleChoiceQuestion__ctor_m06F45A59AFC0FBD2E33CB9755EA4DD9809898240,
	QuestionPack__ctor_mB1472CBDD8C5EF32C9F736E424BC3DA3DDB1AF02,
	QuestionPackLoader_LoadQuestions_m843EA894AC9C3EF3E996F8BCFB2B42A28E2E3F28,
	QuestionPackLoader_LoadAllQuestions_mCC0F0C1DC5C957111E7EA08595F8F4E90B3119B6,
	QuestionPackSet__ctor_m250016998FE9D002A97CDEBD82B184406A64EA19,
	SingleChoiceQuestion_OnEnable_mC845C0B082F844E59AC000246349F86D810895F5,
	SingleChoiceQuestion__ctor_m3759974FD6AE90CF8DA6AEC19C8761CFFC4445FE,
	TrueFalseQuestion__ctor_mC930034CA43333114950633424B1D75B7053E579,
	AlertPopup_OnCloseButtonPressed_m360CFEA80632C2F2E1ED5CB6ACA9C65DE8684F18,
	AlertPopup_SetText_mFD9816494BEF4C3DA848595AD577E43782921A48,
	AlertPopup__ctor_m86C162AF375760FF968E175316BC98AD0FC98ED2,
	GameFinishedPopup_OnReplayButtonPressed_m1E7420E7D4AF0BB6D2871789D879DC88671F68C9,
	GameFinishedPopup_OnQuitButtonPressed_mD7D2D49EEEDF2A8610ACEEEC3897B768E69F104E,
	GameFinishedPopup_SetTrophy_mD74997C20F79D09C5CA1642D4CA8CC5AF07F3F83,
	GameFinishedPopup__ctor_mFB5744F5A8D897AD51DA62F679A7CCDB899A8FFB,
	ProfilePopup_Start_m6924C5830F1EE4C220EB5FB796FE13803F484F23,
	ProfilePopup_OnCloseButtonPressed_m4DDA0B6F8E0DB54E0352DDEE02406CE4D52C7B8C,
	ProfilePopup_OnAvatarButtonPressed_mEE723AAE346C9F1B2BE98FBE7D829B0BBA599B15,
	ProfilePopup_OnPrevButtonPressed_m4EB277D87CC824301C91E08DEDF8C866930E148C,
	ProfilePopup_OnNextButtonPressed_m4902B2DE2B2102BFFC8FF0193932778E5E5327B6,
	ProfilePopup_OnResetProgressButtonPressed_m28A1329CE32D51CD36FC9FC21C4918D711FCE983,
	ProfilePopup_SetAvatar_m8CC94D2C6259BF89F32FDE94C3D64FA242B3B123,
	ProfilePopup_SetQuestionTypeText_m24DB6DDFD4F3D4606642387368B8B196AD2E5BA2,
	ProfilePopup_CreateCategories_m6E4CC0CE606CEE124691238188BB9866911F9147,
	ProfilePopup_LoadCategoryInfo_mF63E36BE9CD64F607534B615D3E0B0B16770281A,
	ProfilePopup__ctor_m8B71B5B0268FA1C4AD53B0AB674DC1FFF9F9ACF1,
	QuitGamePopup_OnYesButtonPressed_m828CC59734D69EFA0BAC13CDDC65053BDB6B090A,
	QuitGamePopup_OnNoButtonPressed_m86CE276CFE9860233F6B92FF96A397990ADC9AA5,
	QuitGamePopup__ctor_m65D001FC2FFF36522CD053562FA66A311B7DCC0A,
	SettingsPopup_Start_m1FE30A67DD716E06EF08A9BFEAE663A23974E3C4,
	SettingsPopup_OnInfoButtonPressed_m3A40C1D524A11E37D648CBE2CD1C68D006597F7F,
	SettingsPopup_OnHelpButtonPressed_m221C6886AC2876034BF1FB2CA15B34427B07F04C,
	SettingsPopup_OnCloseButtonPressed_m856994413671A1B482E0F1F5F938FCD00599A77A,
	SettingsPopup_OnMusicSliderValueChanged_m32974702DD8EC0ACBAD825302F56A46FEBF771E5,
	SettingsPopup_OnSoundSliderValueChanged_mA49E3E987F7B0502839EC011C36520D3E7F85161,
	SettingsPopup__ctor_m046A32E1B7E0A33F283C17713767F2C431BE4224,
	U3CU3Ec__cctor_m1290BD6C61D03A40966D1085E59D07EEAB7AC27D,
	U3CU3Ec__ctor_m833C8EE04413BEDDDE23B2EF009DD26F30C91882,
	U3CU3Ec_U3COnInfoButtonPressedU3Eb__5_0_m9C1CFC1CEA3720C44C7E7098D56B651A32293848,
	U3CU3Ec_U3COnHelpButtonPressedU3Eb__6_0_m43426447931F4629F73DEEA5A0A5321C39776D48,
	CategorySelectionScreen_Start_m00E1118B57AEBC39808441A371CEC1AC7FD7BC4A,
	CategorySelectionScreen_SetCategory_m0AEDC02215253C74A4279E984739AC79BDA6A07E,
	CategorySelectionScreen_OnBackButtonPressed_m338A2D984E75B356642675E21FA18181D33261CE,
	CategorySelectionScreen_OnNextButtonPressed_m4C8875D0E2402C6D34EC921FAE4D5F750FEA8593,
	CategorySelectionScreen_CreateCategoryToggle_mD03FB1B4B0A587DA6633491EF78F48CB1C6B5B7C,
	CategorySelectionScreen__ctor_mFA166C076572AC946A6346799C9AC857391D61CE,
	U3CU3Ec__DisplayClass8_0__ctor_m1FF9E6137BA3307FE1F8318B6BE96EBCEA11B331,
	U3CU3Ec__DisplayClass8_0_U3CCreateCategoryToggleU3Eb__0_m7AE9903196CCEADE7B9C610D8B7A30DE629FB88B,
	GameModeSelectionScreen_Start_m9FAF4AC95A1455DA698A34AEA0D34A7A8FD73FF2,
	GameModeSelectionScreen_SetQuestionType_m71E48A71369873D20D94A9751C264805CFF66639,
	GameModeSelectionScreen_SetTimeMode_m58DEC7D31844AE55F8C2CE8E833645BFD69FFA28,
	GameModeSelectionScreen_StartGame_m258ED967CAF7138D4A259ED66B11ADE667A0831E,
	GameModeSelectionScreen__ctor_m826B2CB495CCCDA8F84BA63E31BCBF6169366EB3,
	GameScreen_Start_mAB8E0638092C15D6A1D482FE63FEE3D5129265DB,
	GameScreen_LoadSingleChoiceQuestionUi_mECF27CB4D40507220973D6A10EA07E92C2D1E894,
	GameScreen_LoadMultipleChoiceQuestionUi_m420E8B2A50D8CD038286943B464C0D8EA01220CC,
	GameScreen_LoadTrueFalseQuestionUi_m20C401F7DDA6839154235416C2B08F3CB08381FD,
	GameScreen_Update_m52C37CE6E6C81A136008AC63F3612E887BCE1D3A,
	GameScreen_LoadQuestions_mAEA9B65BCAC40F6828FE87687E843C057D279D02,
	GameScreen_StartGame_mBCC608A22FBC144ED17EF8FD04ABA4A28081DD43,
	GameScreen_SelectRandomQuestion_mA564577304847914CA9168DF46D04FF39F898BEF,
	GameScreen_LoadQuestion_m3AFAD0FD637FA45A6B09419FE5FF700D7D3BE3B8,
	GameScreen_OnPlayerAnswered_m59D4AF05456C2A61943071C7BAA52885D564BF42,
	GameScreen_AdvanceToNextQuestion_m88ED87D72CD56CC79D4BDA1DD9ED8CDDA2C1A30F,
	GameScreen_SelectRandomQuestionAsync_m5C277593F010CAFD112CBDF42F6995117713FCED,
	GameScreen_OpenGameFinishedPopupAsync_mDDE57F29D5312E3377D12C7BAF15883A4840FD0D,
	GameScreen_StartCountdown_mE157DEDB79FC3A467655101B6B7C463A558308D5,
	GameScreen_StopCountdown_m581C9F6232CBAE4F70E1B4BC1845FC7B751B33A7,
	GameScreen_Restart_m0BEAA1F14CD8612D29E3965CDAD8DFD832A9E85E,
	GameScreen_OnQuitButtonPressed_m7D6E361FE30FEEB49FDDEB498B242E4972D8B2C2,
	GameScreen_OnSelectButtonPressed_m601045A375457144CF21E72BBE29361CABE71893,
	GameScreen__ctor_mD947A94C370DA3B2BC5E33FFACCDFF78711951A5,
	U3CSelectRandomQuestionAsyncU3Ed__48__ctor_mDD37123FC127E790D7CE66592D688D7FCB08F4EA,
	U3CSelectRandomQuestionAsyncU3Ed__48_System_IDisposable_Dispose_m186687311CE86ACB62F2C2DAB46A2D19D096C3D4,
	U3CSelectRandomQuestionAsyncU3Ed__48_MoveNext_mE0AF7B6528FFD235459E9F818A92283F38560A5A,
	U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m369D1C6C107A0124D6D006237E39DA39FA8312E8,
	U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_IEnumerator_Reset_mFC4635B6D4DB5A757FA62E55D63F3B6B6DE6FC7A,
	U3CSelectRandomQuestionAsyncU3Ed__48_System_Collections_IEnumerator_get_Current_m0CD6CF2630BC66C203F5AF4F109DC4054D31272C,
	U3CU3Ec__DisplayClass49_0__ctor_m5B02A9AE24ECF393CD8DBE08EB2975BE5986D581,
	U3CU3Ec__DisplayClass49_0_U3COpenGameFinishedPopupAsyncU3Eb__0_mEF8FEA25949068832D9BB141E5ECD9E6282F041F,
	U3COpenGameFinishedPopupAsyncU3Ed__49__ctor_mDE9A51FCCC24ECE8581480F4F25BCA1B61058984,
	U3COpenGameFinishedPopupAsyncU3Ed__49_System_IDisposable_Dispose_mE050E9F7D72D00202AB3D6B9A44430CBA75C2F60,
	U3COpenGameFinishedPopupAsyncU3Ed__49_MoveNext_m2B9A92E43DC8A40BF3CAF69C306092C63F4B9807,
	U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FF145A247ED7369892CAE203D6BEE9784BF013B,
	U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_IEnumerator_Reset_m1F1766A314E68317CE556A1912D696084B7EF3C8,
	U3COpenGameFinishedPopupAsyncU3Ed__49_System_Collections_IEnumerator_get_Current_m4DEF2131620704154BFF8F8D75FD73E620928170,
	HomeScreen_Start_m317B23F1AFC9541E0F0501ACF4E6521F693E93D0,
	HomeScreen_OnAvatarButtonPressed_m306D7CA00AA9F3F663D3DB12900BDDE62D6F1974,
	HomeScreen_OnSettingsButtonPressed_m1E281C64F73A5C12F2975D7F07D8488E8E398270,
	HomeScreen_SetAvatar_m984975C0AB1459C3834F9F4572CDA0213C31E882,
	HomeScreen__ctor_mD0B37B46C153242A1FB9AB23B5CF55337C8AD020,
	U3CU3Ec__cctor_m6965652C8F23346BF2CA29AC2116C9586F3431E3,
	U3CU3Ec__ctor_m54C16D5362F087382BDFCCB05004EA31D1BDA444,
	U3CU3Ec_U3COnSettingsButtonPressedU3Eb__4_0_mFB7407331C22A79AF287E4FDEB52DAB0424CEC61,
	CategoryScrollItem__ctor_mA5414D69BCD83D51BCC192860C4FE1408093236C,
	CategoryToggle__ctor_m761AC1BD3E16D551689900CDFFEC31BE38667744,
	CategoryToggleWrapper__ctor_m394281D5CE78E2119708F1F82F405252B373A059,
	MultipleChoiceQuestionUi_OnQuestionLoaded_m34C4A9EE8AE26FCEDBD6168F672B6FBE06723DA5,
	MultipleChoiceQuestionUi_OnQuestionAnswered_m025C563D275723ABF6E74B2396A26F075A536B42,
	MultipleChoiceQuestionUi_HighlightCorrectAnswer_m6A449A754912187CAF7C3DAB6D97DB98523F0A55,
	MultipleChoiceQuestionUi_HighlightWrongAnswer_mD5E51D1C158E48FEBD2D05CBF49935C3A93E3B44,
	MultipleChoiceQuestionUi_LockUi_mD6C1B82C58F2043FAE7DF56F2087F30B6DEC1307,
	MultipleChoiceQuestionUi_UnlockUi_m980CE5580B5F85EE0D5137A53D3F821FF8E1059C,
	MultipleChoiceQuestionUi_OnSelectButtonPressed_m7A3FA5C1053299F11982A720798C4DE9AAB6E490,
	MultipleChoiceQuestionUi__ctor_m824535313EE6B5708D48C8DE478931645D49B9E3,
	U3CU3Ec__DisplayClass15_0__ctor_mE9A31FD190056385614CC8F8DBC25B1D949162C7,
	U3CU3Ec__DisplayClass15_0_U3COnQuestionLoadedU3Eb__0_mC4F50DD45F61F8AC3DC83AA9DA286792B999D241,
	U3CU3Ec__DisplayClass15_1__ctor_m5BB46BA874C58FA4F63CCE0B584099539D3EA3E6,
	U3CU3Ec__DisplayClass15_1_U3COnQuestionLoadedU3Eb__1_mD63F3B39CA0891FCD6123E5CDCA101470AA9D6A6,
	QuestionResultUi_OnDisable_m3CB93DE74545DC4EF3DF19CEA44C836C8D56ADBF,
	QuestionResultUi_Hide_m00A22B4911F2A7A9573F12D32814671B408A9CE1,
	QuestionResultUi_FadeIn_m40D67B72B348528063F141133B4E26A4140CBE9F,
	QuestionResultUi_FadeOut_mDA24F36316B5DA1706DF958607DB3CAD61B58AB6,
	QuestionResultUi_FadeInImage_m88CE5B21BCE8FB6C1DD23369B40A49618CAA12B3,
	QuestionResultUi_FadeOutImage_m296752235987311BA65F4025F255C89F6B6ED522,
	QuestionResultUi_FadeInText_mAE52FB41F32DECB053B7A384FC3DD979E727719C,
	QuestionResultUi_FadeOutText_m1F38DCB813084B1E242F5A5ED1A00D2985BFB661,
	QuestionResultUi__ctor_m82D8BD3B8F8A61B13C983166634C9A9F6E530E74,
	U3CFadeInU3Ed__5__ctor_m74D9B0013114CBD245700700C752CA6F9D4C54D2,
	U3CFadeInU3Ed__5_System_IDisposable_Dispose_m426637519063328CC571E171185CC9C3B22F2D31,
	U3CFadeInU3Ed__5_MoveNext_m1CF9D2C7EC388F9BDEE15D12CAC80651C72D7641,
	U3CFadeInU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m746822E4F1F69DA7A0938EC028972D37331C657B,
	U3CFadeInU3Ed__5_System_Collections_IEnumerator_Reset_mC1BC7936550ED852E6166A9DEDFC90B6D2B097D8,
	U3CFadeInU3Ed__5_System_Collections_IEnumerator_get_Current_m67C9FCF94FC33DCB98447834DF9914647C4EEDA3,
	U3CFadeOutU3Ed__6__ctor_mC4DC2218F477AC844A4644E3E926540ADF7F7F62,
	U3CFadeOutU3Ed__6_System_IDisposable_Dispose_m5163A6DD6CBA5506F8FE350E00ECF455462756FB,
	U3CFadeOutU3Ed__6_MoveNext_mCE2D9774F285917F3563AA865B4F34007CBD8A66,
	U3CFadeOutU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE77C57F22C55C3F6DA732834830044D44C6F16A3,
	U3CFadeOutU3Ed__6_System_Collections_IEnumerator_Reset_mDA2ABCD14D54F3D886AD5759E2081F106168507A,
	U3CFadeOutU3Ed__6_System_Collections_IEnumerator_get_Current_mF36B05915D2AE4BC3CAFFC3303A515251245AA3B,
	U3CFadeInImageU3Ed__7__ctor_m0234ABA131890578D682DE3F2387A077CFEA400C,
	U3CFadeInImageU3Ed__7_System_IDisposable_Dispose_m99AA1279F2ADA34145D797F14E50A47CB7D30A9E,
	U3CFadeInImageU3Ed__7_MoveNext_m4FF110A95A93CCEDCE9EEE27C41BA75458AAA6E2,
	U3CFadeInImageU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE90EB29B462BEDAC582A8683F14919F38D87D423,
	U3CFadeInImageU3Ed__7_System_Collections_IEnumerator_Reset_mE2BB5EF1DA866BBC590A0E150D8BE467836078F1,
	U3CFadeInImageU3Ed__7_System_Collections_IEnumerator_get_Current_mAF0AEA8623EE1CC511CD14E5823CB494CC339CD3,
	U3CFadeOutImageU3Ed__8__ctor_m9B38F76689716961CA361C4DDBDFBA39284C870D,
	U3CFadeOutImageU3Ed__8_System_IDisposable_Dispose_m76E85D1BE791F9FFFF071A3833350405667D0B23,
	U3CFadeOutImageU3Ed__8_MoveNext_m9619D9C3DA166059C25E0F2A24A376F33C005363,
	U3CFadeOutImageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50748FB3FEDF7D8B29D1C14B62C9ABA352B0EF6A,
	U3CFadeOutImageU3Ed__8_System_Collections_IEnumerator_Reset_m4D193083D83A5855B8C421D35C1ED3AED0041716,
	U3CFadeOutImageU3Ed__8_System_Collections_IEnumerator_get_Current_m30774CD6115F99D85A67DF56206CEF2F90CF6B9C,
	U3CFadeInTextU3Ed__9__ctor_mFC16E5EAD976D5FF732873A44177E70D9863E456,
	U3CFadeInTextU3Ed__9_System_IDisposable_Dispose_mDE254A21FA9A8AB7E100020985280E49B76FF4D6,
	U3CFadeInTextU3Ed__9_MoveNext_m43AD986AF751E4B70BCC755B0E81A55A0A86B4E9,
	U3CFadeInTextU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C72DE065EF204138607968499DB71937EA857C3,
	U3CFadeInTextU3Ed__9_System_Collections_IEnumerator_Reset_m5FE33FA8B881ABE276980EE13DF8F779D1BD7CCA,
	U3CFadeInTextU3Ed__9_System_Collections_IEnumerator_get_Current_m8FA4E1605670A9C3071476E6B47DD1A8CD613FD5,
	U3CFadeOutTextU3Ed__10__ctor_m04D3968ABEAACEDD769B3FE3A401C3272DE08ECE,
	U3CFadeOutTextU3Ed__10_System_IDisposable_Dispose_m120BC1F80CB95B13361726FE1074AE0A2925FF84,
	U3CFadeOutTextU3Ed__10_MoveNext_m18E186973E300914CE272F043F9779E597C86DD4,
	U3CFadeOutTextU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6488A6CB058E4D52C1FE7618D48809BF472FECE6,
	U3CFadeOutTextU3Ed__10_System_Collections_IEnumerator_Reset_m4A39887766D710ADA09D04FD4F5941AB2C94A1F0,
	U3CFadeOutTextU3Ed__10_System_Collections_IEnumerator_get_Current_m225D35B848312EAE28F8622F0F92ABF9CECA14C7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	QuestionUi__ctor_m9A5B761E8EC4F23C71E2EFB0DC738CDAECEA59A4,
	SingleChoiceQuestionUi_OnQuestionLoaded_m23CE17FCA382E53BA42258677680AA261915765A,
	SingleChoiceQuestionUi_OnQuestionAnswered_m5B35FF71E5368BAA35983B18E782631FB82C3CDA,
	SingleChoiceQuestionUi_HighlightCorrectAnswer_m80499FE86B02A1261F482B9FA1B233D3260F3E72,
	SingleChoiceQuestionUi_HighlightWrongAnswer_m4F8076F69CB69CD3BD4A5FB5C345124F8E4F96D0,
	SingleChoiceQuestionUi_LockUi_m57ABA2BA5A3C0DBECADD9806F95860DD8E2AF927,
	SingleChoiceQuestionUi_UnlockUi_mACE16C265780FF43A557F472EB055CD714899D01,
	SingleChoiceQuestionUi_OnSelectButtonPressed_mF1422D575B5F825FE40C574F8D88D4B5CC9F7957,
	SingleChoiceQuestionUi__ctor_mFE55450DAE9813237C583EEA8E1A81ECEC520E9F,
	U3CU3Ec__DisplayClass10_0__ctor_m0B459CAE5F8A90276A80D5839E1586321305A16C,
	U3CU3Ec__DisplayClass10_0_U3COnQuestionLoadedU3Eb__0_m6B57688D3CBDF59838673A1B2F65E2576C6A46F5,
	U3CU3Ec__DisplayClass10_1__ctor_mB839E865810EE85566BA4C6F95C9E02F1E55784B,
	U3CU3Ec__DisplayClass10_1_U3COnQuestionLoadedU3Eb__1_m464246BFDE8E8215D99FB546CF9082F700680FE5,
	TrueFalseQuestionUi_OnQuestionLoaded_m090D2C4917BF50156FB8335605A5FB17366DE8CD,
	TrueFalseQuestionUi_OnQuestionAnswered_mE535A7DA3936D003A83D16EC7BB77D1E6479D83C,
	TrueFalseQuestionUi_HighlightCorrectAnswer_mBE4DC3853BD0F85246EDE5E9036FF5E39AF7B5BC,
	TrueFalseQuestionUi_HighlightWrongAnswer_m9764636AE6B3DAB746BB9FDCA2BEFBD845424181,
	TrueFalseQuestionUi_LockUi_m0F0480562236BF05A0A986317AE0920ABBF72277,
	TrueFalseQuestionUi_UnlockUi_m70D0AB02850C1860C4F67F06D62626EC42FCFE46,
	TrueFalseQuestionUi_OnSelectButtonPressed_mDB20396A98352BCC889427FDFC04BD70E95B497B,
	TrueFalseQuestionUi__ctor_m13E0973C9B34CC1686393742C7C45A1814BAD451,
	U3CU3Ec__DisplayClass7_0__ctor_mFC2E0510E6B6BB39637C5B2DC25929909615121E,
	U3CU3Ec__DisplayClass7_0_U3COnQuestionLoadedU3Eb__0_m53EF451F78C414A3F5DC548BA4F58BB52A0C1D86,
	U3CU3Ec__DisplayClass7_0_U3COnQuestionLoadedU3Eb__1_mB6466485443D56D6E7D2ED238EC61A4AB65471FC,
	ConsoleBase_get_ButtonHeight_mCA27289A3F104543F20DC06B91A31AB5B6086CBF,
	ConsoleBase_get_MainWindowWidth_mD5DF934DE521C4BAC2D8E5D192C99F671CE9A932,
	ConsoleBase_get_MainWindowFullWidth_m34B6E4A7E5F46D4BA5C4F6776BE41F81A6EC0708,
	ConsoleBase_get_MarginFix_m62D65A386A3E082B094188A8085C0E70EB1C7E30,
	ConsoleBase_get_MenuStack_mDC8955F1DC5D449DEEC9E2B9BCC9EDC1ADACB8EC,
	ConsoleBase_set_MenuStack_m51033E1F07C530521C27F170233FD177D22D45C0,
	ConsoleBase_get_Status_m63CCCED1FBDF986534230A863D3CB49D3D19B644,
	ConsoleBase_set_Status_m395D6D86AE7C62F043B9B4AB438CF39F7216CFC3,
	ConsoleBase_get_LastResponseTexture_m6D4AB4A8E2B4906C8D20DB62B10DDE0D812630BD,
	ConsoleBase_set_LastResponseTexture_m9495BB5A6A1C0F6C2CA39B3818B06565B1CBC2E9,
	ConsoleBase_get_LastResponse_m681ADA0CE897B024487F8F4C50260F71772D9386,
	ConsoleBase_set_LastResponse_mA472606A78F4CA3BA65B94644F4B5DD7F70C532A,
	ConsoleBase_get_ScrollPosition_mF3756B3BDEB874C262A7EB7178080C045F4CF084,
	ConsoleBase_set_ScrollPosition_m896C935B5961450A7221C74985844A8A685D716C,
	ConsoleBase_get_ScaleFactor_mBD5B4AFF1CB11190D92499E2E4FC849F30A36CA5,
	ConsoleBase_get_FontSize_m5324E58BF0BA3C20E0F0207DF52ABDAC5836DECB,
	ConsoleBase_get_TextStyle_m42A89F1195B33C7B6EB0AC5B21CF8DCE0B8F24E2,
	ConsoleBase_get_ButtonStyle_m9FE82D58E7A4DA3A588A0285A160C95CDB18BB1B,
	ConsoleBase_get_TextInputStyle_m4EF1BA3E08B87166B3719AA787380B60D61D185F,
	ConsoleBase_get_LabelStyle_mCC3768907195DE623404701909A36C7A81D5F070,
	ConsoleBase_Awake_mF527AF51A14B16534EE6F207FE429271F30DFCE6,
	ConsoleBase_Button_m227EA5755011461613164FC2E652AF490057B2C5,
	ConsoleBase_LabelAndTextField_m51ED01D9C65067D9BAD30A37EB346E45E9C71151,
	ConsoleBase_IsHorizontalLayout_m7AF2417167256558F464D95D8E37519E2C9ED9C1,
	ConsoleBase_SwitchMenu_m722B511130E38BEB0B82ABEA06EFEF7A8FCA5AAD,
	ConsoleBase_GoBack_m2F521BD6668698924A6ADE7B8A33638C79FA0685,
	ConsoleBase__ctor_m6A9701FD37DF150A0944E6D6011C8AF2F1D01224,
	ConsoleBase__cctor_m638237AA15700C2E8B101F9D9F60E12C0A4E0734,
	LogView_AddLog_mD1DEEAD86B0AAF388524DD3C301212D187094FEE,
	LogView_OnGUI_m09AF694B32089C3DE63F8AF0579666B99080CD87,
	LogView__ctor_m3D72776C6D2FF8290D85E3DC714F7F63120A1FB4,
	LogView__cctor_m4F29B617139DDAA7AA6B4E0E65200A239709F313,
	NULL,
	MenuBase_ShowDialogModeSelector_m894AD0F5A55FD7C40E8EB1D5C119133B94B7DAD6,
	MenuBase_ShowBackButton_m8DD5929D9B8E6A122D061AD13032F284619890FB,
	MenuBase_HandleResult_m5B0AD1ECB3F5776D34528CB77C81FFDDA7E5AF3C,
	MenuBase_OnGUI_mE9F148A8B87F4A735309EECFC1EF7BFD30966C00,
	MenuBase_AddStatus_m3877A7CBA333AB0EFA6FFE5B92820AC6013167D8,
	MenuBase_AddBackButton_m4DB356E0DC7A6DB5A383D1A2F12AE90E2F2B875A,
	MenuBase_AddLogButton_mAE4A3F4F817922DEBACB7CD1CEC37EB60736EB5F,
	MenuBase_AddDialogModeButtons_mA91FE27EE4BB71509169F09212E6C11A4CFB390C,
	MenuBase_AddDialogModeButton_mFAAB203118721B880409F7BD23F7CD59FC64BB04,
	MenuBase__ctor_mF263A4EB96A4DDE5C9B1EC63E949FC693D41DCBF,
	AccessTokenMenu_GetGui_mE1DC403055C5043F64F350888C92C0178311F26E,
	AccessTokenMenu__ctor_m6B70B5C4239CF6ECBADA3DF33BA8AB560E4A21FB,
	AppEvents_GetGui_m3FE779C3A3BD7049EF051CF07CEEA3C065FE83B8,
	AppEvents__ctor_mAC847A17BF516137EA59B65BE56FBB8A7B2B9C7A,
	AppLinks_GetGui_m95A9E96EBB6D94C066E4962BAB3354C897750129,
	AppLinks__ctor_m53275ACDB57B9C32663DB8213897900E3B6321EA,
	AppRequests_GetGui_m64FAC11162FA76E52EAFF4FA458F391369B31BE4,
	AppRequests_GetSelectedOGActionType_m1197DC4A0249B09AEAFF25BFFF8EA2FD9C3BB813,
	AppRequests__ctor_m66CE340D69DB0439EDF947359A0E8ED191D90818,
	DialogShare_ShowDialogModeSelector_m4A94F284A7AA03D20D7EF06817ADDA4F4D22BC7A,
	DialogShare_GetGui_m86B39867A307890A53383C8B1BF7B9724A81384A,
	DialogShare__ctor_mF2E81A32E740BE382EA48FD100DBFB068B400142,
	GraphRequest_GetGui_mC5B2D657033092B6A9AA2B7F8A3BFDD433397E2E,
	GraphRequest_ProfilePhotoCallback_mC9E8B21A2A5EDDDCB2BBC83851077A147D889409,
	GraphRequest_TakeScreenshot_m6E8E3B5A0BBBBAE2E4124A05607205420B89A904,
	GraphRequest__ctor_m2E200B7AF18E01CA23EA4442A4533773D9C8AA08,
	U3CTakeScreenshotU3Ed__4__ctor_mFDE73BDF39095D5CB04C52943F3F749FC1D96B3A,
	U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_mB4A7976DB6C72A46D7BD842E606BA59D639EEA6B,
	U3CTakeScreenshotU3Ed__4_MoveNext_m5CC1B979FCBFA45CC4DE44556BFA5DE5FF8E5C47,
	U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BFEAC3397C268E3CE931454E7DDCAF4AB248BC8,
	U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m7BF49F82A5EFF7F32B3FA82AB48554E368DCF674,
	U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m48CB53297F5D6E3CC52272F3E48E27F03C346A6B,
	MainMenu_ShowBackButton_m255ED4296523851CCDD84E6D431C2F4FB95298D8,
	MainMenu_GetGui_m10B78924EFA55AAB5117C14F7D3BD9E29AF3C68C,
	MainMenu_CallFBLogin_m1B0369D4D6E9E584DD0757FE1EE3614F782138EC,
	MainMenu_CallFBLoginForPublish_m3CC64106CA0E41CBF6955979D91795068EE5050A,
	MainMenu_CallFBLogout_m3B980F09EC74BF2E909272DBD335DF47672F1596,
	MainMenu_OnInitComplete_m3E577DB4E98D84D3102B6C4ABECB5EF469191359,
	MainMenu_OnHideUnity_m3977B8A15B9DD23E5E143E0D499FB66506A77606,
	MainMenu__ctor_m395A9D3C110C6F3E6A0671389C35A6B8B79F3F32,
	Pay_GetGui_mCB2EB9638D21C60BEC97A2BE50BB8C652170BC70,
	Pay_CallFBPay_m7C6E3F32EA8A70293A14F8F4F8E8D51777F31F5C,
	Pay__ctor_m022913AB693A521C412E05FE2782B5D07A6FD689,
};
static const int32_t s_InvokerIndices[408] = 
{
	1575,
	1575,
	1575,
	1554,
	1575,
	1575,
	1575,
	1575,
	824,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1313,
	1340,
	1532,
	1575,
	1575,
	1575,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1575,
	1575,
	1575,
	2511,
	2540,
	2511,
	2540,
	1575,
	1575,
	1342,
	1342,
	1342,
	1342,
	1323,
	2506,
	2342,
	2506,
	2342,
	824,
	531,
	1575,
	1575,
	1575,
	1575,
	1323,
	824,
	1575,
	1575,
	1575,
	1575,
	-1,
	1575,
	-1,
	2214,
	2017,
	1575,
	1575,
	1575,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	2208,
	1323,
	1323,
	1323,
	1323,
	1575,
	1575,
	1575,
	-1,
	2547,
	1575,
	1575,
	1532,
	1323,
	1575,
	1532,
	1575,
	1575,
	1323,
	1575,
	1575,
	1575,
	1575,
	1532,
	1575,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1323,
	1575,
	1575,
	826,
	1575,
	1575,
	1575,
	1575,
	1323,
	1323,
	826,
	826,
	1340,
	1340,
	1575,
	1575,
	1575,
	1575,
	1575,
	1340,
	1575,
	1575,
	1323,
	1340,
	1575,
	1323,
	1313,
	1575,
	1575,
	1575,
	1575,
	2447,
	1575,
	1575,
	1575,
	2447,
	2447,
	1575,
	1575,
	1575,
	1575,
	1575,
	1323,
	1575,
	1575,
	1575,
	498,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	2547,
	1575,
	1323,
	1323,
	1575,
	1313,
	1575,
	1575,
	423,
	1575,
	1575,
	1575,
	1575,
	1313,
	1313,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1323,
	1313,
	1575,
	1532,
	1532,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1575,
	1323,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1575,
	1575,
	1575,
	1313,
	1575,
	2547,
	1575,
	1323,
	1575,
	1575,
	1575,
	824,
	1140,
	1575,
	1313,
	1575,
	1575,
	1575,
	1575,
	1575,
	1152,
	1575,
	1575,
	1575,
	1575,
	621,
	621,
	621,
	1042,
	621,
	621,
	1575,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	824,
	1140,
	1575,
	1313,
	1575,
	1575,
	1575,
	1575,
	824,
	1140,
	1575,
	1313,
	1575,
	1575,
	1575,
	1575,
	1575,
	1152,
	1575,
	1575,
	824,
	1140,
	1575,
	1313,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	2526,
	2526,
	2526,
	2526,
	2531,
	2506,
	1532,
	1323,
	1532,
	1323,
	1532,
	1323,
	1570,
	1359,
	1554,
	1520,
	1532,
	1532,
	1532,
	1532,
	1575,
	1152,
	816,
	1552,
	1323,
	1575,
	1575,
	2547,
	2506,
	1575,
	1575,
	2547,
	1575,
	1552,
	1552,
	1323,
	1575,
	1575,
	1575,
	1575,
	1575,
	1313,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1575,
	1481,
	1575,
	1552,
	1575,
	1575,
	1575,
	1323,
	1532,
	1575,
	1313,
	1575,
	1552,
	1532,
	1575,
	1532,
	1552,
	1575,
	1575,
	1575,
	1575,
	1575,
	1340,
	1575,
	1575,
	1575,
	1575,
};
static const Il2CppTokenRangePair s_rgctxIndices[4] = 
{
	{ 0x0200000D, { 3, 2 } },
	{ 0x0600003A, { 0, 1 } },
	{ 0x0600003C, { 1, 2 } },
	{ 0x0600005C, { 5, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)3, 9119 },
	{ (Il2CppRGCTXDataType)2, 488 },
	{ (Il2CppRGCTXDataType)3, 55 },
	{ (Il2CppRGCTXDataType)3, 9393 },
	{ (Il2CppRGCTXDataType)3, 85 },
	{ (Il2CppRGCTXDataType)2, 1002 },
	{ (Il2CppRGCTXDataType)2, 1309 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	408,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	4,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
