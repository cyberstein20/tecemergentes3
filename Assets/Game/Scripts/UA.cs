using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine;


public class UA : MonoBehaviour
{
    public static UA Instance;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SendBasicEvent(string nameEvent)
    {
        AnalyticsResult result = Analytics.CustomEvent(nameEvent);
        Debug.Log("["+nameEvent+"] event state: "+result);
    }

    public void SendAdvancedEvent(string NameEvent, Dictionary<string, object> Data)
    {
        AnalyticsResult result = Analytics.CustomEvent(NameEvent, Data);
        Debug.Log("[" + NameEvent + "] event state: " + result);
    }
}
