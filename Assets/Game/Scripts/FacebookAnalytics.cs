using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FacebookAnalytics : MonoBehaviour
{
    public static FacebookAnalytics Instance;
    
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        if (!FB.IsInitialized)
        {
            OnStartedApp();
        }
    }

    void OnStartedApp()
    {
        FB.Init(OnInitComplete);
    }

    void OnInitComplete()
    {
        string logMessage = String.Format("OnInitCompleteCalled IsLoggedIn='{0}' IsInitialized='{1}'",
            FB.IsLoggedIn,
            FB.IsInitialized);
        Debug.Log(logMessage);

        if (AccessToken.CurrentAccessToken != null)
        {
            Debug.Log(message:AccessToken.CurrentAccessToken.ToString());
        }
        
        SendFacebookEvent(AppEventName.ActivatedApp, new Dictionary<string, object>()
        {
            {"message", "App iniciada por el usuario XYZ"}
        });
    }
    
    public void SendFacebookEvent(string nameEvent, Dictionary<string, object> data)
    {
        FB.LogAppEvent(
            nameEvent,
            valueToSum:null,
            data
        );
    }

    void OnHideUnity()
    {
        Debug.Log(message:"App hidden");
    }
}
