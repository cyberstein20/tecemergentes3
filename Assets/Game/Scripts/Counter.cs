﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    public float Timer;
    private float _auxTimer;
    public Text TimerText;
    public bool TimeFinished;
    public bool StopCounter;
    
    // Start is called before the first frame update
    void Start()
    {
        _auxTimer = Timer;
    }

    // Update is called once per frame
    void Update()
    {
        if (StopCounter)
        {
            return;
        }
        if (Timer <= 0)
        {
            Timer = 0;
            TimeFinished = true;
        }
        else
        {
            Timer -= Time.deltaTime;
        }
        TimerText.text = Mathf.Max(Mathf.Floor(Timer), 0).ToString();
    }

    public void RestartCounter()
    {
        Timer = _auxTimer;
        TimeFinished = false;
        StopCounter = false;
    }
    public float GetDefaultTime()
    {
        return _auxTimer;
    }
}
